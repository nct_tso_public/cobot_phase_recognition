"""
Script to change the format of the frames
"""

import os
import imageio
import utils
from utils import MyCrop
from PIL import Image
import torchvision.transforms as transforms
import torchvision.transforms.functional as TF

videos_path = "/mnt/g27prist/TCO/TCO-Studenten/CoBot/OPs"
frames_path = "/mnt/g27prist/TCO/TCO-Studenten/CoBot/frames"
frames_path2 = "/mnt/g27prist/TCO/TCO-Studenten/CoBot/frames_smaller"
new_m = transforms.Compose(
        [MyCrop(22, 218, 675, 845), 
        transforms.Resize(256)])
new_m2 = transforms.Compose(
        [MyCrop(22, 218, 675, 845)])
'''for i in range(1,len(utils.CoBot.videos)):
	print(i)
	outputPath = os.path.join(frames_path2, utils.CoBot.videos[i])
	print("out: "+ outputPath)
	os.makedirs(outputPath)
	inputPath = os.path.join(frames_path, utils.CoBot.videos[i])
	print("input: " + inputPath)
	num = len(os.listdir(inputPath))
	print("nim: " + str(num))
	for j in range(num):
		name = "%08d.png" % (j)
		try:
			img = Image.open(os.path.join(inputPath, name))
		except:
			print("break "+ name)
			continue
		img = new_m(img)
		img.save(os.path.join(outputPath, name))'''

new_vids = ['20200827','20201201','20201204']
for i in range(3):
	print(i)
	outputPath = os.path.join(frames_path2, new_vids[i])
	print("out: "+ outputPath)
	os.makedirs(outputPath)
	inputPath = os.path.join(frames_path, new_vids[i])
	print("input: " + inputPath)
	num = len(os.listdir(inputPath))
	print("nim: " + str(num))
	for j in range(num):
		name = "%08d.png" % (j)
		try:
			img = Image.open(os.path.join(inputPath, name))
		except:
			print("break "+ name)
			continue
		img = new_m(img)
		img.save(os.path.join(outputPath, name))
