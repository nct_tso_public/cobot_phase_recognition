 
import torch
import torch.utils.data as data
import os
import numpy as np
import os
import os.path
import time
from sys import stderr
import yaml
import cv2
import parse


# adapted from Isabel Funke
def split_CoBot(anno_dir, out=None):
    videos = []
    for id in range(1, 34):
        anno_file = open(os.path.join(anno_dir, "video{:02d}-phase.txt".format(id)), 'r')
        last_line = anno_file.read().splitlines()[-1]
        length = int(last_line.split('\t')[0]) + 1
        videos.append(("{:02d}".format(id), length))
    # print(videos)

    partitioned = partition(videos)
    test_sets = [None] * 4
    for i in range(len(test_sets)):
        test_sets[i] = []
        for p in range(len(partitioned)):
            # print(partitioned[p])
            idx = np.random.choice(len(partitioned[p]), size=5, replace=False)
            # print(sorted(idx))
            to_remove = []
            for j in idx:
                test_sets[i].append(partitioned[p][j])
                to_remove.append(partitioned[p][j])
            for x in to_remove:
                partitioned[p].remove(x)
            to_remove.clear()
            # print(partitioned[p])
    for p in range(len(partitioned)):
        assert (len(partitioned[p]) == 0)
    del partitioned
    for i in range(len(test_sets)):
        assert (len(test_sets[i]) == 8  or len(test_sets[i]) == 9)
        print(np.mean([x[1] for x in test_sets[i]]))
    all_video_ids = []
    for i in range(len(test_sets)):
        all_video_ids += [int(x[0]) for x in test_sets[i]]
    assert(len(all_video_ids) == 33 and sorted(all_video_ids) == list(range(1, 81)))

    train_sets = [None] * 4
    for i in range(len(train_sets)):
        train_sets[i] = []
        for j in range(4):
            if j != i:
                train_sets[i].extend(test_sets[j])
    for i in range(len(train_sets)):
        assert(len(train_sets[i]) == 60)
        print(np.mean([x[1] for x in train_sets[i]]))

    val_sets = [None] * 4
    for i in range(len(val_sets)):
        val_sets[i] = []
        partitioned = partition(train_sets[i])
        for p in range(len(partitioned)):
            idx = np.random.choice(len(partitioned[p]), size=3 if (p == 1 or p == 2) else 2, replace=False)
            for j in idx:
                val_sets[i].append(partitioned[p][j])
                train_sets[i].remove(partitioned[p][j])
    for i in range(len(val_sets)):
        assert(len(val_sets[i]) == 10)
        assert (len(train_sets[i]) == 50)
        assert (len(test_sets[i]) == 20)
        print(np.mean([x[1] for x in val_sets[i]]))
        print(np.mean([x[1] for x in train_sets[i]]))

    for i in range(4):
        union = sorted([int(x[0]) for x in test_sets[i]] + [int(x[0]) for x in train_sets[i]] + [int(x[0]) for x in val_sets[i]])
        assert(len(union) == 80)
        assert(union == list(range(1, 81)))

    split_dict = {}
    for i in range(len(test_sets)):
        split_dict[i] = {}
        split_dict[i]['train'] = [int(x[0]) for x in train_sets[i]]
        split_dict[i]['val'] = [int(x[0]) for x in val_sets[i]]
        split_dict[i]['test'] = [int(x[0]) for x in test_sets[i]]
    print(split_dict)
    if out is not None:
        yaml.dump(split_dict, open(out, 'w'))


def partition(videos):
    durations = np.array([x[1] for x in videos])
    # print(np.mean(durations))
    thresholds = [np.min(durations), np.quantile(durations, .25), np.quantile(durations, .5),
                  np.quantile(durations, .75), np.max(durations) + 1]
    partitioned = []
    for p in range(1, 5):
        partitioned.append([x for x in videos if x[1] >= thresholds[p - 1] and x[1] < thresholds[p]])
    return partitioned
