import os
import cv2
import imageio
import utils
from utils import CoBot, MyCrop
import torchvision.transforms.functional as TF
from PIL import Image
import csv

from utils import DataPrepCoBot, CoBot
#video_path = "/mnt/g27prist/TCO/TCO-Studenten/CoBot/OPs"
#resolution = [utils.DataPrep.width, utils.DataPrep.height]  # Resolution for output

phase_map = dict()
phase_map['Preparation and intraabdominal orientation'] = 0
phase_map['Mobilization of colon (medial)'] = 1
phase_map['Mobilization of colon (lateral)'] = 2
phase_map['Completion of colon mobilization'] = 3 #sehr selten
phase_map['Total mesorectal excision and dissection of rectum'] = 4
phase_map['Extraabdominal preparation of anastomosis'] = 5
phase_map['Intraabdominal preparation of anastomosis'] = 6
phase_map['Ileostomy'] = 7
phase_map['Closure'] = 8

step_map_2 = dict()
step_map_2['Trocar placement'] = 0
step_map_2['Intraabdominal orientation'] = 1
step_map_2['Peritoneal Incision (medial)'] = 2 #4
step_map_2['Preparation/Clipping/Dissection of Inferior Mesenteric Artery'] = 3 #5
step_map_2['Separation of mesocolon and Gerota\\342\\200\\231s fascia (medial)'] = 4 #6
step_map_2['Preparation/Clipping/Dissection of Inferior Mesenteric Vein'] = 5 #7
step_map_2['Peritoneal Incision (lateral)'] = 6 #9
step_map_2['Separation of mesocolon and Gerota\\342\\200\\231s fascia (lateral)'] = 7 #10
step_map_2['Peritoneal incision superior to peritoneal fold'] = 8 #12
step_map_2['High mesorectal dissection'] = 9 #13
step_map_2['Low mesorectal dissection'] = 10 #14
step_map_2['Linear stapling of rectum'] = 11 #15

steps_phase_map = dict()
steps_phase_map['Preparation and intraabdominal orientation'] = 0
steps_phase_map['Mobilization of colon (medial)'] = 1
steps_phase_map['Peritoneal Incision (medial)'] = 2
steps_phase_map['Preparation/Clipping/Dissection of Inferior Mesenteric Artery'] = 3
steps_phase_map['Separation of mesocolon and Gerota\\342\\200\\231s fascia (medial)'] = 4
steps_phase_map['Preparation/Clipping/Dissection of Inferior Mesenteric Vein'] = 5
steps_phase_map['Mobilization of colon (lateral)'] = 6
steps_phase_map['Total mesorectal excision and dissection of rectum'] = 7
steps_phase_map['Extraabdominal preparation of anastomosis'] = 8

endpic = [35121,18580,21863,22466,14723,22554,35941,28581,15090,22251,13784,11770,25433,
22807,13321,6291,25597,39700,29798,36228,23163,20848,13434,14609,20121]



def test_frame():
    print("Hiiiiii")
    path = "/mnt/g27prist/TCO/TCO-Studenten/CoBot/frames/20191001/00000001.png"
    im = Image.open(path)
    mytr = MyCrop(22, 218, 675, 845)
    im = mytr(im)

    im.save("/home/krellstef/pretrain_tc/newpic3.png")

def analyseDataset():

    all_phases = False

    anno_files = []
    op_pathes  = []
    for op in range(25):
        op_path = os.path.join(utils.frames_path, utils.CoBot.videos[op])
        if os.path.isdir(op_path):
            anno_file = os.path.join(utils.annotation_path, utils.CoBot.videos[op] + ".txt")
            op_pathes.append(op_path)
            anno_files.append(anno_file)
    i = 0
    fg = open("phases_stuff", "w")
    fg1 = open("phases_latPic", "w")
    for annotation_path in anno_files:
        print(CoBot.videos[i])
        fg.write(CoBot.videos[i])
        fg.write("\n")
        fg1.write(CoBot.videos[i])
        fg1.write("\n")
        f = open(annotation_path, "r")
        reader = csv.reader(f, delimiter='\t')
        next(reader, None)
        count = 0
        end_temp = len(os.listdir(op_pathes[i]))
        fg1.write("lastPic: " + str(end_temp))
        fg1.write("\n")
        print(int(end_temp / 60))
        start = 0 
        for row in reader:
            #print(row[0])
            if not all_phases and row[0] not in CoBot.phase_map:
                #print(row[0])
                continue
            startframe = int(int(row[1]) / 1000.0 )#* CoBot.fps_video / 29.0)
            endframe = int(int(row[2]) / 1000.0 )#* CoBot.fps_video / 29.0)

            if count == 0:
                start = startframe

            if endframe > end_temp:
                endframe = end_temp
            duration_phase = endframe - startframe
            pers = int((duration_phase / (end_temp - start)) *100) 
            duration_phase = str(int(duration_phase / 60))#
            that = str(phase_map[row[0]]) + ": " + duration_phase + " min, " + str(pers) + " %"
            print(that)
            fg.write(that)
            fg.write("\n")
            for i in range(5, 1):
                print(i)
            count += 1
        f.close()
        fg.write("\n")
        i += 1
    fg.close()
    fg1.close()
def doStuff():
    anno_files = []
    op_pathes  = []
    for op in range(25):
        op_path = os.path.join(utils.frames_path, utils.CoBot.videos[op])
        if os.path.isdir(op_path):
            anno_file = os.path.join(utils.annotation_path, utils.CoBot.videos[op] + ".txt")
            op_pathes.append(op_path)
            anno_files.append(anno_file)
    i = 0
    f_new = open("phases__.csv", "w")
    f_new.write("op_id,P1,P2,P3,P4,P5,P6,P7,P8")
    f_new.write("\n")
    for annotation_path in anno_files:
        phase_map2 = dict()
        phase_map2['Preparation and intraabdominal orientation'] = 0
        phase_map2['Mobilization of colon (medial)'] = 0
        phase_map2['Mobilization of colon (lateral)'] = 0
        #phase_map2['Completion of colon mobilization'] = 0 #sehr selten
        phase_map2['Total mesorectal excision and dissection of rectum'] = 0
        phase_map2['Extraabdominal preparation of anastomosis'] = 0
        phase_map2['Intraabdominal preparation of anastomosis'] = 0
        phase_map2['Ileostomy'] = 0
        phase_map2['Closure'] = 0
        f_new.write(CoBot.videos[i])
        f = open(annotation_path, "r")
        reader = csv.reader(f, delimiter='\t')
        next(reader, None)
        count = 0
        end_temp = endpic[i]
        for row in reader:
            #print(row[0])
            startframe = int(int(row[1]) / 1000.0 )#* CoBot.fps_video / 29.0)
            endframe = int(int(row[2]) / 1000.0 )#* CoBot.fps_video / 29.0)

            if count == 0:
                start = startframe
            if startframe > end_temp:
                continue
            if endframe > end_temp:
                endframe = end_temp
            duration_phase = endframe - startframe
            phase_map2[row[0]] += duration_phase
            pers = int((duration_phase / (end_temp - start)) *100) 
            duration_phase = str(int(duration_phase / 60))#
            that = str(phase_map[row[0]]) + ": " + duration_phase + " min, " + str(pers) + " %"
            print(that)
            count += 1
        f.close()
        for k,v in phase_map2.items():
            f_new.write(","+str(v))
        f_new.write("\n")
        i+= 1
    f_new.close()
def analyse_Steps():
    anno_files = []
    for op in range(25):
        op_path = os.path.join(utils.frames_path, utils.CoBot.videos[op])
        if os.path.exists(op_path):
            anno_file = op_path
            anno_file = os.path.join(utils.annotation_path_step, utils.CoBot.videos[op] + "step.txt")
            #op_pathes.append(op_path)
            anno_files.append(anno_file)
            #print('found')
        else:
            print("not found")
            exit()
    i = 0
    f_new = open("step_analysis.csv", "w")
    f_new.write("op_id,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,S11,S12")
    f_new.write("\n")
    for annotation_path in anno_files:
        print(annotation_path)
        step_map = dict()
        step_map['Trocar placement'] = 0
        step_map['Intraabdominal orientation'] = 0
        step_map['Peritoneal Incision (medial)'] = 0
        step_map['Preparation/Clipping/Dissection of Inferior Mesenteric Artery'] = 0
        step_map['Separation of mesocolon and Gerota\\342\\200\\231s fascia (medial)'] = 0
        step_map['Preparation/Clipping/Dissection of Inferior Mesenteric Vein'] = 0
        step_map['Peritoneal Incision (lateral)'] = 0
        step_map['Separation of mesocolon and Gerota\\342\\200\\231s fascia (lateral)'] = 0
        step_map['Peritoneal incision superior to peritoneal fold'] = 0
        step_map['High mesorectal dissection'] = 0
        step_map['Low mesorectal dissection'] = 0
        step_map['Linear stapling of rectum'] = 0
        f_new.write(CoBot.videos[i])
        f = open(annotation_path, "r")
        reader = csv.reader(f, delimiter='\t')
        next(reader, None)
        count = 0
        end_temp = endpic[i]
        for row in reader:
            #print(row[0])
            if row[0] not in step_map_2:
                print(row[0] + " is not a defined step")
                continue
            startframe = int(int(row[1]) / 1000.0 )#* CoBot.fps_video / 29.0)
            endframe = int(int(row[2]) / 1000.0 )#* CoBot.fps_video / 29.0)

            if count == 0:
                start = startframe
            if startframe > end_temp:
                continue
            if endframe > end_temp:
                endframe = end_temp
            duration_phase = endframe - startframe
            step_map[row[0]] += duration_phase
            pers = int((duration_phase / (end_temp - start)) *100) 
            duration_phase = str(int(duration_phase / 60))#
            that = str(step_map_2[row[0]]) + ": " + duration_phase + " min, " + str(pers) + " %"
            print(that)
            count += 1
        f.close()
        
        for k,v in step_map.items():
            f_new.write(","+str(v))
        f_new.write("\n")
        i+= 1
    f_new.close()

def analyse_Steps_phases():
    anno_files = []
    for op in utils.CoBot.videos:
        op_path = os.path.join(utils.frames_path, utils.CoBot.videos[op])
        #os.path.join("E:/newnewn/step_annotations_adjusted_new", op + "step.txt")
        if os.path.exists(op_path):
            anno_file = os.path.join(utils.annotation_path_step, utils.CoBot.videos[op] + ".txt")
            #anno_file = op_path
            #op_pathes.append(op_path)
            anno_files.append(anno_file)
            #print('found')
        else:
            print("not found")
            exit()
    i = 0
    f_new = open("step_analysis.csv", "w")
    f_new.write("op_id,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,S11,S12")
    f_new.write("\n")
    for annotation_path in anno_files:
        print(annotation_path)
        phase_map2 = dict()
        phase_map2['Preparation and intraabdominal orientation'] = 0
        phase_map2['Mobilization of colon (medial)'] = 0
        phase_map2['Mobilization of colon (lateral)'] = 0
        phase_map2['Total mesorectal excision and dissection of rectum'] = 0
        phase_map2['Extraabdominal preparation of anastomosis'] = 0
        phase_map2['Peritoneal Incision (medial)'] = 0
        phase_map2['Preparation/Clipping/Dissection of Inferior Mesenteric Artery'] = 0
        phase_map2['Separation of mesocolon and Gerota\\342\\200\\231s fascia (medial)'] = 0
        phase_map2['Preparation/Clipping/Dissection of Inferior Mesenteric Vein'] = 0
  
        f_new.write(CoBot.videos[i])
        f = open(annotation_path, "r")
        reader = csv.reader(f, delimiter='\t')
        next(reader, None)
        count = 0
        end_temp = endpic[i]
        for row in reader:
            #print(row[0])
            if row[0] not in step_map_2:
                print(row[0] + " is not a defined step")
                continue
            startframe = int(int(row[1]) / 1000.0 )#* CoBot.fps_video / 29.0)
            endframe = int(int(row[2]) / 1000.0 )#* CoBot.fps_video / 29.0)

            if count == 0:
                start = startframe
            if startframe > end_temp:
                continue
            if endframe > end_temp:
                endframe = end_temp
            duration_phase = endframe - startframe
            phase_map2[row[0]] += duration_phase
            pers = int((duration_phase / (end_temp - start)) *100) 
            duration_phase = str(int(duration_phase / 60))#
            that = str(steps_phase_map[row[0]]) + ": " + duration_phase + " min, " + str(pers) + " %"
            print(that)
            count += 1
        f.close()
        
        for k,v in phase_map2.items():
            f_new.write(","+str(v))
        f_new.write("\n")
        i+= 1
    f_new.close()

analyse_Steps()


