"""
Script to read and format the annotation file
"""
import os.path
import argparse

#new_vids = ['20200710','20200717','20200727','20200731','20201002','20201006', '20201201','20201204']
new_vids = ['20201208','20190214','20200611','20200123','20200302','20191206', '20210113', '20200212']

if __name__ == "__main__":

	parser = argparse.ArgumentParser(description="Simplify annotation files.")
	parser.add_argument("anno", type=str, choices=["Phase","Step"], help="Phase or Step")
	parser.add_argument("--output_path", type=str, default = "/mnt/g27prist/TCO/TCO-Studenten/CoBot",
                        help="output path")
	parser.add_argument("--videos_path", type=str, default = "/mnt/g27prist/TCO/TCO-Studenten/CoBot/OPs",
                        help="path to annoations")
	args = parser.parse_args()
	
	for v in range(len(new_vids)):
		annotationPath = os.path.join(args.videos_path,new_vids[v], new_vids[v]+" text.txt")
		print(annotationPath )
		start = 0
		stop = 0
		newList = []
		with open(annotationPath,'r') as f:
			i = 0
			state_phase = False
			onephase = dict()
			for line in f:
				i += 1
				readStart = False
				readEnd = False
				for word in line.split():
					#print(word)
					if state_phase:
						if word == 'label:':
							s2 = line.split(' ',1)[1]
							onephase['label'] = s2.rstrip()
					if word == args.anno:
						state_phase = True
					elif state_phase and word == "startTime:":
						readStart = True
					elif readStart:
						readStart = False
						onephase['start'] = word
					elif state_phase and word == "stopTime:":
						readEnd = True
					elif readEnd:
						readEnd = False
						onephase['stop'] = word
						newList.append(onephase)
						onephase = dict()
						state_phase = False
		#print(newList)
		newannotationPath = os.path.join(args.output_path, new_vids[v]+".txt")
		with open(newannotationPath,'w') as f:
			f.write('label\tstart\tstop')
			f.write('\n')
			for element in newList:
				n = element['label'] + '\t'+ element['start'] + '\t'+ element['stop']
				print(n)
				f.write(n)
				f.write('\n')


