"""
Sequence datasets of features
"""


import csv
import os.path
import random as rand

import numpy as np
import torch
import torch.utils.data as data
from PIL import Image
from skimage.measure import compare_ssim as ssim

from utils import Cholec80, CoBot, DataPrep3, DataPrepCoBot

class PhaseData_FeaturesFullVideo(data.Dataset):
    def __init__(self,frames_path, annotation_path, features):
        self.targets = []
        self.frames_path = frames_path
        self.features = torch.stack(features)

        f = open(annotation_path, "r")
        reader = csv.reader(f, delimiter='\t')
        next(reader, None)
        count = 0
        for row in reader:
            if count % (25 * 1) == 0: 
                self.targets.append(Cholec80.phase_map[row[1]])
            count += 1
        f.close()
        self.targets = torch.tensor(self.targets)

    def __getitem__(self, index):

        return self.features, self.targets

    def __len__(self):
        """ Return size of dataset. """
        return 1

class PhaseData_FeaturesFullVideo_List(data.Dataset):
    def __init__(self,frames_pathes, annotation_pathes, features_):
        self.targets = []
        self.frames_pathes = frames_pathes
        self.features = []
        for annotation_path in annotation_pathes:
            target = []
            f = open(annotation_path, "r")
            reader = csv.reader(f, delimiter='\t')
            next(reader, None)
            count = 0
            for row in reader:
                if count % (25 * 1) == 0 : 
                    target.append(Cholec80.phase_map[row[1]])
                count += 1
            f.close()
            target = torch.tensor(target)
            #print(target.size())
            self.targets.append(target)

        for frames_path in frames_pathes:
            f = features_[frames_path]
            f = torch.stack(f)
            #print(f.size())
            self.features.append(f)            

    def __getitem__(self, index):

        return self.features[index], self.targets[index]

    def __len__(self):
        """ Return size of dataset. """
        return len(self.targets)

class PhaseData_Features(data.Dataset):
    def __init__(self,frames_path, annotation_path, features,sequence):
        self.targets = []
        self.frames_path = frames_path
        features = torch.stack(features)
        if sequence == -1:
            self.features = [features]
        else:
            self.features =  features.split(sequence)

        f = open(annotation_path, "r")
        reader = csv.reader(f, delimiter='\t')
        next(reader, None)
        count = 0
        for row in reader:
            if count % (25 * 1) == 0: 
                self.targets.append(Cholec80.phase_map[row[1]])
            count += 1
        f.close()
        self.targets = torch.tensor(self.targets)
        if sequence == -1:
            self.targets = [self.targets]
        else:
            self.targets = self.targets.split(sequence)

    def __getitem__(self, index):

        return self.features[index], self.targets[index]

    def __len__(self):
        """ Return size of dataset. """
        return len(self.targets)


class PhaseData_Features_train(data.Dataset):
    def __init__(self,frames_path, annotation_path, features,sequence):
        self.targets = []
        self.frames_path = frames_path
        self.features = []
        fe = torch.stack(features)
        self.sequence = sequence

        f = open(annotation_path, "r")
        reader = csv.reader(f, delimiter='\t')
        next(reader, None)
        count = 0
        target = []
        for row in reader:
            if count % (25 * 1) == 0: 
                target.append(Cholec80.phase_map[row[1]])
            count += 1
        f.close()
        target = torch.tensor(target)

        for i in range(target.size(0) - self.sequence + 1):
            self.targets.append(target[i : i + self.sequence])
            self.features.append(fe[i : i + self.sequence,:])  

    def __getitem__(self, index):

        return self.features[index], self.targets[index]

    def __len__(self):
        """ Return size of dataset. """
        return len(self.targets)

# takes several videos, return serquences that can overlap --> only for training
class PhaseData_Features_train_list(data.Dataset):
    def __init__(self,frames_pathes, annotation_pathes, features_, sequence):
        self.targets = []
        self.frames_pathes = frames_pathes
        self.features = []
        self.sequence = sequence
        for annotation_path, frames_path in zip(annotation_pathes,frames_pathes):
            target = []
            f = open(annotation_path, "r")
            reader = csv.reader(f, delimiter='\t')
            next(reader, None)
            count = 0
            for row in reader:
                if count % (25 * 1) == 0 : 
                    target.append(Cholec80.phase_map[row[1]])
                count += 1
            f.close()
            target = torch.tensor(target)
            #print(target.size())
            fe = features_[frames_path]
            fe = torch.stack(fe)
            #print(fe.size())
            for i in range(target.size(0) - self.sequence + 1):
                self.targets.append(target[i : i + self.sequence])
                self.features.append(fe[i : i + self.sequence,:])  
            #print(target.size())
        print(len(self.targets))

    def __getitem__(self, index):
        return self.features[index], self.targets[index]

    def __len__(self):
        """ Return size of dataset. """
        return len(self.targets)

# takes one video no overlapping first one is shorter
class PhaseData_Features_test(data.Dataset):
    def __init__(self,frames_path, annotation_path, features,sequence):
        self.targets = []
        self.frames_path = frames_path
        self.features = []
        fe = torch.stack(features)
        self.sequence = sequence

        t = []

        f = open(annotation_path, "r")
        reader = csv.reader(f, delimiter='\t')
        next(reader, None)
        count = 0
        for row in reader:
            if count % (25 * 1) == 0: 
                t.append(Cholec80.phase_map[row[1]])
            count += 1
        f.close()
        t = torch.tensor(t)
        #print(t.size())
        first = t.size(0) % self.sequence
        r = int(np.ceil(t.size(0) / self.sequence))
        #print(r)
        #print(first)
        for index in range(r):
                v = (index - 1) * self.sequence + first
                end = v + self.sequence
                if index == 0:
                    v = 0
                    end = first               
                self.targets.append(t[v : end])
                self.features.append(fe[v :end,:])  

    def __getitem__(self, index):
        return self.features[index], self.targets[index]

    def __len__(self):
        """ Return size of dataset. """
        return len(self.targets)