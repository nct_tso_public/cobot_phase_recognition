"""
datasets for Cobot and Cholec80 
1 image at a time
"""

import torch.utils.data as data
import csv
from PIL import Image
import os.path
import numpy as np
import random as rand
#from skimage.measure import compare_ssim as ssim
from skimage.metrics import structural_similarity as ssim


from utils import Cholec80, DataPrep, DataPrepCoBot, CoBot, EndoVis18
import cv2
import torch
class PhaseData_(data.Dataset):
    """Dataset consisting of the sequential frames and their corresponding phase labels of one specific Cholec80 video.
    """

    def __init__(self, frames_path, annotation_path, fps, size, transform=None, pad_frames=True,
                 sample_tc_tuple=False, delta=None, gamma=None):
        """Create dataset.

        :param frames_path: Path to the directory that contains the video frames.
        :param annotation_path: Path to the text file that contains the phase labels.
        :param fps
        :param size
        :param transform: Image transformation applied to each frame in the dataset. (albumentations transform)
        :param sample_tc_tuple
        :param delta: Parameter to determine the size of the window from which a close frame is sampled.
                      Close frames are at most "delta" seconds away from a reference frame.
        :param gamma: Parameter to determine from where a distant frame is sampled.
                      Distant frames are at least "gamma" seconds away from a reference frame.
        """
        self.frames_path = frames_path
        assert(fps > 0)  # is it okay if Cholec80.fps % fps != 0 ?
        self.fps = fps
        self.size = size
        self.transform = transform
        self.pad_frames = pad_frames

        self.sample_tc_tuple = sample_tc_tuple
        if sample_tc_tuple:
            self.delta = int(math.ceil(delta * fps))  # convert to frame distance
            self.gamma = int(math.ceil(gamma * fps))  # convert to frame distance

            if self.transform is not None:
                additional_targets = {"image_c": "image",
                                      "image_f": "image"}
                self.transform.additional_targets = additional_targets
                self.transform.add_targets(additional_targets)

        self.labels = []
        f = open(annotation_path, "r")
        reader = csv.reader(f, delimiter='\t')
        next(reader, None)
        count = 0
        step = 25  # int(Cholec80.fps // fps)
        for row in reader:
            if count % step == 0:
                self.labels.append(Cholec80.phase_map[row[1]])
                #self.labels.append(phase_map[row[1]])
            count += 1
        f.close()
        assert(len([name for name in os.listdir(self.frames_path) if name.endswith('.png')]) == len(self.labels))

    def load_frame(self, index):  # numpy array, RGB, range: 0..255
        # TODO necessary to adjust index ?
        file_path = os.path.join(self.frames_path, "{:08d}.png".format(index))

        img = cv2.imread(file_path)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        w = img.shape[1]
        h = img.shape[0]

        # scale while preserving aspect ration, shorter image side will become self.size
        if w <= h:
            if w == self.size:
                im_scaled = img
            else:
                im_scaled = cv2.resize(img, (self.size, int(self.size * (h / w))), interpolation=cv2.INTER_AREA)
        else:
            if h == self.size:
                im_scaled = img
            else:
                im_scaled = cv2.resize(img, (int(self.size * (w / h)), self.size), interpolation=cv2.INTER_AREA)
        scaled_w = im_scaled.shape[1]
        scaled_h = im_scaled.shape[0]

        if scaled_w == scaled_h or not self.pad_frames:
            return im_scaled
        else:
            # pad to obtain square dimensions
            new_size = max(scaled_w, scaled_h)
            padded_img = np.zeros((new_size, new_size, 3), np.uint8)
            if scaled_h < scaled_w:
                offset_h = (new_size - scaled_h) // 2
                padded_img[offset_h: offset_h + scaled_h, :, :] = im_scaled
            else:
                offset_w = (new_size - scaled_w) // 2
                padded_img[:, offset_w: offset_w + scaled_w, :] = im_scaled

            return padded_img

    """ 
    @staticmethod
    def calc_ssim(img1, img2):
        img1_gray = Image.fromarray(img1).convert('L')
        img2_gray = Image.fromarray(img2).convert('L')
        return ssim(np.asarray(img1_gray), np.asarray(img2_gray),
                    gaussian_weights=True, sigma=1.5, use_sample_covariance=False, data_range=255)
    """

    def __getitem__(self, index):
        """Return the frame at the given index along with its phase label.

        :param index: The number of the frame to return.
        :return:
        """

        label = self.labels[index]
        img = self.load_frame(index)

        if self.sample_tc_tuple:
            # sample close image
            random_offset = random.choice(list(range(1, self.delta + 1)) + list(range(-self.delta, 0)))
            index_c = index + random_offset
            index_c = min(max(0, index_c), len(self.labels) - 1)
            img_c = self.load_frame(int(index_c))

            # sample far image
            index_f = random.choice(list(range(0, max(0, index - self.gamma + 1)))
                                    + list(range(min(index + self.gamma, len(self.labels) - 1), len(self.labels))))
            img_f = self.load_frame(int(index_f))

            label_c = self.labels[index_c]
            label_f = self.labels[index_f]

            """
            # sample close images
            rand_offset = random.choice(list(range(1, self.delta + 1)) + list(range(-self.delta, 0)))
            index_c2 = index + ( 2 * rand_offset)
            if index_c2 < 0:
                index_c2 *= -1
                if index == index_c2:
                    index_c2 = index + 2
            elif index_c2 >= len(self.labels):
                index_c2 = 2 * (len(self.labels) - 1) - index_c2
                if index == index_c2:
                    index_c2 = index - 2
            assert (index_c2 >= 0 and index_c2 < len(self.labels) and
                    abs(index - index_c2) >= 2 and abs(index - index_c2) <= 2 * self.delta)
            index_c = index + ((index_c2 - index) // 2)
            img_c = self.load_frame(int(index_c))
            img_c2 = self.load_frame(int(index_c2))

            # sample far image
            img_f = None
            success = False
            while not success:
                index_f = random.choice(list(range(0, max(0, index - self.gamma + 1)))
                                        + list(range(min(index + self.gamma, len(self.labels) - 1), len(self.labels))))
                img_f = self.load_frame(int(index_f))
                s = PhaseData.calc_ssim(img, img_f)
                # print('ssim:', s)
                if s > 0.7:
                    img_f = None  # discard "far" images that are too similar
                else:
                    success = True
            """

        if self.transform is not None:
            if self.sample_tc_tuple:
                transformed = self.transform(image=img, image_c=img_c, image_f=img_f)
                img = transformed['image']
                img_c = transformed['image_c']
                img_f = transformed['image_f']
            else:
                img = self.transform(image=img)['image']

        if self.sample_tc_tuple:
            return (img, img_c, img_f,), (label, label_c, label_f)
        else:
            return img, label

    def __len__(self):
        """ Return size of dataset. """

        return len(self.labels)
class MyDataSet(data.Dataset):
    # return complete (padded) sequence
    # if padded_length < 0, simply return complete sequence
    # if sequence is longer than 'padded length', randomly select subsequence of length 'padded_length'
    # apply data augmentation
    #
    # return: feature_seq, label_seq, position_ids, key_padding_mask
    def __init__(self, feature_file, mode,video_id,
                 return_video_ids = False
                 ):
        super(MyDataSet, self).__init__()

        assert(mode in ["train", "test", "val"])

        self.return_video_ids = return_video_ids

        # data structure
        self.video_id = video_id
        self.label_seqs = []  # dict of tensors (seq_length)
        self.feature_seqs = []  # dict of tensors (seq_length x feature_dim)
        self.feature_dim = -1

        self._create_dataset(feature_file, mode)

    def _create_dataset(self, feature_file, mode):
        data = feature_file#torch.load(feature_file)

        data_fps = data['fps']
        self.feature_dim = -1
        label_seq = data['labels'][self.video_id]
        feature_seq = data[mode][self.video_id].squeeze()
        if self.feature_dim < 0:
            self.feature_dim = feature_seq.shape[1]
        else:
            assert(self.feature_dim == feature_seq.shape[1])

        print(label_seq.size())
        print(feature_seq.size())

        assert(label_seq.shape[0] == feature_seq.shape[0])
        self.label_seqs = label_seq
        self.feature_seqs = feature_seq

    def calculate_class_weights(self, num_class=10):
        counts = np.zeros(num_class, dtype=np.float)
        total_frames = 0
        for label_seq in self.label_seqs.values():
            label_seq = label_seq.numpy()
            for i in range(label_seq.shape[0]):
                label = label_seq[i]
                assert(label < num_class)
                counts[label] += 1
                total_frames += 1

        freqs = counts / total_frames
        median_freq = np.median(freqs)

        missing_labels = []
        for label in range(freqs.shape[0]):
            if freqs[label] == 0:
                print("No frames with label {} found!".format(label))
                freqs[label] = 1
                missing_labels.append(label)

        weights = np.ones(num_class, dtype=np.float) * median_freq
        weights = weights / freqs
        for missing in missing_labels:
            weights[missing] = 0

        weights = (weights / np.sum(weights)) * num_class
        print(weights)

        return torch.from_numpy(weights).float()


    def __getitem__(self, index):
        feature_seq = self.feature_seqs[index,:]
        label_seq = self.label_seqs[index]

        return feature_seq, label_seq


    def __len__(self):
        return len(self.feature_seqs)

class MyDataSet_(data.Dataset):
    # return complete (padded) sequence
    # if padded_length < 0, simply return complete sequence
    # if sequence is longer than 'padded length', randomly select subsequence of length 'padded_length'
    # apply data augmentation
    #
    # return: feature_seq, label_seq, position_ids, key_padding_mask
    def __init__(self, feature_file, mode,
                 noise_factor=-1, added_noise=-1,
                 max_duplicate_frames_prob=-1, duplicated_duration=0,
                 max_drop_frames_prob=-1, dropped_duration=0,
                 max_randomly_replace_prob=-1, randomly_replaced_duration=0,
                 max_mask_prob=-1, masked_duration=0,
                 ):
        super(MyDataSet_, self).__init__()

        assert(mode in ["train", "test", "val"])
        assert(max_duplicate_frames_prob <= 1 and max_drop_frames_prob <= 1
               and max_randomly_replace_prob <= 1 and max_mask_prob <= 1)

        # data structure
        self.video_ids = []
        self.label_seqs = {}  # dict of tensors (seq_length)
        self.feature_seqs = {}  # dict of tensors (seq_length x feature_dim)
        self.feature_dim = -1

        self._create_dataset(feature_file, mode)
        self.considered_video_ids = self.video_ids[:]

        self.config = { # save for switching data augmentation on/ off
            "added_noise": added_noise,
            "max_duplicate_frames_prob": max_duplicate_frames_prob,
            "max_drop_frames_prob": max_drop_frames_prob,
            "max_randomly_replace_prob": max_randomly_replace_prob,
            "max_mask_prob": max_mask_prob
        }

        self.added_noise = self.config["added_noise"]
        self.max_duplicate_frames_prob = self.config["max_duplicate_frames_prob"]
        self.max_drop_frames_prob = self.config["max_drop_frames_prob"]
        self.max_randomly_replace_prob = self.config["max_randomly_replace_prob"]
        self.max_mask_prob = self.config["max_mask_prob"]

        sampling_rate = 1#ävideo_fps / video_sampling_step
        self.span_duplicated = int(sampling_rate * duplicated_duration)
        self.span_dropped = int(sampling_rate * dropped_duration)
        self.span_randomly_replaced = int(sampling_rate * randomly_replaced_duration)
        self.span_masked = int(sampling_rate * masked_duration)

    def _create_dataset(self, feature_file, mode):
        data = feature_file#torch.load(feature_file)

        data_fps = data['fps']

        #assert(data_fps == (self.video_fps / self.video_sampling_step))  # TODO

        self.video_ids = sorted(data[mode].keys())

        self.feature_dim = -1
        for video_id in self.video_ids:
            label_seq = data['labels'][video_id]
            feature_seq = data[mode][video_id].squeeze()
            if self.feature_dim < 0:
                self.feature_dim = feature_seq.shape[1]
            else:
                assert(self.feature_dim == feature_seq.shape[1])

            assert(label_seq.shape[0] == feature_seq.shape[0])
            self.label_seqs[video_id] = label_seq
            self.feature_seqs[video_id] = feature_seq

    def calculate_class_weights(self, num_class=10):
        counts = np.zeros(num_class, dtype=np.float)
        total_frames = 0
        for label_seq in self.label_seqs.values():
            label_seq = label_seq.numpy()
            for i in range(label_seq.shape[0]):
                label = label_seq[i]
                assert(label < num_class)
                counts[label] += 1
                total_frames += 1

        freqs = counts / total_frames
        median_freq = np.median(freqs)

        missing_labels = []
        for label in range(freqs.shape[0]):
            if freqs[label] == 0:
                print("No frames with label {} found!".format(label))
                freqs[label] = 1
                missing_labels.append(label)

        weights = np.ones(num_class, dtype=np.float) * median_freq
        weights = weights / freqs
        for missing in missing_labels:
            weights[missing] = 0

        weights = (weights / np.sum(weights)) * num_class
        print(weights)

        return torch.from_numpy(weights).float()

    def __getitem__(self, index):
        video_id = self.considered_video_ids[index]
        feature_seq = self.feature_seqs[video_id]
        label_seq = self.label_seqs[video_id]
        assert(label_seq.shape[0] == feature_seq.shape[0])

        # ***** data augmentation *****

        if self.max_duplicate_frames_prob > 0:
            p = torch.tensor((1,), dtype=torch.float).uniform_(0, self.max_duplicate_frames_prob).item()
            probs = torch.ones((feature_seq.shape[0],), dtype=torch.float) * p
            seeds = torch.bernoulli(probs).bool()
            new_index = list()
            last = -1
            for i in range(feature_seq.shape[0]):
                if seeds[i].item():
                    span = int(torch.tensor((1,), dtype=torch.float).normal_(self.span_duplicated,
                                                                             self.span_duplicated * 0.16667).item())
                    span = min(feature_seq.shape[0] - i, max(0, span))

                    if span > 0:
                        f = torch.tensor((1,), dtype=torch.float).uniform_(0, 1).item()
                        if f >= 0.9:
                            factor = 4
                        elif f >= 0.6:
                            factor = 3
                        else:
                            factor = 2
                        for j in range(span):
                            idx = i + j
                            new_index.extend([idx] * factor)
                        last = idx
                    else:
                        new_index.append(i)
                else:
                    if i > last:
                        new_index.append(i)
            feature_seq = feature_seq[new_index, :]
            label_seq = label_seq[new_index]

        if self.max_drop_frames_prob > 0:
            p = torch.tensor((1,), dtype=torch.float).uniform_(0, self.max_drop_frames_prob).item()
            probs = torch.ones((feature_seq.shape[0],), dtype=torch.float) * p
            seeds = torch.bernoulli(probs).bool()
            new_index = list()
            last = -1
            for i in range(feature_seq.shape[0]):
                if seeds[i].item():
                    span = int(torch.tensor((1,), dtype=torch.float).normal_(self.span_dropped,
                                                                             self.span_dropped * 0.16667).item())
                    span = min(feature_seq.shape[0] - i, max(0, span))

                    if span > 0:
                        f = torch.tensor((1,), dtype=torch.float).uniform_(0, 1).item()
                        if f >= 0.9:
                            factor = 4
                        elif f >= 0.6:
                            factor = 3
                        else:
                            factor = 2
                        j = factor - 1
                        while j < span:
                            new_index.append(i + j)
                            j += factor
                        last = i + (span - 1)
                    else:
                        new_index.append(i)
                else:
                    if i > last:
                        new_index.append(i)
            feature_seq = feature_seq[new_index, :]
            label_seq = label_seq[new_index]

        feature_seq = feature_seq.clone().detach()

        if self.max_randomly_replace_prob > 0:
            p = torch.tensor((1,), dtype=torch.float).uniform_(0, self.max_randomly_replace_prob).item()
            probs = torch.ones((feature_seq.shape[0],), dtype=torch.float) * p
            seeds = torch.bernoulli(probs).bool()
            for i in range(feature_seq.shape[0]):
                if seeds[i].item():
                    span = int(torch.tensor((1,), dtype=torch.float).normal_(self.span_randomly_replaced,
                                                                             self.span_randomly_replaced * 0.16667).item())
                    span = min(feature_seq.shape[0] - i, span)
                    if span > 0:
                        random_offset = torch.randint(0, feature_seq.shape[0] - span + 1, (1,)).item()
                        feature_seq[i: i + span, :] = feature_seq[random_offset: random_offset + span, :].clone().detach()

        if self.added_noise > 0:
            noise = torch.normal(mean=0, std=self.added_noise, size=feature_seq.size())
            feature_seq = feature_seq + noise

        if self.max_mask_prob > 0:
            p = torch.tensor((1,), dtype=torch.float).uniform_(0, self.max_mask_prob).item()
            probs = torch.ones((feature_seq.shape[0],), dtype=torch.float) * p
            seeds = torch.bernoulli(probs).bool()
            for i in range(feature_seq.shape[0]):
                if seeds[i].item():
                    span = int(torch.tensor((1,), dtype=torch.float).normal_(self.span_masked,
                                                                             self.span_masked * 0.16667).item())
                    span = min(feature_seq.shape[0] - i, span)
                    if span > 0:
                        feature_seq[i: i + span, :] = torch.zeros((span, feature_seq.shape[1]), dtype=feature_seq.dtype)

        return feature_seq, label_seq,video_id


    def __len__(self):
        return len(self.considered_video_ids)

def _load_img(path, width, height):
    im = Image.open(path)
    w = im.width
    h = im.height
    im_scaled = im.resize((width, int(width * (h / w))))  # preserve aspect ratio

    img = Image.new('RGB', (width, height), (0, 0, 0))
    offset_y = (height - im_scaled.height) // 2
    img.paste(im_scaled, box=(0, offset_y))
    return img
class PhaseData_CoBot_old(data.Dataset):
    """Dataset consisting of the sequential frames and their corresponding phase labels of one specific Cholec80 video.
    Frames will be sampled at 1 Hz.
    """

    def __init__(self, frames_path, annotation_path, width, height, transform=None, sample_rate = DataPrepCoBot.sample_rate ):
        """Create dataset.
        :param frames_path: Path to the directory that contains the video frames.
        :param annotation_path: Path to the text file that contains the phase labels.
        :param width: Width of each frame in the dataset.
        :param height: Height of each frame in the dataset.
        :param transform: Image transformation applied to each frame in the dataset.
        """
        self.frames_path = frames_path
        self.width = width
        self.height = height
        self.transform = transform
        self.sample_rate = sample_rate

        self.targets = []
        self.start = 0
        self.end = len(os.listdir(self.frames_path)) # number of pictures in directory
        f = open(annotation_path, "r")
        reader = csv.reader(f, delimiter='\t')
        next(reader, None)
        count = 0
        endframe = 0
        for row in reader:
            if row[0] not in CoBot.phase_map:
                #print(row[0])
                continue
            startframe = int(int(row[1]) / 1000.0 )#* CoBot.fps_video / 29.0) # annotation in Millisekunden
            if count == 0:
                self.start = startframe
            endframe = int(int(row[2]) / 1000.0)# * CoBot.fps_video / 29.0)
            if endframe > (self.end):
                #print(endframe)
                endframe = self.end
                #print(row[0])
            #print("start: " + str(startframe) +" end:" + str(endframe))
            st = int(startframe / sample_rate)
            en = int(endframe / sample_rate)
            #print("start2: " + str(st) +" end2:" + str(en))  
            for t in range(st,en):
                self.targets.append(CoBot.phase_map[row[0]])
            count += 1
        #print("Länge Datensatz: " + str(len(self.targets)) + ", Start: " + str(self.start) + ", number pngs: " + str(self.end))
        
        f.close()

    def __getitem__(self, index):
        """Return the frame at the given index along with its phase label.
        :param index: The number of the frame to return.
        :return: The tuple (frame, label).
        """
        #print(index)
        #print("Bild: " + str(index + self.start))
        target = self.targets[index]
        frame = self.frames_path + "/%08d.png" % (self.sample_rate * index + self.start)  # read frames with 1 fps
        #if index > self.start + self.end / DataPrepCoBot.sample_rate -3:
            #print("index: " + str(index) + ", frame: " + frame)
        img = Image.open(frame)#_load_img(frame, self.width, self.height)
        #print(img.shape())
        #exit()

        if self.transform is not None:
            img = self.transform(img)

        return img, target

    def __len__(self):
        """ Return size of dataset. """

        return len(self.targets)
class PhaseData_CoBot(data.Dataset):
    """Dataset consisting of the sequential frames and their corresponding phase labels of one specific Cholec80 video.

    Frames will be sampled at 1 Hz.
    """

    def __init__(self, frames_path, annotation_path, width, height, transform=None, annotation = "phase", step_file = None, sample_rate = DataPrepCoBot.sample_rate):
        """Create dataset.

        :param frames_path: Path to the directory that contains the video frames.
        :param annotation_path: Path to the text file that contains the phase labels.
        :param width: Width of each frame in the dataset.
        :param height: Height of each frame in the dataset.
        :param transform: Image transformation applied to each frame in the dataset.
        :param sample_rate: 1 or higher .
        """
        self.frames_path = frames_path
        self.width = width
        self.height = height
        self.transform = transform
        self.sample_rate = sample_rate

        self.targets = []
        self.start = 0
        self.end = len(os.listdir(self.frames_path)) # number of pictures in directory
        f = open(annotation_path, "r")
        reader = csv.reader(f, delimiter='\t')
        next(reader, None)
        count = 0
        endframe = 0


        if annotation == "phase":
            annotation_map = CoBot.phase_map
        elif annotation == "step":
            annotation_map = CoBot.phase_map_step
        else:
            raise NotImplementedError(" not an available annotation")

        for row in reader:
            if row[0] not in annotation_map:
                print(row[0] + " is not a defined Phase")
                continue
            startframe = int(row[1]) / 1000.0 # annotation in Millisekunden
            if count == 0:
                self.start = int(startframe)
            endframe = int(row[2]) / 1000.0#
            if endframe > (self.end):
                endframe = self.end
            #print("start: " + str(startframe) +" end:" + str(endframe))
            st = int(startframe / sample_rate)
            en = int(endframe / sample_rate)
            #print("start2: " + str(st) +" end2:" + str(en))  
            for t in range(st,en):
                self.targets.append(annotation_map[row[0]])
            count += 1
        #print("Länge Datensatz: " + str(len(self.targets)) + ", Start: " + str(self.start) + ", number pngs: " + str(self.end))
        
        f.close()
        if annotation == "step":
            assert step_file != None
            f = open(step_file, "r")
            reader = csv.reader(f, delimiter='\t')
            next(reader, None)
            for row in reader:
                if row[0] not in CoBot.step_map:
                    print(row[0]+ " is not a defined Step")
                    continue
                startframe = int(row[1]) / 1000.0 # annotation in Millisekunden
                endframe = int(row[2]) / 1000.0
                if endframe > (self.end):
                    endframe = self.end

                st = int(startframe / sample_rate)
                en = int(endframe / sample_rate)

                for t in range(st,en):
                    self.targets[(t-self.start)] = CoBot.step_map[row[0]]
            f.close()
        print(len(self.targets))


    def __getitem__(self, index):
        """Return the frame at the given index along with its phase label.

        :param index: The number of the frame to return.
        :return: The tuple (frame, label).
        """
        #print(index)
        #print("Bild: " + str(index + self.start))
        target = self.targets[index]
        frame = self.frames_path + "/%08d.png" % (self.sample_rate * index + self.start)  # read frames with 1 fps
        #if index > self.start + self.end / DataPrepCoBot.sample_rate -3:
            #print("index: " + str(index) + ", frame: " + frame)
        img = Image.open(frame)#_load_img(frame, self.width, self.height)
        #print(img.shape())
        #exit()

        if self.transform is not None:
            img = self.transform(img)

        return img, target

    def __len__(self):
        """ Return size of dataset. """

        return len(self.targets)

class PhaseData_CoBot_(data.Dataset):
    """Dataset consisting of the sequential frames and their corresponding phase labels of one specific Cholec80 video.

    Frames will be sampled at 1 Hz.
    """

    def __init__(self, frames_path, annotation_path, width, height, transform=None, annotation = "phase", step_file = None, sample_rate = DataPrepCoBot.sample_rate):
        """Create dataset.

        :param frames_path: Path to the directory that contains the video frames.
        :param annotation_path: Path to the text file that contains the phase labels.
        :param width: Width of each frame in the dataset.
        :param height: Height of each frame in the dataset.
        :param transform: Image transformation applied to each frame in the dataset.
        :param sample_rate: 1 or higher .
        """
        self.frames_path = frames_path
        self.width = width
        self.height = height
        self.transform = transform
        self.sample_rate = sample_rate

        self.targets = []
        self.start = 0
        self.end = len(os.listdir(self.frames_path)) # number of pictures in directory
        f = open(annotation_path, "r")
        reader = csv.reader(f, delimiter='\t')
        next(reader, None)
        count = 0
        endframe = 0


        if annotation == "phase":
            annotation_map = CoBot.phase_map
        elif annotation == "step":
            annotation_map = CoBot.phase_map_step
        else:
            raise NotImplementedError(" not an available annotation")

        for row in reader:
            if row[0] not in annotation_map:
                print(row[0] + " is not a defined Phase")
                continue
            startframe = int(row[1]) / 1000.0 # annotation in Millisekunden
            if count == 0:
                self.start = int(startframe)
            endframe = int(row[2]) / 1000.0#
            if endframe > (self.end):
                endframe = self.end
            #print("start: " + str(startframe) +" end:" + str(endframe))
            st = int(startframe / sample_rate)
            en = int(endframe / sample_rate)
            #print("start2: " + str(st) +" end2:" + str(en))  
            for t in range(st,en):
                self.targets.append(annotation_map[row[0]])
            count += 1
        #print("Länge Datensatz: " + str(len(self.targets)) + ", Start: " + str(self.start) + ", number pngs: " + str(self.end))
        
        f.close()
        if annotation == "step":
            assert step_file != None
            f = open(step_file, "r")
            reader = csv.reader(f, delimiter='\t')
            next(reader, None)
            for row in reader:
                if row[0] not in CoBot.step_map:
                    print(row[0]+ " is not a defined Step")
                    continue
                startframe = int(row[1]) / 1000.0 # annotation in Millisekunden
                endframe = int(row[2]) / 1000.0
                if endframe > (self.end):
                    endframe = self.end

                st = int(startframe / sample_rate)
                en = int(endframe / sample_rate)

                for t in range(st,en):
                    self.targets[(t-self.start)] = CoBot.step_map[row[0]]
            f.close()
        print(len(self.targets))

    def __getitem__(self, index):
        """Return the frame at the given index along with its phase label.

        :param index: The number of the frame to return.
        :return: The tuple (frame, label).
        """
        #print(index)
        #print("Bild: " + str(index + self.start))
        target = self.targets[index]
        frame = self.frames_path + "/%08d.png" % (self.sample_rate * index + self.start)  # read frames with 1 fps
        #if index > self.start + self.end / DataPrepCoBot.sample_rate -3:
            #print("index: " + str(index) + ", frame: " + frame)
        img = Image.open(frame)#_load_img(frame, self.width, self.height)
        #print(img.shape())
        #exit()
        img = cv2.imread(frame)
        # By default OpenCV uses BGR color space for color images,
        # so we need to convert the image to RGB color space.
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        if self.transform is not None:
            # Apply transformations
            img = self.transform(image=img)['image']
        return img, target

    def __len__(self):
        """ Return size of dataset. """

        return len(self.targets)

    def get_targets(self):
        return self.targets

class PhaseData(data.Dataset):
    """Dataset consisting of the sequential frames and their corresponding phase labels of one specific Cholec80 video.

    Frames will be sampled at 1 Hz.
    """

    def __init__(self, frames_path, annotation_path, width, height, transform=None, dataset = "Cholec80"):
        """Create dataset.

        :param frames_path: Path to the directory that contains the video frames.
        :param annotation_path: Path to the text file that contains the phase labels.
        :param width: Width of each frame in the dataset.
        :param height: Height of each frame in the dataset.
        :param transform: Image transformation applied to each frame in the dataset.
        """
        self.frames_path = frames_path
        self.width = width
        self.height = height
        self.transform = transform

        self.targets = []
        len_ = len(os.listdir(frames_path))
        if dataset == "Cholec80":
            tar = Cholec80.phase_map
            delimiter = '\t'

        elif dataset == "EndoVis18":
            tar = EndoVis18.phase_map
            delimiter = ','
        else:
            raise NotImplementedError("Not an available datset")
        f = open(annotation_path, "r")
        reader = csv.reader(f, delimiter=delimiter)
        next(reader, None)
        count = 0
        for row in reader:
            if count % 25 == 0:  # read annotations with 1 fps
                self.targets.append(tar[row[1]])
            count += 1
        f.close()
        diff_ = len(self.targets) - len_
        if diff_ > 0:
            print(frames_path + "Too long")
            print(diff_)
            del self.targets[-diff_:]
            #for i in range(diff_):
                #self.targets.pop()

    def __getitem__(self, index):
        """Return the frame at the given index along with its phase label.

        :param index: The number of the frame to return.
        :return: The tuple (frame, label).
        """

        target = self.targets[index]
        frame = self.frames_path + "/%08d.png" % (DataPrep.sample_rate * index)  # read frames with 1 fps
        img = _load_img(frame, self.width, self.height)

        if self.transform is not None:
            img = self.transform(img)

        return img, target

    def __len__(self):
        """ Return size of dataset. """

        return len(self.targets)

class PhaseDataList_CoBot(data.Dataset):
    """Dataset consisting of the frames and their corresponding phase labels of a list of specific Cholec80 video.

    Frames will be sampled at 1 Hz.
    """

    def __init__(self, frames_path_list, annotation_path_list, width, height, transform=None, annotation = "phase",sample_rate = DataPrepCoBot.sample_rate, step_file = None):
        """Create dataset.

        :param frames_path_list: Pathes to the directories that contain the video frames.
        :param annotation_path_list: Pathes to the text file that contains the phase labels.
        :param width: Width of each frame in the dataset.
        :param height: Height of each frame in the dataset.
        :param transform: Image transformation applied to each frame in the dataset.
        """
        self.width = width
        self.height = height
        self.transform = transform
        self.frame_path = []
        self.targets = []

        if annotation == "phase":
            annotation_map = CoBot.phase_map
        elif annotation == "step":
            annotation_map = CoBot.phase_map_step
        else:
            raise NotImplementedError(" not an available annotation")
        i = 0
        for annotation_path in annotation_path_list:
            print(annotation_path)
            f = open(annotation_path, "r")
            reader = csv.reader(f, delimiter='\t')
            next(reader, None)
            count = 0
            j = 0
            end_temp = len(os.listdir(frames_path_list[i]))
            target_length = len(self.targets)
            for row in reader:
                startframe = int(row[1]) / 1000.0 #* CoBot.fps_video / 29.0)
                if count == 0:
                    self.start = int(startframe)
                endframe = int(row[2]) / 1000.0 #* CoBot.fps_video / 29.0)

                if endframe > end_temp:
                    endframe = end_temp
                st = int(startframe / sample_rate)
                en = int(endframe / sample_rate)

                if row[0] not in annotation_map:
                    print(row[0])
                    j = j + (en-st)
                    continue
                for t in range(st, en):
                    self.targets.append(annotation_map[row[0]])
                    #self.frame_path.append(frames_path_list[i]+ "/%08d.png" % (DataPrepCoBot.sample_rate * j + self.start))
                    self.frame_path.append(frames_path_list[i]+ "/%08d.png" % (sample_rate * t))
                    j+=1
                count += 1
            f.close()
            video_length = len(self.targets)-target_length
            print(video_length)

            
            
            if annotation == "step":
                assert step_file[i] != None
                f = open(step_file[i], "r")
                print(step_file[i])
                reader = csv.reader(f, delimiter='\t')
                next(reader, None)
                for row in reader:
                    if row[0] not in CoBot.step_map:
                        print(row[0]+ " is not a defined Step")
                        continue
                    startframe = int(row[1]) / 1000.0 # annotation in Millisekunden
                    endframe = int(row[2]) / 1000.0
                    if endframe > (end_temp):
                        endframe = end_temp

                    st = int(startframe / sample_rate)
                    en = int(endframe / sample_rate)

                    for t in range(st,en):
                        self.targets[(target_length + t-self.start)] = CoBot.step_map[row[0]]
                f.close()
            i += 1


    def __getitem__(self, index):
        """Return the frame at the given index along with its phase label.

        :param index: The number of the frame to return.
        :return: The tuple (frame, label).
        """

        target = self.targets[index]
        frame = self.frame_path[index]
        img = Image.open(frame)#_load_img(frame, self.width, self.height)

        if self.transform is not None:
            img = self.transform(img)

        return img, target
    def __len__(self):
        """ Return size of dataset. """

        return len(self.targets)

class PhaseDataList(data.Dataset):
    """Dataset consisting of the frames and their corresponding phase labels of a list of specific Cholec80 video.

    Frames will be sampled at 1 Hz.
    """

    def __init__(self, frames_path_list, annotation_path_list, width, height, transform=None,dataset = "Cholec80"):
        """Create dataset.

        :param frames_path_list: Pathes to the directories that contain the video frames.
        :param annotation_path_list: Pathes to the text file that contains the phase labels.
        :param width: Width of each frame in the dataset.
        :param height: Height of each frame in the dataset.
        :param transform: Image transformation applied to each frame in the dataset.
        """
        self.width = width
        self.height = height
        self.transform = transform
        self.frame_path = []
        self.targets = []
        i = 0
        if dataset == "Cholec80":
            tar = Cholec80.phase_map
            delimiter = '\t'

        elif dataset == "EndoVis18":
            tar = EndoVis18.phase_map
            delimiter = ','
        else:
            raise NotImplementedError("Not an available datset")

        for annotation_path in annotation_path_list:
            f = open(annotation_path, "r")
            reader = csv.reader(f, delimiter=delimiter)
            next(reader, None)
            count = 0
            j = 0
            for row in reader:
                if count % 25 == 0:  # read annotations with 1 fps
                    self.targets.append(tar[row[1]])
                    self.frame_path.append(frames_path_list[i]+ "/%08d.png" % (DataPrep.sample_rate * j))
                    j += 1
                count += 1
            f.close()
            i += 1


    def __getitem__(self, index):
        """Return the frame at the given index along with its phase label.

        :param index: The number of the frame to return.
        :return: The tuple (frame, label).
        """

        target = self.targets[index]
        frame = self.frame_path[index]
        img = _load_img(frame, self.width, self.height)

        if self.transform is not None:
            img = self.transform(img)

        return img, target
    def __len__(self):
        """ Return size of dataset. """

        return len(self.targets)

def _calc_ssim(img1, img2):
    img1_gray = img1.convert('L')
    img2_gray = img2.convert('L')
    return ssim(np.asarray(img1_gray), np.asarray(img2_gray))

class TupleData(data.Dataset):
    """ Dataset consisting of randomly sampled video frame tuples from one specific Cholec80 video."""

    def __init__(self, frames_path, sample_count=250, width=224, height=224, transform=None, delta=30.0, gamma=120.0, plus_2nd_order_TC=False):
        """Create dataset.

        :param frames_path: Path to the directory that contains the video frames.
        :param sample_count: Number of tuples in the dataset.
        :param width: Width of each frame (in each tuple) in the dataset.
        :param height: Height of each frame (in each tuple) in the dataset.
        :param transform: Image transformation applied to each frame (in each tuple) in the dataset.
        :param delta: Parameter to determine the size of the window from which a close frame is sampled.
                      Close frames are at most "delta" seconds away from a reference frame.
        :param gamma: Parameter to determine from where a distant frame is sampled.
                      Distant frames are at least "gamma" seconds away from a reference frame.
        :param plus_2nd_order_TC: Whether or not a tuple contains two close frames as required for training with
                                  2nd order temporal coherency.
        """
        self.frames_path = frames_path
        self.sample_count = sample_count
        self.delta = int(delta // (1/1))#DataPrep.sample_rate))  # convert to frame-level distance
        self.gamma = int(gamma // (1/1))#DataPrep.sample_rate))  # convert to frame-level distance
        self.plus_2nd_order_TC = plus_2nd_order_TC
        self.transform = transform
        self.width = width
        self.height = height

        self.frames = []

        #sampel_count dependent on video length
        for file in list(np.unique(os.listdir(frames_path))):
            if file.endswith('.png'):
                frame_path = os.path.join(frames_path, file)
                self.frames.append(frame_path)
        #print(len(self.frames))

    def __load_img(self, path):
        return _load_img(path, self.width, self.height)

    def __getitem__(self, index):
        """Return next tuple.

        Each tuple consists of a reference frame, one or two frames that are close to the reference, and a frame that is
        far from the reference.
        :param index: Will be ignored since every tuple is sampled randomly.
        :return: The tuple (reference frame, close frame, far frame) if "plus_2nd_order_TC" has been set to False,
                 otherwise (reference frame, close frame_1, close_frame_2, far frame)
        """
        # sample reference image
        seed = rand.randint(0, len(self.frames) - 1)
        #print("seed: "+ str(seed))
        img = self.__load_img(self.frames[seed])

        # sample close image(s)
        idx_c = rand.choice(list(i for i in list(range(max(0, seed - self.delta),
                                                       min(seed + self.delta + 1, len(self.frames)))) if i != seed))
        img_c2 = None
        if self.plus_2nd_order_TC:
            my_delta = idx_c - seed
            # handle corner case (idx_c2 out of range)
            if (my_delta < 0 and (seed + 2 * my_delta) < 0) or (my_delta > 0 and (seed + 2 * my_delta >= len(self.frames))):
                my_delta = my_delta * -1
                idx_c = seed + my_delta
            idx_c2 = idx_c + my_delta
            img_c2 = self.__load_img(self.frames[idx_c2])
        img_c = self.__load_img(self.frames[idx_c])

        # sample far image
        img_f = None
        success = False
        while not success:
            idx_f = rand.choice(list(range(0, max(0, seed - self.gamma + 1)))
                                  + list(range(min(seed + self.gamma, len(self.frames)), len(self.frames))))
            img_f = self.__load_img(self.frames[idx_f])
            s = _calc_ssim(img, img_f)
            if s > 0.7:
                img_f = None    # discard far images that are too similar to the seed image
            else:
                success = True

        result = []
        if self.plus_2nd_order_TC:
            result = [img, img_c, img_c2, img_f]
        else:
            result = [img, img_c, img_f]

        if self.transform is not None:
            result = map(self.transform, result)

        return tuple(result)

    def __len__(self):
        """ Return size of dataset. """

        return self.sample_count
