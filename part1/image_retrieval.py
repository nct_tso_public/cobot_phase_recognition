"""Image retrieval experiment.

Usage: image_retrieval.py <experiment> [-f <ref_frame1> ... <ref_frameN>] [-o <ref_op>] [-s <ref_op_set>] [-d <results_dir>]
    experiment - Experiment to evaluate, specified as <variant>/<trial_id>.
    ref_frame - Identifier of a frame (8-digit integer), which corresponds to the file name under which the frame is
                saved (e.g. 00000290). The specified frame will be treated as query frame, for which the most similar
                frames are to be retrieved from the test OP videos. The identifier should be divisible by 5.
                A list of several query frames can be specified.
    ref_op - Identifier of the OP video from which the query frames are taken (e.g. 01, 71).
    ref_op_set := A | B | C | D
               - OP set to which the OP video from which the query frames are taken belongs.
    results_dir - Directory where results are to be stored. Default is out_path/Image_retrieval, where out_path will
                  be read from utils.py.

For each query frame <ref_frame>, the script will retrieve the most similar frame <frame*> from each
test OP video <test_op>. It will hold that the Euclidean distance between the embeddings of <ref_frame> and <frame*> is
minimal with regard to all other frames in <test_op>.
The embedding is calculated using the model saved at out_path/Self-supervised/<variant>/<trial_id>/model.pth.
Embeddings are calculated on demand for all frames of a OP video <op> and cached at
out_path/Self-supervised/<variant>/<trial_id>/embeddings/<op_set>/<op>.npz.
The query result <frame*> will be saved at results_dir/<ref_op>_<ref_frame>/<dist>_<test_op>_<frame*>.png, where <dist>
denotes the Euclidean distance between the embeddings of <ref_frame> and <frame*>.
"""

import os.path
import numpy as np
import torch
import torch.nn
from PIL import Image
import argparse

from datasets import PhaseData
from models import FeatureNet

import utils
from utils import Cholec80, DataPrep


def _calc_embeddings(model, op_set, op):
    device_gpu = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    device_cpu = torch.device("cpu")

    batch_size = 128
    anno_file = os.path.join(utils.annotation_path, "video" + op + "-phase.txt")
    dataset = PhaseData(os.path.join(utils.frames_path, op_set, op), anno_file, DataPrep.width, DataPrep.height,
                        DataPrep.standard_transform)
    dataloader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=False, num_workers=4)

    frame_count = len(dataset)
    embeddings = torch.zeros(frame_count, FeatureNet.num_features, device=device_gpu)

    net = FeatureNet()
    net.load(model)
    net.to(device_gpu)

    print("Calculating embeddings for op", op)
    net.eval()
    with torch.no_grad():
        for i, data in enumerate(dataloader, 0):
            img, label = data
            output = net(img.to(device_gpu))
            size = output.shape[0]
            embeddings[i * batch_size: i * batch_size + size, :].copy_(output)
    embeddings = embeddings.to(device_cpu).numpy()

    if not (os.path.exists(os.path.join(embeddings_path, op_set))):
        os.makedirs(os.path.join(embeddings_path, op_set))
    embeddings_file = os.path.join(embeddings_path, op_set, op + ".npz")
    print("Saving to file %s ..." % embeddings_file)
    np.savez(embeddings_file, embeddings=embeddings)


def _load_embeddings(op_set, op):
    embedding_file = os.path.join(embeddings_path, op_set, op + ".npz")
    if not os.path.exists(embedding_file):
        model_file = os.path.join(utils.out_path, "Self-supervised", args.experiment, "model.pth")
        _calc_embeddings(model_file, op_set, op)
    embeddings = np.load(embedding_file)['embeddings']
    return embeddings  # frame_count x 4096 numpy array


def _get_embedding(op_set, op, frame_idx):
    embeddings = _load_embeddings(op_set, op)
    idx = int(frame_idx) // DataPrep.sample_rate  # frames come from 5 fps sampling, embeddings have been calculated for 1 fps
    return embeddings[idx, :]


def _calc_distance(e1, e2):
    D = torch.nn.PairwiseDistance(p=2)
    dist = D(torch.from_numpy(e1).view(1, -1), torch.from_numpy(e2).view(1, -1))
    return dist.item()


def _get_most_similar_frame(ref_op_set, ref_op, ref_frame_idx, cmp_op_set, cmp_op):
    ref_embedding = _get_embedding(ref_op_set, ref_op, ref_frame_idx)
    cmp_embeddings = _load_embeddings(cmp_op_set, cmp_op)

    # calculate similarity ranking
    dists = []
    for i in range(cmp_embeddings.shape[0]):
        dists.append((_calc_distance(ref_embedding, cmp_embeddings[i, :]), i))
    dists = sorted(dists)

    return dists[0][0], ("%08d" % (dists[0][1] * DataPrep.sample_rate))  # distance, frame_idx


def main(args):
    if not os.path.exists(os.path.join(utils.out_path, "Self-supervised", args.experiment)):
        print("Unable to find experiment at %s." % os.path.join(utils.out_path, "Self-supervised", args.experiment))
        exit()
    results_path = os.path.join(utils.out_path, "Image_retrieval")
    if (args.results_dir is not None) and os.path.exists(args.results_dir):
        results_path = args.results_dir
    print("Saving query results to %s." % results_path)

    for ref_frame in args.ref_frames:
        res_dir = os.path.join(results_path, args.ref_op + "_" + ref_frame)
        if not os.path.exists(res_dir):
            os.makedirs(res_dir)
        query = Image.open(os.path.join(utils.frames_path, args.ref_op_set, args.ref_op, ref_frame + ".png"))
        query.save(os.path.join(res_dir, "query.png"))
        for op in os.listdir(os.path.join(utils.frames_path, Cholec80.test_sets[0])):  # iterate over all OPs in test set
            dist, frame_idx = _get_most_similar_frame(args.ref_op_set, args.ref_op, ref_frame, Cholec80.test_sets[0], op)
            result = Image.open(os.path.join(utils.frames_path, Cholec80.test_sets[0], op, frame_idx + ".png"))
            result.save(os.path.join(res_dir, ("%2.2f" % dist) + "_" + op + "_" + frame_idx + ".png"))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Image retrieval experiment.")
    parser.add_argument("experiment", type=str, help="Experiment to evaluate, specified as <variant>/<trial_id>.")
    parser.add_argument("-f", "--ref_frames", metavar='ref_frame', type=str, nargs='+',
                        default=["00000290", "00002520", "00007445", "00007625"], help="Query frames.")
    parser.add_argument("-o", "--ref_op", type=str, default="01", help="OP video to which the query frames belong.")
    parser.add_argument("-s", "--ref_op_set", type=str, choices=['A', 'B', 'C', 'D'], default='B',
                        help="OP video set to which the OP video specified by option -o belongs.")
    parser.add_argument("-d", "--results_dir", type=str, help="Directory where results are to be stored.")
    args = parser.parse_args()

    embeddings_path = os.path.join(utils.out_path, "Self-supervised", args.experiment, "embeddings")
    main(args)

