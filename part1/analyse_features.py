
import torch
import torch.utils.data as data
import os
import numpy as np
import utils
FEATURES ="/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/4/features.pth.tar"# "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/1/features.pth.tar"
num_class = 9

f_log = open("Analysis_video.txt", "a")
def log(msg):
    utils.log(f_log, msg)

data = torch.load(FEATURES)
video_ids = sorted(data["train"].keys())
label_seqs = {}
label_seqs_train = {}
label_seqs_val = {}
label_seqs_test = {}
for video_id in video_ids:
	label_seq = data['labels'][video_id]
	label_seqs[video_id] = label_seq
	label_seqs_train[video_id] = label_seq

video_ids = sorted(data["val"].keys())
for video_id in video_ids:
	label_seq = data['labels'][video_id]
	label_seqs[video_id] = label_seq
	label_seqs_val[video_id] = label_seq


video_ids = sorted(data["test"].keys())

for video_id in video_ids:
	label_seq = data['labels'][video_id]
	label_seqs[video_id] = label_seq
	label_seqs_test[video_id] = label_seq


counts = np.zeros(num_class, dtype=np.float)
counts_ = np.zeros(num_class, dtype=np.float)
phase_oc = np.zeros(num_class, dtype=np.float)
phase_oc_ = np.zeros(num_class, dtype=np.float)
for key, label_seq in label_seqs.items():
	counts = np.zeros(num_class, dtype=np.float)
	phase_oc = np.zeros(num_class, dtype=np.float)
	label_seq = label_seq.numpy()
	
	for i in range(label_seq.shape[0]):
		label = label_seq[i]
		assert(label < num_class)
		counts[label] += 1
		phase_oc[label] = 1

	log(key + " " + str(counts) + " " + str(phase_oc))
	counts_ += counts
	phase_oc_ += phase_oc
log("Num videos with phase "+ str(phase_oc_))
log("phase counts " + str(counts_))
log("average out of num videos " + str(counts_/phase_oc_))
f_log.close()


