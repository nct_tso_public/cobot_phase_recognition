import argparse
import datetime
import os.path
import sys

import torch
import torch.nn as nn
import torch.optim as optim
import torchvision
from torch.optim import lr_scheduler

import utils
from datasets_sequence import PhaseData_sequence2, PhaseData_sequence_list2
from datasets import PhaseData
from models_transformer import PhaseTransformerNet_FeatureVector
from models import CNNmodel
from utils import (AverageMeter, Cholec80, CoBot, DataPrep3, DataPrep_tr,
                   DataPrep)


def main(args):

    transform_train = DataPrep3.standard_transform
    transform_test = DataPrep3.standard_transform

    device_gpu = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    device_cpu = torch.device("cpu")



    print("Loading data...")
    if args.dataset == "CoBot":
        print("CoBot...")
        exit()
        

    elif args.dataset == "Cholec80":
        print("Cholec80...")
        num_class = 7
        test_sets = Cholec80.test_sets
        

        test_data = []
        val_data =[]
        for op_set in test_sets:
            for op in os.listdir(os.path.join(utils.frames_path_Cholec80, op_set)):
                op_path = os.path.join(utils.frames_path_Cholec80, op_set, op)
                if os.path.isdir(op_path):
                    anno_file = os.path.join(utils.annotation_path_Cholec80, "video" + op + "-phase.txt")
                    #dataset = PhaseData_sequence2(op_path, anno_file,args.sequence_length, args.dataset, DataPrep.standard_transform, sample_rate = args.sample_rate, sample_rate_s = args.sample_rate_s)
                    dataset = PhaseData(op_path, anno_file, DataPrep.width, DataPrep.height, DataPrep.standard_transform)
                    if len(val_data) == 8:
                        test_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False, num_workers=4, pin_memory=True))
                    else:
                        val_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False, num_workers=4, pin_memory=True))

                    
        print("\t test data")
        for loader in test_data:
            print("\t\t" + loader.dataset.frames_path)
    
    #net = PhaseTransformerNet_FeatureVector(num_labels = num_class, featurevector_dim = 2048, max_len = args.sequence_length) 
    net = CNNmodel(num_class = num_class, cnn = "resnet50")
    #79.424 %
    file ="/mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/cholec80/out/Phase_segmentation_resnet50/no_pretrain/40/20210203-1215/model.pkl"
    here = os.path.dirname(os.path.abspath(file))
    sys.path.append(here)
    net.load(file)
    #resnet = CNNmodel(num_class = 7, cnn = "resnet50")
    #resnet.load("/home/krellstef/master_code/outCholec80/Phase_segmentation_resnet50/no_pretrain/40/20201101-1723/model50.pkl")
    net = net.to(device_gpu)

    torch.backends.cudnn.benchmark = True
    torch.backends.cudnn.enabled = True

    predictions_path = os.path.join("/mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/cholec80/out/Phase_segmentation_resnet50/no_pretrain/40/20210203-1215", "predictions_bestval_40epoch")
    if not os.path.exists(predictions_path):
        os.makedirs(predictions_path)
    with torch.no_grad():
        net.eval()
            #print("testing...")
        test_count = 0
        test_accuracy = 0
        for op_data in test_data:
            frames_path = op_data.dataset.frames_path
            op_id = frames_path.split('/')[-1] if frames_path.split('/')[-1] else frames_path.split('/')[-2]
            f_out = open(os.path.join(predictions_path, op_id + ".txt"), "w")
            for loader in op_data:
                images, labels_cpu = loader
                images = images.to(device_gpu)
                labels = labels_cpu.to(device_gpu)
                outputs = net(images)
                _, predicted = torch.max(outputs.data, 1)
                predicted_cpu = predicted.to(device_cpu)
                for p, l in zip(predicted_cpu.numpy(), labels_cpu.numpy()):
                    f_out.write(str(p) + "," + str(l) + "\n")
                test_count += labels.size(0)
                test_accuracy += (predicted == labels).sum().item()
            f_out.close()

        summary = "test ( accuracy %.3f)" % \
                  (test_accuracy/test_count)
        print("\t" + summary)



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="test Cholec80")
    #parser.add_argument("num_ops", type=int,
    #                    help="Number of labeled OP videos to use for training. up to 13 for CoBot and 20 40 or 60 for Cholec80")
    parser.add_argument("--lr", type=float, default=0.00001, help="The learning rate.")
    parser.add_argument("--batch_size", type=int, default=32, help="The batch size.") #128
    #parser.add_argument("--epochs", type=int, default=60, help="The maximal number of epochs to train.")
    #parser.add_argument("--save_freq", type=int, default=5,
    #                    help="Defines after how many epochs the current model parameters are saved.")
    parser.add_argument("--dataset", type=str, default="Cholec80", choices=["CoBot","Cholec80"],
                        help="Choice of dataset")
    parser.add_argument("--sequence_length", type=int, default=5, help="sequence length")
    parser.add_argument("--vers", type=int, default=2, help="version 1 or 2")
    parser.add_argument("--opt_step", type=int, default=5,
                        help="Number of batches to accumulate before applying the optimizer.")
    parser.add_argument("--sample_rate", type=int, default=1, help="sample_rate")
    parser.add_argument("--sample_rate_s", type=int, default=1, help="sample_rate_s")
    parser.add_argument("--annotation", type=str, default="phase" , choices=["phase","step"],
                        help="phases or steps annotation")
  
    args = parser.parse_args()
    main(args)
