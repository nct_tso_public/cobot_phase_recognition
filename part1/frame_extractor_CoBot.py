"""
Script to extract frames of the CoBot Dataset 
"""

import os

import cv2
import imageio

import utils
import torchvision.transforms as transforms
import torchvision.transforms.functional as TF
from PIL import Image

myTransformation= transforms.Compose(
        [utils.MyCrop(22, 218, 675, 845), 
        transforms.Resize(256)])

def extractEndoVis18():
    PATH = "/mnt/g27prist/TCO/TCO-Studenten/Datasets/EndoVis18-Workflow"
    PATH2 = "/mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/EndoVis18"

    for op in os.listdir(os.path.join(PATH,"Train/Video")):
        v = os.path.join(PATH,"Train/Video", op)
        f = os.path.join(PATH2,"Train/Frames", os.path.splitext(op)[0])
        computeVideoImages(v,f)

    for op in os.listdir(os.path.join(PATH,"Test/Video")):
        v = os.path.join(PATH,"Test/Video", op)
        f = os.path.join(PATH2,"Test/Frames", os.path.splitext(op)[0])
        computeVideoImages(v,f)


def computeVideoImages2(videoPath, outputPath):
    os.makedirs(outputPath, exist_ok=True)
    videoPath_new = videoPath
    os.system(
            'ffmpeg -i '+ videoPath_new + ' -vf "fps=1" ' + '-start_number 0 ' + outputPath +'/%08d.png'
        )
def computeVideoImages(videoPath, outputPath):
    os.makedirs(outputPath, exist_ok=True)
    vid = imageio.get_reader(videoPath)  # Read video
    fps = vid.get_meta_data()['fps']
    print(fps)
    #startFrame = 0
    #endFrame = vid.get_length()
    sample_rate = int(fps)//1
    count = 0
    target_height = 256
    for i, im in enumerate(vid):
    #for frame in range(startFrame, endFrame, sample_rate):
        if i % sample_rate != 0:
            continue
        try:
            image = im#vid.get_data(frame)
        except imageio.core.format.CannotReadFrameError:
            print("Couldn't read frame %d of" % (i))#print("Couldn't read frame %d of %d" % (frame, endFrame))
            break

        (h, w) = image.shape[:2]
        r = target_height / float(h)
        image = cv2.resize(image, (int(w * r), target_height))  # resize
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        name = str(count).zfill(8) + ".png"
        cv2.imwrite(os.path.join(outputPath, name), image) 
        count += 1

def computeVideoImages_CoBot(videoPath, outputPath):
    if os.path.isdir(outputPath) and os.listdir(outputPath):
        print(outputPath + " already extracted")
        return
    #return
    os.makedirs(outputPath, exist_ok=True)
    outputPath_ = outputPath.replace(" ", "\ ")
    os.system(
            'ffmpeg -i '+ videoPath + ' -vf "scale=1280:720,fps=1" ' + '-start_number 0 ' + outputPath_ +'/%08d.png'
        )
    #os.system('ffmpeg -hide_banner -i ' + videoPath + ' -r 1 ' + '-start_number 0 ./' + outputPath + '/%08d.png')
    for j in os.listdir(outputPath):
        img = Image.open(os.path.join(outputPath,j))
        img = myTransformation(img)
        img.save(os.path.join(outputPath,j))

def computeVideoImages_CoBot_2(videoPath, outputPath, outputPath2):
    os.makedirs(outputPath, exist_ok=True)
    outputPath_ = outputPath.replace(" ", "\ ")
    os.system(
            'ffmpeg -i '+ videoPath + ' -vf "scale=1280:720,fps=1" ' + '-start_number 0 ' + outputPath_ +'/%08d.png'
        )
    #os.system('ffmpeg -hide_banner -i ' + videoPath + ' -r 1 ' + '-start_number 0 ./' + outputPath + '/%08d.png')
    os.makedirs(outputPath2, exist_ok=True)
    for j in os.listdir(outputPath):
        img = Image.open(os.path.join(outputPath,j))
        img = myTransformation(img)
        img.save(os.path.join(outputPath2, j))

def extract_unlabelled():
    list_labelled = ['20190917','20191001','20191011','20191211','20200122','20200129','20200221','20200317',
            '20200330','20200407','20200416','20200423','20200427','20200504','20200622','20201017',
            '20200710','20200717','20200727','20200731','20201002','20201006', '20201201','20201204', '20200827', '20201208']
    videos_path = os.path.join("/mnt/g27prist/TCO/TCO-Studenten/CoBot/OPs", "videos only") #"/mnt/g27prist/TCO/TCO-Studenten/CoBot/OPs/videos\ only"
    frames_path = "/mnt/g27prist/TCO/TCO-Studenten/CoBot/frames_unlabelled"
    for op in os.listdir(videos_path):
        if any(substring in op for substring in list_labelled):
            print(op + "is labelled, next...")
            continue
        v = os.path.join(videos_path, op)
        #print(v)
        v = v.replace(" ", "\ ")
        f = os.path.join(frames_path,os.path.splitext(op)[0])
        computeVideoImages_CoBot(v,f)
def extract_labelled():
    videos_path = "/mnt/g27prist/TCO/TCO-Studenten/CoBot/OPs"
    frames_path = "/mnt/g27prist/TCO/TCO-Studenten/CoBot/frames"
    frames_path2 = "/mnt/g27prist/TCO/TCO-Studenten/CoBot/frames_smaller"
    video_list = ["20190214","20191206","20200123","20200212","20200302","20210113"] #,"20200611""20190906",

    for i in video_list:
        print(i)
        for v_ in os.listdir(os.path.join(videos_path, i)):
            if v_.endswith((".MP4", ".mp4")):
                v = os.path.join(videos_path, i, v_)
                v = v.replace(" ", "\ ")
                f = os.path.join(frames_path,i)
                f2 = os.path.join(frames_path2,i)
                computeVideoImages_CoBot_2(v,f,f2)

def computeVideoImages_CoBot__(videoPath, outputPath, outputPath2):
    os.makedirs(outputPath, exist_ok=True)
    outputPath_ = outputPath.replace(" ", "\ ")
    os.system(
            'ffmpeg -i '+ videoPath + ' -vf "scale=1280:720,fps=1" ' + '-start_number 0 ' + outputPath_ +'/%08d.png'
        )

def extract_labelled_():
    videos_path = "/mnt/g27prist/TCO/TCO-Studenten/CoBot/OPs"
    frames_path = "/mnt/g27prist/TCO/TCO-Studenten/CoBot/frames"
    frames_path2 = "/mnt/g27prist/TCO/TCO-Studenten/CoBot/frames_smaller"
    video_list = ["20190906"] #,"20200611""20190906",

    for i in video_list:
        print(i)
        for v_ in os.listdir(os.path.join(videos_path, i)):
            if v_.endswith((".MP4", ".mp4")):
                v = os.path.join(videos_path, i, v_)
                v = v.replace(" ", "\ ")
                f = os.path.join(frames_path,i)
                f2 = os.path.join(frames_path2,i)
                computeVideoImages_CoBot__(v,f,f2)
def exception_v():
    s_ = os.path.join("/mnt/g27prist/TCO/TCO-Studenten/CoBot/OPs", "videos only","20201208_Anterior Rectal Resection.mp4")#"/mnt/g27prist/TCO/TCO-Studenten/CoBot/OPs/20200611/20200611_Anterior\ Rectal\ Resection.MP4"
    s = s_.replace(" ", "\ ")
    f1 = "/mnt/g27prist/TCO/TCO-Studenten/CoBot/frames/20201208"
    f2 = "/mnt/g27prist/TCO/TCO-Studenten/CoBot/frames_smaller/20201208"
    os.makedirs(f1, exist_ok=True)
    os.system(
            'ffmpeg -i '+ s + ' -vf "scale=1280:720,fps=1" ' + '-start_number 0 ' + f1 +'/%08d.png'
        )

    os.makedirs(f2, exist_ok=True)
    for j in os.listdir(f1):
        img = Image.open(os.path.join(f1,j))
        img = myTransformation(img)
        img.save(os.path.join(f2, j))

if __name__ == "__main__":
    #extractEndoVis18()
    #extract_unlabelled()

    #exception_v()


    s = "20200316_Anterior Rectal Resection.mp4"
    v = os.path.join("/mnt/g27prist/TCO/TCO-Studenten/CoBot/OPs", "videos only", s) #"/mnt/g27prist/TCO/TCO-Studenten/CoBot/OPs/videos\ only"
    v = v.replace(" ", "\ ")
    f = "/mnt/g27prist/TCO/TCO-Studenten/CoBot/frames_unlabelled/20200316_Anterior\ Rectal\ Resection"
    f1 = "/mnt/g27prist/TCO/TCO-Studenten/CoBot/frames_unlabelled/20200316_Anterior Rectal Resection"
    os.makedirs(f1, exist_ok=True)
    os.system(
            'ffmpeg -i '+ v + ' -vf "scale=320:256,fps=1" ' + '-start_number 0 ' + f +'/%08d.png'
        )
    
    #extract_labelled_()
    exit()
    '''videos_path = "/mnt/g27prist/TCO/TCO-Studenten/CoBot/OPs"
    frames_path = "/mnt/g27prist/TCO/TCO-Studenten/CoBot/frames"
    for i in range(len(utils.CoBot.videos)):
        print(utils.CoBot.videos[i])
        videoPath = os.path.join(videos_path, utils.CoBot.videos[i], utils.CoBot.videos[i] + "_Anterior\ Rectal\ Resection_MC_retrosp.mp4")
        if i == 6:
            videoPath = os.path.join(videos_path, utils.CoBot.videos[i], utils.CoBot.videos[i] + "_Anterior\ Rectal\ Resection_FK_retrosp.mp4")
        if i == 5:
            videoPath = os.path.join(videos_path, utils.CoBot.videos[i], utils.CoBot.videos[i] + "_Anterior\ Rectal\ resection_MC_retrosp.mp4")
        if i == 7:
            videoPath = os.path.join(videos_path, utils.CoBot.videos[i], utils.CoBot.videos[i] + "_Anterior\ Rectal\ Resection_FK_live.mp4")
        if i == 9:
            videoPath = os.path.join(videos_path, utils.CoBot.videos[i], utils.CoBot.videos[i] + "_Rectal\ Resection_MC_live_gekürzt.mp4")
        if i == 11 or i == 12:
            videoPath = os.path.join(videos_path, utils.CoBot.videos[i], utils.CoBot.videos[i] + "__Anterior\ Rectal\ Resection_MC_live.mp4")
        if i == 13 or i == 10:
            videoPath = os.path.join(videos_path, utils.CoBot.videos[i], utils.CoBot.videos[i] + "_Anterior\ Rectal\ Resection_MC_live.mp4")
        if i == 14:
            videoPath = os.path.join(videos_path, utils.CoBot.videos[i], utils.CoBot.videos[i] + "_Anterior\ Rectal\ Resection_MC_live.MP4")
        if i == 15:
            videoPath = os.path.join(videos_path, utils.CoBot.videos[i], "20191017_Anterior\ Rectal\ Resection_MC_retrosp.mp4")

        outputPath = os.path.join(frames_path, utils.CoBot.videos[i])
        #try:
        computeVideoImages2(videoPath, outputPath)'''
    videos_path = "/mnt/g27prist/TCO/TCO-Studenten/CoBot/OPs"
    frames_path = "/mnt/g27prist/TCO/TCO-Studenten/CoBot/frames"
    for i in [0,1,2]:
        if i == 0:
            v = "20200827"
            videoPath = os.path.join(videos_path,v, "20200827_Anterior\ Rectal\ Resection.mp4")
        if i == 1:
            v = "20201201"
            videoPath = os.path.join(videos_path,v, "20201201_Anterior\ rectal\ resection.mp4")
        if i == 2:
            v = "20201204"
            videoPath = os.path.join(videos_path,v, "20201204_Anterior\ Rectal\ Resection.mp4")
            

        '''if i == 0:
            videoPath = os.path.join(videos_path, "20201002_Rectal\ Extirpation_MC_live.mp4")
            v = "20201002"

        if i == 1:
            videoPath = os.path.join(videos_path, "20200710\ Anterior\ Rectal\ Resection_MC_live.mp4")
            v = "20200710"
        if i == 2:
            videoPath = os.path.join(videos_path, "20200717\ Anterior\ Rectal\ Resection_MC_live.mp4")
            v = "20200717"
        if i == 3:
            videoPath = os.path.join(videos_path, "20200727\ Anterior\ Rectal\ Resection_MC_live.mp4")
            v = "20200727"
        if i == 4:
            videoPath = os.path.join(videos_path, "20200731_Anterior\ Rectal\ Resection_MC_live.mp4")
            v = "20200731"
        if i == 5:
            videoPath = os.path.join(videos_path, "20200731_Anterior\ Rectal\ Resection_MC_live.mp4")
            v = "20200731"
        if i == 6:
            videoPath = os.path.join(videos_path, "20201006_Rectal\ Extirpation_MC_live.mp4")
            v = "20201006"'''

        outputPath = os.path.join(frames_path, v)
        #try:
        computeVideoImages_CoBot(videoPath, outputPath)
       # except:
           # print(i)
           # print("no video done")
