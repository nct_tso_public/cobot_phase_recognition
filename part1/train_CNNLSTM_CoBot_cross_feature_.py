"""
train LSTM with CoBot dataset 
"""

import argparse
import datetime
import os.path

import torch
import torch.nn as nn
import torch.optim as optim
import torchvision

import utils
from datasets import MyDataSet_
from models import LSTMNet_
from utils import CoBot, DataPrep3
import numpy as np

def main(args):

    device_gpu = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    device_cpu = torch.device("cpu")

    trial_id = datetime.datetime.now().strftime("%Y%m%d-%H%M")
    if args.exp is not None:
        trial_id = args.exp
    experiment = os.path.join("LSTM_new", trial_id, str(args.fold))

    output_folder = os.path.join(utils.out_path, experiment)

    anno_path = utils.annotation_path
    if args.annotation == "phase":
        num_class = 5        
    elif args.annotation == "step":
        num_class = 5 + 4
        output_folder = os.path.join("/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps", experiment)
        step_file = []
    else:
        raise NotImplementedError("not available as annotation")

    #output_folder = os.path.join("C:/Users/steff/Work", experiment)

    os.makedirs(output_folder)
    f_log = open(os.path.join(output_folder, "log.txt"), "w")
    def log(msg):
        utils.log(f_log, msg)
    model_file = os.path.join(output_folder, "model.pkl")
    model_file_final = os.path.join(output_folder, "model_final.pkl")
    model_file_train = os.path.join(output_folder, "model_best_train.pkl")

    log("Run <%s> on device %s" % (experiment, str(device_gpu)))

    log("Used parameters...")
    for arg in vars(args):
        log("\t" + str(arg) + " : " + str(getattr(args, arg)))

    log("Loading data...")
    
    feature_file = torch.load(args.feature_file)
    print(feature_file['split'])
    assert(args.fold == feature_file['split'])

    train_data = []
    log("\t training data")
    train_set = MyDataSet_(feature_file, mode ="train",
                          noise_factor=args.noise_factor, added_noise=args.added_noise,
                          max_duplicate_frames_prob=args.duplicate_frames_prob, duplicated_duration=args.duplicate_duration,
                          max_drop_frames_prob=args.drop_frames_prob, dropped_duration=args.drop_duration,
                          max_randomly_replace_prob=args.randomly_replace_prob, randomly_replaced_duration=args.replace_duration,
                          max_mask_prob=args.mask_prob, masked_duration=args.mask_duration)
    train_data = torch.utils.data.DataLoader(train_set, batch_size=1, shuffle=False,
                                               num_workers=4)
    train_videos = train_set.video_ids
    log("Found {} train videos: {}".format(len(train_videos), train_videos))

    val_data = []
    video_ids = sorted(feature_file['val'].keys())
    log("\t val data")
    dataset = MyDataSet_(mode = 'val', feature_file = feature_file)
    val_data=torch.utils.data.DataLoader(dataset, batch_size=1, shuffle=False,
                                                             num_workers=4)
    val_videos = dataset.video_ids
    log("Found {} val videos: {}".format(len(val_videos), val_videos))
    
    test_data = []
    video_ids = sorted(feature_file['test'].keys())
    log("\t test data")
    dataset = MyDataSet_(mode = 'test', feature_file = feature_file)
    test_videos = dataset.video_ids
    log("Found {} test videos: {}".format(len(test_videos), test_videos))
    test_data=torch.utils.data.DataLoader(dataset, batch_size=1, shuffle=False,
                                                             num_workers=4)
                
    net = LSTMNet_(num_classes = num_class, hidden_size= args.hidden_size,num_layers = args.num_layers, bidirectional= args.bidirectional)
    net.to(device_gpu)

    if args.w == 1:
        weight = train_set.calculate_class_weights(num_class)
        weight = weight.to(device_gpu)
    else:
        weight = None

    criterion = nn.CrossEntropyLoss(weight=weight)
    optimizer = optim.Adam(filter(lambda p: p.requires_grad, net.parameters()), lr=args.lr)

    torch.manual_seed(42)
    torch.backends.cudnn.benchmark = True
    torch.backends.cudnn.enabled = True
    log("Begin training...")

    best_acc = 0
    one = True
    for epoch in range(args.epochs):
        log("Epoch " + str(epoch + 1) + "...")

        train_loss = 0
        train_accuracy = 0
        train_count = 0
        for loader in train_data:

            optimizer.zero_grad()

            loss = 0
            batch_count = 0
            images, labels,video_id = loader
            images = images.to(device_gpu)
            labels = labels.to(device_gpu)
            outputs= net(images)
            outputs= outputs[0]
            labels = labels[0]
            loss += criterion(outputs, labels)
            batch_count += 1

            loss.backward()
            optimizer.step()

            train_loss += loss.item()
            _, predicted = torch.max(outputs.data, 1)
            train_accuracy += (predicted == labels).sum().item()
            train_count += labels.size(0)
        
        val_loss = 0
        val_accuracy = 0
        val_count = 0
        with torch.no_grad():
            for loader in val_data:

                images, labels_cpu,video_id = loader
                images = images.to(device_gpu)
                labels = labels_cpu.to(device_gpu)
                outputs= net(images)
                outputs= outputs[0]
                labels = labels[0]
                loss = criterion(outputs, labels)

                val_loss += loss.item()
                _, predicted = torch.max(outputs.data, 1)
                val_count += labels.size(0)
                val_accuracy += (predicted == labels).sum().item()

        summary = "train (loss %.3f, accuracy %.3f) val (loss %.3f, accuracy %.3f)" % \
                  (train_loss/train_count, train_accuracy/train_count, val_loss/val_count, val_accuracy/val_count)
        log("\t" + summary)

        if (epoch + 1) % args.save_freq == 0:
            log("\tSave model to %s..." % model_file_final)
            net.save(model_file_final)
        act_acc = val_accuracy/val_count
        if best_acc < act_acc:
            best_acc = act_acc
            log("\tSave best val model to %s..." % model_file)
            net.save(model_file)

        if one and train_accuracy/train_count > 0.999:
            net.save(model_file_train)
            one = False


    log("Done. Save final model to %s..." % model_file_final)
    net.save(model_file_final)
    
    log("Update predictions with best val model")
    net.load(model_file)

    predictions_path = os.path.join(output_folder, "predictions")
    if not os.path.exists(predictions_path):
        os.makedirs(predictions_path)
    test_accuracy = 0
    test_count = 0
    with torch.no_grad():
        for loader in test_data:
            images, labels_cpu, video_id = loader

            f_out = open(os.path.join(predictions_path, video_id[0] + ".txt"), "w")
            images = images.to(device_gpu)
            labels = labels_cpu.to(device_gpu)
            outputs = net(images)
            outputs= outputs[0]
            labels = labels[0]
            _, predicted = torch.max(outputs.data, 1)
            predicted_cpu = predicted.to(device_cpu)
            for p, l in zip(predicted_cpu.numpy(), labels_cpu[0].numpy()):
                f_out.write(str(p) + "," + str(l) + "\n")
            test_count += labels.size(0)
            test_accuracy += (predicted == labels).sum().item()
            f_out.close()
    log('best Model: Test Accuracy : %.3f ' % (test_accuracy/test_count))
    f_log.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Finetune a feauture LSTM for surgical phase recognition.")
    parser.add_argument("--feature_file", type=str, default=0, help="feature file to use")
    parser.add_argument("--lr", type=float, default=0.0001, help="The learning rate.")
    #parser.add_argument("--batch_size", type=int, default=40000, help="The batch size.") #128
    parser.add_argument("--opt_step", type=int, default=1,
                        help="Number of batches to accumulate before applying the optimizer.")
    parser.add_argument("--epochs", type=int, default=200, help="The maximal number of epochs to train.")
    parser.add_argument("--save_freq", type=int, default=10,
                        help="Defines after how many epochs the current model parameters are saved.")
    parser.add_argument("--annotation", type=str, default="phase" , choices=["phase","step"],
                        help="phases or steps annotation")
    parser.add_argument("--exp", type=str, default=None, help=".")
    parser.add_argument("--pretrainedCNNLSTM", type=bool, default=False , help="if model is defined use model as LSTM")
    parser.add_argument("--fold", type=int, default=1, choices = [1,2,3,4], help=".")
    parser.add_argument("--w", type=int, default=0, choices = [0,1], help=".")

    parser.add_argument('--added_noise', type=float, default=0.3, help=".")
    parser.add_argument('--duplicate_frames_prob', type=float, default=0.1, help=".")
    parser.add_argument('--duplicate_duration', type=float, default=5, # 0.5,
                        help=".")  # duration in seconds
    parser.add_argument('--drop_frames_prob', type=float, default=0.1, help=".")
    parser.add_argument('--drop_duration', type=float, default=30,  #4,
                        help=".")
    parser.add_argument('--randomly_replace_prob', type=float, default=0.01,  # 0.01,
                        help=".")
    parser.add_argument('--replace_duration', type=float, default=15,  # 0.5,
                        help=".")
    parser.add_argument('--mask_prob', type=float, default=0.01,  #0.05,
                        help=".")
    parser.add_argument('--mask_duration', type=float, default=15,  # 0.5,
                    help=".")
    parser.add_argument('--noise_factor', type=float, default=1.0, help=".")
    parser.add_argument('--hidden_size', type=int, default=512, help=".")
    parser.add_argument('--bidirectional', type=bool, default=False, help=".")
    parser.add_argument('--num_layers', type=int, default=1, help=".")
    args = parser.parse_args()
    main(args)
