"""Evaluate phase segmentation results.

Usage: eval.py <experiment>
experiment - The experiment to evaluate, specified as <variant>/<num_ops>/<trial_id>.
             The script will look at out_path/Phase_segmentation/<variant>/<num_ops>/<trial_id>/predictions for the
             predictions of the model to evaluate (out_path will be read from utils.py).

The evaluation results comprise, per test OP video, the accuracy as well as recall, precision, and F1 score, each per phase
and averaged over phases. They will be stored in out_path/Phase_segmentation/<variant>/<num_ops>/<trial_id>/eval.csv.

As a summary, the script will print mean and standard deviation of accuracy, avg. recall, avg. precision, and avg. F1
score, calculated over all test OP videos. Additionally, the script will print, per phase, mean and standard deviation
of the F1 score calculated over all test OP videos.
"""

### confusion matrix
# from sklearn.metrics import confusion_matrix
# import pandas as pd
# import seaborn as sn
# import matplotlib.pyplot as plt
# %matplotlib inline
# import numpy as np

# y_true = ["honda", "chevrolet", "honda", "toyota", "toyota", "chevrolet"]
# y_pred = ["honda", "chevrolet", "honda", "toyota", "toyota", "honda"]
# data = confusion_matrix(y_true, y_pred)
# df_cm = pd.DataFrame(data, columns=np.unique(y_true), index = np.unique(y_true))
# df_cm.index.name = 'Actual'
# df_cm.columns.name = 'Predicted'
# plt.figure(figsize = (10,7))
# sn.set(font_scale=1.4)#for label size
# sn.heatmap(df_cm, cmap="Blues", annot=True,annot_kws={"size": 16})# font size


import os.path
import numpy as np
import argparse

import utils
from utils import Cholec80


def _fmt(m):
    return "%2.1f" % (m * 100)


def main(args):
    
    if args.dataset == "Cholec80":
        num_phases = Cholec80.num_phases
    elif args.dataset == "CoBotSteps" or args.dataset == "CoBot_steps":
        num_phases = 9
    else:
        num_phases = 5


    predictions_path = os.path.join(args.experiment, "predictions")
    if not os.path.exists(predictions_path):
        print("Unable to read predictions from %s." % predictions_path)
        exit()
    outfile = os.path.join(args.experiment, "eval.csv")

    outfile2 = os.path.join(args.experiment, "eval.txt")
    f2 = open(outfile2, "w")

    f = open(outfile, "w")
    f.write("OP id,Acc.")

    for p in range(num_phases):
        f.write(",P" + str(p + 1) + " Rec.,P" + str(p + 1) + " Prec.,P" + str(p + 1) + " F1")
    f.write(",Avg. Rec.,Avg. Prec.,Avg. F1\n")

    print("Eval %s..." % predictions_path)

    acc = []
    recall = []
    precision = []
    f1 = []

    per_phase_recall = []
    per_phase_precision = []
    per_phase_f1 = []
    for i in range(num_phases):
        per_phase_recall.append([])
        per_phase_precision.append([])
        per_phase_f1.append([])

    for file in sorted(os.listdir(predictions_path)):
        data = np.loadtxt(os.path.join(predictions_path, file), delimiter=",")
        results = np.zeros([num_phases, 3], dtype=np.int64)
        for d in data:
            pred = int(d[0])
            label = int(d[1])

            if pred == label:
                results[pred, 0] += 1  # True positive
            else:
                results[pred, 1] += 1  # False positive
                results[label, 2] += 1  # False negative

        op_id = file.split('.')[0]
        f.write(op_id + ",")
        op_acc = np.sum(results[:, 0]) / len(data)
        f.write(_fmt(op_acc))

        op_recall = []
        op_precision = []
        op_f1 = []
        for p in range(num_phases):
            TP = results[p, 0]
            FP = results[p, 1]
            FN = results[p, 2]
            if TP + FN > 0:
                p_recall = TP / (TP + FN)
                p_precision = 0
                p_f1 = 0
                if TP > 0:
                    p_precision = TP / (TP + FP)
                    p_f1 = (2 * p_precision * p_recall) / (p_precision + p_recall)
                f.write("," + _fmt(p_recall) + "," + _fmt(p_precision) + "," + _fmt(p_f1))

                op_recall.append(p_recall)
                op_precision.append(p_precision)
                op_f1.append(p_f1)

                per_phase_recall[p].append(p_recall)
                per_phase_precision[p].append(p_precision)
                per_phase_f1[p].append(p_f1)
            else:
                f.write(",,,")
        op_avg_recall = np.mean(op_recall)
        op_avg_precision = np.mean(op_precision)
        op_avg_f1 = np.mean(op_f1)
        f.write("," + _fmt(op_avg_recall) + "," + _fmt(op_avg_precision) + "," + _fmt(op_avg_f1) + "\n")

        acc.append(op_acc)
        recall.append(op_avg_recall)
        precision.append(op_avg_precision)
        f1.append(op_avg_f1)
    f.close()

    # print overall metrics
    ss = "Acc. " + _fmt(np.mean(acc)) + "+-" + _fmt(np.std(acc)) + ", Rec. " + _fmt(np.mean(recall)) + "+-" + _fmt(np.std(recall)) + ", Prec. " + _fmt(np.mean(precision)) + "+-" + _fmt(np.std(precision)) + ", F1 " + _fmt(np.mean(f1)) + "+-" + _fmt(np.std(f1))
    print(ss)
    f2.write(ss)

    out = "Phase-wise F1:"
    for p in range(num_phases):
        out += " (P" + str(p + 1) + ") " + _fmt(np.mean(per_phase_f1[p])) + "+-" + _fmt(np.std(per_phase_f1[p]))
    print(out)
    f2.write(out)
    f2.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Evaluate phase segmentation results.")
    parser.add_argument("experiment", type=str,
                        help="Experiment to evaluate, specified as <variant>/<num_ops>/<trial_id>.")
#    parser.add_argument("--plain_cnn", type=str, default="",
#                        help="_AlexNet,_resnet,_ConvLSTM")
    parser.add_argument("--dataset", type=str, default="Cholec80",
                        help="Cholec80, CoBot, CoBotSteps")
    args = parser.parse_args()
    main(args)
