""" 
training CNN (Resnets or AlexNet) with Cholec80
"""

import argparse
import datetime
import os.path

import torch
import torch.nn as nn
import torch.optim as optim
import torchvision
from torch.optim import lr_scheduler

import utils
from datasets import PhaseData, PhaseDataList
from models import CNNmodel
from utils import AverageMeter, DataPrep_EndoVis, EndoVis18

#alexnet batch 512, lr 0.0001 über 70%
#resnet50 batch 256 lr 0.0001 uber 60%

def main(args):
    transform_train = DataPrep_EndoVis.standard_transform
    transform_test = DataPrep_EndoVis.standard_transform

    device_gpu = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    device_cpu = torch.device("cpu")

    num_class = EndoVis18.num_phases

    pretrained_model_file = None
    pretrain_method = "no_pretrain"
    
    trial_id = datetime.datetime.now().strftime("%Y%m%d-%H%M")
    experiment = os.path.join("Phase_segmentation_"+args.CNN_type, pretrain_method, trial_id)
    
    output_folder = os.path.join(utils.out_path_EndoVis18, experiment)
    os.makedirs(output_folder)
    f_log = open(os.path.join(output_folder, "log.txt"), "a")
    def log(msg):
        utils.log(f_log, msg)
    model_file = os.path.join(output_folder, "model.pkl")
    model_file_final = os.path.join(output_folder, "model_final.pkl")

    log("Run <%s> on device %s" % (experiment, str(device_gpu)))
    if pretrained_model_file is not None:
        log("Using pretrained FeatureNet: " + pretrained_model_file)
    log("Used parameters...")
    for arg in vars(args):
        log("\t" + str(arg) + " : " + str(getattr(args, arg)))

    log("Loading data...")

    train_data = []
    op_pathes = []
    anno_files = []
    for op in os.listdir(os.path.join(utils.frames_path_EndoVis18, "Train/Frames")):
        op_path = os.path.join(utils.frames_path_EndoVis18, "Train/Frames", op)
        if os.path.isdir(op_path):
            anno_file = os.path.join(utils.annotation_path_EndoVis18,"Train/Annotation",op + ".csv")
            op_pathes.append(op_path)
            anno_files.append(anno_file)
               
    dataset = PhaseDataList(op_pathes, anno_files, DataPrep_EndoVis.width, DataPrep_EndoVis.height, transform_train, "EndoVis18")
    trainloader = torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=True,num_workers=4)
    log("\t training data")
    for path in op_pathes:
        log("\t\t" + path)
    
    val_data = []
    for op in os.listdir(os.path.join(utils.frames_path_EndoVis18, "Test/Frames")):
        op_path = os.path.join(utils.frames_path_EndoVis18, "Test/Frames", op)
        if os.path.isdir(op_path):
            anno_file = os.path.join(utils.annotation_path_EndoVis18,"Test/Annotations",op + ".csv")
            dataset = PhaseData(op_path, anno_file, DataPrep_EndoVis.width, DataPrep_EndoVis.height, transform_test, "EndoVis18")
            val_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False,
                                                             num_workers=4))
                
    log("\t val data")
    for loader in val_data:
        log("\t\t" + loader.dataset.frames_path)


    pretrainedFile = None
    if args.CNN_type == "AlexNet":
        pretrainedFile = "./outCholec80/Phase_segmentation_AlexNet/no_pretrain/40/20201101-0437/model200.pkl"
    if args.CNN_type == "resnet50":
        if args.m == None:
            pretrainedFile = "/mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/cholec80/out/Phase_segmentation_resnet50/no_pretrain/40/20210203-1215/model.pkl"
        else:
            pretrainedFile = args.m
        #pretrainedFile = "modelCholec80Resnet502021128-1348.pkl"


    if args.pretrained == 1:
        print("pretrained")
        print(pretrainedFile)
        model = CNNmodel(num_class = num_class, cnn = args.CNN_type, pretrainedFile = pretrainedFile)
    
    else:
        model = CNNmodel(num_class = num_class, cnn = args.CNN_type)

    #print(model)
    if args.CNN_type == "resnet50" and not args.train_all_layers_resnet50:
        print("train only layer4")
        for param in model.model.parameters():
            param.requires_grad = False
        for param in model.model.layer4.parameters():
            param.requires_grad = True
        for param in model.model.fc.parameters():
            param.requires_grad = True

    model = model.to(device_gpu)
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.Adam(filter(lambda p: p.requires_grad, model.parameters()), lr=args.lr)

    torch.backends.cudnn.benchmark = True
    torch.backends.cudnn.enabled = True
    scheduler = lr_scheduler.StepLR(optimizer, step_size=50, gamma=0.1)
    log("lr_scheduler.StepLR(optimizer, step_size=50, gamma=0.1)")
    log("Begin training...")
    best_val = 0
    for epoch in range(args.epochs):
        log("Epoch " + str(epoch + 1) + "...")
        # zero lodd and acc
        train_loss = AverageMeter()
        train_acc = AverageMeter()
        
        # Print Learning Rate
        #print('Epoch:', (epoch + 1),'LR:', scheduler.get_lr())
        model.train()
        for _, batch in enumerate(trainloader):
            data, target = batch
            batch_size = target.size(0)
            data = data.to(device_gpu)
            target = target.to(device_gpu)

            # zero the parameter gradients
            optimizer.zero_grad()

            #forward
            output = model(data)
            loss = criterion(output, target)
            # backward + optimize
            loss.backward()
            optimizer.step()
                
            # statistics
            train_loss.update(loss.item(), batch_size)                
            #predicted = torch.nn.Softmax(dim=1)(output)
            _, predicted = torch.max(output, 1)
            acc = (predicted == target).sum().item() / batch_size
            train_acc.update(acc, batch_size)
        # Decay Learning Rate
        scheduler.step()
        log("Epoch {}: Train loss: {train_loss.avg:.4f} Train acc: {train_acc.avg:.3f}"
            .format(epoch + 1, train_loss=train_loss, train_acc=train_acc))
        log("testing with val set...")
        predictions_path = os.path.join(output_folder, "predictions")
        if not os.path.exists(predictions_path):
            os.makedirs(predictions_path)
        correct = 0
        total = 0
        model.eval()
        val_loss = 0
        with torch.no_grad():
            for v in val_data:
                frames_path = v.dataset.frames_path
                op_id = frames_path.split('/')[-1] if frames_path.split('/')[-1] else frames_path.split('/')[-2]
                f_out = open(os.path.join(predictions_path, op_id + ".txt"), "w")
                for loader in v:
                    data,target_cpu = loader
                    batch_size = target.size(0)
                    data = data.to(device_gpu)
                    target = target_cpu.to(device_gpu)
                    output = model(data)
                    loss = criterion(output, target)

                    val_loss += loss.item()
                        #predicted = torch.nn.Softmax(dim=1)(output)
                    _, predicted = torch.max(output, 1)
                    predicted_cpu = predicted.to(device_cpu)
                    for p, l in zip(predicted_cpu.numpy(), target_cpu.numpy()):
                        f_out.write(str(p) + "," + str(l) + "\n")
                    correct += (predicted == target).sum().item()
                    total += batch_size
                f_out.close()
        #log('Val Accuracy : %.3f %%' % (100 * correct / total))
        log('Val Accuracy : %.3f , Val Loss: %.4f' % (correct / total, val_loss/total))
        if (epoch + 1) % args.save_freq == 0 or epoch == 0:           
            model_temp = os.path.join(output_folder, "model"+ str(epoch +1) + ".pkl")
            log("\tSave model to %s..." % model_temp)
            torch.save(model.state_dict(), model_temp)
        act_val = correct / total

        if act_val > best_val:
            best_val = act_val
            log("Done. Save final model to %s..." % model_file)
            torch.save(model.state_dict(), model_file)

        
    log("Done. Save final model to %s..." % model_file_final)
    torch.save(model.state_dict(), model_file_final)
    #f_log.close()

    correct = 0
    total = 0
    log("Evaluate val set with best model...")
    model.eval()
    predictions_path = os.path.join(output_folder, "predictions")
    if not os.path.exists(predictions_path):
        os.makedirs(predictions_path)
    with torch.no_grad():
        for v in val_data:
            frames_path = v.dataset.frames_path
            op_id = frames_path.split('/')[-1] if frames_path.split('/')[-1] else frames_path.split('/')[-2]
            f_out = open(os.path.join(predictions_path, op_id + ".txt"), "w")
            for loader in v:
                data,target_cpu = loader
                batch_size = target.size(0)
                data = data.to(device_gpu)
                target = target_cpu.to(device_gpu)
                output = model(data)
                    #predicted = torch.nn.Softmax(dim=1)(output)
                _, predicted = torch.max(output, 1)
                predicted_cpu = predicted.to(device_cpu)
                for p, l in zip(predicted_cpu.numpy(), target_cpu.numpy()):
                    f_out.write(str(p) + "," + str(l) + "\n")
                correct += (predicted == target).sum().item()
                total += batch_size
            f_out.close()
        summary = "val ( accuracy %.3f)" % \
                  (correct / total)
        log("\t" + summary)
    f_log.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Run AlexNet or ResnNet for surgical phase recognition with EndoVis18.")
    parser.add_argument("--lr", type=float, default=0.0001, help="The learning rate.")
    parser.add_argument("--batch_size", type=int, default=256, help="The batch size.") #128
    parser.add_argument("--epochs", type=int, default=200, help="The maximal number of epochs to train.")
    parser.add_argument("--save_freq", type=int, default=10,
                        help="Defines after how many epochs the current model parameters are saved.")
    parser.add_argument("--CNN_type", type=str, default="resnet50", choices=["AlexNet","resnet18","resnet34","resnet50"],
                        help="AlexNet, resnet18, resnet34, resnet50")
    parser.add_argument("--pretrained", type=int, default=1 , choices=[0,1,2],
                        help="takes pretrained model from cholec80 if true")
    parser.add_argument("--m", type=str, default= None,
                        help="take model")
    parser.add_argument("--train_all_layers_resnet50", type=bool, default=False,
                        help="Train all ResNet50 layers, if false only layer4.")
    args = parser.parse_args()
    main(args)
 
