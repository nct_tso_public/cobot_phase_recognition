"""
Script to plot results
"""

import argparse
import os.path
import pickle

#import torch
import matplotlib.pylab as plt
import numpy as np
from matplotlib.colors import from_levels_and_colors,TABLEAU_COLORS
#from scipy import io as sio
import utils

def load_data(predictions_path, file, num_repeats = 10):
    data = np.loadtxt(os.path.join(predictions_path, file), delimiter=",")
    new_data = np.dstack([data]*num_repeats)
    return np.transpose(new_data)

def load_data_(predictions_path, num_repeats = 10):
    data = np.loadtxt(predictions_path, delimiter=",")
    new_data = np.dstack([data]*num_repeats)
    return np.transpose(new_data)


def main(args):
    #predictions_path = os.path.join(utils.out_path, "Phase_segmentation"+args.plain_cnn, args.experiment, "predictions")
    #predictions_path = "./outCoBot/Phase_segmentation_resnet50/13/20201120-0911/predictions"#"E:/predictions" #"./outCoBot/Phase_segmentation_Transformer/13/20201204-1409/predictions"#os.path.join(utils.out_path, "Phase_segmentation"+args.plain_cnn, args.experiment, "predictions")
    predictions_path = "/mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/cholec80/out/Phase_segmentation/no_pretrain/60/20201217-1112/predictions"
    
    predictions_path = "/home/krellstef/master_code/outCoBot_steps/Phase_segmentation/pretrainedCNNLSTM/14/20210116-1957/predictions"
    predictions_path = "/home/krellstef/master_code/outCoBot/Phase_segmentation/no_pretrain/16/20210118-1236/predictions"

    tcn_phase = "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_tcn_baseline_20210310/train-val/Tecno/1403-17/predictions"
    trans_mlp_phase = "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_base_mlp_20210310/train-val/Tecno/1429-44/predictions"
    trans_conv_phase = "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_conv_20210310/train-val/Tecno/1358-55/predictions"
    lstm_phase = "/home/krellstef/master_code/outCoBot/Phase_segmentation/no_pretrain/20/20210218-0944/predictions"

    tcn_step_o = "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_tcn_baseline_20210310/train-val/Tecno/0939-53/predictions"
    trans_mlp_step_o = "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_base_mlp_20210310/train-val/Tecno/1437-38/predictions"
    trans_conv_step_o = "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_conv_20210310/train-val/Tecno/1414-13/predictions"
    lstm_step_o = "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation/no_pretrain/20/20210217-1138/predictions"


    tcn_step = "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_tcn_baseline_20210319/train-val/Tecno/0958-44/predictions"
    trans_mlp_step = "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_base_mlp_20210322/train-val/Tecno/1051-23/predictions"
    trans_conv_step = "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_conv_20210319/train-val/Tecno/1100-38/predictions"
    #lstm_step = "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation/no_pretrain/20/20210217-1138/predictions"


    predictions_path = trans_mlp_step

    if not os.path.exists(predictions_path):
        print("Unable to read predictions from %s." % predictions_path)
    #fig = plt.figure("huhu", figsize=(20,10))
    i = 0
    #cmap, norm = from_levels_and_colors([-0.5,0.5, 1.5, 2.5, 3.5, 4.5], ['red', 'purple', 'green', 'yellow', 'orange'])
    cmap, norm = from_levels_and_colors([-0.5,0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5], ['red', 'purple', 'royalblue','navy','cyan','lavender','green', 'yellow', 'orange'])
    oneVideo = False
    if oneVideo == True:
        i = 0
        new_data = 0
        fig = None
        fig = plt.figure("huhu", figsize=(20,10))
        new_data= load_data(tcn_step,"video20200504.txt")
        print(new_data.shape)
        ax = plt.subplot(4,1,i+1)
        ax.set_title(str("MS-TCN"))
        i+=1
        ax = plt.imshow(new_data[:,0,:],cmap = cmap, norm = norm)#, interpolation="nearest",)
        plt.xticks([])
        plt.yticks([])
        plt.axis("tight")


        new_data= load_data(trans_conv_step,"video20200504.txt")
        print(new_data.shape)
        ax = plt.subplot(4,1,i+1)
        ax.set_title(str("Transformer"))
        i+=1
        ax = plt.imshow(new_data[:,0,:],cmap = cmap, norm = norm)#, interpolation="nearest",)
        plt.xticks([])
        plt.yticks([])
        plt.axis("tight")

        new_data= load_data(lstm_step,"20200504.txt")
        print(new_data.shape)
        ax = plt.subplot(4,1,i+1)
        ax.set_title(str("ResNet50-LSTM"))
        i+=1
        ax = plt.imshow(new_data[:,0,:],cmap = cmap, norm = norm)#, interpolation="nearest",)
        plt.xticks([])
        plt.yticks([])
        plt.axis("tight")


        ax = plt.subplot(4,1,i+1)
        ax.set_title(str("Ground Truth"))
        i+=1
        plt.imshow(new_data[:,1,:],cmap = cmap, norm = norm)#, interpolation="nearest",)
        plt.xticks([])
        plt.yticks([])
        plt.axis("tight")

        fig.savefig("plot_step_"+"20200504"+".png")
    if oneVideo:
        exit()

    for file in sorted(os.listdir(tcn_step)):
        # if file == "video20210113.txt":
        #     print("hhuh")
        # else:
        #     continue
        i = 0
        fig = None
        new_data = None
        fig = plt.figure("huhu" + file, figsize=(20,10))
        new_data= load_data(tcn_step,file)
        print(new_data.shape)
        ax = plt.subplot(4,1,i+1)
        ax.set_title(str("MS-TCN"))
        i+=1
        ax = plt.imshow(new_data[:,0,:],cmap = cmap, norm = norm)#, interpolation="nearest",)
        plt.xticks([])
        plt.yticks([])
        plt.axis("tight")


        new_data= load_data(trans_conv_step, file)
        print(new_data.shape)
        ax = plt.subplot(4,1,i+1)
        ax.set_title(str("Transformer_conv"))
        i+=1
        ax = plt.imshow(new_data[:,0,:],cmap = cmap, norm = norm)#, interpolation="nearest",)
        plt.xticks([])
        plt.yticks([])
        plt.axis("tight")

        new_data= load_data(trans_mlp_step,file)
        print(new_data.shape)
        ax = plt.subplot(4,1,i+1)
        ax.set_title(str("Transformer_mlp"))
        i+=1
        ax = plt.imshow(new_data[:,0,:],cmap = cmap, norm = norm)#, interpolation="nearest",)
        plt.xticks([])
        plt.yticks([])
        plt.axis("tight")


        ax = plt.subplot(4,1,i+1)
        ax.set_title(str("Ground Truth"))
        i+=1
        plt.imshow(new_data[:,1,:],cmap = cmap, norm = norm)#, interpolation="nearest",)
        plt.xticks([])
        plt.yticks([])
        plt.axis("tight")

        fig.savefig("plot_steps_"+file+".png")
    exit()
    for file in sorted(os.listdir(predictions_path)):
        print(file)

        i = 0
        fig = plt.figure("huhu", figsize=(20,10))
        data = np.loadtxt(os.path.join(predictions_path, file), delimiter=",")
        num_repeats = 10
        new_data= np.dstack([data]*num_repeats) #np.repeat(data[:,:,np.newaxis],10,axis = 2)
        #new_data /= 5.0
        #print(new_data)
        
        new_data = np.transpose(new_data)
        print(new_data.shape)

        #plt.legend()
        ax = plt.subplot(2,1,i+1)
        ax.set_title(file + str(" predictions"))
        i+=1
        ax = plt.imshow(new_data[:,0,:],cmap = cmap, norm = norm)#, interpolation="nearest",)
        
        plt.xticks([])
        plt.yticks([])
        plt.axis("tight")


        ax = plt.subplot(2,1,i+1)
        ax.set_title(file + str(" Ground Truth"))
        i+=1
        plt.imshow(new_data[:,1,:],cmap = cmap, norm = norm)#, interpolation="nearest",)
        
        plt.xticks([])
        plt.yticks([])
        plt.axis("tight")
        ff = os.path.splitext(file)[0]
        fig.savefig("plot_transformer_mlp_step_"+ff+".png")



        #break
    #plt.show()
    #fig.savefig("Cholec80_cnnlstm_20201217-1112.png")
    '''
	split = 1
	S, P, P_f, P_k, Y,Y_k, Y_f = [], [], [], [], [], [], []

	P_k +=[np.squeeze(p) for p in data_k['P'][0]]
	P_f +=[np.squeeze(p) for p in data_f['P'][0]]
	Y_k +=[np.squeeze(y) for y in data_k['Y'][0]]
	Y_f +=[np.squeeze(y) for y in data_f['Y'][0]]
	n_classes = 7
	max_classes = n_classes - 1
	            # # Output all truth/prediction pairs
	plt.figure(split, figsize=(20,10))
	P_test_k = np.array(P_k)/float(n_classes-1)
	P_test_f = np.array(P_f)/float(n_classes-1)
	y_test_ = np.array(Y_k)/float(n_classes-1)

	for i in range(len(Y_k)):
    	plt.legend()
    	P_tmp = np.vstack([y_test_[i],  P_test_k[i]])
    	plt.subplot(5,1,i+1); imshow_(P_tmp, vmin=0, vmax=1)
    	plt.xticks([])
    	plt.yticks([])'''




    return

def plot_cross(lstm,tcn,transM,transC,resnet,phase):
    if phase:
        cmap, norm = from_levels_and_colors([-0.5,0.5, 1.5, 2.5, 3.5, 4.5], [ 'tab:red', 'tab:purple','tab:green','tab:blue', 'tab:orange'])
        anno = "phase"
    else:
        anno = "step"
        cmap, norm = from_levels_and_colors([-0.5,0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5], ['tab:red','tab:pink', 'tab:gray','tab:brown','tab:olive','tab:cyan','tab:green','tab:blue', 'tab:orange'])
     
    # if phase:
    #     cmap, norm = from_levels_and_colors([-0.5,0.5, 1.5, 2.5, 3.5, 4.5], ['red', 'purple', 'green', 'yellow', 'orange'])
    #     anno = "phase"
    # else:
    #     anno = "step"
    #     cmap, norm = from_levels_and_colors([-0.5,0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5], ['red', 'purple', 'royalblue','navy','cyan','lavender','green', 'yellow', 'orange'])
     

    for spl in range(1,5):
        split = utils.get_split(spl)
        for video_id in split['test']:
            video = utils.VIDEOS[video_id]
            lstm_p = os.path.join(lstm,str(spl),"predictions", "video" +video+ ".txt")
            tcn_p = os.path.join(tcn,str(spl),"predictions", "video" +video+ ".txt")
            transM_p = os.path.join(transM,str(spl),"predictions", "video" +video+ ".txt")
            transC_p = os.path.join(transC,str(spl),"predictions", "video" +video+ ".txt")
            resnet_p = os.path.join(resnet,str(spl),"predictions", video+ ".txt")
            i = 0
            new_data = 0
            fig = None
            fig = plt.figure(video, figsize=(20,10))
            new_data= load_data_(resnet_p)
            print(new_data.shape)
            ax = plt.subplot(6,1,i+1)
            #ax.set_title(str("ResNet50"),loc='left')
            i+=1
            ax = plt.imshow(new_data[:,0,:],cmap = cmap, norm = norm)
            plt.xticks([])
            plt.yticks([])
            plt.axis("tight")

            new_data= load_data_(lstm_p)
            print(new_data.shape)
            ax = plt.subplot(6,1,i+1)
            #ax.set_title(str("ResNet50-LSTM"),loc='left')
            i+=1
            ax = plt.imshow(new_data[:,0,:],cmap = cmap, norm = norm)
            plt.xticks([])
            plt.yticks([])
            plt.axis("tight")

            new_data= load_data_(tcn_p)
            print(new_data.shape)
            ax = plt.subplot(6,1,i+1)
            #ax.set_title(str("MS-TCN"),loc='left')
            i+=1
            ax = plt.imshow(new_data[:,0,:],cmap = cmap, norm = norm)
            plt.xticks([])
            plt.yticks([])
            plt.axis("tight")

            new_data=load_data_(transM_p)
            print(new_data.shape)
            ax = plt.subplot(6,1,i+1)
            #ax.set_title(str("Transformer MLP"),loc='left')
            i+=1
            ax = plt.imshow(new_data[:,0,:],cmap = cmap, norm = norm)
            plt.xticks([])
            plt.yticks([])
            plt.axis("tight")

            new_data= load_data_(transC_p)
            print(new_data.shape)
            ax = plt.subplot(6,1,i+1)
            #ax.set_title(str("Transformer Conv"),loc='left')
            i+=1
            ax = plt.imshow(new_data[:,0,:],cmap = cmap, norm = norm)
            plt.xticks([])
            plt.yticks([])
            plt.axis("tight")

            ax = plt.subplot(6,1,i+1)
            #ax.set_title(str("Ground Truth"),loc='left')
            i+=1
            plt.imshow(new_data[:,1,:],cmap = cmap, norm = norm)
            plt.xticks([])
            plt.yticks([])
            plt.axis("tight")

            fig.savefig("plot_"+str(spl)+"_"+anno+video+".png")



    return

from matplotlib.patches import Rectangle
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors


def plot_colortable(colors, title, sort_colors=True, emptycols=0):

    cell_width = 212 * 4
    cell_height = 22 * 4
    swatch_width = 24 * 5
    margin = 12
    topmargin = 40

    # Sort colors by hue, saturation, value and name.
    if sort_colors is True:
        by_hsv = sorted((tuple(mcolors.rgb_to_hsv(mcolors.to_rgb(color))),
                         name)
                        for name, color in colors.items())
        names = [name for hsv, name in by_hsv]
    else:
        names = list(colors)

    n = len(names)
    ncols = 1#4 - emptycols
    nrows = n // ncols + int(n % ncols > 0)

    width = cell_width * 4 + 2 * margin
    height = cell_height * nrows + margin + topmargin
    dpi = 72

    fig, ax = plt.subplots(figsize=(width/dpi , height/dpi), dpi=dpi)
    fig.subplots_adjust(margin/width, margin/height,
                        (width-margin)/width, (height-topmargin)/height)
    ax.set_xlim(0, cell_width * 4)
    ax.set_ylim(cell_height * (nrows-0.5), -cell_height/2.)
    ax.yaxis.set_visible(False)
    ax.xaxis.set_visible(False)
    ax.set_axis_off()
    ax.set_title(title, fontsize=24, loc="left", pad=10)

    names_phases= ["tab:red","tab:purple","tab:green","tab:blue","tab:orange"]
    phases = ["P1","P2","P3","P4","P5"]
    steps = ["P1","P2.1","P2.2","P2.3","P2.4","P2.5","P3","P4","P5"]

    names_steps= ["tab:red","tab:pink","tab:gray","tab:brown","tab:olive","tab:cyan","tab:green","tab:blue","tab:orange"]

    for i, name in enumerate(names_steps):
        row = i #% nrows
        col = 1#i // nrows
        y = row * cell_height *0.8

        swatch_start_x = cell_width * col
        text_pos_x = cell_width * col + swatch_width + 7

        ax.text(text_pos_x, y ,steps[i], fontsize=28,
                horizontalalignment='left',
                verticalalignment='center')

        ax.add_patch(
            Rectangle(xy=(swatch_start_x, y-30), width=swatch_width,
                      height=60, facecolor=colors[name], edgecolor='0.7')
        )

    return fig

## plot tableau colors for legend
#plot_colortable(mcolors.TABLEAU_COLORS, "Tableau Palette",sort_colors=False, emptycols=2)


plt.show()
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Plots.")
    parser.add_argument("--anno", type=str, default = "phase"
                        help="phase or step")
    args = parser.parse_args()
    if args.anno == "phase":
        phase = True
    else:
        phase = False

    if phase:
        resnet ="/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359"
        tcn = "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_tcn_baseline20210415-0935/train-val"
        lstm = "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/LSTM/20210412-1316"#"/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/LSTM/20210406-1300"
        transM = "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_base_mlp_cross_20210415-0931/train-val"
        transC = "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_conv_cross_20210415-1114/train-val"
    else:
        resnet = "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442"
        tcn = "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_tcn_baseline20210415-1242/train-val"
        lstm = "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/LSTM/20210412-1318"#"/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/LSTM/20210406-1300"
        transM = "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_attn_base_mlp_cross_20210415-1257/train-val"
        transC = "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_attn_conv_cross_20210415-1442/train-val"
    
    plot_cross(lstm,tcn,transM,transC,resnet,phase)
    #main(args)
