""" 
training CNN (Resnets or AlexNet) with CoBot
"""

import argparse
import datetime
import os.path

import torch
import torch.nn as nn
import torch.optim as optim
import torchvision
from torch.optim import lr_scheduler

import utils
from datasets import PhaseData_CoBot_
from models import CNNmodel
from utils import AverageMeter, CoBot, DataPrep3, accuracy, average_F1
import cv2
import torchvision.transforms as transforms
import albumentations as A
from albumentations.pytorch import ToTensorV2
import imgaug
import numpy as np


def get_split(fold):
    split = None
    if fold == 1:
        split = {
        'train': [9, 10, 12, 13, 14, 15, 16, 18, 19, 20, 21, 22, 23, 24, 25, 27, 28, 29, 30, 31, 32],
        'val': [8,17,26,11],
        'test': list(range(0,8))
    }
    elif fold == 2:
        split = {
        'train': [0, 1, 3, 4, 5, 6, 7, 17, 18, 20, 21, 22, 23, 24, 26, 27, 28, 29, 30, 31, 32],
        'val': [16,25,2,19],
        'test': list(range(8,16))
    }
    elif fold == 3:
        split = {
        'train': [0, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 25, 26, 28, 29, 30, 31, 32],
        'val': [24,1,10,27],
        'test': list(range(16,24))
    }
    elif fold == 4:
        split = {
        'train':[1, 2, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20, 21, 22, 23],
        'val': [0,9,18,3],
        'test': list(range(24,33))
    }
    else:
        raise RuntimeError("Not a fold!")
    return split


def get_split_2(fold):
    split = None
    if fold == 1:
        split = {
        'train': [9, 10, 12, 13, 14, 15, 16, 18, 19, 20, 21, 22, 23, 24, 25, 27, 28, 29, 30, 31, 32],
        'val': [7,17,26,11],
        'test': [0,1,2,3,4,5,6,8]
    }
    elif fold == 2:
        split = {
        'train': [0, 1, 3, 4, 5, 6, 8, 17, 18, 20, 21, 22, 23, 24, 26, 27, 28, 29, 30, 31, 32],
        'val': [16,25,2,19],
        'test': [7,9,10,11,12,13,14,15]
    }
    elif fold == 3:
        split = {
        'train': [0, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 20, 25, 26, 28, 29, 30, 31],
        'val': [24,1,10,27],
        'test': [16,17,18,19,21,22,23,32]
    }
    elif fold == 4:
        split = {
        'train':[1, 2, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 19, 21, 22, 23,32],
        'val': [0,9,18,3],
        'test': [24,25,26,27,28,29,30,31,20]
    }
    else:
        raise RuntimeError("Not a fold!")
    return split
## model mit endovVis trainiert
## /mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/EndoVis18/out/Phase_segmentation_resnet50/no_pretrain/20210225-1242/model.pkl
VIDEOS = ['20190917','20191001','20191011','20191211','20200122','20200129','20200221','20200317',
            '20200330','20200407','20200416','20200423','20200427','20200710','20200717','20200727','20201002','20201006', '20201201','20201204',
            '20201208','20190214','20200611','20200123','20200302','20200504','20200622','20201017', '20200731','20200827','20191206', '20210113', '20200212']


def calc_class(train_data, num_class):
    print("calc classes")

    counts = np.zeros(num_class, dtype=np.float)
    total_frames = 0
    for label in train_data:
        assert(label < num_class)
        counts[label] += 1
        total_frames += 1

    freqs = counts / total_frames
    median_freq = np.median(freqs)

    missing_labels = []
    for label in range(freqs.shape[0]):
        if freqs[label] == 0:
            print("No frames with label {} found!".format(label))
            freqs[label] = 1
            missing_labels.append(label)

    weights = np.ones(num_class, dtype=np.float) * median_freq
    weights = weights / freqs
    for missing in missing_labels:
        weights[missing] = 0

    weights = (weights / np.sum(weights)) * num_class
    print(weights)
    return torch.from_numpy(weights).float()
    
def main(args):
    transform_train = A.Compose([
        A.RandomBrightnessContrast(brightness_limit=0.1, contrast_limit=0.1, brightness_by_max=True, p=1.0),
        A.OneOf([A.MotionBlur(blur_limit=5, p=0.2),
                 A.GaussNoise(var_limit=(5.0, 75.0), mean=0, p=0.8)], p=0.5),
        A.ShiftScaleRotate(shift_limit=0.1, scale_limit=0.15, rotate_limit=15,
                           interpolation=cv2.INTER_CUBIC, border_mode=cv2.BORDER_CONSTANT, value=0, p=0.8),
        A.ElasticTransform(alpha=1, sigma=50, alpha_affine=7, approximate=True,
                           interpolation=cv2.INTER_CUBIC, border_mode=cv2.BORDER_CONSTANT, value=0, p=0.2),

        A.Resize(height=256, width=320, interpolation=cv2.INTER_CUBIC, always_apply=True, p=1.0),
        A.RandomCrop(224, 224, always_apply=True, p=1.0),
        A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225), max_pixel_value=255.0,
                    always_apply=True, p=1.0),
        ToTensorV2(transpose_mask=True, always_apply=True, p=1.0),
    ], p=0.5)

    transform_test = A.Compose([
        A.Resize(height=256, width=320, interpolation=cv2.INTER_CUBIC, always_apply=True, p=1.0),
        A.CenterCrop(224, 224, always_apply=True, p=1.0),
        A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225), max_pixel_value=255.0,
                    always_apply=True, p=1.0),
        ToTensorV2(transpose_mask=True, always_apply=True, p=1.0),
    ], p=1.0)

    assert(torch.cuda.is_available())
    device_gpu = torch.device("cuda:0")
    device_cpu = torch.device("cpu")


    trial_id = datetime.datetime.now().strftime("%Y%m%d-%H%M")

    if args.exp is not None:
        trial_id = args.exp
    experiment = os.path.join("Phase_segmentation_"+args.CNN_type, "no_pretrain",trial_id,str(args.fold))
    
    if args.pretrained == 1:
        experiment = os.path.join("Phase_segmentation_"+args.CNN_type, "pretrain_cholec80", trial_id, str(args.fold))
    elif args.pretrained == 2:
        experiment = os.path.join("Phase_segmentation_"+args.CNN_type, "pretrain_2", trial_id, str(args.fold))
    elif args.pretrained == 3:
        experiment = os.path.join("Phase_segmentation_"+args.CNN_type, "pretrain_CoEn", trial_id, str(args.fold))

    
    output_folder = os.path.join(utils.out_path, experiment)

    anno_path = utils.annotation_path
    if args.annotation == "phase":
        num_class = 5        
    elif args.annotation == "step":
        num_class = 5 + 4
        #anno_path = utils.annotation_path_step
        output_folder = os.path.join("/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps", experiment)
    else:
        raise NotImplementedError("not available as annotation")
        

    os.makedirs(output_folder)
    f_log = open(os.path.join(output_folder, "log.txt"), "a")
    def log(msg):
        utils.log(f_log, msg)
    model_file = os.path.join(output_folder, "model.pkl")
    model_file_final = os.path.join(output_folder, "model_final.pkl")
    model_file_train = os.path.join(output_folder, "model_train.pkl")

    log("Run <%s> on device %s" % (experiment, str(device_gpu)))
    log("Used parameters...")
    for arg in vars(args):
        log("\t" + str(arg) + " : " + str(getattr(args, arg)))

    log("Loading data...")

    if args.data_split_random:
        split = get_split(args.fold)
    else:
        split = get_split_2(args.fold)

    target_data = []
    train_data = []
    log("\t train data")
    itt = 1
    for id in split['train']:
        op = VIDEOS[id]
        op_path = os.path.join(utils.frames_path, op)
        if os.path.isdir(op_path):
            log(op_path)
            step_file = None
            if args.annotation == "step":
                step_file = os.path.join(utils.annotation_path_step, op + "step.txt")
            anno_file = os.path.join(anno_path, op + ".txt")
            dataset = PhaseData_CoBot_(op_path, anno_file, DataPrep3.width, DataPrep3.height, transform_train,annotation = args.annotation, step_file = step_file, sample_rate = args.sample_rate)
            train_data.append(dataset)
            target_data += dataset.get_targets()

    train_data_con = torch.utils.data.ConcatDataset(train_data)
    #dataset = PhaseDataList_CoBot(op_pathes, anno_files, DataPrep3.width, DataPrep3.height, transform_train,annotation = args.annotation, step_file = step_files, sample_rate = args.sample_rate)
    trainloader = torch.utils.data.DataLoader(train_data_con, batch_size=args.batch_size, shuffle=True,num_workers=4)
    val_data = []
    test_data = []
    print(target_data)
    we = calc_class(target_data,num_class)

    for id in split['test']:
        op = VIDEOS[id]
        op_path = os.path.join(utils.frames_path, op)
        if os.path.isdir(op_path):
            step_file = None
            if args.annotation == "step":
                step_file = os.path.join(utils.annotation_path_step, op + "step.txt")
            anno_file = os.path.join(anno_path, op + ".txt")
            dataset = PhaseData_CoBot_(op_path, anno_file, DataPrep3.width, DataPrep3.height, transform_test,annotation = args.annotation, step_file = step_file, sample_rate = args.sample_rate)
            test_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size *args.batch_factor, shuffle=False,
                                                             num_workers=4))
    for id in split['val']:
        op = VIDEOS[id]
        op_path = os.path.join(utils.frames_path, op)
        if os.path.isdir(op_path):
            step_file = None
            if args.annotation == "step":
                step_file = os.path.join(utils.annotation_path_step, op + "step.txt")
            anno_file = os.path.join(anno_path, op + ".txt")
            dataset = PhaseData_CoBot_(op_path, anno_file, DataPrep3.width, DataPrep3.height, transform_test,annotation = args.annotation, step_file = step_file, sample_rate = args.sample_rate)
            val_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_factor, shuffle=False,
                                                             num_workers=4))
    log("\t val data")
    for loader in val_data:
        log("\t\t" + loader.dataset.frames_path)
    log("\t test data")
    for loader in test_data:
        log("\t\t" + loader.dataset.frames_path)


#"./outCholec80/Phase_segmentation_resnet50/no_pretrain/40/20201101-1723/model50.pkl"
#"./outCholec80/Phase_segmentation_AlexNet/no_pretrain/40/20201101-0437/model200.pkl"
    pretrainedFile = None
    if args.CNN_type == "AlexNet":
        pretrainedFile = "./outCholec80/Phase_segmentation_AlexNet/no_pretrain/40/20201101-0437/model200.pkl"
    if args.CNN_type == "resnet50":
        pretrainedFile = "/mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/cholec80/out/Phase_segmentation_resnet50/no_pretrain/40/20210223-1733/model.pkl"
        #pretrainedFile = "modelCholec80Resnet502021128-1348.pkl"
        if args.model != None:
            pretrainedFile = args.model

    if args.pretrained == 1:
        print("pretrianed")
        model = CNNmodel(num_class = num_class, cnn = args.CNN_type, pretrainedFile = pretrainedFile)
    
    else:
        model = CNNmodel(num_class = num_class, cnn = args.CNN_type)

    if args.pretrained == 2:
        # load from Phase Cobot model
        pretrainedFile = "./outCoBot/Phase_segmentation_resnet50/pretrain/20/20210129-1241/model.pkl"
        model.load_different_num_class(pretrainedFile, num_class_load = 5 ,num_class_now = num_class)

    if args.pretrained == 3:
        # load from Phase EndoVIsModel model
        pretrainedFile = "/mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/EndoVis18/out/Phase_segmentation_resnet50/no_pretrain/20210225-1242/model.pkl"
        if args.model != None:
            pretrainedFile = args.model
        model.load_different_num_class(pretrainedFile, num_class_load = 14 ,num_class_now = num_class)

    if args.pretrained == 4:
        #self
        pretrainedFile = "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Self-supervised/1st+2nd_contrastive/20210329-1439/model.pkl"
        if args.model != None:
            pretrainedFile = args.model
            model.load_different_num_class(args.model, num_class_load = 1000 ,num_class_now = num_class, self_supervised = True)
    if args.pretrained == 5:
        #all
        pretrainedFile = "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Self-supervised/1st+2nd_contrastive/20210329-1505/model.pkl"
        if args.model != None:
            pretrainedFile = args.model
            model.load_different_num_class(args.model, num_class_load = 1000 ,num_class_now = num_class, self_supervised = True)

    # freeze some parameter in ResNet50
    if args.CNN_type == "resnet50" and args.freeze_layer:
        for param in model.model.parameters():
            param.requires_grad = False
        #for param in model.model.layer3.parameters():
         #   param.requires_grad = True
        for param in model.model.layer4.parameters():
            param.requires_grad = True
        for param in model.model.fc.parameters():
            param.requires_grad = True


    model = model.to(device_gpu)
    #we = utils.class_weights_steps if args.annotation == "step" else utils.class_weights_phases
    #print(we)

    #we = torch.from_numpy(we).float()
    we = we.to(device_gpu)
    criterion = nn.CrossEntropyLoss(weight=we)
    optimizer = optim.Adam(params=filter(lambda p: p.requires_grad, model.parameters()), lr=args.lr)

    step_size = max(1, int(args.epochs // 3))
    scheduler = lr_scheduler.StepLR(optimizer, step_size=step_size, gamma=0.5)

    torch.backends.cudnn.benchmark = True
    torch.backends.cudnn.enabled = True
    #log("lr_scheduler.StepLR(optimizer, step_size=50, gamma=0.1)")
    log("Begin training...")
    best_val = 0
    one = True
    for epoch in range(args.epochs):
        log("Epoch " + str(epoch + 1) + "...")
        # zero lodd and acc
        train_loss = AverageMeter()
        train_acc = AverageMeter()

        
        # Print Learning Rate
        #print('Epoch:', (epoch + 1),'LR:', scheduler.get_lr())
        model.train()
        step_count = 0
        for _, batch in enumerate(trainloader):
            data, target = batch
            batch_size = target.size(0)
            data = data.to(device_gpu)
            target = target.to(device_gpu)

            # zero the parameter gradients
            optimizer.zero_grad()

            #forward
            output = model(data)
            loss = criterion(output, target)
            # backward + optimize
            loss.backward()
            optimizer.step()
                
            # statistics
            train_loss.update(loss.item(), batch_size)                
            predicted = torch.nn.Softmax(dim=-1)(output)
            _, predicted = torch.max(predicted, dim=-1, keepdim=False)
            acc = (predicted == target).sum().item() / batch_size
            train_acc.update(acc, batch_size)

            if step_count == args.steps_per_epoch:
                break
            step_count += 1

        # Decay Learning Rate
        scheduler.step()

        log("Epoch {}: Train loss: {train_loss.avg:.4f} Train acc: {train_acc.avg:.3f}"
            .format(epoch + 1, train_loss=train_loss, train_acc=train_acc))
        correct = 0
        total = 0
        # Validation
        model.eval()
        val_loss = 0

        eval_loss = AverageMeter()
        eval_acc = AverageMeter()

        with torch.no_grad():
            for v in val_data:
                for loader in v:
                    
                    data,target_cpu = loader
                    batch_size = target_cpu.size(0)
                    data = data.to(device_gpu)
                    target = target_cpu.to(device_gpu)
                    output = model(data)
                    loss = criterion(output, target)

                    predicted = torch.nn.Softmax(dim=-1)(output)
                    _, predicted = torch.max(predicted, dim=-1, keepdim=False)

                    eval_loss.update(loss, batch_size)
                    acc = (predicted == target).sum().item() / batch_size
                    eval_acc.update(acc, batch_size)

        mode = "Val"
        log("Epoch {}: Val({}) -- ce loss: {:.4f} acc: {:.3f}".format(epoch + 1, mode, eval_loss.avg, eval_acc.avg))
        
        if (epoch + 1) % args.save_freq == 0 or epoch == 0:
            log("\tSave model to %s..." % model_file_final)
            torch.save(model.state_dict(), model_file_final)
            
        act_val = eval_acc.avg
        if  act_val > best_val:
            best_val = act_val
            log("\tSave model with best val_acc to %s..." % model_file)
            torch.save(model.state_dict(), model_file)

        if one and train_loss.avg > 0.999:
            model.save(model_file_train)
            log("\tSave model with best train to %s..." % model_file_train)
            one = False
   
    log("Done. Save final model to %s..." % model_file_final)
    torch.save(model.state_dict(), model_file_final)

    log("Update predictions with best val model")
    correct = 0
    total = 0
        # Testing
    model.load(model_file)
    model.eval()

    eval_acc = AverageMeter()

    predictions_path = os.path.join(output_folder, "predictions")
    if not os.path.exists(predictions_path):
        os.makedirs(predictions_path)
    with torch.no_grad():
        for v in test_data:
            frames_path = v.dataset.frames_path
            op_id = frames_path.split('/')[-1] if frames_path.split('/')[-1] else frames_path.split('/')[-2]
            f_out = open(os.path.join(predictions_path, op_id + ".txt"), "w")
            for loader in v:
                data,target_cpu = loader
                batch_size = target_cpu.size(0)
                data = data.to(device_gpu)
                target = target_cpu.to(device_gpu)
                output = model(data)
                predicted = torch.nn.Softmax(dim=-1)(output)
                _, predicted = torch.max(predicted, dim=-1, keepdim=False)
                predicted_cpu = predicted.to(device_cpu)
                for p, l in zip(predicted_cpu.numpy(), target_cpu.numpy()):
                    f_out.write(str(p) + "," + str(l) + "\n")

                #correct += (predicted == target).sum().item()
                #total += batch_size

                acc = (predicted == target).sum().item() / batch_size
                eval_acc.update(acc, batch_size)

            f_out.close()
    mode = "Test"
    log("Epoch {}: Val({}) -- acc: {:.3f}"
        .format(epoch, mode,  eval_acc.avg))
        
    #log('best Val Model: Test Accuracy : %d %%' % (100 * correct / total))
    f_log.close()
    ## TO DO : Feature extractor

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Run AlexNet or ResnNet for surgical phase recognition.")
    parser.add_argument("--lr", type=float, default=0.0001, help="The learning rate.")
    parser.add_argument("--batch_size", type=int, default=64, help="The batch size.") #128
    parser.add_argument("--freeze_layer", type = bool, default=False, help="Freeze layers in ResNet50.") 
    parser.add_argument("--epochs", type=int, default=30, help="The maximal number of epochs to train.")
    parser.add_argument("--steps_per_epoch", type=int, default=3000, help=".")
    parser.add_argument("--save_freq", type=int, default=10,
                        help="Defines after how many epochs the current model parameters are saved.")
    parser.add_argument("--CNN_type", "--cnn", type=str, default="resnet50", choices=["AlexNet","resnet18","resnet34","resnet50"],
                        help="AlexNet, resnet18, resnet34, resnet50")
    parser.add_argument("--pretrained", type=int, default=0 ,
                        help="takes pretrained model fram cholec80 if true")
    parser.add_argument("--annotation", type=str, default="phase" , choices=["phase","step"],
                        help="phases or steps annotation")
    parser.add_argument("--sample_rate", type=int, default=1,
                        help="Defines in which rate the pictures are taken 1-> every pic 2-> every 2. pic.")
    parser.add_argument("--model", type=str, default=None ,
                        help="pretrained Model")
    parser.add_argument("--fold", type=int, default=1, choices = [1,2,3,4], help=".")

    parser.add_argument("--exp", type=str, default=None, help=".")
    parser.add_argument("--batch_factor", type=int, default=2, help=".")
    parser.add_argument("--data_split_random", type=bool, default=True, help=".")

    args = parser.parse_args()
    main(args)
