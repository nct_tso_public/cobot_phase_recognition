"""
train LSTM with CoBot dataset 
"""

import argparse
import datetime
import os.path

import torch
import torch.nn as nn
import torch.optim as optim
import torchvision

import utils
from datasets import MyDataSet_
from models import LSTMNet_
from utils import CoBot, DataPrep3
import numpy as np

def main(args):

    device_gpu = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    device_cpu = torch.device("cpu")


    anno_path = utils.annotation_path
    if args.annotation == "phase":
        num_class = 5        
    elif args.annotation == "step":
        num_class = 5 + 4
        step_file = []
    else:
        raise NotImplementedError("not available as annotation")


    print("Used parameters...")
    for arg in vars(args):
        print("\t" + str(arg) + " : " + str(getattr(args, arg)))

    print("Loading data...")
    
    feature_file = torch.load(args.feature_file)

    assert(args.fold == feature_file['split'])

    test_data = []
    video_ids = sorted(feature_file['test'].keys())
    print("\t test data")
    dataset = MyDataSet_(mode = 'test', feature_file = feature_file)
    test_videos = dataset.video_ids
    print("Found {} test videos: {}".format(len(test_videos), test_videos))
    test_data=torch.utils.data.DataLoader(dataset, batch_size=1, shuffle=False,
                                                             num_workers=4)
                
    net = LSTMNet_(num_classes = num_class)
    net.to(device_gpu)

    print("Update predictions with best val model")
    net.load(os.path.join(args.model_dir, args.model_file))

    predictions_path = os.path.join(args.model_dir, "predictions")
    if not os.path.exists(predictions_path):
        os.makedirs(predictions_path)
    test_accuracy = 0
    test_count = 0
    with torch.no_grad():
        for loader in test_data:
            images, labels_cpu, video_id = loader

            f_out = open(os.path.join(predictions_path, video_id[0] + ".txt"), "w")
            images = images.to(device_gpu)
            labels_cpu = labels_cpu[0]
            labels = labels_cpu.to(device_gpu)
            outputs = net(images)
            outputs= outputs[0]
            
            _, predicted = torch.max(outputs.data, 1)
            predicted_cpu = predicted.to(device_cpu)
            print(predicted_cpu.size())
            print(labels_cpu.size())
            for p, l in zip(predicted_cpu.numpy(), labels_cpu.numpy()):
                f_out.write(str(p) + "," + str(l) + "\n")
            test_count += labels.size(0)
            test_accuracy += (predicted == labels).sum().item()
            f_out.close()
    print('best Model: Test Accuracy : %.3f ' % (test_accuracy/test_count))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Test LSTM.")
    parser.add_argument("--feature_file", type=str, default="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/1/features.pth.tar", help="feature file to use")
    parser.add_argument("--model_dir", type=str, default="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/LSTM/20210412-1318/1", help="model")
    parser.add_argument("--model_file", type=str, default="model.pkl", help="model")
    parser.add_argument("--annotation", type=str, default="step" , choices=["phase","step"],
                        help="phases or steps annotation")
    parser.add_argument("--fold", type=int, default=1, choices = [1,2,3,4], help=".")
    args = parser.parse_args()
    main(args)
