"""Self-supervised pretraining based on temporal coherence.

Usage: pretrain_self_supervised.py <variant>
variant := contrastive | ranking | 1st+2nd_contrastive
       - the pretraining variant that is to be used
Optional command line parameters can be specified to change default parameter settings.
For further information, run python3 pretrain_self_supervised.py -h .

Running this script will pretrain a FeatureNet using the specified self-supervised learning variant.

Generated files:
model.pth - contains the learned model parameters
log.txt - log file
All files will be saved to the directory out_path/Self-supervised/<variant>/<trial_id>.
    out_path - read from utils.py
    variant - pretraining variant as specified when calling the script
    trial_id - generated based on the current date and time; serves as the experiment's identifier
"""

import torch
import torch.optim as optim
import os.path
import numpy as np
import datetime
import argparse

from datasets import TupleData
from models import FeatureNet
import pretrain_variants as variants
import utils
from utils import DataPrepCoBot
# alle layer
#/mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/EndoVis18/out/Phase_segmentation_resnet50/no_pretrain/20210225-1242/model.pkl

# nur layer4
#/mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/EndoVis18/out/Phase_segmentation_resnet50/no_pretrain/20210222-1316/model.pkl
# 24 ungelabelter Videos, 17(15) 7(6)  --> 16(14) 6(5)
# 28    21(19) 7(5)

def main(args):
    transform_train = DataPrepCoBot.standard_transform
    transform_test = DataPrepCoBot.standard_transform
    sample_count_test = 200#50

    device_gpu = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    trial_id = datetime.datetime.now().strftime("%Y%m%d-%H%M")
    experiment = os.path.join("Self-supervised", args.variant, trial_id)

    output_folder = os.path.join(utils.out_path, experiment)
    os.makedirs(output_folder)
    f_log = open(os.path.join(output_folder, "log.txt"), "w")
    def log(msg):
        utils.log(f_log, msg)
    model_file = os.path.join(output_folder, "model.pth")

    log("Run <%s> on device %s" % (experiment, str(device_gpu)))
    log("Used parameters...")
    for arg in vars(args):
        log("\t" + str(arg) + " : " + str(getattr(args, arg)))

    log("Loading data...")

    plus_2nd_order_TC = (args.variant == "1st+2nd_contrastive")
    log("\t training data")
    train_data_sets = []
    test_data_sets = []
    for op in sorted(os.listdir(utils.frames_path_CoBot_unlabelled)):
        op_path = os.path.join(utils.frames_path_CoBot_unlabelled, op)
        if os.path.isdir(op_path) and len(os.listdir(op_path)) > 0 and "wrong" not in op_path:
            print(op_path)
            if len(train_data_sets) == 21:
                print("test")
                sample_count_test_ = sample_count_test
                if "part" in op_path:
                    sample_count_test_ = int(sample_count_test / 2)
                print(sample_count_test_)
                dataset = TupleData(op_path, sample_count=sample_count_test_,
                                width=320, height=256, transform=transform_test,
                                delta=args.delta, gamma=args.gamma, plus_2nd_order_TC=plus_2nd_order_TC)
                test_data_sets.append(dataset)
            else:
                print("train")
                sample_count_train_ = args.sample_count_train
                if "part" in op_path:
                    sample_count_train_= int(sample_count_train_ / 2)
                print(sample_count_train_)
                dataset = TupleData(op_path, sample_count=sample_count_train_,
                                    width=320, height=256, transform=transform_train,
                                    delta=args.delta, gamma=args.gamma, plus_2nd_order_TC=plus_2nd_order_TC)
                train_data_sets.append(dataset)

    for data in train_data_sets:
        log("\t\t" + data.frames_path)
    train_data = torch.utils.data.ConcatDataset(train_data_sets)
    train_loader = torch.utils.data.DataLoader(train_data, batch_size=args.batch_size, shuffle=True, num_workers=4)

    log("\t test data")
    """test_data_sets = []
    for op in os.listdir(os.path.join(utils.frames_path_CoBot_unlabelled, op_set)):
        op_path = os.path.join(utils.frames_path_CoBot_unlabelled, op_set, op)
        if os.path.isdir(op_path):
            dataset = TupleData(op_path, sample_count=sample_count_test,
                                width=320, height=256, transform=transform_test,
                                delta=args.delta, gamma=args.gamma, plus_2nd_order_TC=plus_2nd_order_TC)
            test_data_sets.append(dataset)"""
    for data in test_data_sets:
        log("\t\t" + data.frames_path)
    test_data = torch.utils.data.ConcatDataset(test_data_sets)
    test_loader = torch.utils.data.DataLoader(test_data, batch_size=args.batch_size, shuffle=False, num_workers=2)

    log("Create FeatureNet")
    net = FeatureNet()

    if args.model != None:
        log("Load pretrained model")
        net.load(args.model,in_old = utils.EndoVis18.num_phases)
    if args.freeze_layers:
        print("freeze layers")
        for param in net.resnet.parameters():
            param.requires_grad = False
        for param in net.resnet.layer4.parameters():
            param.requires_grad = True
    net.to(device_gpu)

    optimizer = optim.Adam(params=filter(lambda p: p.requires_grad, net.parameters()), lr=args.lr)

    def step(batch, train=True):
        if args.variant == "contrastive":
            return variants.step_contrastive(data=batch, margin=args.margin,
                                             net=net, optimizer=optimizer, device=device_gpu, train=train)
        elif args.variant == "ranking":
            return variants.step_ranking(data=batch, margin=args.margin,
                                         net=net, optimizer=optimizer, device=device_gpu, train=train)
        elif args.variant == "1st+2nd_contrastive":
            return variants.step_contrastive_1stAnd2nd(data=batch, margin=args.margin, omega=args.omega,
                                                       net=net, optimizer=optimizer, device=device_gpu, train=train)

    log("Begin training...")
    for epoch in range(1, args.epochs + 1):
        log("Epoch " + str(epoch) + "...")

        loss_train = []
        D_train_close = []
        D_train_far = []

        net.train()
        for data in train_loader:
            loss, D_close, D_far = step(data)

            loss_train.append(loss)
            D_train_close = np.concatenate((D_train_close, D_close))
            D_train_far = np.concatenate((D_train_far, D_far))

        log("\t Train stats...")
        log("\t loss: " + str(np.mean(loss_train)))
        log("\t D close: " + str(np.mean(D_train_close)) + " +- " + str(np.std(D_train_close))
            + "; [" + str(D_train_close.min()) + ", " + str(D_train_close.max()) + "]")
        log("\t D far: " + str(np.mean(D_train_far)) + " +- " + str(np.std(D_train_far))
            + "; [" + str(D_train_far.min()) + ", " + str(D_train_far.max()) + "]")

        loss_test = []
        D_test_close = []
        D_test_far = []

        net.eval()
        with torch.no_grad():
            for data in test_loader:
                loss, D_close, D_far = step(data, train=False)

                loss_test.append(loss)
                D_test_close = np.concatenate((D_test_close, D_close))
                D_test_far = np.concatenate((D_test_far, D_far))

        log("\t Test stats...")
        log("\t loss: " + str(np.mean(loss_test)))
        log("\t D close: " + str(np.mean(D_test_close)) + " +- " + str(np.std(D_test_close))
            + "; [" + str(D_test_close.min()) + ", " + str(D_test_close.max()) + "]")
        log("\t D far: " + str(np.mean(D_test_far)) + " +- " + str(np.std(D_test_far))
            + "; [" + str(D_test_far.min()) + ", " + str(D_test_far.max()) + "]")

        if epoch % args.save_freq == 0:
            log("\t Save model to %s..." % model_file)
            net.save(model_file)

    log("Done. Save final model to %s..." % model_file)
    net.save(model_file)
    f_log.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Self-supervised pretraining based on temporal coherence.")
    parser.add_argument("--variant", type=str, default = "1st+2nd_contrastive", choices=["contrastive", "ranking", "1st+2nd_contrastive"],
                        help="The pretraining variant that is to be used.")
    parser.add_argument("--sample_count_train", type=int, default=1000,
                        help="Number of training tuples to sample per epoch from each video.")
    parser.add_argument("--delta", type=float, default=15.0,  # 15.0 for variant "1st+2nd_contrastive, rest 30.0 sec"
                        help="Parameter for tuple sampling. Close frames will be at most \"delta\" seconds away "
                             "from the reference frame.")
    parser.add_argument("--gamma", type=float, default=240.0,
                        help="Parameter for tuple sampling. Distant frames will be at least \"gamma\" seconds away "
                             "from the reference frame.")
    parser.add_argument("--omega", type=float, default=0.5,
                        help="When training with 1st and 2nd order contrastive loss, the 2nd order contrastive loss "
                             "will be weighted by factor \"omega\".")
    parser.add_argument("--margin", type=float, default=2.0,
                        help="The margin parameter of the loss function.")
    parser.add_argument("--lr", type=float, default=0.0001, help="The learning rate.")
    parser.add_argument("--batch_size", type=int, default=16, help="The batch size.")
    parser.add_argument("--epochs", type=int, default=25, help="The number of epochs to train.")
    parser.add_argument("--save_freq", type=int, default=5,
                        help="Defines after how many epochs the current model parameters are saved.")
    parser.add_argument("--model", type=str, default=None,
                        help="pretrained Model to use")
    parser.add_argument("--freeze_layers", type=bool, default=False, help="Freeze lower layers.")
    
    args = parser.parse_args()
    main(args)
