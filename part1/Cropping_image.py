
import torchvision.transforms as transforms
import torchvision.transforms.functional as TF
from PIL import Image
import os

class MyCrop:
    """Crop """

    def __init__(self, top, left, height, width):
        self.top = top
        self.left = left
        self.height = height
        self.width = width

    def __call__(self, x):
        return TF.crop(x, self.top, self.left, self.height, self.width)
inputPath = "E:/20200622"
outputPath = "E:"
j = 0
# Cropping Transformation of High Resolution images 1280 x 720 + additional resizing
myTransformation_resize = transforms.Compose(
        [MyCrop(22, 218, 675, 845), 
        transforms.Resize(256)])
# Cropping Transformation of High Resolution images 1280 x 720       
myTransformation = MyCrop(22, 218, 675, 845)

name = "%08d.png" % (j)
img = Image.open(os.path.join(inputPath, name))


img = myTransformation(img)
img.save(os.path.join(outputPath,name))


