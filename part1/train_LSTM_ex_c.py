 
""" 
training CNN (Resnets or AlexNet) with CoBot
"""

import argparse
import datetime
import os.path

import torch
import torch.nn as nn
import torch.optim as optim
import torchvision
from torch.optim import lr_scheduler

import utils
from datasets import PhaseData_CoBot, PhaseData_, PhaseData
from models import ResNet50LSTM
from utils import AverageMeter, CoBot, DataPrep3, accuracy, average_F1, Cholec80, DataPrep, VIDEOS
import cv2
import torchvision.transforms as transforms
#import albumentations as A
#from albumentations.pytorch import ToTensorV2
#import imgaug
import numpy as np

def str2bool(v):
	return v.lower() in ("yes", "true", "t", "1")


## model mit endovVis trainiert
## /mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/EndoVis18/out/Phase_segmentation_resnet50/no_pretrain/20210225-1242/model.pkl

#1 [1.81178732 0.44682263 1.41549577 0.27488215 1.05101214]
#2 [1.66680781 0.44370434 1.16201695 0.2778665  1.4496044 ]
#3
#4
  
def main(args):
    

    assert(torch.cuda.is_available())
    device_gpu = torch.device("cuda:0")
    device_cpu = torch.device("cpu")


    trial_id = datetime.datetime.now().strftime("%Y%m%d-%H%M")

    if args.exp is not None:
        trial_id = args.exp
    experiment = os.path.join("CNN_LSTM_exp",trial_id,str(args.fold))
    
    
    output_folder = os.path.join(utils.out_path, experiment)

    anno_path = utils.annotation_path
    if args.annotation == "phase":
        num_class = 5
        we = utils.class_weights_phases
    elif args.annotation == "step":
        num_class = 5 + 4
        output_folder = os.path.join("/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps", experiment)
        we = utils.class_weights_steps
    elif args.annotation == "cholec80":
        num_class = 7
        we = utils.class_weights_cholec80
        output_folder = os.path.join("/media/TCO/TCO-Studenten/Homes/krellstef/outCholec80","CNN_LSTM_exp",trial_id)
    else:
        raise NotImplementedError("not available as annotation")
        

    os.makedirs(output_folder)
    f_log = open(os.path.join(output_folder, "log.txt"), "a")
    def log(msg):
        utils.log(f_log, msg)

    model_file = os.path.join(output_folder, "model.pkl")
    model_file_final = os.path.join(output_folder, "model_final.pkl")

    log("Run <%s> on device %s" % (experiment, str(device_gpu)))
    log("Used parameters...")
    for arg in vars(args):
        log("\t" + str(arg) + " : " + str(getattr(args, arg)))

    log("Loading data...")

    if args.annotation != "cholec80":
        transform_train =None
        transform_test = None

        transform_train = transforms.Compose([
            transforms.ColorJitter(),
            transforms.RandomHorizontalFlip(),
            transforms.Resize(size=(256,256)),
            transforms.RandomCrop(224),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        ])
        transform_test = transforms.Compose([
            transforms.Resize(256,256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        ])
        # transform_train = A.Compose([
        #     A.RandomBrightnessContrast(brightness_limit=0.1, contrast_limit=0.1, brightness_by_max=True, p=1.0),
        #     A.OneOf([A.MotionBlur(blur_limit=5, p=0.2),
        #              A.GaussNoise(var_limit=(5.0, 75.0), mean=0, p=0.8)], p=0.5),
        #     A.ShiftScaleRotate(shift_limit=0.1, scale_limit=0.15, rotate_limit=15,
        #                        interpolation=cv2.INTER_CUBIC, border_mode=cv2.BORDER_CONSTANT, value=0, p=0.8),
        #     A.ElasticTransform(alpha=1, sigma=50, alpha_affine=7, approximate=True,
        #                        interpolation=cv2.INTER_CUBIC, border_mode=cv2.BORDER_CONSTANT, value=0, p=0.2),

        #     A.Resize(height=256, width=320, interpolation=cv2.INTER_CUBIC, always_apply=True, p=1.0),
        #     A.RandomCrop(224, 224, always_apply=True, p=1.0),
        #     A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225), max_pixel_value=255.0,
        #                 always_apply=True, p=1.0),
        #     ToTensorV2(transpose_mask=True, always_apply=True, p=1.0),
        # ], p=0.5)

        # transform_test = A.Compose([
        #     A.Resize(height=256, width=320, interpolation=cv2.INTER_CUBIC, always_apply=True, p=1.0),
        #     A.CenterCrop(224, 224, always_apply=True, p=1.0),
        #     A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225), max_pixel_value=255.0,
        #                 always_apply=True, p=1.0),
        #     ToTensorV2(transpose_mask=True, always_apply=True, p=1.0),
        # ], p=1.0)

        split = utils.get_split(args.fold)

        train_data = []
        log("\t train data")
        for id in split['train']:
            op = VIDEOS[id]
            op_path = os.path.join(utils.frames_path, op)
            if os.path.isdir(op_path):
                log(op_path)
                step_file = None
                if args.annotation == "step":
                    step_file = os.path.join(utils.annotation_path_step, op + "step.txt")
                anno_file = os.path.join(anno_path, op + ".txt")
                dataset = PhaseData_CoBot(op_path, anno_file, DataPrep3.width, DataPrep3.height, transform_train,annotation = args.annotation, step_file = step_file, sample_rate = args.sample_rate)
                trainloader = torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False,num_workers=4)
                train_data.append(trainloader)
                #break

        val_data = []
        test_data = []

        for id in split['test']:
            op = VIDEOS[id]
            op_path = os.path.join(utils.frames_path, op)
            if os.path.isdir(op_path):
                step_file = None
                if args.annotation == "step":
                    step_file = os.path.join(utils.annotation_path_step, op + "step.txt")
                anno_file = os.path.join(anno_path, op + ".txt")
                dataset = PhaseData_CoBot(op_path, anno_file, DataPrep3.width, DataPrep3.height, transform_test,annotation = args.annotation, step_file = step_file, sample_rate = args.sample_rate)
                test_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size *args.batch_factor, shuffle=False,
                                                                 num_workers=4))
                #break

        for id in split['val']:
            op = VIDEOS[id]
            op_path = os.path.join(utils.frames_path, op)
            if os.path.isdir(op_path):
                step_file = None
                if args.annotation == "step":
                    step_file = os.path.join(utils.annotation_path_step, op + "step.txt")
                anno_file = os.path.join(anno_path, op + ".txt")
                dataset = PhaseData_CoBot(op_path, anno_file, DataPrep3.width, DataPrep3.height, transform_test,annotation = args.annotation, step_file = step_file, sample_rate = args.sample_rate)
                val_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size *args.batch_factor, shuffle=False,
                                                                 num_workers=4))
                #break
    else:

        split = {
            'train': list(range(1, 41)),
            'val': list(range(41, 49)),
            'test': list(range(49, 81))
        }
        train_transform = transforms.Compose([
            transforms.ColorJitter(),
            transforms.RandomHorizontalFlip(),
            transforms.Resize(size=(256,256)),
            transforms.RandomCrop(224),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        ])
        test_transform = transforms.Compose([
            transforms.Resize(256,256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        ])
       
        frames_path = utils.frames_path_Cholec80_
        anno_path = utils.annotation_path_Cholec80

        train_data = []
        for id in split['train']:
            train_dataset = PhaseData(os.path.join(frames_path, "{:02d}".format(id)),
                                            os.path.join(anno_path, "video{:02d}-phase.txt".format(id)),
                                            width=256,height = 256, transform=train_transform)
            train_data.append(torch.utils.data.DataLoader(train_dataset, batch_size=args.batch_size, shuffle=False, num_workers=4))

        log("\t train data")
        for loader in train_data:
            log("\t\t" + loader.dataset.frames_path)

        val_data = []
        for id in split['val']:
            val_dataset = PhaseData(os.path.join(frames_path, "{:02d}".format(id)),
                                 os.path.join(anno_path, "video{:02d}-phase.txt".format(id)),
                                 width=256,height = 256, transform=test_transform)
            val_data.append(torch.utils.data.DataLoader(val_dataset, batch_size=args.batch_size *args.batch_factor, shuffle=False, num_workers=4))

        test_data = []
        for id in split['test']:
            test_dataset = PhaseData(os.path.join(frames_path, "{:02d}".format(id)),
                                  os.path.join(anno_path, "video{:02d}-phase.txt".format(id)),
                                  width=256,height = 256, transform=test_transform)
            test_data.append(torch.utils.data.DataLoader(test_dataset, batch_size=args.batch_size *args.batch_factor, shuffle=False, num_workers=4))


    log("\t val data")
    for loader in val_data:
        log("\t\t" + loader.dataset.frames_path)
    log("\t test data")
    for loader in test_data:
        log("\t\t" + loader.dataset.frames_path)
                    

    pretrained_ResNet50 = args.pretrained_ResNet50
    pretrained_LSTM = args.pretrained_LSTM
    if args.annotation != "cholec80":
        if args.pretrained_ResNet50 is not None:
        	pretrained_ResNet50 = os.path.join(args.pretrained_ResNet50, str(args.fold), "model.pkl")
        if args.pretrained_LSTM is not None:
        	pretrained_LSTM = os.path.join(args.pretrained_LSTM, str(args.fold), "model.pkl")

        model = ResNet50LSTM(num_classes = num_class, pretrained_ResNet50 = pretrained_ResNet50, pretrained_LSTM = pretrained_LSTM, hidden_size = args.lstm_size, lstm_layers = args.lstm_layers)
    else:
        model = ResNet50LSTM(num_classes = num_class, pretrained_ResNet50 = None, pretrained_LSTM = None, hidden_size = args.lstm_size, lstm_layers = args.lstm_layers)
        model.load_cnn_cholec80(pretrained_ResNet50)


    # freeze some parameter in ResNet50
    if args.freeze_layer or args.freeze_res:
        for param in model.res.model.parameters():
            param.requires_grad = False
        #for param in model.model.layer3.parameters():
         #   param.requires_grad = True
        if not args.freeze_res:
        	print("train last layers of res")
	        for param in model.res.model.layer4.parameters():
	            param.requires_grad = True
	        for param in model.res.model.fc.parameters():
	            param.requires_grad = True


    model = model.to(device_gpu)
    if args.use_class_weights:
    	print(we)
    	we = torch.from_numpy(we).float()
    	we = we.to(device_gpu)
    else:
    	we = None

    
    criterion = nn.CrossEntropyLoss(weight=we)
    optimizer = optim.Adam(params=filter(lambda p: p.requires_grad, model.parameters()), lr=args.lr)

    step_size = max(1, int(args.epochs // 3))
    scheduler = lr_scheduler.StepLR(optimizer, step_size=step_size, gamma=0.5)

    torch.backends.cudnn.benchmark = True
    torch.backends.cudnn.enabled = True
    #log("lr_scheduler.StepLR(optimizer, step_size=50, gamma=0.1)")

    log("Begin training...")
    best_val = 0
    for epoch in range(args.epochs):
        log("Epoch " + str(epoch + 1) + "...")

        train_loss = 0
        train_accuracy = 0
        train_count = 0
        model.train()
        for op_data in train_data:
            hidden_state = model.init_hidden(device_gpu)
            optimizer.zero_grad()
            optimized = False
            loss = 0
            batch_count = 0
            for loader in op_data:
                images, labels = loader
                images = images.to(device_gpu)
                labels = labels.to(device_gpu)
                outputs, hidden_state = model(images, hidden_state)
                loss += criterion(outputs, labels)
                batch_count += 1
                if batch_count % args.opt_step == 0:
                    loss.backward()
                    optimizer.step()
                    optimized = True
                    optimizer.zero_grad()
                    h, c = hidden_state
                    h = h.detach()
                    c = c.detach()
                    hidden_state = (h, c)
                else:
                    loss.backward(retain_graph=True)
                    optimized = False

                train_loss += loss.item()
                _, predicted = torch.max(outputs.data, 1)
                train_accuracy += (predicted == labels).sum().item()
                train_count += labels.size(0)

                if batch_count % args.opt_step == 0:
                    loss = 0

            if not optimized:
                optimizer.step()


        summary = "train (loss %.3f, accuracy %.3f)" % \
                  (train_loss/train_count, train_accuracy/train_count)
        log("\t" + summary)

        # Decay Learning Rate
        scheduler.step()

        correct = 0
        total = 0
        # Validation
        model.eval()
        val_loss = 0

        eval_loss = AverageMeter()
        eval_acc = AverageMeter()

        with torch.no_grad():
            for v in val_data:
                hidden_state = model.init_hidden(device_gpu)
                for loader in v:
                    data,target_cpu = loader
                    batch_size = target_cpu.size(0)
                    data = data.to(device_gpu)
                    target = target_cpu.to(device_gpu)
                    output,hidden_state = model(data,hidden_state)
                    loss = criterion(output, target)

                    predicted = torch.nn.Softmax(dim=-1)(output)
                    _, predicted = torch.max(predicted, dim=-1, keepdim=False)

                    eval_loss.update(loss, batch_size)
                    acc = (predicted == target).sum().item() / batch_size
                    eval_acc.update(acc, batch_size)

        mode = "Val"
        log("Epoch {}: Val({}) -- ce loss: {:.4f} acc: {:.3f}".format(epoch + 1, mode, eval_loss.avg, eval_acc.avg))
        
        if (epoch + 1) % args.save_freq == 0 or epoch == 0:
            log("\tSave model to %s..." % model_file_final)
            torch.save(model.state_dict(), model_file_final)
            
        act_val = eval_acc.avg
        if  act_val > best_val:
            best_val = act_val
            log("\tSave model with best val_acc to %s..." % model_file)
            torch.save(model.state_dict(), model_file)
   
    log("Done. Save final model to %s..." % model_file_final)
    torch.save(model.state_dict(), model_file_final)

    log("Update predictions with best val model")
    correct = 0
    total = 0
        # Testing
    model.load(model_file)
    model.eval()

    eval_acc = AverageMeter()

    predictions_path = os.path.join(output_folder, "predictions")
    if not os.path.exists(predictions_path):
        os.makedirs(predictions_path)
    with torch.no_grad():
        for v in test_data:
            frames_path = v.dataset.frames_path
            op_id = frames_path.split('/')[-1] if frames_path.split('/')[-1] else frames_path.split('/')[-2]
            f_out = open(os.path.join(predictions_path, op_id + ".txt"), "w")
            hidden_state = model.init_hidden(device_gpu)
            for loader in v:
                data,target_cpu = loader
                batch_size = target_cpu.size(0)
                data = data.to(device_gpu)
                target = target_cpu.to(device_gpu)
                output,hidden_state = model(data,hidden_state)
                predicted = torch.nn.Softmax(dim=-1)(output)
                _, predicted = torch.max(predicted, dim=-1, keepdim=False)
                predicted_cpu = predicted.to(device_cpu)
                for p, l in zip(predicted_cpu.numpy(), target_cpu.numpy()):
                    f_out.write(str(p) + "," + str(l) + "\n")

                #correct += (predicted == target).sum().item()
                #total += batch_size

                acc = (predicted == target).sum().item() / batch_size
                eval_acc.update(acc, batch_size)

            f_out.close()
    mode = "Test"
    log("Epoch {}: Val({}) -- acc: {:.3f}"
        .format(epoch, mode,  eval_acc.avg))
        
    #log('best Val Model: Test Accuracy : %d %%' % (100 * correct / total))
    f_log.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Run AlexNet or ResnNet for surgical phase recognition.")
    parser.register('type', 'bool', str2bool)
    parser.add_argument("--lr", type=float, default=0.00001, help="The learning rate.")
    parser.add_argument("--use_class_weights", type = 'bool', default=True, help=".")
    parser.add_argument("--batch_size", type=int, default=128, help="The batch size.") #128
    parser.add_argument("--freeze_layer", type = bool, default=True, help="Freeze layers in ResNet50.") 
    parser.add_argument("--freeze_res", type = bool, default=False, help="Freeze layers in ResNet50.") 
    parser.add_argument("--epochs", type=int, default=30, help="The maximal number of epochs to train.")
    parser.add_argument("--save_freq", type=int, default=10,
                        help="Defines after how many epochs the current model parameters are saved.")
    parser.add_argument("--annotation", type=str, default="phase" , choices=["phase","step","cholec80"],
                        help="phases or steps annotation")
    parser.add_argument("--sample_rate", type=int, default=1,
                        help="Defines in which rate the pictures are taken 1-> every pic 2-> every 2. pic.")
    parser.add_argument("--model", type=str, default=None ,
                        help="pretrained Model")
    parser.add_argument("--fold", type=int, default=1, choices = [1,2,3,4], help=".")
    parser.add_argument("--opt_step", type=int, default=3, help=".")
    parser.add_argument("--exp", type=str, default=None, help=".")
    parser.add_argument("--batch_factor", type=int, default=2, help=".")
    parser.add_argument("--lstm_size", type=int, default=256, help=".")
    parser.add_argument("--lstm_layers", type=int, default=1, help=".")
    parser.add_argument("--pretrained_ResNet50", type=str, default=None, help="pretrained Model")
    #steps: /media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442
    #phase: /media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359
    parser.add_argument("--pretrained_LSTM", type=str, default=None, help="pretrained Model")
    #steps: /mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/LSTM_new/256_20210609-1258
    #phase:/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/LSTM_new/256_20210609-1249"

    args = parser.parse_args()
    main(args)