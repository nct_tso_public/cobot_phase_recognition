import copy
import math
from typing import Any, Optional
import os
import sys

import numpy as np
import torch
import torch.nn as nn
import torchvision.models.resnet
from torch import Tensor
from torch.autograd import Variable
from torch.nn import Module, MultiheadAttention
from torch.nn import functional as F
from torch.nn.init import constant, normal

from utils import Cholec80


def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        m.weight.data.normal_(0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        m.weight.data.normal_(1.0, 0.02)
        m.bias.data.fill_(0)

# class FeatureNet_orig(nn.Module):
#     """FeatureNet."""

#     num_features = 4096
#     """Number of output neurons, i.e., dimension of the learned feature space."""

#     def __init__(self):
#         super(FeatureNet_orig, self).__init__()
#         self.resnet = torchvision.models.resnet50(pretrained=True)
#         self.feature = nn.Linear(2048, FeatureNet_orig.num_features)
#         self.sig = nn.Sigmoid()

#     def forward(self, x):
#         x = self.resnet.conv1(x)
#         x = self.resnet.bn1(x)
#         x = self.resnet.relu(x)
#         x = self.resnet.maxpool(x)

#         x = self.resnet.layer1(x)
#         x = self.resnet.layer2(x)
#         x = self.resnet.layer3(x)
#         x = self.resnet.layer4(x)

#         x = self.resnet.avgpool(x)
#         x = x.view(x.size(0), -1)
#         x = self.feature(x)

#         return self.sig(x)

#     def load(self, model_file):
#         self.load_state_dict(torch.load(model_file))

#     def save(self, model_file):
#         torch.save(self.state_dict(), model_file)
# class PhaseNet_orig(nn.Module):
#     """PhaseNet."""

#     lstm_size = 512
#     """Number of LSTM neurons."""

#     # featureNet: pretrained featureNet (.pkl file) that is to be used
#     def __init__(self, featureNet=None):
#         """Create PhaseNet.

#         :param featureNet: Path to a file that stores the model parameters of a FeatureNet.
#                            If specified, the FeatureNet layers will be initialized with weights read from this file.
#         """
#         super(PhaseNet_orig, self).__init__()
#         self.featureNet = FeatureNet_orig()

#         if featureNet is not None:
#             self.featureNet.load(featureNet)

#         self.lstm = nn.LSTM(FeatureNet_orig.num_features, PhaseNet_orig.lstm_size, batch_first=True)
#         self.classifier = nn.Linear(PhaseNet_orig.lstm_size, Cholec80.num_phases)

#     def init_hidden(self, device=None):
#         return (torch.zeros(1, 1, PhaseNet_orig.lstm_size, device=device),
#                 torch.zeros(1, 1, PhaseNet_orig.lstm_size, device=device))

#     def forward(self, x, hidden_state):
#         x = self.featureNet.forward(x)
#         x = x.view(1, x.size(0), -1)
#         x, hidden_state = self.lstm(x, hidden_state)
#         x = x.view(x.size(1), -1)
#         x = self.classifier(x)

#         return x, hidden_state

#     def load(self, model_file):
#         self.load_state_dict(torch.load(model_file))

#     def save(self, model_file):
#         torch.save(self.state_dict(), model_file)

class FeatureNet(nn.Module):
    """FeatureNet."""

    num_features = 2048#4096
    """Number of output neurons, i.e., dimension of the learned feature space."""

    def __init__(self):
        super(FeatureNet, self).__init__()
        self.resnet = torchvision.models.resnet50(pretrained=True)
        #num_ftrs = self.resnet.fc.in_features
        #self.resnet.fc = nn.Linear(num_ftrs, 6) ## in machen models drin
        #self.feature = nn.Linear(12288, FeatureNet.num_features)  ## in manchen models drin
        self.sig = nn.Sigmoid()

    def forward(self, x):
        x = self.resnet.conv1(x)
        x = self.resnet.bn1(x)
        x = self.resnet.relu(x)
        x = self.resnet.maxpool(x)

        x = self.resnet.layer1(x)
        x = self.resnet.layer2(x)
        x = self.resnet.layer3(x)
        x = self.resnet.layer4(x)

        x = self.resnet.avgpool(x)
        x = x.view(x.size(0), -1)
        #x = self.feature(x)

        return self.sig(x)

    def load(self, model_file, in_old = None):
        if in_old == None:
            self.load_state_dict(torch.load(model_file))
        else:
            m = CNNmodel(in_old, cnn = "resnet50")
            m.load_state_dict(torch.load(model_file))
            num_ftrs = self.resnet.fc.in_features
            self.resnet.fc = nn.Linear(num_ftrs, in_old)
            self.resnet = m.base_model()
            self.resnet.fc = nn.Linear(num_ftrs, 1000)

    def save(self, model_file):
        torch.save(self.state_dict(), model_file)
    def base(self):
        return self.resnet

class FeatureNet_AlexNet(nn.Module):
    """FeatureNet."""

    num_features = 2048#4096
    """Number of output neurons, i.e., dimension of the learned feature space."""

    def __init__(self):
        super(FeatureNet_AlexNet, self).__init__()
        self.alexnet = torchvision.models.alexnet(pretrained=True)
        self.alexnet.classifier[6].out_features = FeatureNet_AlexNet.num_features
        self.sig = nn.Sigmoid()

    def forward(self, x):
        x = self.alexnet.features(x)
        x = self.alexnet.avgpool(x)
        x = torch.flatten(x, 1)
        x = self.alexnet.classifier(x)
        return self.sig(x)

    def load(self, model_file):
        self.alexnet.classifier[6].out_features = 6
        self.alexnet.load_state_dict(torch.load(model_file))
        self.alexnet.classifier[6].out_features = FeatureNet_AlexNet.num_features

    def save(self, model_file):
        torch.save(self.state_dict(), model_file)

class PhaseNet(nn.Module):
    """PhaseNet."""

    lstm_size = 512
    """Number of LSTM neurons."""

    # featureNet: pretrained featureNet (.pkl file) that is to be used
    def __init__(self, featureNet=None, cnn = "resnet", num_classes = Cholec80.num_phases, num_classes_pre = None):
        """Create PhaseNet.

        :param featureNet: Path to a file that stores the model parameters of a FeatureNet.
                           If specified, the FeatureNet layers will be initialized with weights read from this file.
        """
        super(PhaseNet, self).__init__()
        if cnn == "alexnet":    
            self.featureNet = FeatureNet_AlexNet()
        else:
            self.featureNet = FeatureNet()
            #self.featureNet.resnet.fc = nn.Linear(self.featureNet.resnet.fc.in_features, 6)            

        if featureNet is not None:
            self.featureNet.load(featureNet, num_classes_pre)
            

        self.lstm = nn.LSTM(self.featureNet.num_features, PhaseNet.lstm_size, batch_first=True)
        self.classifier = nn.Linear(PhaseNet.lstm_size, num_classes)

    def init_hidden(self, device=None):
        return (torch.zeros(1, 1, PhaseNet.lstm_size, device=device),
                torch.zeros(1, 1, PhaseNet.lstm_size, device=device))

    def forward(self, x, hidden_state):
        x = self.featureNet.forward(x)
        x = x.view(1, x.size(0), -1)
        x, hidden_state = self.lstm(x, hidden_state)
        x = x.view(x.size(1), -1)
        x = self.classifier(x)
        return x, hidden_state

    def load(self, model_file, num_class_old=None, num_class_new=None):
        if num_class_old is not None:
            #print(self.featureNet.resnet.fc.out_features)
            #self.featureNet.resnet.fc = nn.Linear(self.featureNet.resnet.fc.in_features, 6)
            self.classifier = nn.Linear(PhaseNet.lstm_size, num_class_old)
            self.load_state_dict(torch.load(model_file))
            self.classifier = nn.Linear(PhaseNet.lstm_size, num_class_new)
            #self.featureNet.resnet.fc = nn.Linear(self.featureNet.resnet.fc.in_features, 1000)
        else:
            #self.featureNet.resnet.fc = nn.Linear(self.featureNet.resnet.fc.in_features, 6)
            self.load_state_dict(torch.load(model_file))
            #self.featureNet.resnet.fc = nn.Linear(self.featureNet.resnet.fc.in_features, 1000)

    def save(self, model_file):
        torch.save(self.state_dict(), model_file)


class LSTMNet(nn.Module):
    """PhaseNet."""

    lstm_size = 512
    """Number of LSTM neurons."""

    # featureNet: pretrained featureNet (.pkl file) that is to be used
    def __init__(self, num_classes = Cholec80.num_phases):
        """Create PhaseNet.

        :param featureNet: Path to a file that stores the model parameters of a FeatureNet.
                           If specified, the FeatureNet layers will be initialized with weights read from this file.
        """
        super(LSTMNet, self).__init__()
      

        self.lstm = nn.LSTM(2048, PhaseNet.lstm_size, batch_first=True)
        self.classifier = nn.Linear(PhaseNet.lstm_size, num_classes)

    def init_hidden(self, device=None):
        return (torch.zeros(1, 1, PhaseNet.lstm_size, device=device),
                torch.zeros(1, 1, PhaseNet.lstm_size, device=device))

    def forward(self, x, hidden_state):
        x = x.view(1, x.size(0), -1)
        x, hidden_state = self.lstm(x, hidden_state)
        x = x.view(x.size(1), -1)
        x = self.classifier(x)
        return x, hidden_state

    def load(self, model_file, num_class_old=None, num_class_new=None):
        self.load_state_dict(torch.load(model_file))

    def save(self, model_file):
        torch.save(self.state_dict(), model_file)

class LSTMNet_(nn.Module):
    """LSTM."""
    def __init__(self, num_classes = Cholec80.num_phases, hidden_size = 512, num_layers = 1, bidirectional = False):
        super(LSTMNet_, self).__init__()

        self.hidden_size = hidden_size
        self.bidirectional= bidirectional
        self.num_layers = num_layers
      
        self.lstm = nn.LSTM(2048, self.hidden_size,num_layers, batch_first=True,bidirectional= bidirectional)
        
        if bidirectional:
            self.classifier = nn.Linear(self.hidden_size * 2, num_classes)
        else:
            self.classifier = nn.Linear(self.hidden_size, num_classes)
    def forward(self, x):
        if self.bidirectional:
            n = 2
        else:
            n = 1
        n = n * self.num_layers
        h = (torch.zeros(n, 1, self.hidden_size,device = torch.device("cuda:0")),
                torch.zeros(n, 1, self.hidden_size,device = torch.device("cuda:0")))
        x,h = self.lstm(x,h)
        x = self.classifier(x)
        return x

    def load(self, model_file, num_class_old=None, num_class_new=None):
        self.load_state_dict(torch.load(model_file))

    def save(self, model_file):
        torch.save(self.state_dict(), model_file)

class ResNet50LSTM(nn.Module):
    """ResNet50LSTM."""
    def __init__(self, num_classes = 10, hidden_size = 512, lstm_layers = 1, bidirectional = False, pretrained_LSTM = None, pretrained_ResNet50 = None):
        super(ResNet50LSTM, self).__init__()

        self.hidden_size = hidden_size
        self.bidirectional= bidirectional
        self.num_layers = lstm_layers
        if self.bidirectional:
            n = 2
        else:
            n = 1
        self.n = n * self.num_layers

        self.res = CNNmodel( num_class = num_classes, cnn = "resnet50")
        if pretrained_ResNet50 is not None:
            print("Load ResNet50")
            self.res.load(pretrained_ResNet50)

        self.lstm = LSTMNet_(num_classes = num_classes, hidden_size = hidden_size, num_layers = lstm_layers, bidirectional = bidirectional)
        if pretrained_LSTM is not None:
            print("Load LSTM")
            self.lstm.load(pretrained_LSTM)

    def init_hidden(self, device=None):
        return (torch.zeros(self.n, 1, self.hidden_size, device=device),
                torch.zeros(self.n, 1, self.hidden_size, device=device))

    def forward(self, x, hidden_state):
        x = self.res.model.conv1(x)
        x = self.res.model.bn1(x)
        x = self.res.model.relu(x)
        x = self.res.model.maxpool(x) 
        x = self.res.model.layer1(x)
        x = self.res.model.layer2(x)
        x = self.res.model.layer3(x)
        x = self.res.model.layer4(x)
        x = self.res.model.avgpool(x)
        x = x.view(1, x.size(0), -1)
        x, hidden_state = self.lstm.lstm(x, hidden_state)
        x = self.lstm.classifier(x)
        x = x.view(x.size(1), -1)
        return x, hidden_state
        # 1, batch, variabel

    # def forward(self, x,h):
    #     x = self.res.model.conv1(x)
    #     x = self.res.model.bn1(x)
    #     x = self.res.model.relu(x)
    #     x = self.res.model.maxpool(x) 
    #     x = self.res.model.layer1(x)
    #     x = self.res.model.layer2(x)
    #     x = self.res.model.layer3(x)
    #     x = self.res.model.layer4(x)
    #     x = self.res.model.avgpool(x)
    #     x = x.view(x.size(0), -1)
    #     x,h = self.lstm(x,h)
    #     return x

    def load(self, model_file):
        self.load_state_dict(torch.load(model_file))

    def load_cnn_cholec80(self, model_file):
        self.res.load_(model_file)

    def save(self, model_file):
        torch.save(self.state_dict(), model_file)

class CNNmodel(nn.Module):
    """CNNmodel."""
    # featureNet: pretrained featureNet (.pkl file) that is to be used
    def __init__(self, num_class, cnn = "AlexNet",pretrained = True, pretrainedFile = None, out_features = 7):
        """Create CNN.

        """
        super(CNNmodel, self).__init__()
        if cnn == "AlexNet":
            self.model= torchvision.models.alexnet(pretrained=pretrained)
            self.model.classifier[6].out_features = 7
            if pretrainedFile is not None:
                print("load pretrained")
                self.load(pretrainedFile)
            self.model.classifier[6].out_features = num_class
        else:
            if cnn == "resnet18":
                self.model = torchvision.models.resnet18(pretrained=pretrained)
            elif cnn == "resnet34":
                self.model = torchvision.models.resnet34(pretrained=pretrained)
            elif cnn == "resnet50":
                self.model = torchvision.models.resnet50(pretrained=pretrained)
            else:
                raise ValueError('Unknown base model: {}'.format(cnn))                    
            num_ftrs = self.model.fc.in_features
            if pretrainedFile is not None:
                print("load pretrained")
                self.model.fc = nn.Linear(num_ftrs, out_features)
                here = os.path.dirname(os.path.abspath(pretrainedFile))
                sys.path.append(here)
                self.load(pretrainedFile)
            self.model.fc = nn.Linear(num_ftrs, num_class)
                     # adapt base model
    def forward(self, x):
        return self.model.forward(x)
    def load(self, model_file):
        self.load_state_dict(torch.load(model_file))
    def load_(self, model_file):
        self.load_state_dict(torch.load(model_file)['model_weights'])
    def load_different_num_class(self, model_file, num_class_load,num_class_now, self_supervised = False):
        num_ftrs = self.model.fc.in_features
        self.model.fc = nn.Linear(num_ftrs, num_class_load)
        if self_supervised:
            f = FeatureNet()
            f.load_state_dict(torch.load(model_file))
            print(f)
            self.model = f.base()
        else:
            self.load_state_dict(torch.load(model_file))
        num_ftrs = self.model.fc.in_features
        self.model.fc = nn.Linear(num_ftrs,num_class_now)

    def save(self, model_file):
        torch.save(self.state_dict(), model_file)
    def base_model(self):
        return self.model
    def feature(self,x,mode):
        if mode == "resnet":
            x = self.model.conv1(x)
            x = self.model.bn1(x)
            x = self.model.relu(x)
            x = self.model.maxpool(x)

            
            x = self.model.layer1(x)
            x = self.model.layer2(x)
            x = self.model.layer3(x)
            x = self.model.layer4(x)
            x = self.model.avgpool(x)
            x = x.view(x.size(0), -1)
            return x
        else:
            return None
            


# class CLSTM_cell(nn.Module):
#     """Initialize a basic Conv LSTM cell.
#     Args:
#       shape: int tuple thats the height and width of the hidden states h and c()
#       filter_size: int that is the height and width of the filters
#       num_features: int thats the num of channels of the states, like hidden_size
      
#     """
#     def __init__(self, shape, input_chans, filter_size, num_features):
#         super(CLSTM_cell, self).__init__()
        
#         self.shape = shape#H,W
#         self.input_chans=input_chans
#         self.filter_size=filter_size
#         self.num_features = num_features
#         #self.batch_size=batch_size
#         self.padding=int((filter_size-1)/2)#in this way the output has the same size
#         self.conv = nn.Conv2d(self.input_chans + self.num_features, 4*self.num_features, self.filter_size, 1, self.padding)

    
#     def forward(self, input, hidden_state):
#         hidden,c=hidden_state#hidden and c are images with several channels
#         #print('hidden ' + str(hidden.size()))
#         #print('input '+ str(input.size()))
#         combined = torch.cat((input, hidden), 1)#oncatenate in the channels
#         #print('combined' + str(combined.size()))
#         A=self.conv(combined)
#         (ai,af,ao,ag)=torch.split(A,self.num_features,dim=1)#it should return 4 tensors
#         i=torch.sigmoid(ai)
#         f=torch.sigmoid(af)
#         o=torch.sigmoid(ao)
#         g=torch.tanh(ag)
        
#         next_c=f*c+i*g
#         next_h=o*torch.tanh(next_c)
#         return next_h, next_c

#     def init_hidden(self,batch_size):
#         return (Variable(torch.zeros(batch_size,self.num_features,self.shape[0],self.shape[1])).cuda(),Variable(torch.zeros(batch_size,self.num_features,self.shape[0],self.shape[1])).cuda())

# class CLSTM(nn.Module):
#     """Initialize a basic Conv LSTM cell.
#     Args:
#       shape: int tuple thats the height and width of the hidden states h and c()
#       filter_size: int that is the height and width of the filters
#       num_features: int thats the num of channels of the states, like hidden_size
      
#     """
#     def __init__(self, shape, input_chans, filter_size, num_features,num_layers):
#         super(CLSTM, self).__init__()
        
#         self.shape = shape#H,W
#         self.input_chans=input_chans
#         self.filter_size=filter_size
#         self.num_features = num_features
#         self.num_layers=num_layers
#         cell_list=[]
#         cell_list.append(CLSTM_cell(self.shape, self.input_chans, self.filter_size, self.num_features).cuda())#the first
#         #one has a different number of input channels
        
#         for idcell in range(1,self.num_layers):
#             cell_list.append(CLSTM_cell(self.shape, self.num_features, self.filter_size, self.num_features).cuda())
#         self.cell_list=nn.ModuleList(cell_list)      

    
#     def forward(self, input, hidden_state):
#         """
#         args:
#             hidden_state:list of tuples, one for every layer, each tuple should be hidden_layer_i,c_layer_i
#             input is the tensor of shape seq_len,Batch,Chans,H,W
#         """

#         current_input = input.transpose(0, 1)#now is seq_len,B,C,H,W
#         #current_input=input
#         next_hidden=[]#hidden states(h and c)
#         seq_len=current_input.size(0)

        
#         for idlayer in range(self.num_layers):#loop for every layer

#             hidden_c=hidden_state[idlayer]#hidden and c are images with several channels
#             all_output = []
#             output_inner = []            
#             for t in range(seq_len):#loop for every step
#                 hidden_c=self.cell_list[idlayer](current_input[t,...],hidden_c)#cell_list is a list with different conv_lstms 1 for every layer

#                 output_inner.append(hidden_c[0])

#             next_hidden.append(hidden_c)
#             current_input = torch.cat(output_inner, 0).view(current_input.size(0), *output_inner[0].size())#seq_len,B,chans,H,W


#         return next_hidden, current_input

#     def init_hidden(self,batch_size):
#         init_states=[]#this is a list of tuples
#         for i in range(self.num_layers):
#             init_states.append(self.cell_list[i].init_hidden(batch_size))
#         return init_states
# #https://github.com/rogertrullo/pytorch_convlstm/blob/master/conv_lstm.py
# class ConvLSTM_PhaseNet(nn.Module):
#     """ConvLSTM_PhaseNet."""

#     lstm_size = 16
#     """Number of LSTM neurons."""


#     def __init__(self, input_size, input_channels = 3, kernel = 5, num_classes = 6):
#         """"ConvLSTM_PhaseNet.      
#         """
#         super(ConvLSTM_PhaseNet, self).__init__()

#         self.input_size = input_size

#         self.convlstm = CLSTM(shape = input_size ,input_chans = input_channels, filter_size = kernel, num_features = ConvLSTM_PhaseNet.lstm_size, num_layers = 1)
#         self.convlstm.apply(weights_init)
#         self.conv1 =  nn.Conv2d(ConvLSTM_PhaseNet.lstm_size, 32, kernel_size=3,padding=1)
#         self.conv2 = nn.Conv2d(32, 32, kernel_size=3, padding=1)
#         self.avgpool = nn.AdaptiveAvgPool2d((6, 6))
#         self.classifier = nn.Linear(32 * 6 * 6, num_classes)
#         self.relu = nn.ReLU(inplace=True)
#         self.maxpool= nn.MaxPool2d(kernel_size=3, stride=2)

#     def init_hidden(self, batch_size):
#         return self.convlstm.init_hidden(batch_size)

#     def forward(self, x, hidden_state):
#         #print("x1 " + str(x.shape))
#         x = x.view(x.size(0),1,x.size(1),x.size(2),x.size(3))
#         #print("x2 " + str(x.shape))
#         hidden_state, x = self.convlstm(x, hidden_state)
#         #print("x3 " + str(x.shape))
#         x = x.view(x.size(1), ConvLSTM_PhaseNet.lstm_size,self.input_size[0],self.input_size[1])
#         #print("x4 " + str(x.shape))
#         x = self.conv1(x)
#         x = self.relu(x)
#         x = self.conv2(x)
#         x = self.relu(x)
#         x = self.maxpool(x)
#         x = self.avgpool(x)
#         x = torch.flatten(x,1)
#         x = self.classifier(x)

#         return x, hidden_state

#     def load(self, model_file):
#         self.load_state_dict(torch.load(model_file))

#     def save(self, model_file):
#         torch.save(self.state_dict(), model_file)

# class ConvLSTM_PhaseNet2(nn.Module):
#     """ConvLSTM_PhaseNet."""

#     lstm_size = 8
#     """Number of LSTM neurons."""


#     def __init__(self, input_size, input_channels = 3, kernel = 5, num_classes = 6):
#         """"ConvLSTM_PhaseNet.      
#         """
#         super(ConvLSTM_PhaseNet2, self).__init__()

#         self.input_size = input_size

#         self.convlstm = CLSTM(shape = input_size ,input_chans = input_channels, filter_size = kernel, num_features = ConvLSTM_PhaseNet2.lstm_size, num_layers = 1)
#         self.convlstm.apply(weights_init)
#         self.conv1 =  nn.Conv2d(ConvLSTM_PhaseNet2.lstm_size, 16, kernel_size=3,padding=1)
#         self.avgpool = nn.AdaptiveAvgPool2d((6, 6))
#         self.classifier = nn.Linear(16 * 6 * 6, num_classes)
#         self.relu = nn.ReLU(inplace=True)

#     def init_hidden(self, batch_size):
#         return self.convlstm.init_hidden(batch_size)

#     def forward(self, x, hidden_state):
#         x = x.view(x.size(0),1,x.size(1),x.size(2),x.size(3))
#         hidden_state, x = self.convlstm(x, hidden_state)
#         x = x.view(x.size(1), ConvLSTM_PhaseNet2.lstm_size,self.input_size[0],self.input_size[1])
#         x = self.conv1(x)
#         x = self.relu(x)
#         x = self.avgpool(x)
#         x = torch.flatten(x,1)
#         x = self.classifier(x)

#         return x, hidden_state

#     def load(self, model_file):
#         self.load_state_dict(torch.load(model_file))

#     def save(self, model_file):
#         torch.save(self.state_dict(), model_file)


# ***** MS-TCN adapted from https://github.com/yabufarha/ms-tcn *****
# causal convolutions --> https://github.com/tobiascz/TeCNO
# taken from https://gitlab.com/funkeii/gesture-transformer/-/blob/master/model.py

class MultiStageModel(nn.Module):
    def __init__(self, num_stages, num_layers, num_f_maps, dim, num_classes, causal_conv):
        super(MultiStageModel, self).__init__()
        self.stage1 = SingleStageModel(num_layers,
                                       num_f_maps,
                                       dim,
                                       num_classes,
                                       causal_conv=causal_conv)
        self.stages = nn.ModuleList([copy.deepcopy(  # required?
            SingleStageModel(num_layers,
                             num_f_maps,
                             num_classes,
                             num_classes,
                             causal_conv=causal_conv))
            for _ in range(num_stages - 1)
        ])

    def forward(self, x, mask):
        out = self.stage1(x, mask)
        outputs = out.unsqueeze(0)
        for s in self.stages:
            out = F.softmax(out, dim=1) * mask[:, 0:1, :]  # repeated masking required ?
            out = s(out, mask)
            outputs = torch.cat((outputs, out.unsqueeze(0)), dim=0)
        return outputs


class SingleStageModel(nn.Module):
    def __init__(self, num_layers, num_f_maps, dim, num_classes, causal_conv=False):
        super(SingleStageModel, self).__init__()
        self.conv_1x1 = nn.Conv1d(dim, num_f_maps, 1)
        self.layers = nn.ModuleList([copy.deepcopy(  # required ?
            DilatedResidualLayer(2 ** i,
                                 num_f_maps,
                                 num_f_maps,
                                 causal_conv=causal_conv))
            for i in range(num_layers)
        ])
        self.conv_out = nn.Conv1d(num_f_maps, num_classes, 1)

    def forward(self, x, mask):
        out = self.conv_1x1(x)
        for layer in self.layers:
            out = layer(out, mask)
        out = self.conv_out(out) * mask[:, 0:1, :]
        return out


class DilatedResidualLayer(nn.Module):  # add pre/post norm? -- dropout parameter ?
    def __init__(self, dilation, in_channels, out_channels, causal_conv=False, kernel_size=3):
        super(DilatedResidualLayer, self).__init__()
        self.causal_conv = causal_conv
        self.dilation = dilation
        if self.causal_conv:
            self.conv_dilated = nn.Conv1d(in_channels,
                                          out_channels,
                                          kernel_size,
                                          padding=(dilation * (kernel_size - 1)),
                                          dilation=dilation)
        else:
            self.conv_dilated = nn.Conv1d(in_channels,
                                          out_channels,
                                          kernel_size,
                                          padding=dilation,
                                          dilation=dilation)
        self.conv_1x1 = nn.Conv1d(out_channels, out_channels, 1)  # required ?
        self.dropout = nn.Dropout()

    def forward(self, x, mask):  # x is of shape N x C_in x S, mask is N x S (1 --> keep, 0 --> mask)
        out = F.relu(self.conv_dilated(x))
        if self.causal_conv:
            out = out[:, :, :-(self.dilation * 2)]
        out = self.conv_1x1(out)
        out = self.dropout(out)
        return (x + out) * mask[:, 0:1, :]  # N x C_out x S -- broadcast mask


