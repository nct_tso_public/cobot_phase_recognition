"""Evaluate phase segmentation results.

Usage: eval.py <experiment>
experiment - The experiment to evaluate,

The evaluation results comprise, per test OP video, the accuracy as well as recall, precision, and F1 score, each per phase
and averaged over phases. They will be stored in eval.csv.

As a summary, the script will print mean and standard deviation of accuracy, avg. recall, avg. precision, and avg. F1
score, calculated over all test OP videos. Additionally, the script will print, per phase, mean and standard deviation
of the F1 score calculated over all test OP videos.

+ Additional average over all 4 splits
"""

import os.path
import numpy as np
import argparse


from utils import Cholec80


def _fmt(m):
    return "%2.1f" % (m * 100)

def calc_scores(path_exp,num_phases):

    predictions_path = os.path.join(path_exp, "predictions")
    if not os.path.exists(predictions_path):
        print("Unable to read predictions from %s." % predictions_path)
        exit()
    outfile = os.path.join(path_exp, "eval.csv")

    outfile2 = os.path.join(path_exp, "eval.txt")
    f2 = open(outfile2, "w")

    f = open(outfile, "w")
    f.write("OP id,Acc.")

    for p in range(num_phases):
        f.write(",P" + str(p + 1) + " Rec.,P" + str(p + 1) + " Prec.,P" + str(p + 1) + " F1")
    f.write(",Avg. Rec.,Avg. Prec.,Avg. F1\n")

    print("Eval %s..." % predictions_path)

    acc = []
    recall = []
    precision = []
    f1 = []

    per_phase_recall = []
    per_phase_precision = []
    per_phase_f1 = []
    for i in range(num_phases):
        per_phase_recall.append([])
        per_phase_precision.append([])
        per_phase_f1.append([])

    for file in sorted(os.listdir(predictions_path)):
        data = np.loadtxt(os.path.join(predictions_path, file), delimiter=",")
        results = np.zeros([num_phases, 3], dtype=np.int64)
        for d in data:
            pred = int(d[0])
            label = int(d[1])

            if pred == label:
                results[pred, 0] += 1  # True positive
            else:
                results[pred, 1] += 1  # False positive
                results[label, 2] += 1  # False negative

        op_id = file.split('.')[0]
        f.write(op_id + ",")
        op_acc = np.sum(results[:, 0]) / len(data)
        f.write(_fmt(op_acc))

        op_recall = []
        op_precision = []
        op_f1 = []
        for p in range(num_phases):
            TP = results[p, 0]
            FP = results[p, 1]
            FN = results[p, 2]
            if TP + FN > 0:
                p_recall = TP / (TP + FN)
                p_precision = 0
                p_f1 = 0
                if TP > 0:
                    p_precision = TP / (TP + FP)
                    p_f1 = (2 * p_precision * p_recall) / (p_precision + p_recall)
                f.write("," + _fmt(p_recall) + "," + _fmt(p_precision) + "," + _fmt(p_f1))

                op_recall.append(p_recall)
                op_precision.append(p_precision)
                op_f1.append(p_f1)

                per_phase_recall[p].append(p_recall)
                per_phase_precision[p].append(p_precision)
                per_phase_f1[p].append(p_f1)
            else:
                f.write(",,,")
        op_avg_recall = np.mean(op_recall)
        op_avg_precision = np.mean(op_precision)
        op_avg_f1 = np.mean(op_f1)
        f.write("," + _fmt(op_avg_recall) + "," + _fmt(op_avg_precision) + "," + _fmt(op_avg_f1) + "\n")

        acc.append(op_acc)
        recall.append(op_avg_recall)
        precision.append(op_avg_precision)
        f1.append(op_avg_f1)
    f.close()

    # print overall metrics
    ss = "Acc. " + _fmt(np.mean(acc)) + "+-" + _fmt(np.std(acc)) + ", Rec. " + _fmt(np.mean(recall)) + "+-" + _fmt(np.std(recall)) + ", Prec. " + _fmt(np.mean(precision)) + "+-" + _fmt(np.std(precision)) + ", F1 " + _fmt(np.mean(f1)) + "+-" + _fmt(np.std(f1))
    print(ss)
    f2.write(ss)

    out = "Phase-wise F1:"
    n= []
    for p in range(num_phases):
        out += " (P" + str(p + 1) + ") " + _fmt(np.mean(per_phase_f1[p])) + "+-" + _fmt(np.std(per_phase_f1[p]))
        n.append(np.mean(per_phase_f1[p]))
    print(out)
    f2.write(out)
    f2.close()
    return np.mean(acc),np.mean(recall),np.mean(precision),np.mean(f1),n

def main(args):
    
    if args.dataset == "Cholec80":
        num_phases = Cholec80.num_phases
    elif args.dataset == "CoBotSteps" or args.dataset == "CoBot_steps":
        num_phases = 9
    else:
        num_phases = 5
    acc= []
    recall = []
    precision= []
    f1 = []
    per_phase_f1 = []
    for i in range(1,5):
        exp = os.path.join(args.experiment, str(i))
        print("split " + str(i))
        a,r,p,f,pf = calc_scores(exp,num_phases)
        acc.append(a)
        recall.append(r)
        precision.append(p)
        f1.append(f)
        per_phase_f1.append(pf)

    acc = np.asarray(acc)
    recall = np.asarray(recall)
    precision = np.asarray(precision)
    f1 = np.asarray(f1)
    #print(per_phase_f1.size())

    print("averages over the splits")
    ss = "Acc. " + _fmt(np.mean(acc)) + "+-" + _fmt(np.std(acc)) + ", Rec. " + _fmt(np.mean(recall)) + "+-" + _fmt(np.std(recall)) + ", Prec. " + _fmt(np.mean(precision)) + "+-" + _fmt(np.std(precision)) + ", F1 " + _fmt(np.mean(f1)) + "+-" + _fmt(np.std(f1))
    print(ss)
    out = "Phase-wise F1:"
    for p in range(num_phases):
        pf_n = []
        for i in range(1,5):
            p_ = per_phase_f1[i-1]
            pf_n.append(p_[p])
        pf_n = np.asarray(pf_n)
        out += " (P" + str(p + 1) + ") " + _fmt(np.mean(pf_n)) + "+-" + _fmt(np.std(pf_n))
    print(out)
    outfile2 = os.path.join(args.experiment, "eval.txt")
    #outfile = os.path.join(args.experiment, "eval.csv")
    f2 = open(outfile2, "w")
    #f = open(outfile, "w")
    f2.write(ss)
    f2.write(out)
    f2.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Evaluate phase segmentation results.")
    parser.add_argument("experiment", type=str,
                        help="Experiment to evaluate, specified as <variant>/<num_ops>/<trial_id>.")
    parser.add_argument("--dataset", type=str, default="CoBot",
                        help="Cholec80, CoBot, CoBotSteps")
    args = parser.parse_args()
    main(args)
