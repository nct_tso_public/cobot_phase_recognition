"""
Sequence datasets for one or sequencial outputs
"""


import csv
import os.path
import random as rand

import numpy as np
import torch
import torch.utils.data as data
from PIL import Image
from skimage.measure import compare_ssim as ssim

from utils import Cholec80, CoBot, DataPrep3, DataPrepCoBot


class PhaseData_sequence2(data.Dataset):
    def __init__(self,frames_path, annotation_path, sequence_length = 5, dataset = "CoBot",transform = None,sample_rate = 1, sample_rate_s = 1, annotation_mode = None ):
        """Create dataset.

        :param frames_path: Path to the directory that contains the video frames.
        :param annotation_path: Path to the text file that contains the phase labels.
        """
        self.frames_path = frames_path
        self.dataset = dataset
        self.transform = transform
        self.sequence_length = sequence_length
        self.targets = []
        if sample_rate_s < sample_rate or sample_rate_s % sample_rate != 0 :
            raise NotImplementedError("samplerate for sequence schould be the same or higher")
        self.sample_rate = sample_rate
        self.sample_rate_s = sample_rate_s
        self.start = (sequence_length -1) * self.sample_rate_s
        # start sekunde
        end = len(os.listdir(self.frames_path)) # number of pictures in directory
        if dataset == "CoBot":
            if annotation_mode == "step":
                CoBot_phase_map = CoBot.phase_map_step_combi
                print("step mode")
            else:
                CoBot_phase_map = CoBot.phase_map
                print("phase mode")
            f = open(annotation_path, "r")
            reader = csv.reader(f, delimiter='\t')
            next(reader, None)
            count = 0
            endframe = 0
            for row in reader:
                if row[0] not in CoBot_phase_map:
                    print(row[0])
                    continue
                startframe = int(int(row[1]) / 1000.0 )#* CoBot.fps_video / 29.0) # annotation in Millisekunden
                if count == 0:
                    startframe = startframe	+ self.start
                    self.start = startframe
                    
                endframe = int(int(row[2]) / 1000.0)# * CoBot.fps_video / 29.0)
                if endframe > (end):
                    #print(endframe)
                    endframe = end
                    #print(row[0])
                #print("start: " + str(startframe) +" end:" + str(endframe))
                st = int(startframe / self.sample_rate)
                en = int(endframe / self.sample_rate)
                #print("start2: " + str(st) +" end2:" + str(en))  
                for t in range(st,en):
                    self.targets.append(CoBot_phase_map[row[0]])
                count += 1
            #print("Länge Datensatz: " + str(len(self.targets)) + ", Start: " + str(self.start) + ", number pngs: " + str(end))
            f.close()

        elif dataset == "Cholec80":
            self.start = (sequence_length-1) * self.sample_rate_s
            f = open(annotation_path, "r")
            reader = csv.reader(f, delimiter='\t')
            next(reader, None)
            count = -1
            for row in reader:
                count += 1
                if count % (25 * self.sample_rate) == 0 and count / 25 >= self.start: 
                    self.targets.append(Cholec80.phase_map[row[1]])
                #count += 1
            f.close()
            #print("Länge Datensatz: " + str(len(self.targets)) + ", Start: " + str(self.start) + ", number count: " + str(count))
        else:
            raise RuntimeError("The dataset " + dataset + "has not been implemented")

    def __getitem__(self, index):
        """Return the frame at the given index along with its phase label.

        :param index: The number of the frame to return.
        :return: The tuple (multiple frames, label).
        """
        #return frame + (sequence_length -1) frames before
        images = []
        target = self.targets[index]        
        for i in range((-self.sequence_length + 1),1):
            #if index == 0:
                #print("i: " +  str(i) + " frame " + str(self.sample_rate_s * i + index * self.sample_rate + self.start))
            frame = self.frames_path + "/%08d.png" % (self.sample_rate_s * i + index * self.sample_rate + self.start)  # read frames with 1 fps
            img = Image.open(frame)
            if self.transform is not None:
                img = self.transform(img)
            images.append(img)

        images = torch.stack(images, dim = 0)

        return images, target

    def __len__(self):
        """ Return size of dataset. """

        return len(self.targets)



class PhaseData_sequence_list2(data.Dataset):
    def __init__(self,frames_path_list, annotation_path_list, sequence_length = 5, dataset = "CoBot",transform = None, sample_rate = 1, sample_rate_s = 1, annotation_mode= None):
        """Create dataset.
        :param frames_path: Path to the directory that contains the video frames.
        :param annotation_path: Path to the text file that contains the phase labels.
        """
        self.frames_path = []
        self.dataset = dataset
        self.transform = transform
        self.sequence_length = sequence_length
        if sample_rate_s < sample_rate or sample_rate_s % sample_rate != 0 :
            raise NotImplementedError("samplerate for sequence schould be the same or higher")
        self.targets = []
        self.sample_rate = sample_rate
        self.sample_rate_s = sample_rate_s
        if dataset == "CoBot":            
            i = 0
            if annotation_mode == "step":
                CoBot_phase_map = CoBot.phase_map_step_combi
            else:
                CoBot_phase_map = CoBot.phase_map
            for annotation_path in annotation_path_list:
                f = open(annotation_path, "r")
                reader = csv.reader(f, delimiter='\t')
                next(reader, None)
                count = 0
                j = 0
                endframe = 0
                start = 0
                frames_sequence = []
                frame_path = []
                onetarget = []
                end = len(os.listdir(frames_path_list[i])) # number of pictures in directory
                print(end)
                print(annotation_path)
                for row in reader:
                    #print(row)
                    if row[0] not in CoBot_phase_map:
                        print(row[0])
                        continue
                    startframe = int(int(row[1]) / 1000.0 )#* CoBot.fps_video / 29.0) # annotation in Millisekunden
                    if count == 0:
                        start = startframe
                    endframe = int(int(row[2]) / 1000.0)# * CoBot.fps_video / 29.0)
                    if endframe > (end):
                        #print(endframe)
                        endframe = end
                        #print(row[0])
                    #print("start: " + str(startframe) +" end:" + str(endframe))
                    st = int(startframe / self.sample_rate)
                    en = int(endframe / self.sample_rate)
                    #print("start2: " + str(st) +" end2:" + str(en))  
                    for t in range(st,en):
                        if j >= ((sequence_length -1) * (sample_rate_s / sample_rate)):
                            self.targets.append(CoBot_phase_map[row[0]])
                            onetarget.append(CoBot_phase_map[row[0]])
                        frame_path.append(frames_path_list[i]+ "/%08d.png" % (self.sample_rate * j + start))
                        j+=1
                    count += 1
                #print("Länge Datensatz: " + str(len(self.targets)) + ", Start: " + str(start) + ", number pngs: " + str(self.end))
                f.close()
                for u in range(len(onetarget)):
                    for z in range(self.sequence_length):
                        frames_sequence.append(frame_path[u + z * int(self.sample_rate_s / self.sample_rate)])
                    self.frames_path.append(frames_sequence)
                    frames_sequence = []
                print(len(self.frames_path))
                i+=1

        elif dataset == "Cholec80":
            i = 0
            for annotation_path in annotation_path_list:
                f = open(annotation_path, "r")
                reader = csv.reader(f, delimiter='\t')
                next(reader, None)
                count = 0
                j = 0
                frames_sequence = []
                frame_path = []
                onetarget = []

                for row in reader:
                    if count % (25 *self.sample_rate) == 0:  # read annotations with 1 fps
                        if j >= ((sequence_length -1) * (sample_rate_s / sample_rate)): # die ersten 4 frames haben keine 4 Vorgänger
                            self.targets.append(Cholec80.phase_map[row[1]])
                            onetarget.append(Cholec80.phase_map[row[1]])
                        frame_path.append(frames_path_list[i]+ "/%08d.png" % (self.sample_rate * j))
                        j += 1
                    count += 1
                f.close()
                i+=1
                for u in range(len(onetarget)):
                    for z in range(self.sequence_length):
                        frames_sequence.append(frame_path[u + z * int(self.sample_rate_s / self.sample_rate)])
                    self.frames_path.append(frames_sequence)
                    frames_sequence = []
        else:
        	raise RuntimeError("The dataset " + dataset + "has not been implemented")

    def __getitem__(self, index):
        """Return the frame at the given index along with its phase label.

        :param index: The number of the frame to return.
        :return: The tuple (multiple frames, label).
        """
        #return frame + (sequence_length -1) frames before
        images = []
        target = self.targets[index]
        frame_s = self.frames_path[index] 
        for i in range(self.sequence_length):
            img = Image.open(frame_s[i])
            if self.transform is not None:
                img = self.transform(img)
            images.append(img)

        images = torch.stack(images, dim = 0)

        return images, target

    def __len__(self):
        """ Return size of dataset. """

        return len(self.targets)




class PhaseData_sequence3(data.Dataset):
    def __init__(self,frames_path, annotation_path, sequence_length = 5, dataset = "CoBot",transform = None,sample_rate = 1, sample_rate_s = 1, annotation_mode = None ):
        """Create dataset.

        :param frames_path: Path to the directory that contains the video frames.
        :param annotation_path: Path to the text file that contains the phase labels.
        """
        self.frames_path = frames_path
        self.dataset = dataset
        self.transform = transform
        self.sequence_length = sequence_length
        self.targets = []
        if sample_rate_s < sample_rate or sample_rate_s % sample_rate != 0 :
            raise NotImplementedError("samplerate for sequence schould be the same or higher")
        self.sample_rate = sample_rate
        self.sample_rate_s = sample_rate_s
        self.start = (sequence_length -1) * self.sample_rate_s
        # start sekunde
        end = len(os.listdir(self.frames_path)) # number of pictures in directory
        if dataset == "CoBot":
            ####TODo For 6 images -> 6 labels
            print("test that")
            exit()
            if annotation_mode == "step":
                CoBot_phase_map = CoBot.phase_map_step_combi
                print("step mode")
            else:
                CoBot_phase_map = CoBot.phase_map
                print("phase mode")
            f = open(annotation_path, "r")
            reader = csv.reader(f, delimiter='\t')
            next(reader, None)
            count = 0
            endframe = 0
            for row in reader:
                if row[0] not in CoBot_phase_map:
                    print(row[0])
                    continue
                startframe = int(int(row[1]) / 1000.0 )#* CoBot.fps_video / 29.0) # annotation in Millisekunden
                if count == 0:
                    #startframe = startframe + self.start
                    #self.start = startframe
                    self.start += startframe
                    
                endframe = int(int(row[2]) / 1000.0)# * CoBot.fps_video / 29.0)
                if endframe > (end):
                    #print(endframe)
                    endframe = end
                    #print(row[0])
                #print("start: " + str(startframe) +" end:" + str(endframe))
                st = int(startframe / self.sample_rate)
                en = int(endframe / self.sample_rate)
                #print("start2: " + str(st) +" end2:" + str(en))  
                for t in range(st,en):
                    self.targets.append(CoBot_phase_map[row[0]])
                count += 1
            #print("Länge Datensatz: " + str(len(self.targets)) + ", Start: " + str(self.start) + ", number pngs: " + str(end))
            f.close()

        elif dataset == "Cholec80":
            self.start = (sequence_length-1) * self.sample_rate_s
            targets_temp = []
            f = open(annotation_path, "r")
            reader = csv.reader(f, delimiter='\t')
            next(reader, None)
            count = -1
            #for row in reader:
            #    count += 1
            #    if count % (25 * self.sample_rate) == 0 and count / 25 >= self.start: 
            #        self.targets.append(Cholec80.phase_map[row[1]])
            for row in reader:
                count += 1
                if count % (25 * self.sample_rate) == 0: 
                    self.targets.append(Cholec80.phase_map[row[1]])
                #count += 1
            f.close()
            #print("Länge Datensatz: " + str(len(self.targets)) + ", Start: " + str(self.start) + ", number count: " + str(count))
        else:
            raise RuntimeError("The dataset " + dataset + "has not been implemented")

    def __getitem__(self, index):
        """Return the frame at the given index along with its phase label.

        :param index: The number of the frame to return.
        :return: The tuple (multiple frames, label).
        """
        #return frame + (sequence_length -1) frames before
        images = []
        target = torch.tensor(self.targets[index : index + self.sequence_length])       
        for i in range((-self.sequence_length + 1),1):
            #if index == 0:
                #print("i: " +  str(i) + " frame " + str(self.sample_rate_s * i + index * self.sample_rate + self.start))
            frame = self.frames_path + "/%08d.png" % (self.sample_rate_s * i + index * self.sample_rate + self.start)  # read frames with 1 fps
            img = Image.open(frame)
            if self.transform is not None:
                img = self.transform(img)
            images.append(img)

        images = torch.stack(images, dim = 0)

        return images, target

    def __len__(self):
        """ Return size of dataset. """

        return len(self.targets) - self.sequence_length +1



class PhaseData_sequence_list3(data.Dataset):
    def __init__(self,frames_path_list, annotation_path_list, sequence_length = 5, dataset = "CoBot",transform = None, sample_rate = 1, sample_rate_s = 1, annotation_mode= None):
        """Create dataset.
        :param frames_path: Path to the directory that contains the video frames.
        :param annotation_path: Path to the text file that contains the phase labels.
        """
        self.frames_path = []
        self.dataset = dataset
        self.transform = transform
        self.sequence_length = sequence_length
        if sample_rate_s < sample_rate or sample_rate_s % sample_rate != 0 :
            raise NotImplementedError("samplerate for sequence schould be the same or higher")
        self.targets = []
        self.sample_rate = sample_rate
        self.sample_rate_s = sample_rate_s
        if dataset == "CoBot":  
            ##TODO 
            print("test that")
            exit()          
            i = 0
            if annotation_mode == "step":
                CoBot_phase_map = CoBot.phase_map_step_combi
            else:
                CoBot_phase_map = CoBot.phase_map
            for annotation_path in annotation_path_list:
                f = open(annotation_path, "r")
                reader = csv.reader(f, delimiter='\t')
                next(reader, None)
                count = 0
                j = 0
                endframe = 0
                start = 0
                frames_sequence = []
                frame_path = []
                onetarget = []
                end = len(os.listdir(frames_path_list[i])) # number of pictures in directory
                print(end)
                print(annotation_path)
                for row in reader:
                    #print(row)
                    if row[0] not in CoBot_phase_map:
                        print(row[0])
                        continue
                    startframe = int(int(row[1]) / 1000.0 )#* CoBot.fps_video / 29.0) # annotation in Millisekunden
                    if count == 0:
                        start = startframe
                    endframe = int(int(row[2]) / 1000.0)# * CoBot.fps_video / 29.0)
                    if endframe > (end):
                        #print(endframe)
                        endframe = end
                        #print(row[0])
                    #print("start: " + str(startframe) +" end:" + str(endframe))
                    st = int(startframe / self.sample_rate)
                    en = int(endframe / self.sample_rate)
                    #print("start2: " + str(st) +" end2:" + str(en))  
                    for t in range(st,en):
                        #if j >= ((sequence_length -1) * (sample_rate_s / sample_rate)):
                        #    self.targets.append(CoBot_phase_map[row[0]])
                        onetarget.append(CoBot_phase_map[row[0]])
                        frame_path.append(frames_path_list[i]+ "/%08d.png" % (self.sample_rate * j + start))
                        j+=1
                    count += 1
                #print("Länge Datensatz: " + str(len(self.targets)) + ", Start: " + str(start) + ", number pngs: " + str(self.end))
                f.close()
                for u in range(len(onetarget)):
                    self.targets.append(torch.tensor(onetarget[u:u + sequence_length]))
                    for z in range(self.sequence_length):
                        frames_sequence.append(frame_path[u + z * int(self.sample_rate_s / self.sample_rate)])
                    self.frames_path.append(frames_sequence)
                    frames_sequence = []
                print(len(self.frames_path))
                i+=1

        elif dataset == "Cholec80":
            i = 0
            for annotation_path in annotation_path_list:
                f = open(annotation_path, "r")
                reader = csv.reader(f, delimiter='\t')
                next(reader, None)
                count = 0
                j = 0
                frames_sequence = []
                frame_path = []
                onetarget = []

                '''for row in reader:
                    if count % (25 *self.sample_rate) == 0:  # read annotations with 1 fps
                        if j >= ((sequence_length -1) * (sample_rate_s / sample_rate)): # die ersten 4 frames haben keine 4 Vorgänger
                            self.targets.append(Cholec80.phase_map[row[1]])
                            onetarget.append(Cholec80.phase_map[row[1]])
                        frame_path.append(frames_path_list[i]+ "/%08d.png" % (self.sample_rate * j))
                        j += 1
                    count += 1
                f.close()
                i+=1
                for u in range(len(onetarget)):
                    for z in range(self.sequence_length):
                        frames_sequence.append(frame_path[u + z * int(self.sample_rate_s / self.sample_rate)])
                    self.frames_path.append(frames_sequence)
                    frames_sequence = []'''
                for row in reader:
                    if count % (25 *self.sample_rate) == 0:  # read annotations with 1 fps
                        #self.targets.append(Cholec80.phase_map[row[1]])
                        onetarget.append(Cholec80.phase_map[row[1]])
                        frame_path.append(frames_path_list[i]+ "/%08d.png" % (self.sample_rate * j))
                        j += 1
                    count += 1
                f.close()
                i+=1
                #self.targets = torch.randn(len(onetarget) - sequence_length + 1,sequence_length)
                for u in range(len(onetarget) - sequence_length + 1):
                    self.targets.append(torch.tensor(onetarget[u:u + sequence_length]))
                    for z in range(self.sequence_length):
                        frames_sequence.append(frame_path[u + z * int(self.sample_rate_s / self.sample_rate)])
                    self.frames_path.append(frames_sequence)
                    frames_sequence = []

        else:
            raise RuntimeError("The dataset " + dataset + "has not been implemented")

    def __getitem__(self, index):
        """Return the frame at the given index along with its phase label.

        :param index: The number of the frame to return.
        :return: The tuple (multiple frames, multiple label).
        """
        #return frame + (sequence_length -1) frames before
        images = []
        target = self.targets[index]
        frame_s = self.frames_path[index] 
        for i in range(self.sequence_length):
            img = Image.open(frame_s[i])
            if self.transform is not None:
                img = self.transform(img)
            images.append(img)

        images = torch.stack(images, dim = 0)

        return images, target

    def __len__(self):
        """ Return size of dataset. """

        return len(self.targets)


