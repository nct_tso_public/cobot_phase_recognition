import torch
import torch.nn as nn
import torch.utils.data as data

import torchvision
import torchvision.transforms as transforms
from PIL import Image

import csv
import copy
import random
import argparse

import os
import numpy as np

import albumentations as A
from albumentations.pytorch import ToTensorV2
import cv2

import sys
sys.path.append('../')
from models import CNNmodel
from datasets import PhaseData_CoBot, PhaseData_CoBot_
import utils
from utils import VIDEOS

def extract_features(file, out_dir, out_name, frames_path, annotation_path, pad_frames=True, normalize=False):
    device_gpu = torch.device("cuda:0")
    device_cpu = torch.device("cpu")
    batch_size = 256  # TODO

    storage = dict()
    # add some general info
    storage['dataset'] = "CoBotStep"#"CoBotPhase"
    #storage['split'] = "Tecno"
    storage['fps'] = 1
    storage['feature_extractor'] = "resnet50"
    storage['model_weights'] = file

    # TODO
    seed = 12345  # fix random seed
    load_size = 256
    crop_size = 224
    test_transform = utils.DataPrep3.standard_transform

    # Standard data split
    videos_train = ['20190917','20191001','20191011','20191211','20200122','20200129','20200221','20200317',
            '20200330','20200407','20200416','20200423','20200427','20200710','20200717','20200727','20201002','20201006', '20201201','20201204',
            '20201208','20190214','20200611','20200123','20200302']
    v_t = ['20200317']
    videos_test = ['20200504','20200622','20201017', '20200731','20200827',
                '20191206', '20210113', '20200212']

    train_videos = []
    for i in videos_train:
        train_videos.append("video" + i)
    val_videos = []
    for i in videos_test:
        val_videos.append("video" + i)
    all_videos = train_videos + val_videos


    print("create datasets")
    data_loaders = []
    for op in (videos_train  + videos_test):
        op_path = os.path.join(frames_path, op)
        print(op_path)
        if os.path.isdir(op_path):
            step_file = None
            if True:
                step_file = os.path.join(utils.annotation_path_step, op + "step.txt")
            anno_file = os.path.join(annotation_path, op + ".txt")
            dataset = PhaseData_CoBot(op_path, anno_file, utils.DataPrep3.width, utils.DataPrep3.height, test_transform,annotation = "step", step_file = step_file, sample_rate = 1)
            data_loaders.append(
                torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=False, num_workers=1)
            )
    model = CNNmodel(num_class=9, cnn="resnet50")
    print("load model weights...")
    model.load(file)
    model = model.to(device_gpu)

    print("extract features...")

    torch.manual_seed(seed)
    random.seed(seed)
    torch.backends.cudnn.benchmark = False
    torch.backends.cudnn.deterministic = True

    storage['train'] = {}
    storage['val'] = {}
    storage['labels'] = {}
    storage['predictions'] = {}

    model.eval()

    train_accuracies = []
    val_accuracies = []
    it = 1 
    with torch.no_grad():
        for loader in data_loaders:
            frames_path = loader.dataset.frames_path
            video_id = "video" + frames_path.split("/")[-1]
            assert(video_id in all_videos)
            # if it < 15:
            #     it += 1
            #     continue
            # it += 1
            # print("hi")


            features = []
            predictions = []
            labels = torch.tensor([], dtype=torch.long)
            correct = 0
            frame_no = 0
            for batch in loader:
                img, label = batch
                labels = torch.cat([labels, label], dim=0)
                #imgs, _labels = batch
                #img = imgs[0]
                #label = _labels[0]

                #print("img", type(img), img.shape)
                #print("label", type(label), label.shape)

                img = img.to(device_gpu)
                feature = model.feature(x=img, mode="resnet")
                #feature = torch.squeeze(feature)
                out = model(img)
                features.append(feature.to(device_cpu))

                # test model accuracy
                _, predicted = torch.max(out, 1)
                #predicted = torch.nn.Softmax(dim=-1)(out)
                #_, predicted = torch.max(predicted, dim=-1, keepdim=False)
                predicted = predicted.to(device_cpu)
                predictions.append(predicted)
                correct += (predicted == label).sum().item()
                frame_no += label.shape[0]

            print(len(features))

            feature_tensor = torch.cat(features, dim=0)
            prediction_tensor = torch.cat(predictions, dim=0)

            accuracy = correct / frame_no
            print("{} - acc: {:.4f}".format(video_id, accuracy))
            if video_id in val_videos:
                val_accuracies.append(accuracy)
                storage['val'][video_id] = feature_tensor
            else:
                assert(video_id in train_videos)
                train_accuracies.append(accuracy)
                storage['train'][video_id] = feature_tensor

            assert(labels.shape[0] == feature_tensor.shape[0])
            assert(labels.dim() == 1)
            storage['labels'][video_id] = labels
            assert(prediction_tensor.shape[0] == labels.shape[0])
            assert(prediction_tensor.dim() == 1)
            storage['predictions'][video_id] = prediction_tensor

    print("mean train accuracy: {:.4f}".format(np.mean(train_accuracies)))
    print("mean val accuracy: {:.4f}".format(np.mean(val_accuracies)))

    storage['val_accuracy'] = np.mean(val_accuracies)

    torch.save(storage, os.path.join(out_dir, "{}.pth.tar".format(out_name)))

    if normalize:  # create normalized version
        storage_norm = copy.deepcopy(storage)

        # calculate mean and std on available training data
        train_data = []
        for video_id in train_videos:
            train_data.append(storage['train'][video_id])
        train_data = torch.cat(train_data, dim=0)
        mean_vector = torch.mean(train_data, dim=0)
        std_vector = torch.std(train_data, dim=0)

        # normalize
        for video_id in all_videos:
            mode = "train"
            if video_id in val_videos:
                mode = "val"
            storage_norm[mode][video_id] = (storage[mode][video_id] - mean_vector.unsqueeze(0)) / std_vector.unsqueeze(0)

        torch.save(storage_norm, os.path.join(out_dir, "{}_normalized.pth.tar".format(out_name)))

def extract_features_cross(file, out_dir, out_name, frames_path, annotation_path, pad_frames=True, normalize=False, fold=1, anno = "phase"):
    device_gpu = torch.device("cuda:0")
    device_cpu = torch.device("cpu")
    batch_size = 384 # TODO

    storage = dict()
    # add some general info
    storage['dataset'] = "CoBot" + anno#"CoBotStep"#"CoBotPhase"
    storage['split'] = fold
    storage['fps'] = 1
    storage['feature_extractor'] = "resnet50"
    storage['model_weights'] = file

    # TODO
    seed = 12345  # fix random seed
    load_size = 256
    crop_size = 224
    test_transform = A.Compose([
        A.CenterCrop(crop_size, crop_size, always_apply=True, p=1.0),
        A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225), max_pixel_value=255.0,
                    always_apply=True, p=1.0),
        ToTensorV2(transpose_mask=True, always_apply=True, p=1.0),
    ], p=1.0)

    split = utils.get_split(fold)

    train_videos = []
    for i in split['train']:
        print(VIDEOS[i])
        train_videos.append("video" + VIDEOS[i])
    val_videos = []
    for i in split['val']:
        print(VIDEOS[i])
        val_videos.append("video" + VIDEOS[i])
    test_videos = []
    for i in split['test']:
        print(VIDEOS[i])
        test_videos.append("video" + VIDEOS[i])
    all_videos = train_videos + val_videos + test_videos


    print("create datasets")
    data_loaders = []
    for op_ in (split['train']  + split['val'] + split['test']):
        op = VIDEOS[op_]
        op_path = os.path.join(frames_path, op)
        print(op_path)
        if os.path.isdir(op_path):
            step_file = None
            if anno == "step":
                step_file = os.path.join(utils.annotation_path_step, op + "step.txt")
            anno_file = os.path.join(annotation_path, op + ".txt")
            dataset = PhaseData_CoBot_(op_path, anno_file, utils.DataPrep3.width, utils.DataPrep3.height, test_transform,annotation = anno, step_file = step_file, sample_rate = 1)
            data_loaders.append(
                torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=False, num_workers=1)
            )

    num_class = 0
    if anno == "phase":
        num_class = 5
    elif anno == "step":
        num_class = 9

    model = CNNmodel(num_class=num_class, cnn="resnet50")
    print("load model weights...")
    model.load(file)
    model = model.to(device_gpu)

    print("extract features...")

    torch.manual_seed(seed)
    random.seed(seed)
    torch.backends.cudnn.benchmark = False
    torch.backends.cudnn.deterministic = True

    storage['train'] = {}
    storage['val'] = {}
    storage['test'] = {}
    storage['labels'] = {}
    storage['predictions'] = {}

    model.eval()

    train_accuracies = []
    val_accuracies = []
    test_accuracies = []
    it = 1 
    with torch.no_grad():
        for loader in data_loaders:
            frames_path = loader.dataset.frames_path
            video_id = "video" + frames_path.split("/")[-1]
            assert(video_id in all_videos)
            # if it < 15:
            #     it += 1
            #     continue
            # it += 1
            # print("hi")


            features = []
            predictions = []
            labels = torch.tensor([], dtype=torch.long)
            correct = 0
            frame_no = 0
            for batch in loader:
                img, label = batch
                labels = torch.cat([labels, label], dim=0)
                #imgs, _labels = batch
                #img = imgs[0]
                #label = _labels[0]

                #print("img", type(img), img.shape)
                #print("label", type(label), label.shape)

                img = img.to(device_gpu)
                feature = model.feature(x=img, mode="resnet")
                #feature = torch.squeeze(feature)
                out = model(img)
                features.append(feature.to(device_cpu))

                # test model accuracy
                #_, predicted = torch.max(out, 1)
                predicted = torch.nn.Softmax(dim=-1)(out)
                _, predicted = torch.max(predicted, dim=-1, keepdim=False)
                predicted = predicted.to(device_cpu)
                predictions.append(predicted)
                correct += (predicted == label).sum().item()
                frame_no += label.shape[0]

            print(len(features))

            feature_tensor = torch.cat(features, dim=0)
            prediction_tensor = torch.cat(predictions, dim=0)

            accuracy = correct / frame_no
            print("{} - acc: {:.4f}".format(video_id, accuracy))
            if video_id in val_videos:
                val_accuracies.append(accuracy)
                storage['val'][video_id] = feature_tensor
            elif video_id in test_videos:
                test_accuracies.append(accuracy)
                storage['test'][video_id] = feature_tensor
            else:
                assert(video_id in train_videos)
                train_accuracies.append(accuracy)
                storage['train'][video_id] = feature_tensor

            assert(labels.shape[0] == feature_tensor.shape[0])
            assert(labels.dim() == 1)
            storage['labels'][video_id] = labels
            assert(prediction_tensor.shape[0] == labels.shape[0])
            assert(prediction_tensor.dim() == 1)
            storage['predictions'][video_id] = prediction_tensor

    print("mean train accuracy: {:.4f}".format(np.mean(train_accuracies)))
    print("mean val accuracy: {:.4f}".format(np.mean(val_accuracies)))
    print("mean test accuracy: {:.4f}".format(np.mean(test_accuracies)))

    storage['val_accuracy'] = np.mean(val_accuracies)
    storage['test_accuracy'] = np.mean(test_accuracies)

    torch.save(storage, os.path.join(out_dir, "{}.pth.tar".format(out_name)))

    if normalize:  # create normalized version
        storage_norm = copy.deepcopy(storage)

        # calculate mean and std on available training data
        train_data = []
        for video_id in train_videos:
            train_data.append(storage['train'][video_id])
        train_data = torch.cat(train_data, dim=0)
        mean_vector = torch.mean(train_data, dim=0)
        std_vector = torch.std(train_data, dim=0)

        # normalize
        for video_id in all_videos:
            mode = "train"
            if video_id in val_videos:
                mode = "val"
            storage_norm[mode][video_id] = (storage[mode][video_id] - mean_vector.unsqueeze(0)) / std_vector.unsqueeze(0)

        torch.save(storage_norm, os.path.join(out_dir, "{}_normalized.pth.tar".format(out_name)))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="")
    parser.add_argument("exp", type=str, help=".")  # "train_cnn_20210301/0612-11/"
    parser.add_argument("--feature_file_name", type=str,default="features", help=".")  # "cnn_20210301"
    parser.add_argument("--img_padding", type=str, choices=["True", "False"], default="False", help=".")
    parser.add_argument("--normalize", type=str, choices=["True", "False"], default="False", help=".")
    parser.add_argument("--anno", type=str, choices=["step", "phase"], default="phase", help=".")
    parser.add_argument("--fold", type=int, choices=[1,2,3,4], default = 1, help=".")
    args = parser.parse_args()

    print(args.exp)
    
    pad_frames = args.img_padding == "True"
    normalize = args.normalize == "True"

    data_path = "/mnt/g27prist/TCO/TCO-Studenten/CoBot"
    frames_path = os.path.join(data_path, "frames_smaller")
    annotation_path = os.path.join(data_path,"annotations_phases_steps", "phase_annotations")
    
    out = os.path.join(args.exp,str(args.fold))
    #model_dir = "./outCoBot/Phase_segmentation_resnet50"
    
    model_file = os.path.join(args.exp,str(args.fold), "model.pkl")
    feature_file = os.path.join(out, "{}.pth.tar".format(args.feature_file_name))

    if os.path.isfile(feature_file):
        print("Found ", feature_file)
        features = torch.load(feature_file)
        print("val_accuracy ", features['val_accuracy'])
        print("test_accuracy ", features['test_accuracy'])
    else:
        extract_features_cross(model_file, out, args.feature_file_name, frames_path, annotation_path,
                         pad_frames=pad_frames, normalize=normalize, anno =args.anno, fold = args.fold)

    # test
    data = torch.load(feature_file)

    print(data['train'].keys())
    print(data['val'].keys())
    print(data['test'].keys())
    # print(data['labels'].keys())

    video_id = list(data['train'].keys())[0]
    print(data['train'][video_id].shape)
    print(data['labels'][video_id].shape)

    print(data['predictions'].keys())
    print(data['predictions'][video_id].shape)

