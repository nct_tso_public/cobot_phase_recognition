 
""" 
training CNN (Resnets or AlexNet) with CoBot
"""

import argparse
import datetime
import os.path

import torch
import torch.nn as nn
import torch.optim as optim
import torchvision
from torch.optim import lr_scheduler

import utils
from datasets import PhaseData_CoBot_
from models import ResNet50LSTM
from utils import AverageMeter, CoBot, DataPrep3, accuracy, average_F1
import cv2
import torchvision.transforms as transforms
import albumentations as A
from albumentations.pytorch import ToTensorV2
import imgaug
import numpy as np


def get_split(fold):
    split = None
    if fold == 1:
        split = {
        'train': [9, 10, 12, 13, 14, 15, 16, 18, 19, 20, 21, 22, 23, 24, 25, 27, 28, 29, 30, 31, 32],
        'val': [8,17,26,11],
        'test': list(range(0,8))
    }
    elif fold == 2:
        split = {
        'train': [0, 1, 3, 4, 5, 6, 7, 17, 18, 20, 21, 22, 23, 24, 26, 27, 28, 29, 30, 31, 32],
        'val': [16,25,2,19],
        'test': list(range(8,16))
    }
    elif fold == 3:
        split = {
        'train': [0, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 25, 26, 28, 29, 30, 31, 32],
        'val': [24,1,10,27],
        'test': list(range(16,24))
    }
    elif fold == 4:
        split = {
        'train':[1, 2, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20, 21, 22, 23],
        'val': [0,9,18,3],
        'test': list(range(24,33))
    }
    else:
        raise RuntimeError("Not a fold!")
    return split


## model mit endovVis trainiert
## /mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/EndoVis18/out/Phase_segmentation_resnet50/no_pretrain/20210225-1242/model.pkl
VIDEOS = ['20190917','20191001','20191011','20191211','20200122','20200129','20200221','20200317',
            '20200330','20200407','20200416','20200423','20200427','20200710','20200717','20200727','20201002','20201006', '20201201','20201204',
            '20201208','20190214','20200611','20200123','20200302','20200504','20200622','20201017', '20200731','20200827','20191206', '20210113', '20200212']
#1 [1.81178732 0.44682263 1.41549577 0.27488215 1.05101214]
#2 [1.66680781 0.44370434 1.16201695 0.2778665  1.4496044 ]
#3
#4
  
def main(args):
    transform_train = A.Compose([
        A.RandomBrightnessContrast(brightness_limit=0.1, contrast_limit=0.1, brightness_by_max=True, p=1.0),
        A.OneOf([A.MotionBlur(blur_limit=5, p=0.2),
                 A.GaussNoise(var_limit=(5.0, 75.0), mean=0, p=0.8)], p=0.5),
        A.ShiftScaleRotate(shift_limit=0.1, scale_limit=0.15, rotate_limit=15,
                           interpolation=cv2.INTER_CUBIC, border_mode=cv2.BORDER_CONSTANT, value=0, p=0.8),
        A.ElasticTransform(alpha=1, sigma=50, alpha_affine=7, approximate=True,
                           interpolation=cv2.INTER_CUBIC, border_mode=cv2.BORDER_CONSTANT, value=0, p=0.2),

        A.Resize(height=256, width=320, interpolation=cv2.INTER_CUBIC, always_apply=True, p=1.0),
        A.RandomCrop(224, 224, always_apply=True, p=1.0),
        A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225), max_pixel_value=255.0,
                    always_apply=True, p=1.0),
        ToTensorV2(transpose_mask=True, always_apply=True, p=1.0),
    ], p=0.5)

    transform_test = A.Compose([
        A.Resize(height=256, width=320, interpolation=cv2.INTER_CUBIC, always_apply=True, p=1.0),
        A.CenterCrop(224, 224, always_apply=True, p=1.0),
        A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225), max_pixel_value=255.0,
                    always_apply=True, p=1.0),
        ToTensorV2(transpose_mask=True, always_apply=True, p=1.0),
    ], p=1.0)

    assert(torch.cuda.is_available())
    device_gpu = torch.device("cuda:0")
    device_cpu = torch.device("cpu")


    trial_id = datetime.datetime.now().strftime("%Y%m%d-%H%M")

    if args.exp is not None:
        trial_id = args.exp
    experiment = os.path.join("CNN_LSTM_exp",trial_id,str(args.fold))
    
    
    output_folder = os.path.join(utils.out_path, experiment)

    anno_path = utils.annotation_path
    if args.annotation == "phase":
        num_class = 5        
    elif args.annotation == "step":
        num_class = 5 + 4
        #anno_path = utils.annotation_path_step
        output_folder = os.path.join("/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps", experiment)
    else:
        raise NotImplementedError("not available as annotation")
        

    os.makedirs(output_folder)
    f_log = open(os.path.join(output_folder, "log.txt"), "a")
    def log(msg):
        utils.log(f_log, msg)

    model_file = os.path.join(output_folder, "model.pkl")
    model_file_final = os.path.join(output_folder, "model_final.pkl")

    log("Run <%s> on device %s" % (experiment, str(device_gpu)))
    log("Used parameters...")
    for arg in vars(args):
        log("\t" + str(arg) + " : " + str(getattr(args, arg)))

    log("Loading data...")

    split = get_split(args.fold)

    train_data = []
    log("\t train data")
    itt = 1
    for id in split['train']:
        op = VIDEOS[id]
        op_path = os.path.join(utils.frames_path, op)
        if os.path.isdir(op_path):
            log(op_path)
            step_file = None
            if args.annotation == "step":
                step_file = os.path.join(utils.annotation_path_step, op + "step.txt")
            anno_file = os.path.join(anno_path, op + ".txt")
            dataset = PhaseData_CoBot_(op_path, anno_file, DataPrep3.width, DataPrep3.height, transform_train,annotation = args.annotation, step_file = step_file, sample_rate = args.sample_rate)
            trainloader = torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False,num_workers=4)
            train_data.append(trainloader)
            if itt == 5:
                break
            itt += 1


 

    pretrained_ResNet50 = None
    pretrained_LSTM = None
    if args.pretrained_ResNet50 is not None:
    	pretrained_ResNet50 = os.path.join(args.pretrained_ResNet50, str(args.fold), "model.pkl")
    if args.pretrained_LSTM is not None:
    	pretrained_LSTM = os.path.join(args.pretrained_LSTM, str(args.fold), "model.pkl")

    model = ResNet50LSTM(num_classes = num_class, pretrained_ResNet50 = pretrained_ResNet50, pretrained_LSTM = pretrained_LSTM, hidden_size = args.lstm_size, lstm_layers = args.lstm_layers)


    # freeze some parameter in ResNet50
    if args.freeze_layer or args.freeze_res:
        for param in model.res.model.parameters():
            param.requires_grad = False
        #for param in model.model.layer3.parameters():
         #   param.requires_grad = True
        if not args.freeze_res:
        	print("train last layers of res")
	        for param in model.res.model.layer4.parameters():
	            param.requires_grad = True
	        for param in model.res.model.fc.parameters():
	            param.requires_grad = True


    model = model.to(device_gpu)
    we = utils.class_weights_steps if args.annotation == "step" else utils.class_weights_phases
    print(we)

    we = torch.from_numpy(we).float()
    we = we.to(device_gpu)
    criterion = nn.CrossEntropyLoss(weight=we)
    optimizer = optim.Adam(params=filter(lambda p: p.requires_grad, model.parameters()), lr=args.lr)

    step_size = max(1, int(args.epochs // 3))
    scheduler = lr_scheduler.StepLR(optimizer, step_size=step_size, gamma=0.5)

    torch.backends.cudnn.benchmark = True
    torch.backends.cudnn.enabled = True
    #log("lr_scheduler.StepLR(optimizer, step_size=50, gamma=0.1)")

    log("Begin training...")
    correct = 0
    total = 0
        # Validation
    model.eval()
    val_loss = 0

    eval_loss = AverageMeter()
    eval_acc = AverageMeter()
    epoch = 0
    with torch.no_grad():
        for v in train_data:
            hidden_state = model.init_hidden(device_gpu)
            for loader in v:
                data,target_cpu = loader
                batch_size = target_cpu.size(0)
                data = data.to(device_gpu)
                target = target_cpu.to(device_gpu)
                output,hidden_state = model(data,hidden_state)
                loss = criterion(output, target)

                predicted = torch.nn.Softmax(dim=-1)(output)
                _, predicted = torch.max(predicted, dim=-1, keepdim=False)

                eval_loss.update(loss, batch_size)
                acc = (predicted == target).sum().item() / batch_size
                eval_acc.update(acc, batch_size)

        mode = "Train"
        log("Epoch {}: Val({}) -- ce loss: {:.4f} acc: {:.3f}".format(epoch + 1, mode, eval_loss.avg, eval_acc.avg))

    
        
    #log('best Val Model: Test Accuracy : %d %%' % (100 * correct / total))
    f_log.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Run AlexNet or ResnNet for surgical phase recognition.")
    parser.add_argument("--lr", type=float, default=0.00001, help="The learning rate.")
    parser.add_argument("--batch_size", type=int, default=256, help="The batch size.") #128
    parser.add_argument("--freeze_layer", type = bool, default=True, help="Freeze layers in ResNet50.") 
    parser.add_argument("--freeze_res", type = bool, default=False, help="Freeze layers in ResNet50.") 
    parser.add_argument("--epochs", type=int, default=1, help="The maximal number of epochs to train.")
    parser.add_argument("--save_freq", type=int, default=10,
                        help="Defines after how many epochs the current model parameters are saved.")
    parser.add_argument("--annotation", type=str, default="phase" , choices=["phase","step"],
                        help="phases or steps annotation")
    parser.add_argument("--sample_rate", type=int, default=1,
                        help="Defines in which rate the pictures are taken 1-> every pic 2-> every 2. pic.")
    parser.add_argument("--model", type=str, default=None ,
                        help="pretrained Model")
    parser.add_argument("--fold", type=int, default=1, choices = [1,2,3,4], help=".")
    parser.add_argument("--opt_step", type=int, default=3, help=".")
    parser.add_argument("--exp", type=str, default=None, help=".")
    parser.add_argument("--batch_factor", type=int, default=2, help=".")
    parser.add_argument("--lstm_size", type=int, default=256, help=".")
    parser.add_argument("--lstm_layers", type=int, default=1, help=".")
    parser.add_argument("--pretrained_ResNet50", type=str, default=None, help="pretrained Model")
    #steps: /media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442
    #phase: /media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359
    parser.add_argument("--pretrained_LSTM", type=str, default=None, help="pretrained Model")
    #steps: /mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/LSTM_new/256_20210609-1258
    #phase:/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/LSTM_new/256_20210609-1249"

    args = parser.parse_args()
    main(args)