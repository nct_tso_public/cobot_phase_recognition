"""
Specifics of CoBot and Cholec80
Transformations
Pathes
"""
from __future__ import print_function

import os.path
import time
from sys import stderr
import numpy as np
import argparse

import torchvision.transforms as transforms
import torchvision.transforms.functional as TF

import scipy
#from numba import jit

import sklearn.metrics as sm

from collections import OrderedDict

def _fmt(m):
    return "%2.1f" % (m * 100)
def eval_(predictions_path, num_phases = 7):
    if not os.path.exists(predictions_path):
        print("Unable to read predictions from %s." % predictions_path)
        return
    #f = open(outfile, "w")
    #f.write("OP id,Acc.")

    #for p in range(num_phases):
    #    f.write(",P" + str(p + 1) + " Rec.,P" + str(p + 1) + " Prec.,P" + str(p + 1) + " F1")
    #f.write(",Avg. Rec.,Avg. Prec.,Avg. F1\n")

    #print("Eval %s..." % predictions_path)

    acc = []
    recall = []
    precision = []
    f1 = []

    per_phase_recall = []
    per_phase_precision = []
    per_phase_f1 = []
    for i in range(num_phases):
        per_phase_recall.append([])
        per_phase_precision.append([])
        per_phase_f1.append([])

    for file in sorted(os.listdir(predictions_path)):
        data = np.loadtxt(os.path.join(predictions_path, file), delimiter=",")
        results = np.zeros([num_phases, 3], dtype=np.int64)
        for d in data:
            pred = int(d[0])
            label = int(d[1])

            if pred == label:
                results[pred, 0] += 1  # True positive
            else:
                results[pred, 1] += 1  # False positive
                results[label, 2] += 1  # False negative

        op_id = file.split('.')[0]
        #f.write(op_id + ",")
        op_acc = np.sum(results[:, 0]) / len(data)
        #f.write(_fmt(op_acc))

        op_recall = []
        op_precision = []
        op_f1 = []
        for p in range(num_phases):
            TP = results[p, 0]
            FP = results[p, 1]
            FN = results[p, 2]
            if TP + FN > 0:
                p_recall = TP / (TP + FN)
                p_precision = 0
                p_f1 = 0
                if TP > 0:
                    p_precision = TP / (TP + FP)
                    p_f1 = (2 * p_precision * p_recall) / (p_precision + p_recall)
                #f.write("," + _fmt(p_recall) + "," + _fmt(p_precision) + "," + _fmt(p_f1))

                op_recall.append(p_recall)
                op_precision.append(p_precision)
                op_f1.append(p_f1)

                per_phase_recall[p].append(p_recall)
                per_phase_precision[p].append(p_precision)
                per_phase_f1[p].append(p_f1)
#            else:
#                f.write(",,,")
        op_avg_recall = np.mean(op_recall)
        op_avg_precision = np.mean(op_precision)
        op_avg_f1 = np.mean(op_f1)
        #f.write("," + _fmt(op_avg_recall) + "," + _fmt(op_avg_precision) + "," + _fmt(op_avg_f1) + "\n")

        acc.append(op_acc)
        recall.append(op_avg_recall)
        precision.append(op_avg_precision)
        f1.append(op_avg_f1)
    #f.close()

    # print overall metrics
    print("Acc. " + _fmt(np.mean(acc)) + "+-" + _fmt(np.std(acc)) +
          ", Rec. " + _fmt(np.mean(recall)) + "+-" + _fmt(np.std(recall)) +
          ", Prec. " + _fmt(np.mean(precision)) + "+-" + _fmt(np.std(precision)) +
          ", F1 " + _fmt(np.mean(f1)) + "+-" + _fmt(np.std(f1)))

def eval__(predictions_path, num_phases = 7):
    if not os.path.exists(predictions_path):
        print("Unable to read predictions from %s." % predictions_path)
        return
    #f = open(outfile, "w")
    #f.write("OP id,Acc.")

    #for p in range(num_phases):
    #    f.write(",P" + str(p + 1) + " Rec.,P" + str(p + 1) + " Prec.,P" + str(p + 1) + " F1")
    #f.write(",Avg. Rec.,Avg. Prec.,Avg. F1\n")

    #print("Eval %s..." % predictions_path)

    acc = []
    recall = []
    precision = []
    f1 = []

    per_phase_recall = []
    per_phase_precision = []
    per_phase_f1 = []
    for i in range(num_phases):
        per_phase_recall.append([])
        per_phase_precision.append([])
        per_phase_f1.append([])

    for file in sorted(os.listdir(predictions_path)):
        data = np.loadtxt(os.path.join(predictions_path, file), delimiter=",")
        results = np.zeros([num_phases, 3], dtype=np.int64)
        for d in data:
            pred = int(d[0])
            label = int(d[1])

            if pred == label:
                results[pred, 0] += 1  # True positive
            else:
                results[pred, 1] += 1  # False positive
                results[label, 2] += 1  # False negative

        op_id = file.split('.')[0]
        #f.write(op_id + ",")
        op_acc = np.sum(results[:, 0]) / len(data)
        #f.write(_fmt(op_acc))

        op_recall = []
        op_precision = []
        op_f1 = []
        for p in range(num_phases):
            TP = results[p, 0]
            FP = results[p, 1]
            FN = results[p, 2]
            if TP + FN > 0:
                p_recall = TP / (TP + FN)
                p_precision = 0
                p_f1 = 0
                if TP > 0:
                    p_precision = TP / (TP + FP)
                    p_f1 = (2 * p_precision * p_recall) / (p_precision + p_recall)
                #f.write("," + _fmt(p_recall) + "," + _fmt(p_precision) + "," + _fmt(p_f1))

                op_recall.append(p_recall)
                op_precision.append(p_precision)
                op_f1.append(p_f1)

                per_phase_recall[p].append(p_recall)
                per_phase_precision[p].append(p_precision)
                per_phase_f1[p].append(p_f1)
#            else:
#                f.write(",,,")
        op_avg_recall = np.mean(op_recall)
        op_avg_precision = np.mean(op_precision)
        op_avg_f1 = np.mean(op_f1)
        #f.write("," + _fmt(op_avg_recall) + "," + _fmt(op_avg_precision) + "," + _fmt(op_avg_f1) + "\n")

        acc.append(op_acc)
        recall.append(op_avg_recall)
        precision.append(op_avg_precision)
        f1.append(op_avg_f1)
    #f.close()

    # print overall metrics
    print("Acc. " + _fmt(np.mean(acc)) + "+-" + _fmt(np.std(acc)) +
          ", Rec. " + _fmt(np.mean(recall)) + "+-" + _fmt(np.std(recall)) +
          ", Prec. " + _fmt(np.mean(precision)) + "+-" + _fmt(np.std(precision)) +
          ", F1 " + _fmt(np.mean(f1)) + "+-" + _fmt(np.std(f1)))
    return _fmt(np.mean(f1))

def debug_memory():
    import collections
    import gc
    import resource

    import torch
    print('maxrss = {}'.format(
        resource.getrusage(resource.RUSAGE_SELF).ru_maxrss))
    tensors = collections.Counter((str(o.device), o.dtype, tuple(o.shape))
                                  for o in gc.get_objects()
                                  if torch.is_tensor(o))
    for line in sorted(tensors.items()):
        print('{}\t{}'.format(*line))



class EndoVis18:

    num_phases = 14
    """Number of surgical phases."""

    phase_map = dict()
    phase_map['0'] = 0
    phase_map['1'] = 1
    phase_map['2'] = 2
    phase_map['3'] = 3
    phase_map['4'] = 4
    phase_map['5'] = 5
    phase_map['6'] = 6
    phase_map['7'] = 7
    phase_map['8'] = 8
    phase_map['9'] = 9
    phase_map['10'] = 10
    phase_map['11'] = 11
    phase_map['12'] = 12
    phase_map['13'] = 13


class CoBot:
    """Define Parameters related to CoBot dataset""" 
    #videos with annotation
    phase_map = dict()
    phase_map['Preparation and intraabdominal orientation'] = 0
    phase_map['Mobilization of colon (medial)'] = 1
    phase_map['Mobilization of colon (lateral)'] = 2
    phase_map['Total mesorectal excision and dissection of rectum'] = 3
    phase_map['Extraabdominal preparation of anastomosis'] = 4

    videos = ['20190917','20191001','20191011','20191211','20200122','20200129','20200221','20200317',
            '20200330','20200407','20200416','20200423','20200427','20200504','20200622','20201017',
            '20200710','20200717','20200727','20200731','20201002','20201006', '20201201','20201204', '20200827']

    videos_train = ['20190917','20191001','20191011','20191211','20200122','20200129','20200221','20200317',
            '20200330','20200407','20200416','20200423','20200427','20200710','20200717','20200727','20201002','20201006', '20201201','20201204',
            '20201208','20190214','20200611','20200123','20200302']
          
    videos_test = ['20200504','20200622','20201017', '20200731','20200827',
                '20191206', '20210113', '20200212']

    phase_map_old = dict()
    phase_map_old['Preparation and intraabdominal orientation'] = 0
    phase_map_old['Mobilization of colon (medial)'] = 1
    phase_map_old['Mobilization of colon (lateral)'] = 2
    phase_map_old['Completion of colon mobilization'] = 3 #sehr selten
    phase_map_old['Total mesorectal excision and dissection of rectum'] = 4
    phase_map_old['Extraabdominal preparation of anastomosis'] = 5
    '''step_map = dict()
    step_map['Trocar placement'] = 1
    step_map['Intraabdominal orientation'] = 2
    step_map['Peritoneal Incision (medial)'] = 1 #4
    step_map['Preparation/Clipping/Dissection of Inferior Mesenteric Artery'] = 2 #5
    step_map['Separation of mesocolon and Gerota\\342\\200\\231s fascia (medial)'] = 3 #6
    step_map['Preparation/Clipping/Dissection of Inferior Mesenteric Vein'] = 4 #7
    step_map['Peritoneal Incision (lateral)'] = 1 #9
    step_map['Separation of mesocolon and Gerota\\342\\200\\231s fascia (lateral)'] = 2 #10
    step_map['Peritoneal incision superior to peritoneal fold'] = 1 #12
    step_map['High mesorectal dissection'] = 2 #13
    step_map['Low mesorectal dissection'] = 3 #14
    step_map['Linear stapling of rectum'] = 4 #15

    phase_map_step = dict()
    phase_map_step['Preparation and intraabdominal orientation'] = 0
    phase_map_step['Mobilization of colon (medial)'] = 3
    phase_map_step['Mobilization of colon (lateral)'] = 8
    phase_map_step['Total mesorectal excision and dissection of rectum'] = 11
    phase_map_step['Extraabdominal preparation of anastomosis'] = 16'''

    step_map = dict()
    step_map['Peritoneal Incision (medial)'] = 2 #2
    step_map['Preparation/Clipping/Dissection of Inferior Mesenteric Artery'] = 3 #3
    step_map['Separation of mesocolon and Gerota\\342\\200\\231s fascia (medial)'] = 4 #4
    step_map['Preparation/Clipping/Dissection of Inferior Mesenteric Vein'] = 5 #5

    phase_map_step = dict()
    phase_map_step['Preparation and intraabdominal orientation'] = 0
    phase_map_step['Mobilization of colon (medial)'] = 1
    phase_map_step['Mobilization of colon (lateral)'] = 6
    phase_map_step['Total mesorectal excision and dissection of rectum'] = 7
    phase_map_step['Extraabdominal preparation of anastomosis'] = 8


    

    phase_map_step_combi = dict()
    phase_map_step_combi['Preparation and intraabdominal orientation'] = 0
    phase_map_step_combi['Mobilization of colon (medial)'] = 1
    phase_map_step_combi['Peritoneal Incision (medial)'] = 2 #2
    phase_map_step_combi['Preparation/Clipping/Dissection of Inferior Mesenteric Artery'] = 3 #3
    phase_map_step_combi['Separation of mesocolon and Gerota\\342\\200\\231s fascia (medial)'] = 4 #4
    phase_map_step_combi['Preparation/Clipping/Dissection of Inferior Mesenteric Vein'] = 5 #5
    phase_map_step_combi['Mobilization of colon (lateral)'] = 6
    phase_map_step_combi['Total mesorectal excision and dissection of rectum'] = 7
    phase_map_step_combi['Extraabdominal preparation of anastomosis'] = 8


    #train_sets_old = #[0,1,2,3,4,5,6,7,8,9,10,11,12] 
    #train_sets = #[0,1,2,3,4,5,6,7,8,9,10,11,12,16,17,18,20,21,22,23] 
    #test_sets = [13,14,15,19,24] 
    #test_sets_old = [13,14,15]
    fps_video = 29.97 

    
class Cholec80:
    """Define parameters related to the Cholec80 dataset."""

    num_phases = 7
    """Number of surgical phases."""

    phase_map = dict()
    phase_map['Preparation'] = 0
    phase_map['CalotTriangleDissection'] = 1
    phase_map['ClippingCutting'] = 2
    phase_map['GallbladderDissection'] = 3
    phase_map['GallbladderPackaging'] = 4
    phase_map['CleaningCoagulation'] = 5
    phase_map['GallbladderRetraction'] = 6

    op_sets = ['A', 'B', 'C', 'D']
    op_set_size = 20
    videos_in_set = [['02','04','06','12','24','29','34','37','38','39','44','58','60','61','64','66','75','78','79','80'],
                    ['01','03','05','09','13','16','18','21','22','25','31','36','45','46','48','50','62','71','72','73'],
                    ['10','15','17','20','32','41','42','43','47','49','51','52','53','55','56','69','70','74','76','77'],
                    ['07','08','11','14','19','23','26','27','28','30','33','35','40','54','57','59','63','65','67','68']]
    train_sets = op_sets[:2]
    test_sets = op_sets[2:]


class DataPrep:
    """Define parameters related to data preparation."""

    sample_rate = 1
    """Sample rate in Hz (fps) used for video frame extraction."""

    width = 384
    """Width in pixels of each extracted video frame."""

    height = 216
    """Height in pixels of each extracted video frame."""

    standard_transform = transforms.Compose(
        [transforms.ToTensor(),
         transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])])
    """Image transformation applied to each extracted video frame."""
class DataPrep_tr:
    """Define parameters related to data preparation."""

    sample_rate = 1
    """Sample rate in Hz (fps) used for video frame extraction."""

    width = 384
    """Width in pixels of each extracted video frame."""

    height = 216
    """Height in pixels of each extracted video frame."""

    standard_transform = transforms.Compose(
        [transforms.Resize(size = (216,216)),
        transforms.ToTensor(),
         transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])])
    """Image transformation applied to each extracted video frame."""

class DataPrep_EndoVis:
    """Define parameters related to data preparation."""

    sample_rate = 1
    """Sample rate in Hz (fps) used for video frame extraction."""

    width = 455
    """Width in pixels of each extracted video frame."""

    height = 256
    """Height in pixels of each extracted video frame."""

    standard_transform = transforms.Compose(
        [transforms.CenterCrop(224),transforms.ToTensor(),
         transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])])
    """Image transformation applied to each extracted video frame."""

class DataPrep3:
    """Define parameters related to data preparation."""

    sample_rate = 1
    """Sample rate in Hz (fps) used for video frame extraction."""

    width = 384
    """Width in pixels of each extracted video frame."""

    height = 216
    """Height in pixels of each extracted video frame."""

    standard_transform = transforms.Compose(
        [transforms.Resize(256),
    transforms.CenterCrop(224),transforms.ToTensor(),
         transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])])
    """Image transformation applied to each extracted video frame."""

class MyCrop:
    """Crop """

    def __init__(self, top, left, height, width):
        self.top = top
        self.left = left
        self.height = height
        self.width = width

    def __call__(self, x):
        return TF.crop(x, self.top, self.left, self.height, self.width)

class DataPrepCoBot:
    """Define parameters related to data preparation."""

    sample_rate = 1  # 1 frame every 2 seconds
    """Sample rate in Hz (fps) used for video frame extraction."""

    width = 224
    """Width in pixels of each extracted video frame."""

    height = 224
    """Height in pixels of each extracted video frame."""

    """eraseBlackBorder = False # if true pics need to be in format 720 x 1280
    if eraseBlackBorder:
        standard_transform = transforms.Compose(
        [MyCrop(22, 218, 675, 845), 
        transforms.Resize(256),
    transforms.CenterCrop(224),transforms.ToTensor(),
         transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])])
    else:"""

    standard_transform = transforms.Compose(
        [transforms.CenterCrop(224),transforms.ToTensor(),
         transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])])


    
    """Image transformation applied to each extracted video frame."""

def log(file, msg):
    """Log a message.


    :param file: File object to which the message will be written.
    :param msg:  Message to log (str).
    """
    print(time.strftime("[%d.%m.%Y %H:%M:%S]: "), msg, file=stderr)
    file.write(time.strftime("[%d.%m.%Y %H:%M:%S]: ") + msg + os.linesep)

class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


class ComputeMetrics:

    metric_types = ["accuracy", "average_F1", "edit_score", "overlap_f1"]
    # metric_types = ["macro_accuracy", "acc_per_class"]
    # metric_types += ["classification_accuracy"]
    # metric_types += ["precision", "recall"]
    # metric_types += ["mAP1", "mAP5", "midpoint"]
    trials = []


    def __init__(self, metric_types=None, overlap=.1, bg_class=None, n_classes=None):
        if metric_types is not None:
            self.metric_types = metric_types

        self.scores = OrderedDict()
        self.attrs = {"overlap":overlap, "bg_class":bg_class, "n_classes":n_classes}
        self.trials = []

        for m in self.metric_types:
            self.scores[m] = OrderedDict()

    @property
    def n_classes(self):
        return self.attrs['n_classes']

    def set_classes(self, n_classes):
        self.attrs['n_classes'] = n_classes

    def add_predictions(self, trial, P, Y):
        if trial not in self.trials:
            self.trials += [trial]

        for m in self.metric_types:
            self.scores[m][trial] = globals()[m](P, Y, **self.attrs)

    def print_trials(self, metric_types=None):
        if metric_types is None:
            metric_types = self.metric_types

        for trial in self.trials:
            scores = [self.scores[m][trial] for m in metric_types]
            scores_txt = []
            for m,s in zip(metric_types, scores):
                if type(s) is np.float64:
                    scores_txt += ["{}:{:.04}".format(m, s)]
                else:
                    scores_txt += [("{}:[".format(m)+"{:.04},"*len(s)).format(*s)+"]"]
            # txt = "Trial {}: ".format(trial) + " ".join(["{}:{:.04}".format(metric_types[i], scores[i]) for i in range(len(metric_types))])
            txt = "Trial {}: ".format(trial) + ", ".join(scores_txt)
            print(txt)


    def print_scores(self, metric_types=None):
        if metric_types is None:
            metric_types = self.metric_types

        scores = [np.mean([self.scores[m][trial] for trial in self.trials]) for m in metric_types]
        txt = "All: " + " ".join(["{}:{:.04}".format(metric_types[i], scores[i]) for i in range(len(metric_types))])
        print(txt)



def accuracy(P, Y, **kwargs):
    def acc_(p,y):
        return np.mean(p==y)*100
    if type(P) == list:
        return np.mean([np.mean(P[i]==Y[i]) for i in range(len(P))])*100
    else:
        return acc_(P,Y)

def average_F1(P, Y, n_classes, **kwargs):
    if type(P) == list:
        tmp = [average_F1(P[i], Y[i], n_classes) for i in range(len(P))]
        return np.mean(tmp)
    else:
        # return sm.f1_score(Y, P, average='macro')

        conf_mat = np.zeros([n_classes, n_classes], dtype=np.int64)

        results = np.zeros([n_classes, 3], dtype=np.int64)
        for pred, label in zip(P, Y):

            conf_mat[label, pred] += 1

            if pred == label:
                results[pred, 0] += 1  # True positive
            else:
                results[pred, 1] += 1  # False positive
                results[label, 2] += 1  # False negative

        avg_precision = []
        avg_recall = []
        avg_f1 = []
        for p in range(n_classes):
            TP = results[p, 0]
            FP = results[p, 1]
            FN = results[p, 2]
            if TP + FN > 0:
                p_recall = TP / (TP + FN)
                p_precision = 0
                p_f1 = 0
                if TP > 0:
                    p_precision = TP / (TP + FP)
                    p_f1 = (2 * p_precision * p_recall) / (p_precision + p_recall)
                avg_precision.append(p_precision)
                avg_recall.append(p_recall)
                avg_f1.append(p_f1)
        return np.mean(avg_f1)*100, np.mean(avg_precision)*100, np.mean(avg_recall)*100, conf_mat

def macro_accuracy(P, Y, n_classes, bg_class=None, return_all=False, **kwargs):
    def macro_(P, Y, n_classes=None, bg_class=None, return_all=False):
        conf_matrix = sm.confusion_matrix(Y, P, labels=np.arange(n_classes))
        conf_matrix = conf_matrix/(conf_matrix.sum(0)[:,None]+1e-5)
        conf_matrix = np.nan_to_num(conf_matrix)
        diag = conf_matrix.diagonal()*100.

        # Remove background score
        if bg_class is not None:
            diag = np.array([diag[i] for i in range(n_classes) if i!=bg_class])

        macro = diag.mean()
        if return_all:
            return macro, diag
        else:
            return macro

    if type(P) == list:
        out = [macro_(P[i], Y[i], n_classes=n_classes, bg_class=bg_class, return_all=return_all) for i in range(len(P))]
        if return_all:
            return (np.mean([o[0] for o in out]), np.mean([o[1] for o in out],0))
        else:
            return np.mean(out)
    else:
        return macro_(P,Y, n_classes=n_classes, bg_class=bg_class, return_all=return_all)


def acc_per_class(P, Y, bg_class=None,n_classes=None, **kwargs):
    return macro_accuracy(P, Y, bg_class=bg_class, return_all=True, n_classes=n_classes, **kwargs)[1]


# ---------------------------------------------------
def classification_accuracy(P, Y, bg_class=None, **kwargs):
    # Assumes known temporal segmentation
    # P can either be a set of predictions (1d) or scores (2d)
    def clf_(p, y, bg_class):
        sums = 0.
        n_segs = 0.

        S_true = segment_labels(y)
        I_true = np.array(segment_intervals(y))

        for i in range(len(S_true)):
            if S_true[i] == bg_class:
                continue

            # If p is 1d, compute the most likely label, otherwise take the max over the score
            if p.ndim==1:
                pred_label = scipy.stats.mode(p[I_true[i][0]:I_true[i][1]])[0][0]
            else:
                pred_label = p[I_true[i][0]:I_true[i][1]].mean(1).argmax()
            sums += pred_label==S_true[i]
            n_segs += 1

        return sums / n_segs * 100

    if type(P) == list:
        return np.mean([clf_(P[i], Y[i], bg_class) for i in range(len(P))])
    else:
        return clf_(P, Y, bg_class)


#@jit("float64(int64[:], int64[:], boolean)")
def levenstein_(p,y, norm=False):
    m_row = len(p)    
    n_col = len(y)
    D = np.zeros([m_row+1, n_col+1], np.float)
    for i in range(m_row+1):
        D[i,0] = i
    for i in range(n_col+1):
        D[0,i] = i

    for j in range(1, n_col+1):
        for i in range(1, m_row+1):
            if y[j-1]==p[i-1]:
                D[i,j] = D[i-1,j-1] 
            else:
                D[i,j] = min(D[i-1,j]+1,
                             D[i,j-1]+1,
                             D[i-1,j-1]+1)
    
    if norm:
        score = (1 - D[-1,-1]/max(m_row, n_col) ) * 100
    else:
        score = D[-1,-1]

    return score

def edit_score(P, Y, norm=True, bg_class=None, **kwargs):
    if type(P) == list:
        tmp = [edit_score(P[i], Y[i], norm, bg_class) for i in range(len(P))]
        return np.mean(tmp)
    else:
        P_ = segment_labels(P)
        Y_ = segment_labels(Y)
        if bg_class is not None:
            P_ = [c for c in P_ if c!=bg_class]
            Y_ = [c for c in Y_ if c!=bg_class]
        return levenstein_(P_, Y_, norm)


def overlap_f1(P, Y, n_classes=0, bg_class=None, overlap=.1, **kwargs):
    def overlap_(p,y, n_classes, bg_class, overlap):

        true_intervals = np.array(segment_intervals(y))
        true_labels = segment_labels(y)
        pred_intervals = np.array(segment_intervals(p))
        pred_labels = segment_labels(p)

        # Remove background labels
        if bg_class is not None:
            true_intervals = true_intervals[true_labels!=bg_class]
            true_labels = true_labels[true_labels!=bg_class]
            pred_intervals = pred_intervals[pred_labels!=bg_class]
            pred_labels = pred_labels[pred_labels!=bg_class]

        n_true = true_labels.shape[0]
        n_pred = pred_labels.shape[0]

        # We keep track of the per-class TPs, and FPs.
        # In the end we just sum over them though.
        TP = np.zeros(n_classes, np.float)
        FP = np.zeros(n_classes, np.float)
        true_used = np.zeros(n_true, np.float)

        for j in range(n_pred):
            # Compute IoU against all others
            intersection = np.minimum(pred_intervals[j,1], true_intervals[:,1]) - np.maximum(pred_intervals[j,0], true_intervals[:,0])
            union = np.maximum(pred_intervals[j,1], true_intervals[:,1]) - np.minimum(pred_intervals[j,0], true_intervals[:,0])
            IoU = (intersection / union)*(pred_labels[j]==true_labels)

            # Get the best scoring segment
            idx = IoU.argmax()

            # If the IoU is high enough and the true segment isn't already used
            # Then it is a true positive. Otherwise is it a false positive.
            if IoU[idx] >= overlap and not true_used[idx]:
                TP[pred_labels[j]] += 1
                true_used[idx] = 1
            else:
                FP[pred_labels[j]] += 1


        TP = TP.sum()
        FP = FP.sum()
        # False negatives are any unused true segment (i.e. "miss")
        FN = n_true - true_used.sum()
        
        precision = TP / (TP+FP)
        recall = TP / (TP+FN)
        F1 = 2 * (precision*recall) / (precision+recall)

        # If the prec+recall=0, it is a NaN. Set these to 0.
        F1 = np.nan_to_num(F1)

        return F1*100

    if type(P) == list:
        return np.mean([overlap_(P[i],Y[i], n_classes, bg_class, overlap) for i in range(len(P))])
    else:
        return overlap_(P, Y, n_classes, bg_class, overlap)


def overlap_score(P, Y, bg_class=None, **kwargs):
    # From ICRA paper:
    # Learning Convolutional Action Primitives for Fine-grained Action Recognition
    # Colin Lea, Rene Vidal, Greg Hager 
    # ICRA 2016

    def overlap_(p,y, bg_class):
        true_intervals = np.array(segment_intervals(y))
        true_labels = segment_labels(y)
        pred_intervals = np.array(segment_intervals(p))
        pred_labels = segment_labels(p)

        if bg_class is not None:
            true_intervals = np.array([t for t,l in zip(true_intervals, true_labels) if l!=bg_class])
            true_labels = np.array([l for l in true_labels if l!=bg_class])
            pred_intervals = np.array([t for t,l in zip(pred_intervals, pred_labels) if l!=bg_class])
            pred_labels = np.array([l for l in pred_labels if l!=bg_class])            

        n_true_segs = true_labels.shape[0]
        n_pred_segs = pred_labels.shape[0]
        seg_scores = np.zeros(n_true_segs, np.float)

        for i in range(n_true_segs):
            for j in range(n_pred_segs):
                if true_labels[i]==pred_labels[j]:
                    intersection = min(pred_intervals[j][1], true_intervals[i][1]) - max(pred_intervals[j][0], true_intervals[i][0])
                    union        = max(pred_intervals[j][1], true_intervals[i][1]) - min(pred_intervals[j][0], true_intervals[i][0])
                    score_ = float(intersection)/union
                    seg_scores[i] = max(seg_scores[i], score_)

        return seg_scores.mean()*100

    if type(P) == list:
        return np.mean([overlap_(P[i],Y[i], bg_class) for i in range(len(P))])
    else:
        return overlap_(P, Y, bg_class)


# ------------- Segment functions (from utils.py) -------------


def segment_labels(Yi):
    idxs = [0] + (np.nonzero(np.diff(Yi))[0]+1).tolist() + [len(Yi)]
    Yi_split = np.array([Yi[idxs[i]] for i in range(len(idxs)-1)])
    return Yi_split

def segment_data(Xi, Yi):
    idxs = [0] + (np.nonzero(np.diff(Yi))[0]+1).tolist() + [len(Yi)]
    Xi_split = [np.squeeze(Xi[:,idxs[i]:idxs[i+1]]) for i in range(len(idxs)-1)]
    Yi_split = np.array([Yi[idxs[i]] for i in range(len(idxs)-1)])
    return Xi_split, Yi_split

def segment_intervals(Yi):
    idxs = [0] + (np.nonzero(np.diff(Yi))[0]+1).tolist() + [len(Yi)]
    intervals = [(idxs[i],idxs[i+1]) for i in range(len(idxs)-1)]
    return intervals

def segment_lengths(Yi):
    idxs = [0] + (np.nonzero(np.diff(Yi))[0]+1).tolist() + [len(Yi)]
    intervals = [(idxs[i+1]-idxs[i]) for i in range(len(idxs)-1)]
    return np.array(intervals)

def get_split(fold):
    split = None
    if fold == 1:
        split = {
        'train': [9, 10, 12, 13, 14, 15, 16, 18, 19, 20, 21, 22, 23, 24, 25, 27, 28, 29, 30, 31, 32],
        'val': [8,17,26,11],
        'test': list(range(0,8))
    }
    elif fold == 2:
        split = {
        'train': [0, 1, 3, 4, 5, 6, 7, 17, 18, 20, 21, 22, 23, 24, 26, 27, 28, 29, 30, 31, 32],
        'val': [16,25,2,19],
        'test': list(range(8,16))
    }
    elif fold == 3:
        split = {
        'train': [0, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 25, 26, 28, 29, 30, 31, 32],
        'val': [24,1,10,27],
        'test': list(range(16,24))
    }
    elif fold == 4:
        split = {
        'train':[1, 2, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20, 21, 22, 23],
        'val': [0,9,18,3],
        'test': list(range(24,33))
    }
    else:
        raise RuntimeError("Not a fold!")
    return split
## model mit endovVis trainiert
## /mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/EndoVis18/out/Phase_segmentation_resnet50/no_pretrain/20210225-1242/model.pkl
VIDEOS = ['20190917','20191001','20191011','20191211','20200122','20200129','20200221','20200317',
            '20200330','20200407','20200416','20200423','20200427','20200710','20200717','20200727','20201002','20201006', '20201201','20201204',
            '20201208','20190214','20200611','20200123','20200302','20200504','20200622','20201017', '20200731','20200827','20191206', '20210113', '20200212']



#data_path2 = "/media/krellstef/INTENSO/cholec80"#"G:/cholec80"
data_path2 = "/mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/cholec80"#"G:/cholec80"
data_path = "/mnt/g27prist/TCO/TCO-Studenten/CoBot"#"G:/cholec80"
"""Path to the directory that contains the prepared Cholec80 data."""

out_path = "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot"
out_path_Cholec80 = "/mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/cholec80/out"#"./outCholec80"
"""Path to the directory where experimental results are stored."""

videos_path = os.path.join(data_path2, "videos")
frames_path = os.path.join(data_path, "frames_smaller")
annotation_path = os.path.join(data_path,"annotations_phases_steps", "phase_annotations")
annotation_path_step = os.path.join(data_path,"annotations_phases_steps", "step_annotations_adjusted_new")

frames_path_Cholec80 = os.path.join(data_path2, "frames_1fps")
frames_path_Cholec80_ = os.path.join(data_path2, "frames_1fps_new")
annotation_path_Cholec80 = os.path.join(data_path2, "phase_annotations")

frames_path_CoBot_unlabelled = os.path.join(data_path, "frames_unlabelled")



annotation_path_EndoVis18 = "/mnt/g27prist/TCO/TCO-Studenten/Datasets/EndoVis18-Workflow"
frames_path_EndoVis18 = "/mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/EndoVis18"
out_path_EndoVis18 = "/mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/EndoVis18/out"





class_weights_steps = np.array([0.68427643, 0.72188491, 1.77115411, 1.11698883, 0.65192167, 2.59070569,0.8013521,  0.14903291, 0.51268335])

class_weights_phases = np.array([1.451537,   0.4451935,  1.69958875, 0.31613947, 1.08754127])
class_weights_cholec80 = np.array([1.5779, 0.1608, 0.8091, 0.2459, 1.5958, 0.8212, 1.7894])
#[1.81178732 0.44682263 1.41549577 0.27488215 1.05101214]
#[1.66680781 0.44370434 1.16201695 0.2778665  1.4496044 ]