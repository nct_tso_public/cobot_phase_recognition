# CoBot Phase Recognition

Use of
 * https://gitlab.com/nct_tso_public/pretrain_tc for part1
 * https://gitlab.com/funkeii/gesture-transformer for part2

## Requirements
The implementation is in Python 3.8 using [PyTorch 1.7.1](https://pytorch.org).
The following Python packages are used:
> torch torchvision numpy os sys random argparse time datetime csv imageio PIL skimage albumentations reformer_pytorch performer_pytorch


## Data preparation

The data was prepared by extracting the images from the video and the CoBot annotation files were simplified.
### Frames extraction

```
cd part1
python3 frame_extractor_CoBot.py
```

Folders with extracted images:
* CoBot with annotation: `/mnt/g27prist/TCO/TCO-Studenten/CoBot/frames_smaller`
* CoBot without annotation: `/mnt/g27prist/TCO/TCO-Studenten/CoBot/frames_unlabelled`
* EndoVis-18: `/mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/EndoVis18`
* Cholec80: `/mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/cholec80/frames_1fps`



### Create simple annotation files from originals

```
cd part1
python3 read_annotation_file.py Step
python3 read_annotation_file.py Phase
```

Folders: 
* phase annotation: /mnt/g27prist/TCO/TCO-Studenten/CoBot/annotations_phases_steps/phase_annotations
* step annoation (with manual adjustments because step ends were wrong due to annotating programm) /mnt/g27prist/TCO/TCO-Studenten/CoBot/annotations_phases_steps/step_annotations_adjusted_new

## CNN Training

As a CNN a ResNet50 was used, although with the scripts also AlexNet and other ResNets are possible to use.
### EndoVis18

pretrained on cholec80
```
cd part1
python3 trainCNN_EndoVis18.py --pretrained 1 --train_all_layers_resnet50 True
```

### Cholec80
```
cd part1
python3 trainCNN_Cholec80.py 40 --train_all_layers_resnet50 True
```

### Self-supervised CNN-Training with unlabelled CoBot
```
cd part1
python3 pretrain_self_supervised.py
```
### CoBot
4-fold-cross validation is performed

`./train_cnn_cross.sh`

`python3 trainCNN_CoBot_cross.py --fold $i --exp "$EXP" --epochs 30 --annotation step --pretrained 4`

* annotation: step or phase
* pretrained: 0 no, 1 c, 3 c + e, 4 s, 5 c + e + s
(c: Chole80, e: EndoVis18, s: self-supervised)

### Evaluate results with cross validation 
```cd part1
python3 eval_cross.py experiment --dataset 
```
--dataset: CoBot or CoBotSteps


### Extract features from CNN

`./extract_features.sh`

Features for 4 folds ($i in 1,2,3,4):

* Phases: /media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar

* Steps: /media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar"

## Training with features from CNN
### Train LSTM

`./train_lstm.sh`

### Train Tcn

`./train_tcn.sh`

### Train Transformer Mlp

`./train_Transformer_Mlp.sh`

### Train Transformer Conv

`./train_Transformer_Conv.sh`

### Evaluation
LSTM: `eval_cross.py`
Rest: `./eval.sh`


## Additional files in part1:

Frame extraction for Cholec80:
`python3 prepare_dataset.py`

Analyse CoBot data:
`python3 analyse_features.py`

Change frame format:
`python3 changeformat.py`

Plot test results of all networks:
`python3 plot_results.py --anno step`

## Video folder
That are the scripts used to create the video with phase annotation and visualisatons.

