import os.path
import cv2
import numpy as np
import glob

INPUT_FRAMES_PATH = 'D:/Studium/11.Semester/Masterarbeit/20200622_high_noBorder_segmentation'

OUTPUT_FRAMES_PATH = "pics_video_new"

# folders with bar plots of phases
PLOTS_PREDICTION =""
PLOTS_ANNOTATION =""
VIDEONAME = "CoBot_7180_7386_3.avi" 
# load prediction and groundtruth for Video 20200622
ANNO_FILE = "20200622.txt"
PHASE_DATA = np.loadtxt(ANNO_FILE, delimiter=",")
print(PHASE_DATA.shape)

PHASES = ['Preparation and intraabdominal orientation','Mobilization of colon (medial)','Mobilization of colon (lateral)','Total mesorectal excision and dissection of rectum','Extraabdominal preparation of anastomosis']

#Steps: (only Phase Mobilization of colon (medial) is splitted into steps)
# 0 Preparation and intraabdominal orientation
# 1 Mobilization of colon (medial) no Step
# 2 Peritoneal Incision (medial)
# 3 Separation of mesocolon and Gerotas fascia (medial)
# 4 Preparation/Clipping/Dissection of Inferior Mesenteric Artery
# 5 Preparation/Clipping/Dissection of Inferior Mesenteric Vein
# 6 Mobilization of colon (lateral)
# 7 Total mesorectal excision and dissection of rectum
# 8 Extraabdominal preparation of anastomosis


def generateVideo():
    img_array = []
    sr = OUTPUT_FRAMES_PATH
    for filename in sorted(glob.glob(os.path.join(sr, "*.png"))):
    #for f in range(7180,7387):
        #filename = os.path.join(sr,  "%08d.png" % (f))
        img = cv2.imread(filename)
        height, width, layers = img.shape
        size = (width,height)
        img_array.append(img)
    out = cv2.VideoWriter(VIDEONAME,cv2.VideoWriter_fourcc(*'DIVX'), 3, size)
 
    for i in range(len(img_array)):
        out.write(img_array[i])
    out.release()
    return

def logoOverlay(image,logo,alpha=1.0,x=0, y=0, scale=1.0, scale_diff = 1.0):
    (h, w) = image.shape[:2]
    image = np.dstack([image, np.ones((h, w), dtype="uint8") * 255])
    overlay = cv2.resize(logo, None,fx=scale*scale_diff,fy=scale)
    print(overlay.shape)
    (wH, wW) = overlay.shape[:2]
    output = image.copy()
    # blend the two images together using transparent overlays
    try:
        if x<0 : x = w+x
        if y<0 : y = h+y
        if x+wW > w: wW = w-x  
        if y+wH > h: wH = h-y
        print(x,y,wW,wH)
        overlay=cv2.addWeighted(output[y:y+wH, x:x+wW],alpha,overlay[:wH,:wW],1.0,0)
        output[y:y+wH, x:x+wW ] = overlay
    except Exception as e:
        print("Error: Logo position is overshooting image!")
        print(e)
    output= output[:,:,:3]
    return output
    
# annotated steps of 0622
def step_0622(number):
    #step in video 0622
    step_string = "---"
    if number >= 2 and number < 384:
        step_string= "Trocar Placement"
    elif number >= 384 and number < 1056:
        step_string= "Intraabdominal orientation"
    elif number >= 1841 and number < 2583:
        step_string= "Peritoneal Incision (medial)"
    elif number >= 2583 and number < 2807:
        step_string= "Preparation/Clipping/Dissection of Inferior Mesenteric Artery"
    elif number >= 2807 and number < 3299 or number >= 3742 and number < 4867:
        step_string= "Separation of mesocolon and Gerotas fascia (medial)"
    elif number >= 3299 and number < 3742:
        step_string= "Preparation/Clipping/Dissection of Inferior Mesenteric Vein"
    elif number >= 4867 and number < 5199:
        step_string= "Peritoneal Incision (lateral)"
    elif number >= 6999 and number < 9232:
        step_string= "High mesorectal dissection"
    elif number >= 9232 and number < 11282:
        step_string= "Low mesorectal dissection"
    elif number >= 112822:
        step_string= "Linear stapling of rectum"
    return step_string

# creates image with bar plots and text (predction and GT of phases and GT of steps)
def customBlending(NUM):
    name = "%08d.png" % (NUM)
    plot_prediction = PLOTS_PREDICTION + name
    plot_annotation = PLOTS_ANNOTATION + name


        #combine plot with image
    background = cv2.imread(os.path.join(INPUT_FRAMES_PATH, name))
    background = cv2.resize(background,(845,675))
    overlay_pred = cv2.imread(plot_prediction, cv2.IMREAD_UNCHANGED)
    overlay_anno = cv2.imread(plot_annotation, cv2.IMREAD_UNCHANGED)
    #print(overlay_pred.shape) # must be (x,y,4)
    #print(overlay_anno.shape) # must be (x,y,4)
    #print(background.shape) # must be (x,y,3)

        # downscale logo by half and position on bottom right reference
    #out = logoOverlay(background,overlay,alpha = 0.5, scale=0.08,y=-30,x= -300,scale_diff = 2.0)
    #out = logoOverlay(out,overlay,alpha = 0.5, scale=0.08,y=-70,x= -300,scale_diff = 2.0)
    out = logoOverlay(background,overlay_anno,alpha = 0.3, scale=0.15,y=-120,x= -1500,scale_diff = 2.0)
    out = logoOverlay(out,overlay_pred,alpha = 0.3, scale=0.15,y=-60,x= -1500,scale_diff = 2.0)
    #print(out.shape)
    font = cv2.FONT_HERSHEY_SIMPLEX
    out = np.array(out)
    fontscale = 0.6#0.5
    
    x = 10
    phase_predicted = PHASES[int(PHASE_DATA[NUM,0])]
    phase_annotated = PHASES[int(PHASE_DATA[NUM,1])]
    step_annotated = step_0622(NUM)
    
    cv2.putText(img = out , text = 'Annotation:', org = (x,80),fontFace = font, fontScale = fontscale, color = (0, 255, 0),thickness = 1, lineType =cv2.LINE_AA)
    cv2.putText(img = out , text = 'Phase: '+ phase_annotated, org = (x,100),fontFace = font, fontScale = fontscale, color = (0, 255, 0),thickness = 1, lineType =cv2.LINE_AA)
    cv2.putText(img = out , text = 'Step: '+ step_annotated, org = (x,120),fontFace = font, fontScale = fontscale, color = (0, 255, 0),thickness = 1, lineType =cv2.LINE_AA)
    cv2.putText(img = out , text = 'Prediction:', org = (x,150),fontFace = font, fontScale = fontscale, color = (0, 255, 0),thickness = 1, lineType =cv2.LINE_AA)
    cv2.putText(img = out , text = 'Phase: ' + phase_predicted, org = (x,170),fontFace = font, fontScale = fontscale, color = (0, 255, 0),thickness = 1, lineType =cv2.LINE_AA)
    
    cv2.imwrite(os.path.join(OUTPUT_FRAMES_PATH, name),out)
    return
    #cv2.imshow("test",out)
    #cv2.waitKey(0)      

start = 0
stop = 12000
for i in range(start,stop):
   customBlending(i)
generateVideo()
