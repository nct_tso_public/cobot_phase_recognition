import os.path
import numpy as np
import matplotlib.pylab as plt
from matplotlib.colors import from_levels_and_colors
from scipy import io as sio
from PIL import Image
import cv2
import glob
import argparse


ANNO_FILE = "20200622.txt" #file of the predictions and Ground Truth

NUM_IMAGES = 13321 #number of images of Video 0622
data_orig = np.loadtxt(ANNO_FILE, delimiter=",")

#cmap, norm = from_levels_and_colors([-0.5,0.5, 1.5, 2.5, 3.5, 4.5, 6.5], ['red', 'blue', 'green', 'purple', 'orange', 'black'])
#cmap, norm = from_levels_and_colors([-0.5,0.5, 5.5, 6.5, 7.5, 8.5, 9.5], ['red', 'blue', 'green', 'yellow', 'orange', 'black'])
cmap, norm = from_levels_and_colors([-0.5,0.5, 1.5, 2.5, 3.5, 4.5, 9.5], ['red', 'purple', 'green', 'yellow', 'orange', 'black'])

# Bar with phases + black line
def loop_with_black(start,stop,mode):
    if mode == "prediction":
        folder = "plots_prediction"
        datarow = 0
    else:
        folder = "plots_annotation"
        datarow = 1
    for NUM in range(start,stop):
        fig = plt.figure("huhu", figsize=(20,10))
        data = data_orig.copy()   

        # Marker for the current file:
        #manipulate importet FILE in the way that the value is 6 for both GT and predictions
        data[NUM-60 : NUM+60, : ] = 9

        new_data= np.dstack([data]*10)
        new_data = np.transpose(new_data)
        ax = plt.subplot(2,1,1)

        ax = plt.imshow(new_data[:,datarow,:],cmap = cmap, norm = norm)
                
        plt.xticks([])
        plt.yticks([])
        plt.axis("tight")
        name = "%08d.png" % (NUM)
        plot_name = folder +"/20200622_"+ name
        fig.savefig(plot_name, bbox_inches='tight',transparent=True, pad_inches=0)

# Bar with phases
def loop_without_black(start,stop,mode):
    if mode == "prediction":
        folder = "plots_prediction"
        datarow = 0
    else:
        folder = "plots_annotation"
        datarow = 1

    for NUM in range(start,stop):
        fig = plt.figure("huhu", figsize=(20,10))
        data = data_orig.copy()   

        new_data= np.dstack([data]*10)
        new_data = np.transpose(new_data)
        ax = plt.subplot(2,1,1)

        ax = plt.imshow(new_data[:,datarow,:],cmap = cmap, norm = norm)
                
        plt.xticks([])
        plt.yticks([])
        plt.axis("tight")
        name = "%08d.png" % (NUM)
        plot_name = folder+"/20200622_"+ name
        fig.savefig(plot_name, bbox_inches='tight',transparent=True, pad_inches=0)
        exit()

mode_prediction = "annotation"
loop_without_black(0,61, mode_prediction)
loop_with_black(61,NUM_IMAGES-61, mode_prediction)
loop_without_black(NUM_IMAGES-61,NUM_IMAGES, mode_prediction)

mode_prediction = "prediction"
loop_without_black(0,61, mode_prediction)
loop_with_black(61,NUM_IMAGES-61, mode_prediction)
loop_without_black(NUM_IMAGES-61,NUM_IMAGES, mode_prediction)

