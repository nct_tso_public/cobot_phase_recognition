cd part1

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" --exp "$EXP" --fold $i --w 1 --epochs 100 --annotation "step"
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" --exp "$EXP" --fold $i --w 1 --epochs 100
done