import argparse


def str2bool(v):
    return v.lower() in ("yes", "true", "t", "1")


parser = argparse.ArgumentParser(description="Train model.")
parser.register('type', 'bool', str2bool)

# Experiment
parser.add_argument('--exp', type=str, required=True, help="Name (description) of the experiment to run.")
parser.add_argument('--seed', type=int, default=42, help="Random seed.")
parser.add_argument('--task', type=str, choices=['Cholec80', 'Suturing', 'Needle_Passing', 'Knot_Tying','CoBotPhase','CoBotStep'],
                    default='CoBotPhase', help="Task to evaluate.")
parser.add_argument('--eval_scheme', type=str, choices=['train-val-test', 'LOSO', 'LOUO', 'train-val',"4fold"], default='train-val',
                    help="Cross-validation scheme to use: Leave one supertrial out (LOSO) or Leave one user out (LOUO).")
parser.add_argument('--split', type=str, default='Tecno', choices=['NCT', 'Tecno','1','2','3','4'], help="Cross-validation fold (data split) to evaluate.")

# Data
# parser.add_argument('--feature_type', type=str, default='visual', choices=['visual', 'kinematic', 'visAndKin'],
#                    help="")  # required ?
parser.add_argument('--feature_file', type=str, default="/media/TCO/TCO-Studenten/Homes/krellstef/CoBot_features.pth.tar", help=".")
# parser.add_argument('--transcriptions_dir', type=str, default="/local_home/funkeisab/JIGSAWS/{}/transcriptions",
#                    help="Path to folder containing the transcription files (gesture annotations). One file per video.")
# parser.add_argument('--video_lists_dir', type=str, default="./Splits/{}/",
#                    help="Path to directory containing information about each video in the form of video list files. "
#                         "One subfolder per evaluation scheme, one file per evaluation fold.")
parser.add_argument('--video_sampling_step', type=int, default=25,
                    help="Describes how the available video data has been downsampled from the original temporal "
                         "resolution (by taking every <video_sampling_step>th frame).")
parser.add_argument('--max_length', type=int, default=-1, help="Cut sequences to have at most this length.")
# parser.add_argument('--pad_duration', type=int, default=1, help="Pad duration in seconds.")

# data augmentation
parser.add_argument('--noise_factor', type=float, default=1.0, help=".")  # to remove
parser.add_argument('--added_noise', type=float, default=0.3, help=".")
parser.add_argument('--duplicate_frames_prob', type=float, default=0.1, help=".")
parser.add_argument('--duplicate_duration', type=float, default=5, # 0.5,
                    help=".")  # duration in seconds
parser.add_argument('--drop_frames_prob', type=float, default=0.1, help=".")
parser.add_argument('--drop_duration', type=float, default=30,  #4,
                    help=".")
parser.add_argument('--randomly_replace_prob', type=float, default=-1,  # 0.01,
                    help=".")
parser.add_argument('--replace_duration', type=float, default=5,  # 0.5,
                    help=".")
parser.add_argument('--mask_prob', type=float, default=-1,  #0.05,
                    help=".")
parser.add_argument('--mask_duration', type=float, default=5,  # 0.5,
                    help=".")

# Transformer Model
parser.add_argument('--d_model', type=int, default=128, help=".")
parser.add_argument('--causal', type='bool', default=True, help="Causal model: only look back")

parser.add_argument('--nlayers', type=int, default=5, help=".")
parser.add_argument('--nhead', type=int, default=2, help=".")
parser.add_argument('--ff_type', type=str, default="feed_forward", help="")
parser.add_argument('--d_ff', type=int, default=256, help=".")
parser.add_argument('--pre_norm', type='bool', default=True, help="Pre or post norm.")
parser.add_argument('--initial_norm', type='bool', default=False, help=".")
parser.add_argument('--conv_kernel', type=int, default=None, help=".")
parser.add_argument('--conv_dilation', type=int, default=None, help=".")
parser.add_argument('--conv_layers', type=int, default=None, help=".")
parser.add_argument('--activation', type=str, default='gelu', help="")
parser.add_argument('--hidden_dropout', type=float, default=0.1, help=".")
parser.add_argument('--ff_dropout', type=float, default=0.5, help=".")
# parser.add_argument('--reduce_dim', type='bool', default=False, help=".")

parser.add_argument('--pos_embed_type', type=str, default=None, help="")
parser.add_argument('--repeat_pos_info', type='bool', default=False, help=".")
parser.add_argument('--scale_pos_encoding', type='bool', default=False, help=".")
parser.add_argument('--T5_num_buckets', type=int, default=64, help=".")
parser.add_argument('--T5_max_distance', type=int, default=1000, help=".")  # TODO: specify in seconds

parser.add_argument('--attn_dropout', type=float, default=0.3, help=".")
parser.add_argument('--attn_local_window_duration', type=int, default=-1, help=".")  # duration in seconds
parser.add_argument('--attn_local_stride_duration', type=float, default=-1, help=".")
parser.add_argument('--attn_global_stride_duration', type=int, default=-1, help=".")
parser.add_argument('--attn_global_type', type=str, default="grid", help=".")
parser.add_argument('--attn_impl', type=str, default="pytorch", help=".")
parser.add_argument('--attn_sparsity', type=str, default=None, help=".")
parser.add_argument('--attn_topk', type=int, default=None, help=".")
parser.add_argument('--gated_attn', type='bool', default=False, help=".")
parser.add_argument('--gate_fn', type=str, default="sigmoid", help=".")
parser.add_argument('--gate_forward_only', type='bool', default=False, help=".")

# parser.add_argument('--attn_type', type=str, default='backward', choices=["backward", "bidirectional"], help="")
# parser.add_argument('--pe_damping', type=float, default=1.0, help=".")  # counter-intuitive? ;)
parser.add_argument('--short', type='bool', default=False, help=".")
### TO REMOVE 
parser.add_argument('--do_tecno', type='bool', default=False, help=".")
parser.add_argument('--inner_softmax', type='bool', default=True, help=".")
parser.add_argument('--reduce_dim_tcn', type='bool', default=False, help=".")
parser.add_argument('--tcn_norm', type='bool', default=False, help=".")
parser.add_argument('--tcn_linear_proj', type='bool', default=False, help=".")
parser.add_argument('--tcn_initial_norm', type='bool', default=False, help=".")
parser.add_argument('--initial_tcn_layers', type=int, default=5, help=".")
parser.add_argument('--final_tcn_layers', type=int, default=0, help=".")
parser.add_argument('--tcn_dropout', type=float, default=0.5, help=".")
###

# TCN Model
parser.add_argument('--tcn_baseline', type=bool, default=False, help="Use TCN model.")
parser.add_argument('--tcn_stages', type=int, default=2, help=".")
parser.add_argument('--tcn_layers', type=int, default=5, help=".")

# parser.add_argument('--nstages', type=int, default=1, help=".")
# parser.add_argument('--tcn_layers', type=int, default=5, help=".")
# parser.add_argument('--d_tcn', type=int, default=64, help=".")  # dimension of TCN feature maps

# SDC
# parser.add_argument('--downsampling', type='bool', default=True, help=".")
# parser.add_argument('--attn_layer', type='bool', default=True, help=".")

# Training
parser.add_argument('--resume_exp', type=str, default=None,
                    help="Path to results of former experiment that shall be resumed (untested).")

parser.add_argument('-j', '--workers', type=int, default=4, help="Number of threads used for data loading.")
parser.add_argument('-b', '--batch-size', type=int, default=4, help="Batch size.")
parser.add_argument('--use_weights', type='bool', default=True, help="Use weighted loss")
parser.add_argument('--label_smoothing_eps', type=float, default=0.2, help="Label smoothing.")
parser.add_argument('--regular', type=float, default=0.0, help="Regularization")
parser.add_argument('--feature_trunc_mse_factor', type=float, default=0.1,
                    help="Apply truncated MSE loss to features.")
parser.add_argument('--lr', type=float, default=0.0001, help="Learning rate.")
parser.add_argument('--lr_schedule', type=str, default=None, help=".")
parser.add_argument('--steps', type=int, default=10000, help="Number of epochs to train.")
parser.add_argument('--warmup_steps', type=int, default=0, help=".")
# parser.add_argument('--eval_overlap', type=float, default=0.5, help="Learning rate.")
parser.add_argument('--deep_supervision', type='bool', default=False, help="")

parser.add_argument('--adam', type='bool', default=False, help="")

parser.add_argument('--log_freq', '-lf', type=int, default=1, help=".")
parser.add_argument('--eval_freq', '-ef', type=int, default=1, help="Validate model every <eval_freq> epochs.")
parser.add_argument('--save_freq', '-sf', type=int, default=100, help="Save checkpoint every <save_freq> epochs.")

parser.add_argument('--out', type=str, default="/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot",
                    help="Path to output folder, where all models and results will be stored.")


parser.add_argument('--lstm', type='bool', default=False, help=".")
##### evaluation
parser.add_argument('--to_eval', type=str)
parser.add_argument('--eval_out', type=str,
                    default="/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_eval")
