import torch
import torch.nn as nn
import torch.utils.data as data

import torchvision
import torchvision.transforms as transforms
from PIL import Image

import csv
import copy

import os
import numpy as np

from util import Cholec80

# based on extract_featurevector.py from Stefanie's code (https://github.com/stefaniekrell/Master_code)

# Cholec80 feature extractor
class CNNmodel(nn.Module):
    """CNNmodel."""

    # featureNet: pretrained featureNet (.pkl file) that is to be used
    def __init__(self, num_class, cnn="AlexNet", pretrained=True, pretrainedFile=None):
        """Create CNN.

        """
        super(CNNmodel, self).__init__()
        if cnn == "AlexNet":
            self.model = torchvision.models.alexnet(pretrained=pretrained)
            self.model.classifier[6].out_features = 7
            if pretrainedFile is not None:
                print("load pretrained")
                self.load(pretrainedFile)
            self.model.classifier[6].out_features = num_class
        else:
            if cnn == "resnet18":
                self.model = torchvision.models.resnet18(pretrained=pretrained)
            elif cnn == "resnet34":
                self.model = torchvision.models.resnet34(pretrained=pretrained)
            elif cnn == "resnet50":
                self.model = torchvision.models.resnet50(pretrained=pretrained)
            else:
                raise ValueError('Unknown base model: {}'.format(cnn))
            num_ftrs = self.model.fc.in_features
            self.model.fc = nn.Linear(num_ftrs, 7)
            if pretrainedFile is not None:
                print("load pretrained")
                self.load(pretrainedFile)
            self.model.fc = nn.Linear(num_ftrs, num_class)
            # adapt base model

    def forward(self, x):
        return self.model.forward(x)

    def load(self, model_file):
        self.load_state_dict(torch.load(model_file))

    def load_different_num_class(self, model_file, num_class_load, num_class_now):
        num_ftrs = self.model.fc.in_features
        self.model.fc = nn.Linear(num_ftrs, num_class_load)
        self.load_state_dict(torch.load(model_file))
        num_ftrs = self.model.fc.in_features
        self.model.fc = nn.Linear(num_ftrs, num_class_now)

    def save(self, model_file):
        torch.save(self.state_dict(), model_file)

    def base_model(self):
        return self.model

    def feature(self, x, mode):
        if mode == "resnet":
            x = self.model.conv1(x)
            x = self.model.bn1(x)
            x = self.model.relu(x)
            x = self.model.maxpool(x)

            x = self.model.layer1(x)
            x = self.model.layer2(x)
            x = self.model.layer3(x)
            x = self.model.layer4(x)
            x = self.model.avgpool(x)
            x = x.view(x.size(0), -1)
            return x
        else:
            return None


class PhaseData(data.Dataset):
    """Dataset consisting of the sequential frames and their corresponding phase labels of one specific Cholec80 video.
    Frames will be sampled at 1 Hz.
    """

    def __init__(self, frames_path, annotation_path, width, height, transform=None, dataset = "Cholec80"):
        """Create dataset.
        :param frames_path: Path to the directory that contains the video frames.
        :param annotation_path: Path to the text file that contains the phase labels.
        :param width: Width of each frame in the dataset.
        :param height: Height of each frame in the dataset.
        :param transform: Image transformation applied to each frame in the dataset.
        """
        self.frames_path = frames_path
        self.width = width
        self.height = height
        self.transform = transform

        self.targets = []
        len_ = len([ f for f in os.listdir(frames_path) if f.endswith('.png')])
        if dataset == "Cholec80":
            tar = Cholec80.phase_map
            delimiter = '\t'
        # elif dataset == "EndoVis18":
        #     tar = EndoVis18.phase_map
        #     delimiter = ','
        else:
            raise NotImplementedError("Not an available datset")
        f = open(annotation_path, "r")
        reader = csv.reader(f, delimiter=delimiter)
        next(reader, None)
        count = 0
        for row in reader:
            if count % 25 == 0:  # read annotations with 1 fps
                self.targets.append(tar[row[1]])
            count += 1
        f.close()

        assert(len(self.targets) == len_)

    def __getitem__(self, index):
        """Return the frame at the given index along with its phase label.
        :param index: The number of the frame to return.
        :return: The tuple (frame, label).
        """

        target = self.targets[index]
        frame = self.frames_path + "/%08d.png" % (index)  # read frames with 1 fps
        img = _load_img(frame, self.width, self.height)

        if self.transform is not None:
            img = self.transform(img)

        return img, target

    def __len__(self):
        """ Return size of dataset. """

        return len(self.targets)


def _load_img(path, width, height):
    im = Image.open(path)
    w = im.width
    h = im.height
    im_scaled = im.resize((width, int(width * (h / w))))  # preserve aspect ratio

    img = Image.new('RGB', (width, height), (0, 0, 0))
    offset_y = (height - im_scaled.height) // 2
    img.paste(im_scaled, box=(0, offset_y))
    return img


class DataPrep:
    """Define parameters related to data preparation."""

    sample_rate = 1
    """Sample rate in Hz (fps) used for video frame extraction."""

    width = 384
    """Width in pixels of each extracted video frame."""

    height = 216
    """Height in pixels of each extracted video frame."""

    standard_transform = transforms.Compose(
        [transforms.ToTensor(),
         transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])])
    """Image transformation applied to each extracted video frame."""


def extract_features(file, out_dir, frames_path, annotation_path, normalize=False):
    device_gpu = torch.device("cuda:0")
    device_cpu = torch.device("cpu")
    batch_size = 384

    storage = dict()
    # add some general info
    storage['dataset'] = "Cholec80"
    storage['split'] = "NCT"
    storage['fps'] = 1
    storage['feature_extractor'] = "resnet50"
    storage['model_weights'] = file

    # NCT data split
    train_videos = Cholec80.train_set[:]
    for i in range(len(train_videos)):
        train_videos[i] = "video" + train_videos[i]
    val_videos = Cholec80.val_set[:]
    for i in range(len(val_videos)):
        val_videos[i] = "video" + val_videos[i]
    test_videos = Cholec80.test_set[:]
    for i in range(len(test_videos)):
        test_videos[i] = "video" + test_videos[i]
    all_videos = train_videos + test_videos + val_videos

    print("create datasets")
    data_loaders = []
    for op_set in Cholec80.op_sets:
        for op in os.listdir(os.path.join(frames_path, op_set)):
            op_path = os.path.join(frames_path, op_set, op)
            if os.path.isdir(op_path):
                anno_file = os.path.join(annotation_path, "video" + op + "-phase.txt")
                print(anno_file)
                dataset = PhaseData(op_path, anno_file, DataPrep.width, DataPrep.height, DataPrep.standard_transform)
                data_loaders.append(
                    torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=False, num_workers=0)
                )

    model = CNNmodel(num_class=7, cnn="resnet50")
    print("load model weights...")
    model.load(file)
    model = model.to(device_gpu)

    print("extract features...")

    storage['train'] = {}
    storage['val'] = {}
    storage['test'] = {}
    storage['labels'] = {}

    model.eval()

    train_accuracies = []
    val_accuracies = []
    test_accuracies = []
    with torch.no_grad():
        for loader in data_loaders:
            frames_path = loader.dataset.frames_path
            video_id = "video" + frames_path.split("/")[-1]
            print(video_id)
            assert(video_id in all_videos)

            features = []
            labels = torch.tensor([], dtype=torch.long)
            correct = 0
            frame_no = 0
            for batch in loader:
                data, target = batch
                labels = torch.cat([labels, target], dim=0)

                data = data.to(device_gpu)
                feature = model.feature(x=data, mode="resnet")
                feature = torch.squeeze(feature)
                features.append(feature.to(device_cpu))

                # test model accuracy
                output = model(data)
                predicted = torch.nn.Softmax(dim=-1)(output)
                _, predicted = torch.max(predicted, dim=-1, keepdim=False)
                correct += (predicted.to(device_cpu) == target).sum().item()
                frame_no += target.shape[0]

            feature_tensor = torch.cat(features, dim=0)

            accuracy = correct / frame_no
            print("{} - acc: {:.4f}".format(video_id, accuracy))
            if video_id in val_videos:
                val_accuracies.append(accuracy)
                storage['val'][video_id] = feature_tensor
            elif video_id in test_videos:
                test_accuracies.append(accuracy)
                storage['test'][video_id] = feature_tensor
            else:
                assert(video_id in train_videos)
                train_accuracies.append(accuracy)
                storage['train'][video_id] = feature_tensor

            assert(labels.shape[0] == feature_tensor.shape[0])
            assert(labels.dim() == 1)
            storage['labels'][video_id] = labels

    print("mean train accuracy: {:.4f}".format(np.mean(train_accuracies)))
    print("mean val accuracy: {:.4f}".format(np.mean(val_accuracies)))
    print("mean test accuracy: {:.4f}".format(np.mean(test_accuracies)))

    storage['test_accuracy'] = np.mean(test_accuracies)

    filename = "Cholec80_NCT.pth.tar"
    torch.save(storage, os.path.join(out_dir, filename))

    if normalize:  # create normalized version
        storage_norm = copy.deepcopy(storage)

        # calculate mean and std on available training data
        train_data = []
        for video_id in train_videos:
            train_data.append(storage['train'][video_id])
        train_data = torch.cat(train_data, dim=0)
        mean_vector = torch.mean(train_data, dim=0)
        std_vector = torch.std(train_data, dim=0)

        # normalize
        for video_id in all_videos:
            mode = "train"
            if video_id in val_videos:
                mode = "val"
            elif video_id in test_videos:
                mode = "test"
            storage_norm[mode][video_id] = (storage[mode][video_id] - mean_vector.unsqueeze(0)) / std_vector.unsqueeze(0)

        torch.save(storage_norm, os.path.join(out_dir, "Cholec80_NCT_normalized.pth.tar"))


if __name__ == '__main__':
    # file = "/mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/cholec80/out/Phase_segmentation_resnet50/no_pretrain/40/20210203-1215/model.pkl"
    file = "/mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/cholec80/out/Phase_segmentation_resnet50/no_pretrain/40/20210223-1733/model.pkl"
    out = "/local_home/funkeisab/cholec80/features"

    data_path = "/local_home/funkeisab/cholec80"
    frames_path = os.path.join(data_path, "frames_1fps")
    annotation_path = os.path.join(data_path, "phase_annotations")

    extract_features(file, out, frames_path, annotation_path, normalize=True)

    # test
    data = torch.load(os.path.join(out, "Cholec80_NCT.pth.tar"))

    print(data['train'].keys())
    print(data['val'].keys())
    print(data['test'].keys())
    print(data['labels'].keys())

    video_id = list(data['train'].keys())[0]
    print(data['train'][video_id].shape)
    print(data['labels'][video_id].shape)

