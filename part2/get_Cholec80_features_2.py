import torch
import torch.nn as nn
import torch.utils.data as data

import torchvision
import torchvision.transforms as transforms
from PIL import Image

import csv
import copy
import random
import argparse

import os
import numpy as np

import albumentations as A
from albumentations.pytorch import ToTensorV2
import cv2

import sys
sys.path.append('../')
from phase_transformer.models import CNNmodel
from phase_transformer.data import PhaseData

def extract_features(file, out_dir, out_name, frames_path, annotation_path, pad_frames=True, normalize=False):
    device_gpu = torch.device("cuda:0")
    device_cpu = torch.device("cpu")
    batch_size = 384  # TODO

    storage = dict()
    # add some general info
    storage['dataset'] = "Cholec80"
    storage['split'] = "Tecno"
    storage['fps'] = 1
    storage['feature_extractor'] = "resnet50"
    storage['model_weights'] = file

    # TODO
    seed = 12345  # fix random seed
    load_size = 256
    crop_size = 224
    test_transform = A.Compose([
        A.Resize(load_size, load_size, interpolation=cv2.INTER_CUBIC, always_apply=True, p=1.0),
        A.CenterCrop(crop_size, crop_size, always_apply=True, p=1.0),
        A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225), max_pixel_value=255.0,
                    always_apply=True, p=1.0),
        ToTensorV2(transpose_mask=True, always_apply=True, p=1.0),
    ], p=1.0)

    # Standard data split
    split = {
        'train': list(range(1, 41)),
        'val': list(range(41, 49)),
        'test': list(range(49, 81))
    }

    train_videos = []
    for i in range(len(split['train'])):
        train_videos.append("video" + "{:02d}".format(split['train'][i]))
    val_videos = []
    for i in range(len(split['val'])):
        val_videos.append("video" + "{:02d}".format(split['val'][i]))
    test_videos = []
    for i in range(len(split['test'])):
        test_videos.append("video" + "{:02d}".format(split['test'][i]))
    all_videos = train_videos + test_videos + val_videos

    print("create datasets")
    data_loaders = []
    for op in list(range(1, 81)):
        op = "{:02d}".format(op)
        op_path = os.path.join(frames_path, op)
        if os.path.isdir(op_path):
            anno_file = os.path.join(annotation_path, "video" + op + "-phase.txt")
            # print(anno_file)
            dataset = PhaseData(op_path, anno_file, fps=1, size=load_size, transform=test_transform,
                                pad_frames=pad_frames, sample_tc_tuple=False)
            data_loaders.append(
                torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=False, num_workers=1)
            )

    model = CNNmodel(num_class=7, cnn="resnet50", pretrained=False, return_feature=True)
    print("load model weights...")
    model_weights = torch.load(file)['model_weights']
    model.load_state_dict(model_weights)
    model = model.to(device_gpu)

    print("extract features...")

    torch.manual_seed(seed)
    random.seed(seed)
    torch.backends.cudnn.benchmark = False
    torch.backends.cudnn.deterministic = True

    storage['train'] = {}
    storage['val'] = {}
    storage['test'] = {}
    storage['labels'] = {}
    storage['predictions'] = {}

    model.eval()

    train_accuracies = []
    val_accuracies = []
    test_accuracies = []
    with torch.no_grad():
        for loader in data_loaders:
            frames_path = loader.dataset.frames_path
            video_id = "video" + frames_path.split("/")[-1]
            # print(video_id)
            assert(video_id in all_videos)

            features = []
            predictions = []
            labels = torch.tensor([], dtype=torch.long)
            correct = 0
            frame_no = 0
            for batch in loader:
                imgs, _labels = batch
                img = imgs[0]
                label = _labels[0]

                # print("img", type(img), img.shape)
                # print("label", type(label), label.shape)

                labels = torch.cat([labels, label], dim=0)

                img = img.to(device_gpu)
                _output = model(img)
                out, feature = _output
                features.append(feature.to(device_cpu))

                # test model accuracy
                predicted = torch.nn.Softmax(dim=-1)(out)
                _, predicted = torch.max(predicted, dim=-1, keepdim=False)
                predicted = predicted.to(device_cpu)
                predictions.append(predicted)
                correct += (predicted == label).sum().item()
                frame_no += label.shape[0]

            feature_tensor = torch.cat(features, dim=0)
            prediction_tensor = torch.cat(predictions, dim=0)

            accuracy = correct / frame_no
            print("{} - acc: {:.4f}".format(video_id, accuracy))
            if video_id in val_videos:
                val_accuracies.append(accuracy)
                storage['val'][video_id] = feature_tensor
            elif video_id in test_videos:
                test_accuracies.append(accuracy)
                storage['test'][video_id] = feature_tensor
            else:
                assert(video_id in train_videos)
                train_accuracies.append(accuracy)
                storage['train'][video_id] = feature_tensor

            assert(labels.shape[0] == feature_tensor.shape[0])
            assert(labels.dim() == 1)
            storage['labels'][video_id] = labels
            assert(prediction_tensor.shape[0] == labels.shape[0])
            assert(prediction_tensor.dim() == 1)
            storage['predictions'][video_id] = prediction_tensor

    print("mean train accuracy: {:.4f}".format(np.mean(train_accuracies)))
    print("mean val accuracy: {:.4f}".format(np.mean(val_accuracies)))
    print("mean test accuracy: {:.4f}".format(np.mean(test_accuracies)))

    storage['val_accuracy'] = np.mean(val_accuracies)
    storage['test_accuracy'] = np.mean(test_accuracies)

    torch.save(storage, os.path.join(out_dir, "{}.pth.tar".format(out_name)))

    if normalize:  # create normalized version
        storage_norm = copy.deepcopy(storage)

        # calculate mean and std on available training data
        train_data = []
        for video_id in train_videos:
            train_data.append(storage['train'][video_id])
        train_data = torch.cat(train_data, dim=0)
        mean_vector = torch.mean(train_data, dim=0)
        std_vector = torch.std(train_data, dim=0)

        # normalize
        for video_id in all_videos:
            mode = "train"
            if video_id in val_videos:
                mode = "val"
            elif video_id in test_videos:
                mode = "test"
            storage_norm[mode][video_id] = (storage[mode][video_id] - mean_vector.unsqueeze(0)) / std_vector.unsqueeze(0)

        torch.save(storage_norm, os.path.join(out_dir, "{}_normalized.pth.tar".format(out_name)))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="")
    parser.add_argument("model_folder", type=str, help=".")  # "train_cnn_20210301/0612-11/"
    parser.add_argument("feature_file_name", type=str, help=".")  # "cnn_20210301"
    parser.add_argument("--img_padding", type=str, choices=["True", "False"], default="False", help=".")
    parser.add_argument("--normalize", type=str, choices=["True", "False"], default="False", help=".")
    args = parser.parse_args()

    pad_frames = args.img_padding == "True"
    normalize = args.normalize == "True"

    data_path = "/local_home/funkeisab/cholec80"
    frames_path = os.path.join(data_path, "frames_1fps_480")
    annotation_path = os.path.join(data_path, "phase_annotations")
    out = "/local_home/funkeisab/cholec80/features"
    model_dir = "/media/data/funkeisab/feature-extractor"

    model_file = os.path.join(model_dir, args.model_folder, "model_best_acc.pth.tar")
    feature_file = os.path.join(out, "{}.pth.tar".format(args.feature_file_name))

    if os.path.isfile(feature_file):
        print("Found ", feature_file)
        features = torch.load(feature_file)
        print("val_accuracy ", features['val_accuracy'])
        print("test_accuracy ", features['test_accuracy'])
    else:
        extract_features(model_file, out, args.feature_file_name, frames_path, annotation_path,
                         pad_frames=pad_frames, normalize=normalize)

    # test
    data = torch.load(feature_file)

    print(data['train'].keys())
    print(data['val'].keys())
    print(data['test'].keys())
    # print(data['labels'].keys())

    video_id = list(data['train'].keys())[0]
    print(data['train'][video_id].shape)
    print(data['labels'][video_id].shape)

    print(data['predictions'].keys())
    print(data['predictions'][video_id].shape)

