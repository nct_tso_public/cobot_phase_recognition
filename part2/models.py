"""
Sequence-to-Sequence Modeling with nn.Transformer and TorchText
===============================================================

https://pytorch.org/tutorials/beginner/transformer_tutorial.html

This is a tutorial on how to train a sequence-to-sequence model
that uses the
`nn.Transformer <https://pytorch.org/docs/master/nn.html?highlight=nn%20transformer#torch.nn.Transformer>`__ module.
"""

import torch
import torch.nn as nn
import torch.nn.functional as F
from huggingface import BertSelfAttention, create_sinusoidal_embeddings, T5Attention, T5DenseGatedGeluDense

import copy
import math
from einops import rearrange, repeat, reduce

from activations import get_activation

from reformer_pytorch import LSHSelfAttention
from performer_pytorch import SelfAttention


class CNN_linear_layer(nn.Module):
    def __init__(self,file, num_class):
        super(CNN_linear_layer, self).__init__()
        m = CNNmodel(num_class, cnn = "resnet50")
        m.load(file)
        #x = m.base_model()
        self.linear = m.model.fc

    def forward(self, x):
        return self.linear(x)
class CNNmodel(nn.Module):
    """CNNmodel."""
    # featureNet: pretrained featureNet (.pkl file) that is to be used
    def __init__(self, num_class, cnn = "AlexNet",pretrained = True, pretrainedFile = None, out_features = 7):
        """Create CNN.

        """
        super(CNNmodel, self).__init__()
        import torchvision.models.resnet
        if cnn == "AlexNet":
            self.model= torchvision.models.alexnet(pretrained=pretrained)
            self.model.classifier[6].out_features = 7
            if pretrainedFile is not None:
                print("load pretrained")
                self.load(pretrainedFile)
            self.model.classifier[6].out_features = num_class
        else:
            if cnn == "resnet18":
                self.model = torchvision.models.resnet18(pretrained=pretrained)
            elif cnn == "resnet34":
                self.model = torchvision.models.resnet34(pretrained=pretrained)
            elif cnn == "resnet50":
                self.model = torchvision.models.resnet50(pretrained=pretrained)
            else:
                raise ValueError('Unknown base model: {}'.format(cnn))                    
            num_ftrs = self.model.fc.in_features
            if pretrainedFile is not None:
                print("load pretrained")
                self.model.fc = nn.Linear(num_ftrs, out_features)
                here = os.path.dirname(os.path.abspath(pretrainedFile))
                sys.path.append(here)
                self.load(pretrainedFile)
            self.model.fc = nn.Linear(num_ftrs, num_class)
                     # adapt base model
    def forward(self, x):
        return self.model.forward(x)
    def load(self, model_file):
        self.load_state_dict(torch.load(model_file))
    def save(self, model_file):
        torch.save(self.state_dict(), model_file)
    def base_model(self):
        return self.model
    def feature(self,x,mode):
        if mode == "resnet":
            x = self.model.conv1(x)
            x = self.model.bn1(x)
            x = self.model.relu(x)
            x = self.model.maxpool(x)

            
            x = self.model.layer1(x)
            x = self.model.layer2(x)
            x = self.model.layer3(x)
            x = self.model.layer4(x)
            x = self.model.avgpool(x)
            x = x.view(x.size(0), -1)
            return x
        else:
            return None
        
class CombinedModel(nn.Module):
    def __init__(self, num_class, d_in, d_model, max_pos_id, padded_length,
                 ff_type, n_encoder_blocks, nhead, dim_ff, pre_norm, tcn_norm,
                 causal_conv=True, initial_norm=False, activation="relu", conv_dilation=1, conv_kernel=3,
                 hidden_dropout=0.1, ff_dropout=0.1, attn_dropout=0.1,
                 position_embedding_type="absolute_sinusoidal", repeat_pos_info=False,
                 attn_local_window=-1, attn_local_stride=-1, attn_global_stride=-1, attn_global_type="grid",
                 attn_type="pytorch", attn_sparsity=None, topk=None, T5_attn_config=None,
                 initial_tcn_layers=0, final_tcn_layers=0, tcn_dropout=0.0,
                 return_hiddens=False, use_target_dim=False):
        # max_seq_len: required for position embeddings - basically max position id that can ever occur
        super(CombinedModel, self).__init__()

        if initial_tcn_layers > 0:
            if not tcn_norm:
                norm = "none"
            else:
                norm = "pre-norm" if pre_norm else "post-norm"
            self.initial_tcn = MySingleStageModel(num_layers=initial_tcn_layers, d_model=d_model, in_dim=d_in,
                                                  num_classes=num_class, dropout=tcn_dropout,
                                                  hidden_dropout=hidden_dropout, norm=norm, activation=activation,
                                                  causal_conv=causal_conv, linear_proj=True, do_initial_norm=False)
        else:
            self.initial_tcn = None

        # TODO

        next_in_dim = num_class  # TODO
        if final_tcn_layers > 0:
            if not tcn_norm:
                norm = "none"
            else:
                norm = "pre-norm" if pre_norm else "post-norm"
            self.final_tcn = MySingleStageModel(num_layers=final_tcn_layers, d_model=d_model, in_dim=next_in_dim,
                                                num_classes=num_class, dropout=tcn_dropout,
                                                hidden_dropout=hidden_dropout, norm=norm, activation=activation,
                                                causal_conv=causal_conv, linear_proj=True, do_initial_norm=False)
        else:
            self.final_tcn = None

    # collect all outputs for error backpropagation


class GestureTransformer(nn.Module):
    def __init__(self, num_class, d_in, d_model, max_pos_id, padded_length,
                 ff_type, n_encoder_blocks, nhead, dim_ff, pre_norm,
                 causal_conv=True, initial_norm=False, activation="relu", conv_dilation=1, conv_kernel=3, conv_layers=0,
                 hidden_dropout=0.1, ff_dropout=0.1, attn_dropout=0.1,
                 position_embedding_type="absolute_sinusoidal", repeat_pos_info=False, scale_pos=False,
                 attn_local_window=-1, attn_local_stride=-1, attn_global_stride=-1, attn_global_type="grid",
                 attn_impl="pytorch", attn_sparsity=None, topk=None, T5_attn_config=None,
                 rel_pos_num_buckets=24, rel_pos_bucket_size=240,  # TODO: make configurable
                 gated_attn=False, gate_fn="sigmoid", forward_only=False,
                 ):
        # max_pos_id: required for position embeddings - max position id that can ever occur
        super(GestureTransformer, self).__init__()

        assert(n_encoder_blocks > 0)

        self.position_embedding_type = position_embedding_type
        self.padded_length = padded_length
        self.in_dim = d_in
        self.d_model = d_model
        self.nhead = nhead
        self.pre_norm = pre_norm
        self.repeat_pos_info = repeat_pos_info  # repeatedly inject position info in attention blocks

        if attn_local_window > 0:  # use (strided) local (+global) attention pattern
            assert(attn_sparsity is None)

        self.in_proj = nn.Linear(d_in, d_model)

        self.pos_encoder = None
        self.forward_relative_pe = False
        if position_embedding_type is not None:
            assert(position_embedding_type in ["absolute_sinusoidal", "absolute_sinusoidal_hf", "absolute_learned",
                                               "relative_key_simple", "relative_key_query_simple",
                                               "relative_key", "relative_key_query",
                                               "relative_position_bias", "relative_T5"])
            if position_embedding_type in ["absolute_learned", "absolute_sinusoidal_hf"]:
                self.pos_encoder = nn.Embedding(max_pos_id, d_model)
                if position_embedding_type == "absolute_sinusoidal_hf":
                    create_sinusoidal_embeddings(n_pos=max_pos_id, dim=d_model, out=self.pos_encoder.weight)
            elif position_embedding_type == "absolute_sinusoidal":
                self.pos_encoder = PositionalEncoding(d_model, max_pos_id)
            elif position_embedding_type in ["relative_key_simple", "relative_key_query_simple"]:
                self._calculate_relative_position_embedding()
                self.forward_relative_pe = True
            elif position_embedding_type == "relative_position_bias":
                max_distance = rel_pos_num_buckets * rel_pos_bucket_size
                self.rel_pos_encoder = RelativePositionBias(padded_length=self.padded_length, causal=causal_conv,
                                                            heads=nhead,
                                                            num_buckets=rel_pos_num_buckets, max_distance=max_distance)
                self.forward_relative_pe = True

        if self.pos_encoder is not None and scale_pos:
            self.pe_factor = torch.nn.Parameter(torch.ones(1))
        else:
            self.pe_factor = None

        if initial_norm:
            self.initial_norm = nn.LayerNorm(d_model)
        else:
            self.initial_norm = None
        self.dropout = nn.Dropout(hidden_dropout)

        if position_embedding_type is None or ("relative" not in position_embedding_type):
            _pos_embed_type = "absolute"
        else:
            _pos_embed_type = position_embedding_type

        encoder_layer = EncoderBlock(d_model, nhead, pre_norm=pre_norm, ff_type=ff_type,
                                     d_ff=dim_ff, activation=activation,
                                     causal_conv=causal_conv, dilation=conv_dilation, kernel_size=conv_kernel,
                                     tcn_layers=conv_layers,
                                     ff_dropout=ff_dropout, attn_dropout=attn_dropout, hidden_dropout=hidden_dropout,
                                     attn_impl=attn_impl, attn_sparsity=attn_sparsity, topk=topk,
                                     max_seq_len=max_pos_id, position_embedding_type=_pos_embed_type,
                                     T5_attn_config=T5_attn_config,
                                     gated_attn=gated_attn, gate_fn=gate_fn, forward_only=forward_only)
        self.encoder = EncoderStack(encoder_layer, n_encoder_blocks)
        if pre_norm:
            self.norm = nn.LayerNorm(d_model)

        self.out_proj = nn.Linear(d_model, num_class)

        self._calculate_attention_mask(padded_length, is_causal=causal_conv,
                                       local_window_size=attn_local_window, local_stride=attn_local_stride,
                                       global_stride=attn_global_stride, global_attn_type=attn_global_type)

        self._init_weights()

    def _init_weights(self):
        """  The Annotated Transformer:
        # Initialize parameters with Glorot / fan_avg.
        for p in self.parameters():
            if p.dim() > 1:
                torch.nn.init.xavier_uniform(p)
        """
        l_in = (1 / self.in_dim) ** 0.5
        nn.init.uniform_(self.in_proj.weight, a=-l_in, b=l_in)
        nn.init.uniform_(self.in_proj.bias, a=-l_in, b=l_in)

        l_out = (1 / self.d_model) ** 0.5
        nn.init.uniform_(self.out_proj.weight, a=-l_out, b=l_out)
        nn.init.uniform_(self.out_proj.bias, a=-l_out, b=l_out)

    def _calculate_relative_position_embedding(self):
        attention_head_size = int(self.d_model / self.nhead)
        seq_length = self.padded_length  # that's too much :(
        pe = PositionalEncoding.calc_sinusoidal_position_encoding(2 * seq_length - 1, attention_head_size)

        position_ids_l = torch.arange(seq_length, dtype=torch.long).view(-1, 1)
        position_ids_r = torch.arange(seq_length, dtype=torch.long).view(1, -1)
        distance = position_ids_l - position_ids_r
        distance += seq_length - 1
        pe_relative = pe[distance]
        print("pe_relative.shape", pe_relative.shape)
        self.register_buffer('pe_relative', pe_relative)

    def _calculate_attention_mask(self, size, is_causal=True, local_window_size=-1, local_stride=1, global_stride=-1,
                                  global_attn_type="grid"):
        if local_window_size > 0:  # use (strided) local + global attention pattern
            mask = torch.eye(size, dtype=torch.bool)
            for i in range(size):
                k = local_stride
                while k <= local_window_size:
                    if i + k < size:
                        mask[i, i + k] = True
                    if i - k >= 0:
                        mask[i, i - k] = True
                    k += local_stride
            if global_stride > 0:
                assert(global_attn_type in ["grid", "custom_linear", "custom_double"])
                if global_attn_type == "grid":
                    for i in range(int(global_stride / 2), size, global_stride):
                        mask[i, :] = True
                        mask[:, i] = True
                else:
                    for i in range(size):
                        k = global_stride
                        while k < size:
                            if i + k < size:
                                mask[i, i + k] = True
                            if i - k >= 0:
                                mask[i, i - k] = True
                            if global_attn_type == "custom_linear":
                                k += global_stride
                            elif global_attn_type == "custom_double":
                                k *= 2
        else:
            mask = torch.ones(size, size, dtype=torch.bool)

        if is_causal:
            causal_mask = torch.triu(torch.ones(size, size)).transpose(0, 1)
            causal_mask = (causal_mask == 1)  # to bool 
            mask = mask * causal_mask

        print("mask.shape", mask.shape)
        self.register_buffer('attn_mask', torch.logical_not(mask))

    def forward(self, seq, position_ids, key_padding_mask=None, output_attentions = False):  # seq dim: S x N x E, position_ids: N x S

        seq = self.in_proj(seq)
        if self.pos_encoder is not None or self.forward_relative_pe:
            if self.forward_relative_pe:
                if self.position_embedding_type == "relative_position_bias":
                    pos_embeddings = self.rel_pos_encoder(None)
                else:
                    pos_embeddings = self.pe_relative  # S x S x D
            else:
                pos_embeddings = self.pos_encoder(position_ids)  # N x S x F
                pos_embeddings = pos_embeddings.transpose(0, 1)  # S x N x F
            if self.pe_factor is not None:
                pos_embeddings *= self.pe_factor
            if not (self.forward_relative_pe or self.repeat_pos_info):  # otherwise, each encoder block will handle
                                                                        # pos info individually
                seq = seq + pos_embeddings
        else:
            pos_embeddings = None
        if self.initial_norm is not None:
            seq = self.initial_norm(seq)
        seq = self.dropout(seq)
        if output_attentions:
            outputs,a = self.encoder(seq, attn_mask=self.attn_mask, key_padding_mask=key_padding_mask,
                              pos_embeddings=(pos_embeddings if (self.forward_relative_pe or self.repeat_pos_info)
                                              else None), output_attentions = output_attentions)
        else:
            outputs = self.encoder(seq, attn_mask=self.attn_mask, key_padding_mask=key_padding_mask,
                                  pos_embeddings=(pos_embeddings if (self.forward_relative_pe or self.repeat_pos_info)
                                                  else None))
        final_outputs = ()
        for output in outputs:  # output: T x N x E
            if self.pre_norm:
                 output = self.norm(output)
            output = self.out_proj(output)  # output: T x N x C
            final_outputs = final_outputs + (output, )
        # separate proj matrices for each stage ?
        if output_attentions:
            return final_outputs, a
        return final_outputs


class PositionalEncoding(nn.Module):
    # ``PositionalEncoding`` module injects some information about the
    # relative or absolute position of the tokens in the sequence. The
    # positional encodings have the same dimension as the embeddings so that
    # the two can be summed. Here, we use ``sine`` and ``cosine`` functions of
    # different frequencies.
    #
    # From https://pytorch.org/tutorials/beginner/transformer_tutorial.html

    def __init__(self, d_model, max_seq_len=5000):
        super(PositionalEncoding, self).__init__()

        pe = PositionalEncoding.calc_sinusoidal_position_encoding(max_seq_len, d_model)
        self.register_buffer('pe', pe)

    @staticmethod
    def calc_sinusoidal_position_encoding(max_seq_len, d_model):
        pe = torch.zeros(max_seq_len, d_model)  # S x F
        position = torch.arange(0, max_seq_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, d_model, 2).float() * (-math.log(10000.0) / d_model))
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)

        return pe

    def forward(self, position_ids):
        return self.pe[position_ids, :]  # N x S x F


# https://github.com/lucidrains/x-transformers/
class RelativePositionBias(nn.Module):
    def __init__(self, padded_length, causal = False, num_buckets = 32, max_distance = 128, heads = 8):
        super().__init__()
        self.causal = causal
        self.num_buckets = num_buckets
        self.max_distance = max_distance
        self.relative_attention_bias = nn.Embedding(num_buckets, heads)

        q_pos = torch.arange(padded_length, dtype = torch.long)
        k_pos = torch.arange(padded_length, dtype = torch.long)
        rel_pos = k_pos[None, :] - q_pos[:, None]
        rp_bucket = self._relative_position_bucket(rel_pos, causal=self.causal, num_buckets=self.num_buckets,
                                                   max_distance=self.max_distance)
        self.register_buffer('rp_bucket', rp_bucket)

    @staticmethod
    def _relative_position_bucket(relative_position, causal = True, num_buckets = 32, max_distance = 128):
        ret = 0
        n = -relative_position
        if not causal:
            num_buckets //= 2
            ret += (n < 0).long() * num_buckets
            n = torch.abs(n)
        else:
            n = torch.max(n, torch.zeros_like(n))

        max_exact = num_buckets // 2
        is_small = n < max_exact

        val_if_large = max_exact + (
            torch.log(n.float() / max_exact) / math.log(max_distance / max_exact) * (num_buckets - max_exact)
        ).long()
        val_if_large = torch.min(val_if_large, torch.full_like(val_if_large, num_buckets - 1))

        ret += torch.where(is_small, n, val_if_large)
        return ret

    def forward(self, x=None):
        bias = self.relative_attention_bias(self.rp_bucket)
        return bias.permute(2, 0, 1)  # Q x K x H --> H x Q x K


class EncoderStack(nn.Module):
    def __init__(self, encoder_layer, num_layers):
        super(EncoderStack, self).__init__()
        self.layers = _get_clones(encoder_layer, num_layers)
        self.num_layers = num_layers

    def forward(self, x, attn_mask=None, key_padding_mask=None, pos_embeddings=None, output_attentions = False):
        outputs = ()
        output = x
        for _, layer in enumerate(self.layers):
            if _ == 0 and output_attentions:
                output,a = layer(output, attn_mask=attn_mask, key_padding_mask=key_padding_mask,
                           pos_embeddings=pos_embeddings, output_attentions = output_attentions)
            else:
                output = layer(output, attn_mask=attn_mask, key_padding_mask=key_padding_mask,
                               pos_embeddings=pos_embeddings)
            outputs = outputs + (output, )
        if output_attentions:
            return outputs, a
        return outputs


def _get_clones(module, N):
    return nn.ModuleList([copy.deepcopy(module) for i in range(N)])


class EncoderBlock(nn.Module):
    def __init__(self, d_model, nhead, pre_norm=True, ff_type="feed_forward",
                 d_ff=2048, activation="relu",
                 causal_conv=True, dilation=1, kernel_size=3, tcn_layers=1,
                 ff_dropout=0.5, attn_dropout=0.1, hidden_dropout=0.1,
                 attn_impl="pytorch", attn_sparsity=None, topk=None,
                 max_seq_len=None, position_embedding_type="absolute", T5_attn_config=None,
                 gated_attn=False, gate_fn="sigmoid", forward_only=False,
                 ):
        super(EncoderBlock, self).__init__()
        self.pre_norm = pre_norm
        self.has_relative_pe = ("relative" in position_embedding_type)
        self.has_conv = (ff_type == "conv" or ff_type == "tcn")

        if gated_attn:
            self.attn_block = GatedAttentionSublayer(d_model, nhead, dropout=attn_dropout, pre_norm=pre_norm,
                                                     hidden_dropout=hidden_dropout, attn_impl=attn_impl,
                                                     attn_sparsity=attn_sparsity, topk=topk, max_seq_len=max_seq_len,
                                                     position_embedding_type=position_embedding_type,
                                                     T5_attn_config=T5_attn_config,
                                                     activation=activation, gate_fn=gate_fn, forward_only=forward_only)
        else:
            self.attn_block = AttentionSublayer(d_model, nhead, dropout=attn_dropout, pre_norm=pre_norm,
                                                hidden_dropout=hidden_dropout, attn_impl=attn_impl,
                                                attn_sparsity=attn_sparsity, topk=topk, max_seq_len=max_seq_len,
                                                position_embedding_type=position_embedding_type,
                                                T5_attn_config=T5_attn_config)
        if ff_type == "feed_forward":
            self.ff_block = FeedForwardSublayer(d_model, d_ff=d_ff, dropout=ff_dropout, pre_norm=pre_norm,
                                                hidden_dropout=hidden_dropout, activation=activation)
        elif ff_type == "glu":
            self.ff_block = GatedFeedForwardSublayer(d_model, d_ff=d_ff, dropout=ff_dropout, pre_norm=pre_norm,
                                                     hidden_dropout=hidden_dropout, activation=activation)
        elif ff_type == "conv":
            norm = "pre-norm" if pre_norm else "post-norm"
            self.ff_block = DilatedConvSublayer(dilation, d_model, causal_conv=causal_conv, dropout=ff_dropout,
                                                kernel_size=kernel_size, norm=norm, hidden_dropout=hidden_dropout,
                                                activation=activation)
        elif ff_type == "tcn":
            self.ff_block = TCN_Sublayer(num_layers=tcn_layers, d_model=d_model, causal_conv=causal_conv,
                                         dropout=ff_dropout, pre_norm=pre_norm, hidden_dropout=hidden_dropout,
                                         activation=activation, kernel_size=kernel_size, dilation=dilation)
        else:
            assert(ff_type == "none")
            self.ff_block = None

    def forward(self, x, attn_mask=None, key_padding_mask=None, pos_embeddings=None, output_attentions = False):
        if not self.has_relative_pe and pos_embeddings is not None:
            x = x + pos_embeddings   # inject position information
        if output_attentions:
            attn_out,a = self.attn_block(x, attn_mask=attn_mask, key_padding_mask=key_padding_mask,
                                       pos_embeddings=pos_embeddings, output_attentions = output_attentions)
        else:
            attn_out = self.attn_block(x, attn_mask=attn_mask, key_padding_mask=key_padding_mask,
                                       pos_embeddings=pos_embeddings)
        # print("attn_out", attn_out)
        if self.ff_block is not None:
            if self.has_conv:
                out = self.ff_block(attn_out, key_padding_mask)
            else:
                out = self.ff_block(attn_out)
        else:
            out = attn_out
        # print("out", out)
        if output_attentions:
            return out,a
        return out


class AttentionSublayer(nn.Module):
    def __init__(self, d_model, nhead, dropout=0.5, pre_norm=True, hidden_dropout=0.1, attn_impl="pytorch",
                 attn_sparsity=None, topk=None,  # sparsity
                 max_seq_len=None, position_embedding_type="absolute",  # relative pos embeddings
                 T5_attn_config=None  # --> num_buckets (32), max_distance (128), is_bidirectional (False)
                 ):
        super(AttentionSublayer, self).__init__()

        self.pre_norm = pre_norm
        self.norm = nn.LayerNorm(d_model)
        self.self_attn = AttentionWrapper(d_model, nhead, dropout,
                                          attn_impl=attn_impl, attn_sparsity=attn_sparsity, topk=topk,
                                          max_pos_id=max_seq_len, position_embedding_type=position_embedding_type,
                                          T5_attn_config=T5_attn_config)
        self.dropout = nn.Dropout(hidden_dropout)

        self._init_weights()

    def _init_weights(self):
        pass

    def forward(self, x, attn_mask=None, key_padding_mask=None, pos_embeddings=None,output_attentions = False):
        out = self.norm(x) if self.pre_norm else x
        if output_attentions:
            out,a = self.self_attn(out, attn_mask=attn_mask, key_padding_mask=key_padding_mask,
                             pos_embeddings=pos_embeddings,output_attentions = output_attentions)
        else:
            out = self.self_attn(out, attn_mask=attn_mask, key_padding_mask=key_padding_mask,
                                 pos_embeddings=pos_embeddings)

        #print("out ", out.size())
        #print("x ", x.size())

        out = x + self.dropout(out)
        if not self.pre_norm:
            out = self.norm(out)
        if output_attentions:
            return out,a
        return out


class GatedAttentionSublayer(nn.Module):
    def __init__(self, d_model, nhead, dropout=0.5, pre_norm=True, hidden_dropout=0.1, attn_impl="pytorch",
                 attn_sparsity=None, topk=None,  # sparsity
                 max_seq_len=None, position_embedding_type="absolute",  # relative pos embeddings
                 T5_attn_config=None,  # --> num_buckets (32), max_distance (128), is_bidirectional (False)
                 activation="relu", gate_fn="sigmoid", forward_only=False,
                 ):
        super(GatedAttentionSublayer, self).__init__()

        self.forward_only = forward_only
        self.pre_norm = pre_norm
        self.norm = nn.LayerNorm(d_model)
        self.self_attn = AttentionWrapper(d_model, nhead, dropout,
                                          attn_impl=attn_impl, attn_sparsity=attn_sparsity, topk=topk,
                                          max_pos_id=max_seq_len, position_embedding_type=position_embedding_type,
                                          T5_attn_config=T5_attn_config)


        self.linear_gate_c = nn.Linear(d_model, d_model)
        self.linear_gate_x = nn.Linear(d_model, d_model)
        self.gate_fn = get_activation(gate_fn)

        if not self.forward_only:
            self.linear_out_c = nn.Linear(d_model, d_model)
            self.linear_out_x = nn.Linear(d_model, d_model)
            self.activation = get_activation(activation)

        self.dropout = nn.Dropout(hidden_dropout)

        self._init_weights()

    def _init_weights(self):
        nn.init.xavier_uniform_(self.linear_gate_c.weight)
        nn.init.constant_(self.linear_gate_c.bias, 0.)
        nn.init.xavier_uniform_(self.linear_gate_x.weight)
        nn.init.constant_(self.linear_gate_x.bias, 0.)

        if not self.forward_only:
            nn.init.kaiming_normal_(self.linear_out_c.weight, mode='fan_out', nonlinearity='relu')
            nn.init.constant_(self.linear_out_c.bias, 0.)
            nn.init.kaiming_normal_(self.linear_out_x.weight, mode='fan_out', nonlinearity='relu')
            nn.init.constant_(self.linear_out_x.bias, 0.)

    def forward(self, x, attn_mask=None, key_padding_mask=None, pos_embeddings=None):
        residual = x
        x = self.norm(x) if self.pre_norm else x
        context = self.self_attn(x, attn_mask=attn_mask, key_padding_mask=key_padding_mask,
                                 pos_embeddings=pos_embeddings)
        gate = self.gate_fn(self.linear_gate_c(context) + self.linear_gate_x(x))

        if self.forward_only:
            out = context * gate  # gated context
        else:
            gated_context = self.linear_out_c(context) * gate
            out = self.activation(gated_context + self.linear_out_x(x))  # this should be a more meaningful summary ;)

        # print("out", out)
        out = residual + self.dropout(out)
        if not self.pre_norm:
            out = self.norm(out)
        return out

class AttentionWrapper(nn.Module):
    def __init__(self, d_model, nhead, dropout, attn_impl="pytorch",
                 attn_sparsity=None, topk=None,  # sparsity
                 max_pos_id=None, position_embedding_type="absolute",  # relative pos embeddings
                 T5_attn_config=None):
        # max_pos_id: required for relative position embeddings - basically max position id that can ever occur
        super(AttentionWrapper, self).__init__()

        assert (position_embedding_type in ["absolute", "relative_key", "relative_key_query",
                                            "relative_T5", "relative_position_bias",
                                            "relative_key_simple", "relative_key_query_simple"])

        if attn_sparsity is not None:
            if attn_impl != "bert_hf":
                print("Sparse attention only implemented in BERT-like attention (huggingface)")
            assert(position_embedding_type != "relative_T5")
            if position_embedding_type != "absolute":
                assert (max_pos_id is not None)
            self.attn = BertSelfAttention(d_model, nhead, max_pos_id, position_embedding_type=position_embedding_type,
                                          attention_probs_dropout_prob=dropout, embedding_size=None,
                                          attn_sparsity=attn_sparsity, topk=topk)
            self.type = "bert_hf"
        elif position_embedding_type == "relative_T5":
            assert(T5_attn_config is not None)
            num_buckets = T5_attn_config.num_buckets  # e.g. 32
            max_distance = T5_attn_config.max_distance  # e.g. 128
            is_bidirectional = T5_attn_config.is_bidirectional  # --> True if model not causal
            self.attn = T5Attention(d_model, nhead, dropout, has_relative_attention_bias=True,
                                    relative_attention_num_buckets=num_buckets,
                                    relative_attention_max_distance=max_distance,
                                    relative_attention_bidirectional=is_bidirectional,
                                    d_kv=None, is_decoder=False)
            self.type = "T5_hf"
        elif position_embedding_type == "absolute" and attn_impl == "pytorch":
            self.attn = nn.MultiheadAttention(d_model, nhead, dropout=dropout)
            """
            input: S x N x d_model
            attention_mask (same for all heads and all instances in the batch): S x S
            key_padding_mask: N x S 
                masks are Boolean tensors, "True" positions will be masked as float(-inf)

            returns:
            attn_output: T x N x d_model
            optionally - attn_output_weights: (N * h) x T x S
            """
            self.type = "pytorch"
        elif attn_impl == "lsh":
            self.attn = LSHSelfAttention(dim = d_model,heads = nhead,bucket_size = 64,n_hashes = 8,causal = True)
            self.type = "lsh"
        elif attn_impl == "performer":
            self.attn = SelfAttention(dim = d_model,heads = nhead,causal = True)
            self.type = "performer"
        else:
            assert(max_pos_id is not None)
            self.attn = BertSelfAttention(d_model, nhead, max_pos_id, position_embedding_type=position_embedding_type,
                                          attention_probs_dropout_prob=dropout, embedding_size=None)
            """
            input:
            hidden_states: N x S x d_model
            attention_mask (same for all heads and all instances in the batch): S x S
            key_padding_mask: N x S 
                masks are Boolean tensors, "True" positions will be masked as float(-inf)

            returns:
            context_layer: N x S x d_model
            optionally - attention_probs: N x h x S x S        
            """
            self.type = "bert_hf"

        self._init_weights()

    def _init_weights(self):
        if self.type == "pytorch":
            """
            Pytorch implementation: 
            if self._qkv_same_embed_dim:
                xavier_uniform_(self.in_proj_weight)
            else:
                xavier_uniform_(self.q_proj_weight)
                xavier_uniform_(self.k_proj_weight)
                xavier_uniform_(self.v_proj_weight)
    
            if self.in_proj_bias is not None:
                constant_(self.in_proj_bias, 0.)
                constant_(self.out_proj.bias, 0.)
            if self.bias_k is not None:
                xavier_normal_(self.bias_k)
            if self.bias_v is not None:
                xavier_normal_(self.bias_v)
            """
            nn.init.xavier_uniform_(self.attn.out_proj.weight)
            nn.init.constant_(self.attn.out_proj.bias, 0.)

    def forward(self, x, attn_mask=None, key_padding_mask=None, pos_embeddings=None, output_attentions = False):
        if self.type == "pytorch":
            # attn_mask = None
            # key_padding_mask = None
            # print("attn_mask", attn_mask)
            # print("key_padding_mask", key_padding_mask)
            return self.attn(x, x, x, attn_mask=attn_mask, key_padding_mask=key_padding_mask)[0]
        elif self.type == "bert_hf":
            x = x.transpose(0, 1)  # S x N x F --> N x S x F
            if output_attentions == False:
                x = self.attn(x, attn_mask=attn_mask, key_padding_mask=key_padding_mask,
                              rel_pos_embeddings=pos_embeddings,
                              head_mask=None, encoder_hidden_states=None, encoder_attention_mask=None,
                              past_key_value=None, output_attentions=False)[0]
                return x.transpose(0, 1)  # N x S x F --> S x N x F
            else:
                x,a = self.attn(x, attn_mask=attn_mask, key_padding_mask=key_padding_mask,
                              rel_pos_embeddings=pos_embeddings,
                              head_mask=None, encoder_hidden_states=None, encoder_attention_mask=None,
                              past_key_value=None, output_attentions=True)#[0]
                #print(a.size())
                #print(x.size())
                #exit()
            return x.transpose(0, 1),a  # N x S x F --> S x N x F
        elif self.type == "lsh":
            x = x.transpose(0, 1)  # S x N x F --> N x S x F
            #keys = None, input_mask = None, input_attn_mask = None, context_mask = None
            #print(x.size())
            x = self.attn(x,input_mask=key_padding_mask)#[0]
            #x = x.unsqueeze(1)

            return x.transpose(0, 1)  # N x S x F --> S x N x F
        elif self.type == "performer":
            x = x.transpose(0, 1)  # S x N x F --> N x S x F
            x = self.attn(x,imask=key_padding_mask)#[0]
            return x.transpose(0, 1)  # N x S x F --> S x N x F
        else:
            assert(self.type == "T5_hf")
            x = x.transpose(0, 1)  # S x N x F --> N x S x F
            x = self.attn(x, attn_mask=attn_mask, key_padding_mask=key_padding_mask,
                          position_bias=None, key_value_states=None, past_key_value=None, layer_head_mask=None,
                          query_length=None, use_cache=False, output_attentions=False)[0]
            return x.transpose(0, 1)  # N x S x F --> S x N x F


class T5AttnConfig:
    def __init__(self, num_buckets=32, max_distance=128, is_bidirectional=True):
        self.num_buckets = num_buckets
        self.max_distance = max_distance
        self.is_bidirectional = is_bidirectional


class FeedForwardSublayer(nn.Module):
    def __init__(self, d_model, d_ff=2048, dropout=0.5, pre_norm=True, hidden_dropout=0.1, activation="relu"):
        super(FeedForwardSublayer, self).__init__()

        self.pre_norm = pre_norm
        self.norm = nn.LayerNorm(d_model)

        self.linear1 = nn.Linear(d_model, d_ff)
        self.activation = get_activation(activation)
        self.dropout = nn.Dropout(dropout)
        self.linear2 = nn.Linear(d_ff, d_model)
        self.dropout2 = nn.Dropout(hidden_dropout)

        self._init_weights()

    def _init_weights(self):
        nn.init.kaiming_normal_(self.linear1.weight, mode='fan_out', nonlinearity='relu')
        nn.init.constant_(self.linear1.bias, 0.)

        nn.init.xavier_uniform_(self.linear2.weight)
        nn.init.constant_(self.linear2.bias, 0.)

    def forward(self, x):
        out = self.norm(x) if self.pre_norm else x
        out = self.linear2(self.dropout(self.activation(self.linear1(out))))
        out = x + self.dropout2(out)
        if not self.pre_norm:
            out = self.norm(out)
        return out


class GatedFeedForwardSublayer(nn.Module):
    def __init__(self, d_model, d_ff=2048, dropout=0.5, pre_norm=True, hidden_dropout=0.1, activation="gelu_new"):
        super(GatedFeedForwardSublayer, self).__init__()

        self.pre_norm = pre_norm
        self.norm = nn.LayerNorm(d_model)
        self.ff = T5DenseGatedGeluDense(d_model, d_ff, dropout, activation)
        self.dropout2 = nn.Dropout(hidden_dropout)

        self._init_weights()

    def _init_weights(self):
        pass

    def forward(self, x):
        out = self.norm(x) if self.pre_norm else x
        out = self.ff(out)
        out = x + self.dropout2(out)
        if not self.pre_norm:
            out = self.norm(out)
        return out


class TCN_Sublayer(nn.Module):
    def __init__(self, num_layers, d_model, dropout=0.5, pre_norm=True, hidden_dropout=0.1, causal_conv=False,
                 activation="relu", kernel_size=None, dilation=None):
        super(TCN_Sublayer, self).__init__()

        if num_layers == 1 and kernel_size is not None and dilation is not None:
            self.layers = nn.ModuleList([
                DilatedConvSublayer(dilation=dilation,
                                    kernel_size=kernel_size,
                                    d_model=d_model,
                                    causal_conv=causal_conv,
                                    dropout=dropout,
                                    hidden_dropout=hidden_dropout,
                                    norm="pre-norm" if pre_norm else "post-norm",
                                    activation=activation)])
        else:
            self.layers = nn.ModuleList([copy.deepcopy(  # required ?
                DilatedConvSublayer(2 ** i,
                                    kernel_size=3,
                                    d_model=d_model,
                                    causal_conv=causal_conv,
                                    dropout=dropout,
                                    hidden_dropout=hidden_dropout,
                                    norm="pre-norm" if pre_norm else "post-norm",
                                    activation=activation))
                for i in range(num_layers)
            ])

        self._init_weights()

    def _init_weights(self):
        pass

    def forward(self, x, key_padding_mask):
        out = x
        for layer in self.layers:
            out = layer(out, key_padding_mask)
        return out


class DilatedConvSublayer(nn.Module):
    def __init__(self, dilation, d_model, causal_conv=False, dropout=0.0, kernel_size=3,
                 norm="none", hidden_dropout=0.5, activation="relu"):
        super(DilatedConvSublayer, self).__init__()
        if norm == "none":  # this is ugly ;P
            self.norm = None
        else:
            self.norm = nn.LayerNorm(d_model)
            if norm == "pre-norm":
                self.pre_norm = True
            else:
                assert (norm == "post-norm")
                self.pre_norm = False
        self.causal_conv = causal_conv
        self.dilation = dilation
        self.kernel_size = kernel_size
        if self.causal_conv:
            self.conv_dilated = nn.Conv1d(d_model,
                                          d_model,
                                          kernel_size,
                                          padding=(dilation * (kernel_size - 1)),
                                          dilation=dilation)
        else:
            self.conv_dilated = nn.Conv1d(d_model,
                                          d_model,
                                          kernel_size,
                                          padding=(dilation * (kernel_size - 1)) // 2,
                                          dilation=dilation)
        self.activation = get_activation(activation)
        self.dropout = nn.Dropout(dropout)
        self.conv_1x1 = nn.Conv1d(d_model, d_model, 1)
        self.dropout2 = nn.Dropout(hidden_dropout)

        self._init_weights()

    def _init_weights(self):
        nn.init.kaiming_normal_(self.conv_dilated.weight, mode='fan_out', nonlinearity='relu')
        nn.init.constant_(self.conv_dilated.bias, 0.)

        nn.init.xavier_uniform_(self.conv_1x1.weight)
        nn.init.constant_(self.conv_1x1.bias, 0.)

    def forward(self, x, key_padding_mask):  # x is of shape S x N x d_model, mask is N x S (False --> keep, True --> mask)
        residual = x
        out = self.norm(x) if (self.norm is not None and self.pre_norm) else x

        out = out.permute(1, 2, 0)  # S x N x F --> N x F x S
        out = out * torch.logical_not(key_padding_mask).long().unsqueeze(1)  # keep un-masked values
#         print("before conv_dilated", out.shape)
        out = self.conv_dilated(out)
#        print("after conv_dilated", out.shape)
        if self.causal_conv:
            out = out[:, :, :-(self.dilation * (self.kernel_size - 1))]
#        print("before conv_1x1", out.shape)
        out = self.conv_1x1(self.dropout(self.activation(out)))
#        print("after conv_1x1", out.shape)
        out = self.dropout2(out)

        residual = residual.permute(1, 2, 0)  # S x N x F --> N x F x S
        out = residual + out
        out = out * torch.logical_not(key_padding_mask).long().unsqueeze(1)  # to be sure

        out = out.permute(2, 0, 1)  # N x F x S --> S x N x F
        if self.norm is not None and (not self.pre_norm):
            out = self.norm(out)
        return out


class MySingleStageModel(nn.Module):
    def __init__(self, num_layers, d_model, in_dim, num_classes,
                 dropout=0.0, hidden_dropout=0.5, norm="none", activation="relu",
                 causal_conv=False, linear_proj=False, do_initial_norm=False):
        super(MySingleStageModel, self).__init__()

        self.in_dim = in_dim
        self.d_model = d_model

        if do_initial_norm:
            self.initial_norm = nn.LayerNorm(d_model)
        else:
            self.initial_norm = None
        if norm == "pre-norm":  # add final normalization layer
            self.final_norm = nn.LayerNorm(d_model)
        else:
            self.final_norm = None

        if linear_proj:
            self.in_proj = nn.Linear(in_dim, d_model)
        else:
            self.in_proj = nn.Conv1d(in_dim, d_model, 1)
        self.layers = nn.ModuleList([copy.deepcopy(  # required ?
            DilatedConvSublayer(2 ** i,
                                d_model=d_model,
                                causal_conv=causal_conv,
                                dropout=dropout,
                                hidden_dropout=hidden_dropout,
                                norm=norm,
                                activation=activation))
            for i in range(num_layers)
        ])
        if linear_proj:
            self.out_proj = nn.Linear(d_model, num_classes)
        else:
            self.out_proj = nn.Conv1d(d_model, num_classes, 1)
        self.do_linear_proj = linear_proj

        self._init_weights()

    def _init_weights(self):
        if self.do_linear_proj:
            l_in = (1 / self.in_dim) ** 0.5
            nn.init.uniform_(self.in_proj.weight, a=-l_in, b=l_in)
            nn.init.uniform_(self.in_proj.bias, a=-l_in, b=l_in)

            l_out = (1 / self.d_model) ** 0.5
            nn.init.uniform_(self.out_proj.weight, a=-l_out, b=l_out)
            nn.init.uniform_(self.out_proj.bias, a=-l_out, b=l_out)

    def forward(self, x, key_padding_mask):
        if self.do_linear_proj:
            out = self.in_proj(x)
        else:
            x = x.permute(1, 2, 0)  # S x N x F --> N x F x S
            out = self.in_proj(x)
            out = out.permute(2, 0, 1)  # N x F x S --> S x N x F
        if self.initial_norm is not None:
            out = self.initial_norm(out)

        for layer in self.layers:
            out = layer(out, key_padding_mask)
        if self.final_norm is not None:
            out = self.final_norm(out)

        if self.do_linear_proj:
            out = self.out_proj(out)  # S x N x F
            out = out * torch.logical_not(key_padding_mask.transpose(0, 1)).long().unsqueeze(-1)  # keep un-masked values
        else:
            out = out.permute(1, 2, 0)  # S x N x F --> N x F x S
            out = self.out_proj(out) * torch.logical_not(key_padding_mask).long().unsqueeze(1)  # keep un-masked values
            out = out.permute(2, 0, 1)  # N x F x S --> S x N x F
        return out

""" 
class TCN(nn.Module):  # @deprecated
    def __init__(self, num_layers, d_model, causal_conv=False, dropout=0.0, hidden_dropout=0.5):
        super(TCN, self).__init__()
        self.layers = nn.ModuleList([copy.deepcopy(  # required ?
            DilatedConvSublayer(2 ** i,
                                d_model=d_model,
                                causal_conv=causal_conv,
                                dropout=dropout,
                                hidden_dropout=hidden_dropout,
                                norm="none",
                                activation="relu"))
            for i in range(num_layers)
        ])

    def forward(self, x, key_padding_mask):
        for layer in self.layers:
            x = layer(x, key_padding_mask)
        return x
"""


class MyMultiStageModel(nn.Module):
    def __init__(self, num_stages, num_layers, d_model, in_dim, num_classes, causal_conv,
                 dropout=0.0, hidden_dropout=0.5, norm="none", activation="relu",
                 linear_proj=False, reduce_dim=False, inner_softmax=True, do_initial_norm=False):
        super(MyMultiStageModel, self).__init__()
        self.inner_softmax = inner_softmax
        self.stage1 = MySingleStageModel(num_layers,
                                         in_dim=in_dim,
                                         d_model=d_model,
                                         num_classes=num_classes,
                                         dropout=dropout, hidden_dropout=hidden_dropout, norm=norm, activation=activation,
                                         causal_conv=causal_conv,
                                         linear_proj=linear_proj,
                                         do_initial_norm=do_initial_norm)
        self.stages = nn.ModuleList([copy.deepcopy(  # required?
            MySingleStageModel(num_layers,
                               in_dim=num_classes,
                               d_model=num_classes if reduce_dim else d_model,
                               num_classes=num_classes,
                               dropout=dropout, hidden_dropout=hidden_dropout, norm=norm, activation=activation,
                               causal_conv=causal_conv, linear_proj=linear_proj,
                               do_initial_norm=do_initial_norm)
        )
            for _ in range(num_stages - 1)
        ])

    def forward(self, x, pos_ids, key_padding_mask):
        out = self.stage1(x, key_padding_mask)  # S x N x F
        outputs = (out,)
        for s in self.stages:
            if self.inner_softmax:
                out = out.permute(1, 2, 0)  # S x N x F --> N x F x S
                out = F.softmax(out, dim=1) * torch.logical_not(key_padding_mask).long().unsqueeze(1)  # keep un-masked values
                out = out.permute(2, 0, 1)  # N x F x S --> S x N x F
            out = s(out, key_padding_mask)
            outputs = outputs + (out, )
        return outputs


""" 
class ScaleNorm(nn.Module):
    # https://github.com/tnq177/transformers_without_tears
    # ScaleNorm(d_model ** 0.5)

    def __init__(self, scale, eps=1e-5):
        super(ScaleNorm, self).__init__()
        self.scale = Parameter(torch.tensor(scale))
        self.eps = eps

    def forward(self, x):
        norm = self.scale / torch.norm(x, dim=-1, keepdim=True).clamp(min=self.eps)
        return x * norm
"""

# ***** MS-TCN adapted from https://github.com/yabufarha/ms-tcn *****
# causal convolutions --> https://github.com/tobiascz/TeCNO

class MyMultiStageModelWrapper(nn.Module):
    def __init__(self, num_stages, num_layers, d_model, in_dim, num_classes, causal_conv):
        super(MyMultiStageModelWrapper, self).__init__()
        self.model = MultiStageModel(num_stages, num_layers, d_model, in_dim, num_classes, causal_conv,
                                     inner_softmax=True)

    def forward(self, x, pos_ids, mask):
        x = x.permute(1, 2, 0)  # S x N x F --> N x F x S
        mask = torch.logical_not(mask).unsqueeze(1)

        outputs = self.model(x, mask)

        num_stages = len(self.model.stages) + 1
        outputs = outputs.view(num_stages, -1, outputs.shape[-2], outputs.shape[-1])  # model has 1 or 2 stages usually

        out = (outputs[0].permute(2, 0, 1), )  # N x C x S --> S x N x C
        for i in range(1, outputs.shape[0]):
            out = out + (outputs[i].permute(2, 0, 1), )

        return out


class MultiStageModel(nn.Module):
    def __init__(self, num_stages, num_layers, num_f_maps, dim, num_classes, causal_conv, inner_softmax=True):
        super(MultiStageModel, self).__init__()
        self.inner_softmax = inner_softmax
        self.stage1 = SingleStageModel(num_layers,
                                       num_f_maps,
                                       dim,
                                       num_classes,
                                       causal_conv=causal_conv)
        self.stages = nn.ModuleList([copy.deepcopy(  # required?
            SingleStageModel(num_layers,
                             num_f_maps,
                             num_classes,
                             num_classes,
                             causal_conv=causal_conv))
            for _ in range(num_stages - 1)
        ])

    def forward(self, x, mask):
        out = self.stage1(x, mask)
        outputs = out.unsqueeze(0)
        for s in self.stages:
            if self.inner_softmax:
                out = F.softmax(out, dim=1) * mask[:, 0:1, :]
            out = s(out, mask)
            outputs = torch.cat((outputs, out.unsqueeze(0)), dim=0)
        return outputs


class SingleStageModel(nn.Module):
    def __init__(self, num_layers, num_f_maps, dim, num_classes, causal_conv=False):
        super(SingleStageModel, self).__init__()
        self.conv_1x1 = nn.Conv1d(dim, num_f_maps, 1)
        self.layers = nn.ModuleList([copy.deepcopy(  # required ?
            DilatedResidualLayer(2 ** i,
                                 num_f_maps,
                                 num_f_maps,
                                 causal_conv=causal_conv))
            for i in range(num_layers)
        ])
        self.conv_out = nn.Conv1d(num_f_maps, num_classes, 1)

    def forward(self, x, mask):
        out = self.conv_1x1(x)
        for layer in self.layers:
            out = layer(out, mask)
        out = self.conv_out(out) * mask[:, 0:1, :]
        return out


class DilatedResidualLayer(nn.Module):
    def __init__(self, dilation, in_channels, out_channels, causal_conv=False, kernel_size=3):
        super(DilatedResidualLayer, self).__init__()
        self.causal_conv = causal_conv
        self.dilation = dilation
        if self.causal_conv:
            self.conv_dilated = nn.Conv1d(in_channels,
                                          out_channels,
                                          kernel_size,
                                          padding=(dilation * (kernel_size - 1)),
                                          dilation=dilation)
        else:
            self.conv_dilated = nn.Conv1d(in_channels,
                                          out_channels,
                                          kernel_size,
                                          padding=dilation,
                                          dilation=dilation)
        self.conv_1x1 = nn.Conv1d(out_channels, out_channels, 1)
        self.dropout = nn.Dropout()

    def forward(self, x, mask):  # x is of shape N x C_in x S, mask is N x S (1 --> keep, 0 --> mask)
        out = F.relu(self.conv_dilated(x))
        if self.causal_conv:
            out = out[:, :, :-(self.dilation * 2)]
        out = self.conv_1x1(out)
        out = self.dropout(out)
        return (x + out) * mask[:, 0:1, :]  # N x C_out x S -- broadcast mask


""" 
Training:
Standard Adam Optimizer
Data Loader --> input, target, mask

Cross-Entropy & Truncated MSE

Hyperparameters:
num_stages: 4 --> 2
num_layers (per stage): 10 --> 5
feature_dim: 64
dropout: 0.5

num_epochs: 50
learning_rate: 0.0005
sample_rate: 15 fps ?
(batch_size = 1)

(input_dim = 2 * 1024, num_classes)

"""


## Symmetric Dilated Convolution for Surgical Gesture Recognition (???)
class TCN_Attn_EncoderDecoder(nn.Module):
    def __init__(self, dim, num_classes, num_layers=10, num_f_maps=128, downsample=True, attn_layer=True):
        super(TCN_Attn_EncoderDecoder, self).__init__()

        self.downsample = downsample
        self.attn = attn_layer

        self.conv_1x1 = nn.Conv1d(dim, num_f_maps, 1)
        self.enc_layers = nn.ModuleList([copy.deepcopy(  # required ?
            DilatedResidualLayer(2 ** i,
                                 num_f_maps,
                                 num_f_maps,
                                 causal_conv=False))
            for i in range(num_layers)
        ])

        if self.downsample:
            self.maxpool = nn.MaxPool1d(4, stride=4, padding=0, dilation=1)
            self.upsample = nn.ConvTranspose1d(num_f_maps, num_f_maps, 4, stride=4, padding=0, output_padding=0,
                                               dilation=1)

        if self.attn:
            # ???
            self.attention = EncoderBlock(d_model=num_f_maps, nhead=1, pre_norm=False,
                                          d_ff=num_f_maps, dropout=0.5, activation="relu",
                                          )  # vdim=16, kdim=16)  # effect ?

        self.dec_layers = nn.ModuleList([copy.deepcopy(  # required ?
            DilatedResidualLayer(2 ** i,
                                 num_f_maps,
                                 num_f_maps,
                                 causal_conv=False))
            for i in range(num_layers)
        ])

        self.conv_out = nn.Conv1d(num_f_maps, num_classes, 1)

    def forward(self, x, mask):  # N x F x S
        out = self.conv_1x1(x)
        for layer in self.enc_layers:
            out = layer(out, mask)

        if self.downsample:
            out = self.maxpool(out)
            # print("after maxpool", out.shape)

        # attention
        if self.attn:
            if self.downsample:
                attn_key_mask = torch.logical_not(self.maxpool(mask.float()).detach().squeeze(1))  # N x S
            else:
                attn_key_mask = torch.logical_not(mask.squeeze(1))
            out = torch.transpose(out, 1, 2)  # N x F x S --> N x S x F
            out = torch.transpose(out, 0, 1)  # N x S x F --> S x N x F
            out = self.attention(out, src_mask=None, src_key_padding_mask=attn_key_mask)
            out = torch.transpose(out, 0, 1)
            out = torch.transpose(out, 1, 2)

        if self.downsample:
            # print("before upsample", out.shape)
            out = self.upsample(out)
            # print("after upsample", out.shape)

        for layer in self.dec_layers:
            out = layer(out, mask)
        out = self.conv_out(out) * mask[:, 0:1, :]
        return out

class LSTMNet(nn.Module):
    """PhaseNet."""

    lstm_size = 512
    """Number of LSTM neurons."""

    # featureNet: pretrained featureNet (.pkl file) that is to be used
    def __init__(self, in_dim, num_classes):
        """Create PhaseNet.

        :param featureNet: Path to a file that stores the model parameters of a FeatureNet.
                           If specified, the FeatureNet layers will be initialized with weights read from this file.
        """
        super(LSTMNet, self).__init__()
      

        self.lstm = nn.LSTM(in_dim, 512, batch_first=True)
        self.classifier = nn.Linear(512, num_classes)

    def init_hidden(self, device=None):
        return (torch.zeros(1, 1, 512, device=device),
                torch.zeros(1, 1, 512, device=device))

    def forward(self, x):
        h = (torch.zeros(1, 1, 512, device=device),
                torch.zeros(1, 1, 512, device=device))
        x,h = self.lstm(x,h)
        #x = x.view(x.size(1), -1)
        x = self.classifier(x)
        return x

    def load(self, model_file):
        self.load_state_dict(torch.load(model_file))

    def save(self, model_file):
        torch.save(self.state_dict(), model_file)
"""
Training: CE loss, Adam, LR 0.01 
"""


