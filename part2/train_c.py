from train_opts import parser
from models import GestureTransformer, T5AttnConfig, MyMultiStageModel, MyMultiStageModelWrapper, CNN_linear_layer, LSTMNet
from dataset import MyDataSet, MyDataSet_short
from train_utils import LabelSmoothingLoss, truncated_MSE_loss, reg_loss
from metrics import accuracy, average_F1, edit_score, overlap_f1
from util import AverageMeter, splits_LOSO, splits_LOUO, splits_LOUO_NP, gestures_SU, gestures_NP, gestures_KT, \
    JIGSAWS_FPS, MAX_VIDEO_LENGTHS, LABEL_IGN_IDX, Cholec80
import util

import os.path
import datetime
import numpy as np
import math
import torch
from torch.utils.tensorboard import SummaryWriter


def main(args):
    if not torch.cuda.is_available():
        print("GPU not found - exit")
        return

    device_gpu = torch.device("cuda:0")
    device_cpu = torch.device("cpu")

    checkpoint = None
    if args.resume_exp:
        output_folder = args.resume_exp
    else:
        output_folder = os.path.join(args.out, args.exp + "_" + datetime.datetime.now().strftime("%Y%m%d"),
                                     args.eval_scheme, args.split, datetime.datetime.now().strftime("%H%M-%S"))
        os.makedirs(output_folder)

    f_log = open(os.path.join(output_folder, "log.txt"), "a")
    def log(msg):
        util.log(f_log, msg)
    checkpoint_file = os.path.join(output_folder, "checkpoint" + ".pth.tar")

    if args.resume_exp:
        checkpoint = torch.load(checkpoint_file)
        args_checkpoint = checkpoint['args']
        for arg in args_checkpoint:
            setattr(args, arg, args_checkpoint[arg])
        log("====================================================================")
        log("Resuming experiment...")
        log("====================================================================")
    else:
        """ 
        if len([t for t in string.Formatter().parse(args.video_lists_dir)]) > 1:
            args.video_lists_dir = args.video_lists_dir.format(args.task)
        if len([t for t in string.Formatter().parse(args.transcriptions_dir)]) > 1:
            args.transcriptions_dir = args.transcriptions_dir.format(args.task)
        """

        # TODO -- check whether all arguments are valid

        log("Used parameters...")
        for arg in sorted(vars(args)):
            log("\t" + str(arg) + " : " + str(getattr(args, arg)))

    args_dict = {}
    for arg in vars(args):
        args_dict[str(arg)] = getattr(args, arg)

    writer = SummaryWriter(log_dir=output_folder, purge_step=checkpoint['epoch'] if args.resume_exp else None)

    gesture_ids = None
    if args.task == "Suturing":
        gesture_ids = gestures_SU
    elif args.task == "Needle_Passing":
        gesture_ids = gestures_NP
    elif args.task == "Knot_Tying":
        gesture_ids = gestures_KT
    if gesture_ids is not None:
        num_class = len(gesture_ids)
    elif args.task == "Cholec80":
        num_class = Cholec80.num_phases
    elif args.task == "CoBotPhase":
        num_class = 5
    elif args.task == "CoBotStep":
        num_class = 9


    # feature_file = "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/25/20210312-2057/model.pkl"
    # feature_model = CNN_linear_layer(file = feature_file , num_class = num_class)
    # print(feature_model) 
    # for param in feature_model.linear.parameters():
    #         param.requires_grad = False
    # feature_model = feature_model.to(device_gpu)
    feature_model = None

    bool__ = args.task != "Cholec80" and args.task != "CoBotPhase" and args.task != "CoBotStep" 
    if bool__:
        splits = None  # TODO
        if args.eval_scheme == 'LOSO':
            splits = splits_LOSO
        elif args.eval_scheme == 'LOUO':
            if args.task == "Needle_Passing":
                splits = splits_LOUO_NP
            else:
                splits = splits_LOUO
        split_no = int(args.split)
        assert (split_no >= 0 and split_no < len(splits))

    # ===== load data =====

    if bool__:
        assert(torch.load(args.feature_file)['split'] == "{}-{}".format(args.eval_scheme, args.split))
    else:
        features = torch.load(args.feature_file)
        assert (str(features['split']) == str(args.split))
        log("Initial test accuracy (CNN): {:.4f}".format(features['test_accuracy']))

    video_fps = 25 if (args.task == "Cholec80" or args.task == "CoBotPhase" or args.task == "CoBotStep") else JIGSAWS_FPS
    max_video_length = MAX_VIDEO_LENGTHS[args.task]
    if bool__:
        max_video_length /= args.video_sampling_step
    padded_length = int(max_video_length + 1)
    if args.max_length >= 0 and args.max_length < padded_length:
        padded_length = args.max_length
    max_pos_id = int(max_video_length * 1.5)

    log("Creating data sets... ")

    # ----- train data -----

    train_set = MyDataSet(args.feature_file, "train", max_pos_id, padded_length=padded_length,
                          video_fps=video_fps, video_sampling_step=args.video_sampling_step,
                          noise_factor=args.noise_factor, added_noise=args.added_noise,
                          max_duplicate_frames_prob=args.duplicate_frames_prob, duplicated_duration=args.duplicate_duration,
                          max_drop_frames_prob=args.drop_frames_prob, dropped_duration=args.drop_duration,
                          max_randomly_replace_prob=args.randomly_replace_prob, randomly_replaced_duration=args.replace_duration,
                          max_mask_prob=args.mask_prob, masked_duration=args.mask_duration)
    train_loader = torch.utils.data.DataLoader(train_set, batch_size=args.batch_size, shuffle=True,
                                               num_workers=args.workers,
                                               pin_memory=False, persistent_workers=False)

    # ----- test data -----
    if args.short:
        eval_train_set = MyDataSet_short(args.feature_file, "train", max_pos_id, padded_length=padded_length,
                               video_fps=video_fps, video_sampling_step=args.video_sampling_step,
                              )
    else:
        eval_train_set = MyDataSet(args.feature_file, "train", max_pos_id, padded_length=padded_length,
                               video_fps=video_fps, video_sampling_step=args.video_sampling_step,
                              )
    train_videos = eval_train_set.video_ids
    log("Found {} train videos: {}".format(len(train_videos), train_videos))
    eval_train_loader = torch.utils.data.DataLoader(eval_train_set, batch_size=args.batch_size, shuffle=False,
                                                    num_workers=1, pin_memory=False, persistent_workers=False)

    if args.task == "Cholec80" or args.task == "CoBotPhase" or args.task == "CoBotStep":
        if args.short:
            eval_val_set = MyDataSet_short(args.feature_file, "val", max_pos_id, padded_length=padded_length,
                                     video_fps=video_fps, video_sampling_step=args.video_sampling_step)
        else:
            eval_val_set = MyDataSet(args.feature_file, "val", max_pos_id, padded_length=padded_length,
                                     video_fps=video_fps, video_sampling_step=args.video_sampling_step)
        val_videos = eval_val_set.video_ids
        log("Found {} val videos: {}".format(len(val_videos), val_videos))
        eval_val_loader = torch.utils.data.DataLoader(eval_val_set, batch_size=args.batch_size, shuffle=False,
                                                      num_workers=1, pin_memory=False, persistent_workers=False)
    if args.short:
        eval_test_set = MyDataSet_short(args.feature_file, "test", max_pos_id, padded_length=padded_length,
                              video_fps=video_fps, video_sampling_step=args.video_sampling_step)
    else:
        eval_test_set = MyDataSet(args.feature_file, "test", max_pos_id, padded_length=padded_length,
                              video_fps=video_fps, video_sampling_step=args.video_sampling_step)
    test_videos = eval_test_set.video_ids
    log("Found {} test videos: {}".format(len(test_videos), test_videos))
    eval_test_loader = torch.utils.data.DataLoader(eval_test_set, batch_size=args.batch_size, shuffle=False,
                                                    num_workers=1, pin_memory=False, persistent_workers=False)

    log("Creating model... ")

    # ===== prepare model =====

    if args.pos_embed_type == "relative_T5":
        T5_attn_config = T5AttnConfig(num_buckets=args.T5_num_buckets, max_distance=args.T5_max_distance,
                                      is_bidirectional=not args.causal)
    else:
        T5_attn_config = None

    sampling_rate = video_fps / args.video_sampling_step
    attn_local_window = int(args.attn_local_window_duration * sampling_rate)
    attn_local_stride = int(math.ceil(args.attn_local_stride_duration * sampling_rate))
    attn_global_stride = int(args.attn_global_stride_duration * sampling_rate)

    if attn_local_stride < 0:
        attn_local_stride = 1

    if args.do_tecno:  # @ deprecated
        if args.final_tcn_layers == 0:
            num_stages = 1
        else:
            assert(args.final_tcn_layers == args.initial_tcn_layers)
            num_stages = 2
        if not args.tcn_norm:
            norm = "none"
        else:
            norm = "pre-norm" if args.pre_norm else "post-norm"
        model = MyMultiStageModel(num_stages=num_stages, num_layers=args.initial_tcn_layers, d_model=args.d_model,
                                  in_dim=train_set.feature_dim, num_classes=num_class, causal_conv=args.causal,
                                  dropout=args.tcn_dropout, hidden_dropout=args.hidden_dropout,
                                  norm=norm, activation=args.activation, linear_proj=args.tcn_linear_proj,
                                  reduce_dim=args.reduce_dim_tcn, inner_softmax=args.inner_softmax,
                                  do_initial_norm=args.tcn_initial_norm)
    elif args.tcn_baseline:
        model = MyMultiStageModelWrapper(num_stages=args.tcn_stages, num_layers=args.tcn_layers,
                                         d_model=args.d_model, in_dim=train_set.feature_dim, num_classes=num_class,
                                         causal_conv=args.causal)
    elif args.lstm:
        model = LSTMNet(in_dim=train_set.feature_dim, num_classes=num_class)
    else:
        model = GestureTransformer(num_class=num_class, d_in=train_set.feature_dim, d_model=args.d_model,
                                   max_pos_id=max_pos_id, padded_length=train_set.padded_length,
                                   n_encoder_blocks=args.nlayers, nhead=args.nhead, dim_ff=args.d_ff, pre_norm=args.pre_norm,
                                   ff_type=args.ff_type,
                                   causal_conv=args.causal, initial_norm=args.initial_norm, activation=args.activation,
                                   conv_kernel=args.conv_kernel, conv_dilation=args.conv_dilation,
                                   conv_layers=args.conv_layers,
                                   hidden_dropout=args.hidden_dropout, ff_dropout=args.ff_dropout,
                                   attn_dropout=args.attn_dropout, position_embedding_type=args.pos_embed_type,
                                   repeat_pos_info=args.repeat_pos_info, scale_pos=args.scale_pos_encoding,
                                   attn_local_window=attn_local_window, attn_local_stride=attn_local_stride,
                                   attn_global_stride=attn_global_stride, attn_global_type=args.attn_global_type,
                                   attn_impl=args.attn_impl, attn_sparsity=args.attn_sparsity, topk=args.attn_topk,
                                   T5_attn_config=T5_attn_config,
                                   gated_attn=args.gated_attn, gate_fn=args.gate_fn, forward_only=args.gate_forward_only)



    if checkpoint:
        # load model weights
        model.load_state_dict(checkpoint['model_weights'])

    log("param count: {}".format(sum(p.numel() for p in model.parameters())))
    log("trainable params: {}".format(sum(p.numel() for p in model.parameters() if p.requires_grad)))

    log("{}".format(model))

    if args.feature_trunc_mse_factor > 0:
        assert(args.feature_trunc_mse_factor < 1)
        # model.return_hiddens = True

    if args.use_weights:
        weight = train_set.calculate_class_weights(num_class)
        weight = weight.to(device_gpu)
    else:
        weight = None
    if args.label_smoothing_eps > 0:
        assert(args.label_smoothing_eps < 1)
        criterion = LabelSmoothingLoss(args.label_smoothing_eps, weight=weight, ignore_index=LABEL_IGN_IDX)
    else:
        criterion = torch.nn.CrossEntropyLoss(weight=weight, ignore_index=LABEL_IGN_IDX)

    if args.adam:
        optimizer = torch.optim.Adam(filter(lambda p: p.requires_grad, model.parameters()), lr=args.lr)
    else:
        optimizer = torch.optim.AdamW(filter(lambda p: p.requires_grad, model.parameters()), lr=args.lr,
                                      betas=(0.9, 0.999), eps=1e-08, weight_decay=0.01, amsgrad=False)
    """ 
    if do_SGD:
        optimizer = torch.optim.SGD(filter(lambda p: p.requires_grad, model.parameters()), lr=args.lr,
                                    momentum=0.9, weight_decay=0.001)
    """
    if checkpoint:
        # load optimizer state
        optimizer.load_state_dict(checkpoint['optimizer'])
        for state in optimizer.state.values():
            for k, v in state.items():
                if isinstance(v, torch.Tensor):
                    state[k] = v.to(device_gpu)

    last_epoch = -1
    if checkpoint:
        last_epoch = checkpoint['epoch']

    if args.lr_schedule == "linear":
        def lr_lambda(current_step):
            if current_step < args.warmup_steps:
                return float(current_step) / float(max(1, args.warmup_steps))
            return max(0.0, float(args.steps - current_step) / float(max(1, args.steps - args.warmup_steps)))
        scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer, lr_lambda, last_epoch)
    elif args.lr_schedule == "cosine":
        def lr_lambda(current_step):
            if current_step < args.warmup_steps:
                return float(current_step) / float(max(1, args.warmup_steps))
            num_cycles = 0.5
            progress = float(current_step - args.warmup_steps) / float(max(1, args.steps - args.warmup_steps))
            return max(0.0, 0.5 * (1.0 + math.cos(math.pi * float(num_cycles) * 2.0 * progress)))
        scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer, lr_lambda, last_epoch)
    else:
        scheduler = None

    # ===== train model =====

    torch.manual_seed(args.seed)
    # np.random.seed(args.seed)
    if checkpoint:
        torch.set_rng_state(checkpoint['rng'])

    # for strict reproducibility --> testing
    # torch.backends.cudnn.benchmark = False
    # torch.backends.cudnn.deterministic = True

    log("Start training...")

    model = model.to(device_gpu)

    train_loss = AverageMeter()
    train_acc = AverageMeter()

    best_val_accuracy = 0
    achieved_test_accuracy = 0
    achieved_test_20_accuracy = 0
    start_epoch = max(last_epoch, 0)
    for epoch in range(start_epoch, args.steps):

        model.train()
        for j, batch in enumerate(train_loader):

            loss, acc, num_frames = forward(batch, model, criterion, device_gpu, device_cpu, args, optimizer, train=True, feature_model = feature_model)

            train_loss.update(loss, num_frames)
            train_acc.update(acc, num_frames)

        if (epoch + 1) % args.log_freq == 0:
            writer.add_scalar('Train/loss_estimate', train_loss.avg, epoch)
            writer.add_scalar('Train/acc_estimate', train_acc.avg, epoch)

            log("Epoch {}: Train loss: {:.4f} Train acc: {:.3f}".format(epoch, train_loss.avg, train_acc.avg))
            f_log.flush()
            os.fsync(f_log) 

            train_loss.reset()
            train_acc.reset()

        if (epoch + 1) % args.eval_freq == 0 or epoch == args.steps - 1:  # validation

            # check performance on train data
            if args.short:
                test_complete_sequences_2(eval_train_loader, model, criterion, num_class, 'train', epoch, writer, log,
                                    device_gpu, device_cpu, args, calc_segmental_scores=bool__)
            else:
                test_complete_sequences(eval_train_loader, model, criterion, num_class, 'train', epoch, writer, log,
                                    device_gpu, device_cpu, args, calc_segmental_scores=bool__)

            if args.task == "Cholec80" or args.task == "CoBotPhase" or args.task == "CoBotStep":
                if args.short:
                    val_acc = test_complete_sequences_2(eval_val_loader, model, criterion, num_class, 'val', epoch,
                                                      writer, log, device_gpu, device_cpu,
                                                      args, calc_segmental_scores=bool__)
                else:
                    val_acc = test_complete_sequences(eval_val_loader, model, criterion, num_class, 'val', epoch,
                                                  writer, log, device_gpu, device_cpu,
                                                  args, calc_segmental_scores=bool__)

            # check performance on test data
            if args.short:
                test_acc = test_complete_sequences_2(eval_test_loader, model, criterion, num_class, 'test', epoch,
                                               writer, log, device_gpu, device_cpu,
                                               args, calc_segmental_scores=bool__)
            else:
                test_acc = test_complete_sequences(eval_test_loader, model, criterion, num_class, 'test', epoch,
                                               writer, log, device_gpu, device_cpu,
                                               args, calc_segmental_scores=bool__)

            if args.task == "Cholec80" and args.split == "NCT":
                # additionally check on 20 video subset
                log("***** testing on 20 videos (part D) only... *****")
                eval_test_loader.dataset.restrict_video_ids(Cholec80.test_videos_20)
                test_acc_20 = test_complete_sequences(eval_test_loader, model, criterion, num_class, 'test', epoch,
                                                      writer, log, device_gpu, device_cpu,
                                                      args, calc_segmental_scores=bool__)
                eval_test_loader.dataset.unrestrict_video_ids()
                log("*** ***** ***** ***** ***** ***** ***** ***** ***")
    
            if args.task == "Cholec80" and val_acc > best_val_accuracy:
                best_val_accuracy = val_acc
                achieved_test_accuracy = test_acc

                current_state = {'epoch': epoch + 1,
                                 'model_weights': model.state_dict(),
                                 'args': args_dict,
                                 'val_acc': best_val_accuracy,
                                 'test_acc': achieved_test_accuracy,
                                 }
                model_file = os.path.join(output_folder, "model_best.pth.tar")
                torch.save(current_state, model_file)
                log("Saved currently best model (test acc {:.3f}) to {}"
                    .format(achieved_test_accuracy, model_file))
            elif val_acc > best_val_accuracy:
                best_val_accuracy = val_acc
                achieved_test_accuracy = test_acc

                current_state = {'epoch': epoch + 1,
                                 'model_weights': model.state_dict(),
                                 'args': args_dict,
                                 'val_acc': best_val_accuracy,
                                 'test_acc': achieved_test_accuracy,
                                 }
                model_file = os.path.join(output_folder, "model_best.pth.tar")
                torch.save(current_state, model_file)
                log("Saved currently best model (test acc {:.3f}) to {}"
                    .format(achieved_test_accuracy, model_file))

            # ===== save checkpoint =====
            current_state = {'epoch': epoch + 1,
                             'model_weights': model.state_dict(),
                             'optimizer': optimizer.state_dict(),
                             'rng': torch.get_rng_state(),
                             'args': args_dict
                             }
            torch.save(current_state, checkpoint_file)

        if scheduler is not None:
            scheduler.step()

        if (epoch + 1) % args.save_freq == 0 or epoch == args.steps - 1:
            # ===== save model =====
            model_file = os.path.join(output_folder, "model_" + str(epoch) + ".pth")
            torch.save(model.state_dict(), model_file)
            log("Saved model to " + model_file)

    log("Achieved test accuracy {:.4f}".format(achieved_test_accuracy))

    f_log.close()

    writer.flush()
    writer.close()


def forward(batch, model, criterion, device_gpu, device_cpu, args, optimizer=None, train=True, feature_model = None):
    if train:
        optimizer.zero_grad()

    seq, target, pos_ids, key_padding_mask, v_id = batch
    num_frames = (seq.shape[0] * seq.shape[1]) - key_padding_mask.sum()
    if not train:
        out_mask = np.logical_not(key_padding_mask.numpy())
        labels = target.numpy()

    seq = seq.to(device_gpu)
    target = target.to(device_gpu)
    pos_ids = pos_ids.to(device_gpu)
    key_padding_mask = key_padding_mask.to(device_gpu)

    seq = torch.transpose(seq, 0, 1)  # N x S x F --> S x N x F
    out_att = False
    if args.regular > 0:
        outputs, att = model(seq, pos_ids, key_padding_mask, output_attentions = True)
    else:
        outputs = model(seq, pos_ids, key_padding_mask)
    output = outputs[-1]  # prediction of final layer
    # S x N x num_class

    if args.label_smoothing_eps > 0:
        loss = criterion(output, target)
    else:
        loss = criterion(output.permute(1, 2, 0), target)  # --> N x num_class x S

    if args.feature_trunc_mse_factor > 0:
        truncated_mse_loss = truncated_MSE_loss(output, key_padding_mask)
        truncated_mse_loss *= args.feature_trunc_mse_factor
        if train:
            loss += truncated_mse_loss
        else:
            tmse_loss = truncated_mse_loss.item()

    if args.regular > 0 and feature_model != None:
        pred_f = feature_model(seq)
        pred_f = pred_f.permute(1, 2, 0)
        pred_f = torch.nn.Softmax(dim=2)(pred_f)
        loss += args.regular * reg_loss(att = att, pred_feature = pred_f, y = target, key_padding_mask = key_padding_mask)

    if (args.deep_supervision or args.do_tecno or args.tcn_baseline) and (len(outputs) > 1):  # error on intermediate outputs
        for j in range(0, len(outputs) - 1):
            intermediate = outputs[j]
            if args.label_smoothing_eps > 0:
                loss += criterion(intermediate, target)
            else:
                loss += criterion(intermediate.permute(1, 2, 0), target)  # --> N x num_class x S

            if args.feature_trunc_mse_factor > 0:
                truncated_mse_loss = truncated_MSE_loss(intermediate, key_padding_mask)
                truncated_mse_loss *= args.feature_trunc_mse_factor
                if train:
                    loss += truncated_mse_loss
                else:
                    tmse_loss += truncated_mse_loss.item()

    if not train:
        ce_loss = loss.item()

    if train:
        loss.backward()
        # clip gradients ?
        optimizer.step()

    output = torch.transpose(output, 0, 1)  # S x N x num_class --> N x S x num_class
    predicted = torch.nn.Softmax(dim=2)(output)
    _, predicted = torch.max(predicted, dim=2, keepdim=False)

    if train:
        correct = torch.masked_fill(predicted == target, key_padding_mask, 0)
        acc = correct.sum().item() / num_frames

        return loss.item(), acc, num_frames

    else:
        predicted = predicted.to(device_cpu).numpy()
        losses = (ce_loss,) if args.feature_trunc_mse_factor <= 0 else (ce_loss, tmse_loss)
        return predicted, labels, out_mask, losses, num_frames

def test_complete_sequences(data_loader, model, criterion, num_class, mode, epoch, writer, log, device_gpu, device_cpu,
                            args, calc_segmental_scores=False):
    eval_loss_ce = AverageMeter()
    eval_loss_truncated_mse = AverageMeter()
    eval_acc = AverageMeter()
    eval_avg_f1 = AverageMeter()
    eval_avg_prec = AverageMeter()
    eval_avg_rec = AverageMeter()
    eval_edit = AverageMeter()
    eval_f1_10 = AverageMeter()

    data_loader.dataset.eval()
    model.eval()
    with torch.no_grad():
        for _, batch in enumerate(data_loader):

            predicted, labels, out_mask, losses, num_frames = forward(batch, model, criterion, device_gpu, device_cpu,
                                                                      args, optimizer=None, train=False)
            eval_loss_ce.update(losses[0], num_frames)
            if args.feature_trunc_mse_factor > 0:
                eval_loss_truncated_mse.update(losses[1], num_frames)

            batch_size = predicted.shape[0]
            for i in range(batch_size):
                P = predicted[i, :]
                Y = labels[i, :]

                # remove key padding
                P = P[out_mask[i, :]]
                Y = Y[out_mask[i, :]]

                acc = accuracy(P, Y)
                avg_f1, avg_prec, avg_rec, _ = average_F1(P, Y, n_classes=num_class)
                eval_acc.update(acc)
                eval_avg_f1.update(avg_f1)
                eval_avg_prec.update(avg_prec)
                eval_avg_rec.update(avg_rec)

                if calc_segmental_scores:
                    edit = edit_score(P, Y)
                    f1_10 = overlap_f1(P, Y, n_classes=num_class, overlap=0.1)
                    eval_edit.update(edit)
                    eval_f1_10.update(f1_10)

    # log results
    writer.add_scalar('Val_loss/{}_ce'.format(mode), eval_loss_ce.avg, epoch)
    writer.add_scalar('Val_loss/{}_tmse'.format(mode), eval_loss_truncated_mse.avg, epoch)
    writer.add_scalars('Metrics_frame/{}'.format(mode), {'acc': eval_acc.avg,
                                                         'avg_f1': eval_avg_f1.avg}, epoch)
    if calc_segmental_scores:
        writer.add_scalars('Metrics_segm/{}'.format(mode), {'edit': eval_edit.avg,
                                                            'f1_10': eval_f1_10.avg}, epoch)

    if calc_segmental_scores:
        log("Epoch {}: Val({}) -- ce loss: {:.4f} acc: {:.3f} avg_f1: {:.3f} edit: {:.3f} f1_10: {:.3f}"
            .format(epoch, mode, eval_loss_ce.avg, eval_acc.avg, eval_avg_f1.avg, eval_edit.avg, eval_f1_10.avg))
    else:
        log("Epoch {}: Val({}) -- ce loss: {:.4f} acc: {:.3f} avg_f1: {:.3f} avg_prec: {:.3f}, avg_rec: {:.3f}"
            .format(epoch, mode, eval_loss_ce.avg, eval_acc.avg, eval_avg_f1.avg, eval_avg_prec.avg, eval_avg_rec.avg))

    return eval_acc.avg


def test_complete_sequences_2(data_loader, model, criterion, num_class, mode, epoch, writer, log, device_gpu, device_cpu,
                            args, calc_segmental_scores=False):
    eval_loss_ce = AverageMeter()
    eval_loss_truncated_mse = AverageMeter()
    eval_acc = AverageMeter()
    eval_avg_f1 = AverageMeter()
    eval_avg_prec = AverageMeter()
    eval_avg_rec = AverageMeter()
    eval_edit = AverageMeter()
    eval_f1_10 = AverageMeter()
    vid_id_old = 0

    data_loader.dataset.eval()
    model.eval()
    with torch.no_grad():
        P_list = []
        Y_list = []
        first = True
        P = None
        Y = None
        for _, batch in enumerate(data_loader):
            (a,b,c,d,vid_id,it) = batch
            batch_ = (a,b,c,d,vid_id)
            predicted, labels, out_mask, losses, num_frames = forward(batch_, model, criterion, device_gpu, device_cpu,
                                                                      args, optimizer=None, train=False)
            eval_loss_ce.update(losses[0], num_frames)
            if args.feature_trunc_mse_factor > 0:
                eval_loss_truncated_mse.update(losses[1], num_frames)

            batch_size = predicted.shape[0]
            assert(batch_size == 1)

            P_ = predicted[0, :]
            Y_ = labels[0, :]
            # remove key padding
            if first:
                P = P_[out_mask[0, :]]
                Y = Y_[out_mask[0, :]]
                first = False
            elif vid_id == vid_id_old:
                P = np.concatenate((P,P_[out_mask[0, :]]),axis=0)
                Y = np.concatenate((Y,Y_[out_mask[0, :]]),axis=0)
            else:
                P_list.append(P)
                Y_list.append(Y)
                P = P_[out_mask[0, :]]
                Y = Y_[out_mask[0, :]]
                
            vid_id_old = vid_id
        P_list.append(P)
        Y_list.append(Y)
        for i in range(len(P_list)):
            P = P_list[i]
            Y = Y_list[i]
            acc = accuracy(P, Y)
            avg_f1, avg_prec, avg_rec, _ = average_F1(P, Y, n_classes=num_class)
            eval_acc.update(acc)
            eval_avg_f1.update(avg_f1)
            eval_avg_prec.update(avg_prec)
            eval_avg_rec.update(avg_rec)

            if calc_segmental_scores:
                edit = edit_score(P, Y)
                f1_10 = overlap_f1(P, Y, n_classes=num_class, overlap=0.1)
                eval_edit.update(edit)
                eval_f1_10.update(f1_10)

            

    # log results
    writer.add_scalar('Val_loss/{}_ce'.format(mode), eval_loss_ce.avg, epoch)
    writer.add_scalar('Val_loss/{}_tmse'.format(mode), eval_loss_truncated_mse.avg, epoch)
    writer.add_scalars('Metrics_frame/{}'.format(mode), {'acc': eval_acc.avg,
                                                         'avg_f1': eval_avg_f1.avg}, epoch)
    if calc_segmental_scores:
        writer.add_scalars('Metrics_segm/{}'.format(mode), {'edit': eval_edit.avg,
                                                            'f1_10': eval_f1_10.avg}, epoch)

    if calc_segmental_scores:
        log("Epoch {}: Val({}) -- ce loss: {:.4f} acc: {:.3f} avg_f1: {:.3f} edit: {:.3f} f1_10: {:.3f}"
            .format(epoch, mode, eval_loss_ce.avg, eval_acc.avg, eval_avg_f1.avg, eval_edit.avg, eval_f1_10.avg))
    else:
        log("Epoch {}: Val({}) -- ce loss: {:.4f} acc: {:.3f} avg_f1: {:.3f} avg_prec: {:.3f}, avg_rec: {:.3f}"
            .format(epoch, mode, eval_loss_ce.avg, eval_acc.avg, eval_avg_f1.avg, eval_avg_prec.avg, eval_avg_rec.avg))

    return eval_acc.avg
""" 
def test_split_sequences(data_loaders, model, criterion, padding_size, attn_type, num_class, device_gpu, device_cpu):
    eval_loss = AverageMeter()
    eval_acc = AverageMeter()
    eval_avg_f1 = AverageMeter()
    eval_edit = AverageMeter()
    eval_f1_10 = AverageMeter()

    for data_loader in data_loaders:
        P = np.array([], dtype=np.int64)
        Y = np.array([], dtype=np.int64)

        # print(data_loader.dataset.video_id)
        video_length = data_loader.dataset.no_frames
        final_padding = data_loader.dataset.last_part_key_padding

        size = data_loader.dataset.size
        step = data_loader.dataset.step
        offset = size - step  # step <= size
        if attn_type == "bidirectional" and (offset > 0):
            assert(offset % 2 == 0)
            offset = int(offset // 2)
        frame_count = 0

        # collect predictions for this video
        model.eval()
        with torch.no_grad():
            for b_no, batch in enumerate(data_loader):
                is_last_batch = (b_no == (len(data_loader) - 1))
                seq, target, pos_enc, attn_mask = batch
                num_frames = (seq.shape[0] * seq.shape[1]) - (final_padding if is_last_batch else 0)
                labels = target.numpy()

                seq = seq.to(device_gpu)
                target = target.to(device_gpu)
                pos_enc = pos_enc.to(device_gpu)
                # attn_mask = attn_mask.to(device_gpu)
                if attn_mask.ndim == 1 and attn_mask[0].item() == NONE_VAL:
                    attn_mask = None  # hack ;P
                else:            
                    if attn_mask.ndim == 3:
                        attn_mask = attn_mask[0, :, :]
                    attn_mask = attn_mask.to(device_gpu)
                    
                if is_last_batch and final_padding > 0:
                    batch_size = seq.shape[0]
                    assert(seq.shape[1] == size)
                    key_padding = torch.zeros((batch_size, size), dtype=torch.bool)
                    key_padding[-1, :] = torch.tensor(([False for _ in range(size - final_padding)]
                                                       + [True for _ in range(final_padding)]), dtype=torch.bool)
                    key_padding = key_padding.to(device_gpu)
                else:
                    key_padding = None

                seq = torch.transpose(seq, 0, 1)  # N x S x F --> S x N x F
                pos_enc = torch.transpose(pos_enc, 0, 1)
                output = model(seq, pos_enc, attn_mask, key_padding)
                output = torch.transpose(output, 0, 1)  # S x N x num_class --> N x S x num_class

                loss = criterion(torch.transpose(output, 1, 2), target)
                eval_loss.update(loss.item(), num_frames)

                predicted = torch.nn.Softmax(dim=2)(output)
                _, predicted = torch.max(predicted, dim=2, keepdim=False)
                predictions = predicted.to(device_cpu).numpy()

                for i in range(predictions.shape[0]):
                    is_last_part = (is_last_batch and (i == (predictions.shape[0] - 1)))
                    part = predictions[i, :]
                    part_labels = labels[i, :]
                    if frame_count == 0:
                        # first part
                        future_offset = offset if (attn_type == "bidirectional" and not is_last_part) else 0
                        P = np.append(P, part[0: size - future_offset])
                        Y = np.append(Y, part_labels[0: size - future_offset])
                        frame_count = size - future_offset
                    else:
                        if is_last_part:
                            P = np.append(P, part[offset:])
                            Y = np.append(Y, part_labels[offset:])
                            frame_count += size - offset
                        else:
                            P = np.append(P, part[offset: offset + step])
                            Y = np.append(Y, part_labels[offset: offset + step])
                            frame_count += step

        assert (P.shape[0] == frame_count and Y.shape[0] == frame_count)

        if final_padding > 0:
            P = P[0: frame_count - final_padding]
            Y = Y[0: frame_count - final_padding]

        # remove padding
        seq_length = P.shape[0]
        if attn_type == "backward":
            P = P[padding_size:]
            Y = Y[padding_size:]
        else:
            P = P[padding_size: seq_length - padding_size]
            Y = Y[padding_size: seq_length - padding_size]

        assert (P.shape[0] == video_length)

        acc = accuracy(P, Y)
        avg_f1, _ = average_F1(P, Y, n_classes=num_class)
        edit = edit_score(P, Y)
        f1_10 = overlap_f1(P, Y, n_classes=num_class, overlap=0.1)
        
        eval_acc.update(acc)
        eval_avg_f1.update(avg_f1)
        eval_edit.update(edit)
        eval_f1_10.update(f1_10)

    return eval_loss.avg, eval_acc.avg, eval_avg_f1.avg, eval_edit.avg, eval_f1_10.avg
"""


if __name__ == '__main__':
    args = parser.parse_args()

    main(args)


