# 
cd /home/krellstef/gesture-transformer-master

# for i in 1 2 3 4
# do
# EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_tcn_baseline_cross_pre_all_20210419-1854/train-val/$i"
# EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_tcn_baseline_cross_pre_all_20210419-1854/train-val/$i"
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210408-0839/$i/features.pth.tar" \
# --tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 5 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
# done

# for i in 1 2 3 4
# do
# EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_tcn_baseline_cross_pre_un_20210419-1511/train-val/$i"
# EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_tcn_baseline_cross_pre_un_20210419-1511/train-val/$i"
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210406-1007/$i/features.pth.tar" \
# --tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 5 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
# done


# for i in 1 2 3 4
# do
# EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_tcn_baseline_cross_pre_all_20210416-1727/train-val/$i"
# EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_tcn_baseline_cross_pre_all_20210416-1727/train-val/$i"
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210406-1016/$i/features.pth.tar" \
# --tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 5 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotPhase" --adam True
# done

for i in 1 2 3 4
do
EVAL="/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_tcn_baseline_cross_pre_un_20210422-0947/train-val/$i"
EVAL_OUT="/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_tcn_baseline_cross_pre_un_20210422-0947/train-val/$i"
python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210401-1009/$i/features.pth.tar" \
--tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 5 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotPhase" --adam True
done

cd /home/krellstef/master_code
python3 eval_cross.py "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_tcn_baseline_cross_pre_un_20210422-0947/train-val"
#python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_tcn_baseline_cross_pre_un_20210419-1511/train-val" --dataset "CoBotSteps"
#python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_tcn_baseline_cross_pre_all_20210419-1854/train-val" --dataset "CoBotSteps"

