#!/bin/bash

cd ~/Code/gesture-transformer

EXP=test_attn_base

for i in 42 123 512 222
do

python train.py --exp "$EXP" --seed $i --feature_file "/local_home/funkeisab/cholec80/features/Cholec80_ResNet50.pth.tar" \
--causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
--pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
--ff_type "conv" --d_ff 64 --ff_dropout 0.5 --conv_kernel 5 --conv_dilation 4 \
 --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3

done
