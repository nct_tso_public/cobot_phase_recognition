#!/bin/bash

cd ~/Code/gesture-transformer

EXP=test_attn_based_gated

python train.py --exp "$EXP" --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 \
--attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
--pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
--gated_attn True --gate_fn "sigmoid" --gate_forward_only False --ff_type "none" \
--causal True --lr 0.0005 -b 2 --steps 60 --added_noise 0.3 \
--label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3

