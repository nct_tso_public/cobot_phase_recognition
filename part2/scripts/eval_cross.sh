cd /home/krellstef/gesture-transformer-master
# for i in 1 2 3 4
# do
# EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_base_mlp_cross_pre_un_20210415-1841/train-val/$i"
# EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_base_mlp_cross_pre_un_20210415-1841/train-val/$i"
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210401-1009/$i/features.pth.tar" \
# --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "feed_forward" --d_ff 256 --ff_dropout 0.5 \
# --lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --max_length 9000 --short True --task "CoBotPhase" --adam True
# done

# for i in 1 2 3 4
# do
# EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_conv_cross_pre_un_20210415-1701/train-val/$i"
# EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_conv_cross_pre_un_20210415-1701/train-val/$i"
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210401-1009/$i/features.pth.tar" \
# --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01 --mask_prob 0.01 --replace_duration 15 --mask_duration 15 \
# --duplicate_frames_prob 0.1 --drop_frames_prob 0.1 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "conv" --d_ff 64 --ff_dropout 0.5 --conv_kernel 5 --conv_dilation 4 --conv_layers 1 \
# --lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --deep_supervision False --max_length 9000 --short True --task "CoBotPhase" --adam True
# done

# for i in 1 2 3 4
# do
# EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_attn_base_mlp_cross_pre_un_20210415-2034/train-val/$i"
# EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_attn_base_mlp_cross_pre_un_20210415-2034/train-val/$i"
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210406-1007/$i/features.pth.tar" \
# --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "feed_forward" --d_ff 256 --ff_dropout 0.5 \
# --lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --max_length 9000 --short True --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
# done

for i in 1 2 3 4
do
EVAL="/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_attn_conv_cross_pre_un_20210422-1223/train-val/$i"
EVAL_OUT="/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_attn_conv_cross_pre_un_20210422-1223/train-val/$i"
python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210406-1007/$i/features.pth.tar" \
--causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
--randomly_replace_prob 0.01 --mask_prob 0.01 --replace_duration 15 --mask_duration 15 \
--duplicate_frames_prob 0.1 --drop_frames_prob 0.1 --added_noise 0.3 \
--attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
--pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
--ff_type "conv" --d_ff 64 --ff_dropout 0.5 --conv_kernel 5 --conv_dilation 4 --conv_layers 1 \
--lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --deep_supervision False --max_length 9000 --short True --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
done
cd /home/krellstef/master_code
#python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_base_mlp_cross_pre_un_20210415-1841/train-val"
#python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_conv_cross_pre_un_20210415-1701/train-val"
python3 eval_cross.py "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_attn_conv_cross_pre_un_20210422-1223/train-val" --dataset "CoBotSteps"
#python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_attn_conv_cross_pre_all_20210419-1523/train-val" --dataset "CoBotSteps"
# for i in 1 2 3 4
# do
# EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_base_mlp_cross_pre_all_20210416-1542/train-val/$i"
# EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_base_mlp_cross_pre_all_20210416-1542/train-val/$i"
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210406-1016/$i/features.pth.tar" \
# --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "feed_forward" --d_ff 256 --ff_dropout 0.5 \
# --lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --max_length 9000 --short True --task "CoBotPhase" --adam True
# done

# for i in 1 2 3 4
# do
# EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_conv_cross_pre_all_20210416-1358/train-val/$i"
# EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_conv_cross_pre_all_20210416-1358/train-val/$i"
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210406-1016/$i/features.pth.tar" \
# --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01 --mask_prob 0.01 --replace_duration 15 --mask_duration 15 \
# --duplicate_frames_prob 0.1 --drop_frames_prob 0.1 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "conv" --d_ff 64 --ff_dropout 0.5 --conv_kernel 5 --conv_dilation 4 --conv_layers 1 \
# --lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --deep_supervision False --max_length 9000 --short True --task "CoBotPhase" --adam True
# done

# for i in 1 2 3 4
# do
# EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_attn_base_mlp_cross_pre_all_20210419-1713/train-val/$i"
# EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_attn_base_mlp_cross_pre_all_20210419-1713/train-val/$i"
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210408-0839/$i/features.pth.tar" \
# --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "feed_forward" --d_ff 256 --ff_dropout 0.5 \
# --lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --max_length 9000 --short True --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
# done

# for i in 1 2 3 4
# do
# EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_attn_conv_cross_pre_all_20210419-1523/train-val/$i"
# EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_attn_conv_cross_pre_all_20210419-1523/train-val/$i"
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210408-0839/$i/features.pth.tar" \
# --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01 --mask_prob 0.01 --replace_duration 15 --mask_duration 15 \
# --duplicate_frames_prob 0.1 --drop_frames_prob 0.1 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "conv" --d_ff 64 --ff_dropout 0.5 --conv_kernel 5 --conv_dilation 4 --conv_layers 1 \
# --lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --deep_supervision False --max_length 9000 --short True --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
# done
# cd /home/krellstef/master_code
# python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_base_mlp_cross_pre_all_20210416-1542/train-val"
# python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_conv_cross_pre_all_20210416-1358/train-val"
# python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_attn_base_mlp_cross_pre_all_20210419-1713/train-val" --dataset "CoBotSteps"
# python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_attn_conv_cross_pre_all_20210419-1523/train-val" --dataset "CoBotSteps"


# for i in 1 2 3 4
# do
# EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_base_mlp_cross_20210415-0931/train-val/$i"
# EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_base_mlp_cross_20210415-0931/train-val/$i"
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" \
# --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "feed_forward" --d_ff 256 --ff_dropout 0.5 \
# --lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --max_length 9000 --short True --task "CoBotPhase" --adam True
# done

# for i in 1 2 3 4
# do
# EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_conv_cross_20210415-1114/train-val/$i"
# EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_conv_cross_20210415-1114/train-val/$i"
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" \
# --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01 --mask_prob 0.01 --replace_duration 15 --mask_duration 15 \
# --duplicate_frames_prob 0.1 --drop_frames_prob 0.1 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "conv" --d_ff 64 --ff_dropout 0.5 --conv_kernel 5 --conv_dilation 4 --conv_layers 1 \
# --lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --deep_supervision False --max_length 9000 --short True --task "CoBotPhase" --adam True
# done

# for i in 1 2 3 4
# do
# EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_attn_base_mlp_cross_20210415-1257/train-val/$i"
# EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_attn_base_mlp_cross_20210415-1257/train-val/$i"
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" \
# --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "feed_forward" --d_ff 256 --ff_dropout 0.5 \
# --lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --max_length 9000 --short True --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
# done

# for i in 1 2 3 4
# do
# EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_attn_conv_cross_20210415-1442/train-val/$i"
# EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_attn_conv_cross_20210415-1442/train-val/$i"
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" \
# --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01 --mask_prob 0.01 --replace_duration 15 --mask_duration 15 \
# --duplicate_frames_prob 0.1 --drop_frames_prob 0.1 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "conv" --d_ff 64 --ff_dropout 0.5 --conv_kernel 5 --conv_dilation 4 --conv_layers 1 \
# --lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --deep_supervision False --max_length 9000 --short True --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
# done

# for i in 1 2 3 4
# do
# EVAL="/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_tcn_baseline20210415-1242/train-val/$i"
# EVAL_OUT="/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_tcn_baseline20210415-1242/train-val/$i"
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" \
# --tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 5 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
# done
#cd /home/krellstef/master_code
#python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_base_mlp_cross_20210415-0931/train-val"
#python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_conv_cross_20210415-1114/train-val"

#python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_attn_base_mlp_cross_20210415-1257/train-val" --dataset "CoBotSteps"
#python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_attn_conv_cross_20210415-1442/train-val" --dataset "CoBotSteps"

#python3 eval_cross.py "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_tcn_baseline20210415-1242/train-val" --dataset "CoBotSteps"

