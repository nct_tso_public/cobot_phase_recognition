#!/bin/bash

cd /home/krellstef/gesture-transformer-master

# EVAL="/mnt/g27prist/TCO/TCO-Staff/Homes/funkeisab/Phase-Transformer/test_attn_conv_20210302/train-val-test/Tecno"
# EVAL_OUT="/mnt/g27prist/TCO/TCO-Staff/Homes/funkeisab/Phase-Transformer/eval_attn_conv_20210302"

# python eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/local_home/funkeisab/cholec80/features/Cholec80_ResNet50.pth.tar" \
# --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01 --mask_prob 0.01 --replace_duration 15 --mask_duration 15 \
# --duplicate_frames_prob 0.1 --drop_frames_prob 0.1 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "conv" --d_ff 64 --ff_dropout 0.5 --conv_kernel 5 --conv_dilation 4 --conv_layers 1 \
#  --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --deep_supervision False


# EVAL="/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_tcn_baseline_20210310/train-val/Tecno/1403-17"
# EVAL_OUT="/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_tcn_baseline_20210310/train-val/Tecno/1403-17/eval"


# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/features_CoBot.pth.tar" \
# --tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 5 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --task "CoBotPhase"

# EVAL="/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_tcn_baseline_20210310/train-val/Tecno/0939-53"
# EVAL_OUT="/media/TCO/TCO-Studenten/Homes/krellstef//test_tcn_baseline_20210310/train-val/Tecno/0939-53/eval"
# EVAL="/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_tcn_baseline_20210319/train-val/Tecno/0958-44"
# EVAL_OUT="/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_tcn_baseline_20210319/train-val/Tecno/0958-44/eval"
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/features_CoBot_steps_n.pth.tar" \
# --tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 5 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --task "CoBotStep"

# EVAL="/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_conv_20210310/train-val/Tecno/1358-55"
# EVAL_OUT="/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_conv_20210310/train-val/Tecno/1358-55/eval"

# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/features_CoBot.pth.tar" \
# --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01 --mask_prob 0.01 --replace_duration 15 --mask_duration 15 \
# --duplicate_frames_prob 0.1 --drop_frames_prob 0.1 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "conv" --d_ff 64 --ff_dropout 0.5 --conv_kernel 5 --conv_dilation 4 --conv_layers 1 \
#  --lr 0.0005 -b 1 --steps 50 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --deep_supervision False --max_length 9000 --short True --task "CoBotPhase"

# EVAL="/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_conv_20210319/train-val/Tecno/1100-38"

# EVAL_OUT="/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_conv_20210319/train-val/Tecno/1100-38/eval"

# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/features_CoBot_steps_n.pth.tar" \
# --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01 --mask_prob 0.01 --replace_duration 15 --mask_duration 15 \
# --duplicate_frames_prob 0.1 --drop_frames_prob 0.1 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "conv" --d_ff 64 --ff_dropout 0.5 --conv_kernel 5 --conv_dilation 4 --conv_layers 1 \
#  --lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --deep_supervision False --task "CoBotStep" --max_length 9000 --short True 

# EVAL="/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_base_mlp_20210310/train-val/Tecno/1429-44"
# EVAL_OUT="/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_base_mlp_20210310/train-val/Tecno/1429-44/eval"

# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/features_CoBot.pth.tar" \
# --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "feed_forward" --d_ff 256 --ff_dropout 0.5 \
# --lr 0.0005 -b 1 --steps 50 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --max_length 9000 --short True --task "CoBotPhase"



EVAL="/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_base_mlp_20210322/train-val/Tecno/1051-23"
EVAL_OUT="/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/test_attn_base_mlp_20210322/train-val/Tecno/1051-23"

 python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/features_CoBot_steps_n.pth.tar" \
 --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
 --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
 --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
 --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
 --ff_type "feed_forward" --d_ff 256 --ff_dropout 0.5 \
 --lr 0.0005 -b 1 --steps 50 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --max_length 9000 --short True --task "CoBotStep"


