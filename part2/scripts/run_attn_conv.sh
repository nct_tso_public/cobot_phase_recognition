#!/bin/bash

cd /home/krellstef/gesture-transformer-master

EXP=test_attn_conv

for i in 42 #123 512 222
do

# python3 train.py --exp "$EXP" --seed $i --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/features_CoBot.pth.tar" \
# --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01 --mask_prob 0.01 --replace_duration 15 --mask_duration 15 \
# --duplicate_frames_prob 0.1 --drop_frames_prob 0.1 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "conv" --d_ff 64 --ff_dropout 0.5 --conv_kernel 5 --conv_dilation 4 --conv_layers 1 \
#  --lr 0.0005 -b 1 --steps 50 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --deep_supervision False --max_length 9000 --short True --task "CoBotPhase"


python3 train.py --exp "$EXP" --seed $i --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/features_CoBot_steps_n.pth.tar" \
--causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
--randomly_replace_prob 0.01 --mask_prob 0.01 --replace_duration 15 --mask_duration 15 \
--duplicate_frames_prob 0.1 --drop_frames_prob 0.1 --added_noise 0.3 \
--attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
--pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
--ff_type "conv" --d_ff 64 --ff_dropout 0.5 --conv_kernel 5 --conv_dilation 4 --conv_layers 1 \
 --lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --deep_supervision False --task "CoBotStep" --max_length 9000 --short True --regular 1.0

done

