#!/bin/bash
#SBATCH -e err-test-%J.err
#SBATCH -o out-test-%J.out
#SBATCH --time=8:00:00
#SBATCH --cpus-per-task=4 
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --gpus=1
#SBATCH --mail-user=isabel.funke@nct-dresden.de
#SBATCH --mail-type=BEGIN,END,FAIL,REQUEUE,TIME_LIMIT_90
#SBATCH --mem=32G

cd /home/funkeisab
source ~/.bashrc
conda init bash
conda activate torch17

cd /home/funkeisab/Code/gesture-transformer 

EXP=test_attn_conv_nlayers-5_head-4

for i in 42 123 512 222
do

python train.py --exp "$EXP" --seed $i \
--feature_file "/media/data/local_home/funkeisab/cholec80/features/Cholec80_ResNet50.pth.tar" \
--out "/media/data/local_home/funkeisab/gesture-transformer" \
--causal True --d_model 64 --nlayers 5 --nhead 4 --attn_dropout 0.3 --hidden_dropout 0.1 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
--pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
--ff_type "conv" --d_ff 64 --ff_dropout 0.5 --conv_kernel 5 --conv_dilation 4 --conv_layers 1 \
 --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --deep_supervision False

done


exit 0 
