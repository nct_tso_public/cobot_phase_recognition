#!/bin/bash

cd /home/krellstef/gesture-transformer-master

EXP=test_attn_base_mlp_cross_
EXP+=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
echo "$EXP"
python3 train.py --exp "$EXP" --split $i --seed 42 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" \
--causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
--pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
--ff_type "feed_forward" --d_ff 256 --ff_dropout 0.5 \
--lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --max_length 9000 --short True --task "CoBotPhase"
done

EXP=test_attn_conv_cross_
EXP+=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train.py --exp "$EXP" --split $i --seed 42 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" \
--causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
--randomly_replace_prob 0.01 --mask_prob 0.01 --replace_duration 15 --mask_duration 15 \
--duplicate_frames_prob 0.1 --drop_frames_prob 0.1 --added_noise 0.3 \
--attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
--pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
--ff_type "conv" --d_ff 64 --ff_dropout 0.5 --conv_kernel 5 --conv_dilation 4 --conv_layers 1 \
--lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --deep_supervision False --max_length 9000 --short True --task "CoBotPhase"
done

EXP=test_attn_base_mlp_cross_
EXP+=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train.py --exp "$EXP" --split $i --seed 42 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" \
--causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
--pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
--ff_type "feed_forward" --d_ff 256 --ff_dropout 0.5 \
--lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --max_length 9000 --short True --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps"
done

EXP=test_attn_conv_cross_
EXP+=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train.py --exp "$EXP" --split $i --seed 42 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" \
--causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
--randomly_replace_prob 0.01 --mask_prob 0.01 --replace_duration 15 --mask_duration 15 \
--duplicate_frames_prob 0.1 --drop_frames_prob 0.1 --added_noise 0.3 \
--attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
--pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
--ff_type "conv" --d_ff 64 --ff_dropout 0.5 --conv_kernel 5 --conv_dilation 4 --conv_layers 1 \
--lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --deep_supervision False --max_length 9000 --short True --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps"
done

#cd /home/krellstef/master_code
#python3 pretrain_self_supervised.py

 # python3 train.py --exp "$EXP" --seed $i --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/features_CoBot_step.pth.tar" \
 # --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
 # --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
 # --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
 # --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
 # --ff_type "feed_forward" --d_ff 256 --ff_dropout 0.5 \
 # --lr 0.0005 -b 1 --steps 50 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --max_length 9000 --short True --task "CoBotStep"


 #  python3 train.py --exp "$EXP" --seed $i --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/features_CoBot_steps_n.pth.tar" \
 # --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
 # --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
 # --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
 # --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
 # --ff_type "feed_forward" --d_ff 256 --ff_dropout 0.5 \
 # --lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --task "CoBotStep" --max_length 9000 --short True --regular 1.0
