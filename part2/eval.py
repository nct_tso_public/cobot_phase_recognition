from train_opts import parser
from models import GestureTransformer, T5AttnConfig, MyMultiStageModel, MyMultiStageModelWrapper
from dataset import MyDataSet, MyDataSet_short
from train_utils import LabelSmoothingLoss, truncated_MSE_loss
from metrics import accuracy, average_F1, edit_score, overlap_f1
from util import AverageMeter, splits_LOSO, splits_LOUO, splits_LOUO_NP, gestures_SU, gestures_NP, gestures_KT, \
    JIGSAWS_FPS, MAX_VIDEO_LENGTHS, LABEL_IGN_IDX, Cholec80
import util

import os.path
import datetime
import numpy as np
import math
import torch
from torch.utils.tensorboard import SummaryWriter

from sklearn.metrics import accuracy_score, recall_score, precision_score, f1_score, jaccard_score

def evaluate_framewise():
    feature_file = "/local_home/funkeisab/cholec80/features/Cholec80_ResNet50.pth.tar"
    eval_out = "/mnt/g27prist/TCO/TCO-Staff/Homes/funkeisab/Phase-Transformer/eval_cnn"

    data_info = torch.load(feature_file)
    test_videos = sorted(list(data_info['test'].keys()))

    for video_id in test_videos:
        data_info['labels'][video_id] = data_info['labels'][video_id].numpy()
        data_info['predictions'][video_id] = data_info['predictions'][video_id].numpy()

    results = {}

    results['accuracy'] = []
    for video_id in test_videos:
        results['accuracy'].append(accuracy_score(data_info['labels'][video_id], data_info['predictions'][video_id]) * 100)

    results['recall'] = []
    for video_id in test_videos:
        results['recall'].append(recall_score(data_info['labels'][video_id], data_info['predictions'][video_id],
                                       average='macro') * 100)

    results['precision'] = []
    for video_id in test_videos:
        results['precision'].append(precision_score(data_info['labels'][video_id], data_info['predictions'][video_id],
                          average='macro') * 100)

    results['f_1'] = []
    for video_id in test_videos:
        results['f_1'].append(f1_score(data_info['labels'][video_id], data_info['predictions'][video_id],
                          average='macro') * 100)

    results['jaccard'] = []
    for video_id in test_videos:
        results['jaccard'].append(jaccard_score(data_info['labels'][video_id], data_info['predictions'][video_id],
                       average='macro') * 100)

    torch.save(results, eval_out + ".pth.tar")

    txt_file = open(eval_out + ".txt", "w")
    txt_file.write("acc: {:.2f} +- {:.2f}\n".format(np.mean(results['accuracy']), np.std(results['accuracy'], ddof=1)))
    txt_file.write("rec: {:.2f} +- {:.2f}\n".format(np.mean(results['recall']), np.std(results['recall'], ddof=1)))
    txt_file.write("prc: {:.2f} +- {:.2f}\n".format(np.mean(results['precision']), np.std(results['precision'], ddof=1)))
    txt_file.write("f_1: {:.2f} +- {:.2f}\n".format(np.mean(results['f_1']), np.std(results['f_1'], ddof=1)))
    txt_file.write("jcc: {:.2f} +- {:.2f}\n".format(np.mean(results['jaccard']), np.std(results['jaccard'], ddof=1)))
    txt_file.close()

    print("acc: {:.2f} +- {:.2f}\n".format(np.mean(results['accuracy']), np.std(results['accuracy'], ddof=1)))
    print("rec: {:.2f} +- {:.2f}\n".format(np.mean(results['recall']), np.std(results['recall'], ddof=1)))
    print("prc: {:.2f} +- {:.2f}\n".format(np.mean(results['precision']), np.std(results['precision'], ddof=1)))
    print("f_1: {:.2f} +- {:.2f}\n".format(np.mean(results['f_1']), np.std(results['f_1'], ddof=1)))
    print("jcc: {:.2f} +- {:.2f}\n".format(np.mean(results['jaccard']), np.std(results['jaccard'], ddof=1)))

def evaluate_2(args):
    data_info = torch.load(args.feature_file)
    test_videos = sorted(list(data_info['test'].keys()))

    results = {'labels': {},
               'predictions': {},
               }
    for video_id in data_info['labels']:
        results['labels'][video_id] = data_info['labels'][video_id].numpy()

    model_file = os.path.join(args.to_eval, "model_best.pth.tar")
    print(model_file)
    rep_results = get_predictions(args, model_file)
    return

    # assert(sorted(list(rep_results['predictions'].keys())) == test_videos)
    # for video_id in test_videos:
    #     print(video_id)
    #     print(np.array_equal(data_info['labels'][video_id], rep_results['labels'][video_id]))
    #     #assert(np.array_equal(data_info['labels'][video_id], rep_results['labels'][video_id]))
    # #results['predictions'] = rep_results['predictions']
    # results = rep_results


    # results['accuracy'] = []
    # for video_id in test_videos:
    #     accuracy = []
    #     accuracy.append(accuracy_score(results['labels'][video_id], results['predictions'][video_id]) * 100)
    #     results['accuracy'].append(np.mean(accuracy))

    # results['recall'] = []
    # for video_id in test_videos:
    #     recall = []
    #     recall.append(recall_score(results['labels'][video_id], results['predictions'][video_id],
    #                                    average='macro') * 100)
    #     results['recall'].append(np.mean(recall))

    # results['precision'] = []
    # for video_id in test_videos:
    #     precision = []
    #     precision.append(precision_score(results['labels'][video_id], results['predictions'][video_id],
    #                       average='macro') * 100)
    #     results['precision'].append(np.mean(precision))

    # results['f_1'] = []
    # for video_id in test_videos:
    #     f_1 = []
    #     f_1.append(f1_score(results['labels'][video_id], results['predictions'][video_id],
    #                       average='macro') * 100)
    #     results['f_1'].append(np.mean(f_1))

    # results['jaccard'] = []
    # for video_id in test_videos:
    #     jaccard = []
    #     jaccard.append(jaccard_score(results['labels'][video_id], results['predictions'][video_id],
    #                    average='macro') * 100)
    #     results['jaccard'].append(np.mean(jaccard))


    # torch.save(results, args.eval_out + ".pth.tar")

    # txt_file = open(args.eval_out + ".txt", "w")
    # txt_file.write("acc: {:.2f} +- {:.2f}\n".format(np.mean(results['accuracy']), np.std(results['accuracy'], ddof=1)))
    # txt_file.write("rec: {:.2f} +- {:.2f}\n".format(np.mean(results['recall']), np.std(results['recall'], ddof=1)))
    # txt_file.write("prc: {:.2f} +- {:.2f}\n".format(np.mean(results['precision']), np.std(results['precision'], ddof=1)))
    # txt_file.write("f_1: {:.2f} +- {:.2f}\n".format(np.mean(results['f_1']), np.std(results['f_1'], ddof=1)))
    # txt_file.write("jcc: {:.2f} +- {:.2f}\n".format(np.mean(results['jaccard']), np.std(results['jaccard'], ddof=1)))
    # txt_file.close()

    # print("acc: {:.2f} +- {:.2f}\n".format(np.mean(results['accuracy']), np.std(results['accuracy'], ddof=1)))
    # print("rec: {:.2f} +- {:.2f}\n".format(np.mean(results['recall']), np.std(results['recall'], ddof=1)))
    # print("prc: {:.2f} +- {:.2f}\n".format(np.mean(results['precision']), np.std(results['precision'], ddof=1)))
    # print("f_1: {:.2f} +- {:.2f}\n".format(np.mean(results['f_1']), np.std(results['f_1'], ddof=1)))
    # print("jcc: {:.2f} +- {:.2f}\n".format(np.mean(results['jaccard']), np.std(results['jaccard'], ddof=1)))






def evaluate(args):
    print(args.to_eval)
    # /mnt/g27prist/TCO/TCO-Staff/Homes/funkeisab/Phase-Transformer/test_tcn_baseline_20210302/train-val-test/Tecno

    # file subfolders
    reps = [d for d in os.listdir(args.to_eval) if (os.path.isdir(os.path.join(args.to_eval, d)) and not d.startswith('.'))]
    print("Found {} replicates: {}".format(len(reps), reps))

    data_info = torch.load(args.feature_file)
    test_videos = sorted(list(data_info['test'].keys()))

    results = {'labels': {},
               'predictions': {},
               }
    for video_id in data_info['labels']:
        results['labels'][video_id] = data_info['labels'][video_id].numpy()

    for rep in reps:
        model_file = os.path.join(args.to_eval, rep, "model_best.pth.tar")
        rep_results = get_predictions(args, model_file)

        assert(sorted(list(rep_results['predictions'].keys())) == test_videos)
        for video_id in test_videos:
            assert(np.array_equal(data_info['labels'][video_id], rep_results['labels'][video_id]))

        results['predictions'][rep] = rep_results['predictions']

    results['accuracy'] = []
    for video_id in test_videos:
        accuracy = []
        for rep in reps:
            accuracy.append(accuracy_score(results['labels'][video_id], results['predictions'][rep][video_id]) * 100)
        results['accuracy'].append(np.mean(accuracy))

    results['recall'] = []
    for video_id in test_videos:
        recall = []
        for rep in reps:
            recall.append(recall_score(results['labels'][video_id], results['predictions'][rep][video_id],
                                       average='macro') * 100)
        results['recall'].append(np.mean(recall))

    results['precision'] = []
    for video_id in test_videos:
        precision = []
        for rep in reps:
            precision.append(precision_score(results['labels'][video_id], results['predictions'][rep][video_id],
                          average='macro') * 100)
        results['precision'].append(np.mean(precision))

    results['f_1'] = []
    for video_id in test_videos:
        f_1 = []
        for rep in reps:
            f_1.append(f1_score(results['labels'][video_id], results['predictions'][rep][video_id],
                          average='macro') * 100)
        results['f_1'].append(np.mean(f_1))

    results['jaccard'] = []
    for video_id in test_videos:
        jaccard = []
        for rep in reps:
            jaccard.append(jaccard_score(results['labels'][video_id], results['predictions'][rep][video_id],
                       average='macro') * 100)
        results['jaccard'].append(np.mean(jaccard))

    torch.save(results, args.eval_out + ".pth.tar")

    txt_file = open(args.eval_out + ".txt", "w")
    txt_file.write("acc: {:.2f} +- {:.2f}\n".format(np.mean(results['accuracy']), np.std(results['accuracy'], ddof=1)))
    txt_file.write("rec: {:.2f} +- {:.2f}\n".format(np.mean(results['recall']), np.std(results['recall'], ddof=1)))
    txt_file.write("prc: {:.2f} +- {:.2f}\n".format(np.mean(results['precision']), np.std(results['precision'], ddof=1)))
    txt_file.write("f_1: {:.2f} +- {:.2f}\n".format(np.mean(results['f_1']), np.std(results['f_1'], ddof=1)))
    txt_file.write("jcc: {:.2f} +- {:.2f}\n".format(np.mean(results['jaccard']), np.std(results['jaccard'], ddof=1)))
    txt_file.close()

    print("acc: {:.2f} +- {:.2f}\n".format(np.mean(results['accuracy']), np.std(results['accuracy'], ddof=1)))
    print("rec: {:.2f} +- {:.2f}\n".format(np.mean(results['recall']), np.std(results['recall'], ddof=1)))
    print("prc: {:.2f} +- {:.2f}\n".format(np.mean(results['precision']), np.std(results['precision'], ddof=1)))
    print("f_1: {:.2f} +- {:.2f}\n".format(np.mean(results['f_1']), np.std(results['f_1'], ddof=1)))
    print("jcc: {:.2f} +- {:.2f}\n".format(np.mean(results['jaccard']), np.std(results['jaccard'], ddof=1)))


def get_predictions(args, model_file):
    if not torch.cuda.is_available():
        print("GPU not found - exit")
        return

    device_gpu = torch.device("cuda:0")
    device_cpu = torch.device("cpu")

    if args.task == "Cholec80":
        num_class = Cholec80.num_phases
    elif args.task == "CoBotPhase":
        num_class = 5
    elif args.task == "CoBotStep":
        num_class = 9

    features = torch.load(args.feature_file)
    #assert (features['split'] == args.split)

    video_fps = 25 if (args.task == "Cholec80" or args.task == "CoBotPhase" or args.task == "CoBotStep") else JIGSAWS_FPS
    max_video_length = MAX_VIDEO_LENGTHS[args.task]
    bool__ = args.task != "Cholec80" and args.task != "CoBotPhase" and args.task != "CoBotStep" 
    if bool__:
        max_video_length /= args.video_sampling_step
    padded_length = int(max_video_length + 1)
    if args.max_length >= 0 and args.max_length < padded_length:
        padded_length = args.max_length
    print(max_video_length)
    max_pos_id = int(max_video_length * 1.5)

    print("Loading test data ... ")
    if args.short:
        test_set = MyDataSet_short(args.feature_file, "test", max_pos_id, padded_length=padded_length,
                              video_fps=video_fps, video_sampling_step=args.video_sampling_step, return_video_ids=True)
    else:    
        test_set = MyDataSet(args.feature_file, "test", max_pos_id, padded_length=padded_length,
                         video_fps=video_fps, video_sampling_step=args.video_sampling_step, return_video_ids=True)
    test_videos = test_set.video_ids
    print("Found {} test videos: {}".format(len(test_videos), test_videos))
    test_loader = torch.utils.data.DataLoader(test_set, batch_size=args.batch_size, shuffle=False,
                                              num_workers=1, pin_memory=False, persistent_workers=False)

    print("Creating model... ")

    # ===== prepare model =====

    if args.pos_embed_type == "relative_T5":
        T5_attn_config = T5AttnConfig(num_buckets=args.T5_num_buckets, max_distance=args.T5_max_distance,
                                      is_bidirectional=not args.causal)
    else:
        T5_attn_config = None

    sampling_rate = video_fps / args.video_sampling_step
    attn_local_window = int(args.attn_local_window_duration * sampling_rate)
    attn_local_stride = int(math.ceil(args.attn_local_stride_duration * sampling_rate))
    attn_global_stride = int(args.attn_global_stride_duration * sampling_rate)

    if attn_local_stride < 0:
        attn_local_stride = 1

    if args.do_tecno:  # @ deprecated
        if args.final_tcn_layers == 0:
            num_stages = 1
        else:
            assert(args.final_tcn_layers == args.initial_tcn_layers)
            num_stages = 2
        if not args.tcn_norm:
            norm = "none"
        else:
            norm = "pre-norm" if args.pre_norm else "post-norm"
        model = MyMultiStageModel(num_stages=num_stages, num_layers=args.initial_tcn_layers, d_model=args.d_model,
                                  in_dim=test_set.feature_dim, num_classes=num_class, causal_conv=args.causal,
                                  dropout=args.tcn_dropout, hidden_dropout=args.hidden_dropout,
                                  norm=norm, activation=args.activation, linear_proj=args.tcn_linear_proj,
                                  reduce_dim=args.reduce_dim_tcn, inner_softmax=args.inner_softmax,
                                  do_initial_norm=args.tcn_initial_norm)
    elif args.tcn_baseline:
        model = MyMultiStageModelWrapper(num_stages=args.tcn_stages, num_layers=args.tcn_layers,
                                         d_model=args.d_model, in_dim=test_set.feature_dim, num_classes=num_class,
                                         causal_conv=args.causal)
    else:
        model = GestureTransformer(num_class=num_class, d_in=test_set.feature_dim, d_model=args.d_model,
                                   max_pos_id=max_pos_id, padded_length=test_set.padded_length,
                                   n_encoder_blocks=args.nlayers, nhead=args.nhead, dim_ff=args.d_ff, pre_norm=args.pre_norm,
                                   ff_type=args.ff_type,
                                   causal_conv=args.causal, initial_norm=args.initial_norm, activation=args.activation,
                                   conv_kernel=args.conv_kernel, conv_dilation=args.conv_dilation,
                                   conv_layers=args.conv_layers,
                                   hidden_dropout=args.hidden_dropout, ff_dropout=args.ff_dropout,
                                   attn_dropout=args.attn_dropout, position_embedding_type=args.pos_embed_type,
                                   repeat_pos_info=args.repeat_pos_info, scale_pos=args.scale_pos_encoding,
                                   attn_local_window=attn_local_window, attn_local_stride=attn_local_stride,
                                   attn_global_stride=attn_global_stride, attn_global_type=args.attn_global_type,
                                   attn_impl=args.attn_impl, attn_sparsity=args.attn_sparsity, topk=args.attn_topk,
                                   T5_attn_config=T5_attn_config,
                                   gated_attn=args.gated_attn, gate_fn=args.gate_fn, forward_only=args.gate_forward_only)
    #print(model)
    print("Loading model weights")
    model_weights = torch.load(model_file)['model_weights']
    model.load_state_dict(model_weights)

    # ===== train model =====

    torch.manual_seed(args.seed)
    # np.random.seed(args.seed)
    torch.backends.cudnn.benchmark = False
    torch.backends.cudnn.deterministic = True

    print("Calculate predictions ...")

    model = model.to(device_gpu)

    results = {'predictions': {},
               'labels': {}
               }

    test_loader.dataset.eval()
    model.eval()
    predictions_path = os.path.join(args.to_eval, "predictions")
    if not os.path.exists(predictions_path):
        os.makedirs(predictions_path)
    if args.short:
        with torch.no_grad():
            first = True
            P = None
            Y = None
            vid_id_old = None
            
            for _, batch in enumerate(test_loader):
                (a,b,c,d,vid_id,it) = batch
                batch_ = (a,b,c,d)#,vid_id)
                predicted, labels, out_mask, num_frames = forward(batch_, model, device_gpu, device_cpu,
                                                                          args, optimizer=None, train=False, criterion=None)

                batch_size = predicted.shape[0]
                assert(batch_size == 1)

                P_ = predicted[0, :]
                Y_ = labels[0, :]
                print(vid_id)
                # remove key padding
                if first:
                    P = P_[out_mask[0, :]]
                    Y = Y_[out_mask[0, :]]
                    first = False
                elif vid_id == vid_id_old:
                    P = np.concatenate((P,P_[out_mask[0, :]]),axis=0)
                    Y = np.concatenate((Y,Y_[out_mask[0, :]]),axis=0)
                else:
                    f_out = open(os.path.join(predictions_path, vid_id_old[0] + ".txt"), "w")
                    for p, l in zip(P, Y):
                            f_out.write(str(p) + "," + str(l) + "\n")
                    f_out.close
                    P = P_[out_mask[0, :]]
                    Y = Y_[out_mask[0, :]]
                    
                vid_id_old = vid_id
            f_out = open(os.path.join(predictions_path, vid_id_old[0] + ".txt"), "w")
            for p, l in zip(P, Y):
                f_out.write(str(p) + "," + str(l) + "\n")
            f_out.close
      
    else:
        with torch.no_grad():
            for _, batch1 in enumerate(test_loader):
                video_ids = batch1[-1]
                _batch = batch1[0:-1]
                #_batch = batch1

                predicted, labels, out_mask, num_frames = forward(_batch, model, device_gpu, device_cpu, args,
                                                                  criterion=None, optimizer=None, train=False)

                batch_size = predicted.shape[0]
                for i in range(batch_size):
                    video_id = video_ids[i]
                    P = predicted[i, :]
                    Y = labels[i, :]

                    # remove key padding
                    P = P[out_mask[i, :]]
                    Y = Y[out_mask[i, :]]

                    print(video_id)
                    results['predictions'][video_id] = P
                    results['labels'][video_id] = Y
                    f_out = open(os.path.join(predictions_path, video_id + ".txt"), "w")
                    for p, l in zip(P, Y):
                            f_out.write(str(p) + "," + str(l) + "\n")
                    f_out.close

    return results


def forward(batch, model, device_gpu, device_cpu, args, criterion=None, optimizer=None, train=True):
    if train:
        optimizer.zero_grad()

    seq, target, pos_ids, key_padding_mask = batch
    num_frames = (seq.shape[0] * seq.shape[1]) - key_padding_mask.sum()
    if not train:
        out_mask = np.logical_not(key_padding_mask.numpy())
        labels = target.numpy()

    seq = seq.to(device_gpu)
    target = target.to(device_gpu)
    pos_ids = pos_ids.to(device_gpu)
    key_padding_mask = key_padding_mask.to(device_gpu)

    seq = torch.transpose(seq, 0, 1)  # N x S x F --> S x N x F
    outputs = model(seq, pos_ids, key_padding_mask)
    output = outputs[-1]  # prediction of final layer
    # S x N x num_class

    if train or criterion is not None:
        if args.label_smoothing_eps > 0:
            loss = criterion(output, target)
        else:
            loss = criterion(output.permute(1, 2, 0), target)  # --> N x num_class x S

        if args.feature_trunc_mse_factor > 0:
            truncated_mse_loss = truncated_MSE_loss(output, key_padding_mask)
            truncated_mse_loss *= args.feature_trunc_mse_factor
            if train:
                loss += truncated_mse_loss
            else:
                tmse_loss = truncated_mse_loss.item()

        if (args.deep_supervision or args.do_tecno or args.tcn_baseline) and (len(outputs) > 1):  # error on intermediate outputs
            for j in range(0, len(outputs) - 1):
                intermediate = outputs[j]
                if args.label_smoothing_eps > 0:
                    loss += criterion(intermediate, target)
                else:
                    loss += criterion(intermediate.permute(1, 2, 0), target)  # --> N x num_class x S

                if args.feature_trunc_mse_factor > 0:
                    truncated_mse_loss = truncated_MSE_loss(intermediate, key_padding_mask)
                    truncated_mse_loss *= args.feature_trunc_mse_factor
                    if train:
                        loss += truncated_mse_loss
                    else:
                        tmse_loss += truncated_mse_loss.item()

        if not train:
            ce_loss = loss.item()

    if train:
        loss.backward()
        # clip gradients ?
        optimizer.step()

    output = torch.transpose(output, 0, 1)  # S x N x num_class --> N x S x num_class
    predicted = torch.nn.Softmax(dim=2)(output)
    _, predicted = torch.max(predicted, dim=2, keepdim=False)

    if train:
        correct = torch.masked_fill(predicted == target, key_padding_mask, 0)
        acc = correct.sum().item() / num_frames

        return loss.item(), acc, num_frames

    else:
        predicted = predicted.to(device_cpu).numpy()
        # losses = (ce_loss,) if args.feature_trunc_mse_factor <= 0 else (ce_loss, tmse_loss)
        return predicted, labels, out_mask, num_frames


if __name__ == '__main__':
    #evaluate_framewise()

    args = parser.parse_args()

    evaluate_2(args)
    #evaluate(args)


