import os
import time
from sys import stderr


splits_LOSO = ['data_1.csv', 'data_2.csv', 'data_3.csv', 'data_4.csv', 'data_5.csv']
splits_LOUO = ['data_B.csv', 'data_C.csv', 'data_D.csv', 'data_E.csv', 'data_F.csv', 'data_G.csv', 'data_H.csv', 'data_I.csv']
splits_LOUO_NP = ['data_B.csv', 'data_C.csv', 'data_D.csv', 'data_E.csv', 'data_F.csv', 'data_H.csv', 'data_I.csv']

gestures_SU = ['G1', 'G2', 'G3', 'G4', 'G5', 'G6', 'G8', 'G9', 'G10', 'G11']
gestures_NP = ['G1', 'G2', 'G3', 'G4', 'G5', 'G6', 'G8', 'G11']
gestures_KT = ['G1', 'G11', 'G12', 'G13', 'G14', 'G15']

JIGSAWS_FPS = 30
MAX_VIDEO_LENGTHS = {"Suturing": 9024,
                     "Needle_Passing": 4773,
                     "Knot_Tying": 3864,
                     "Cholec80": 5994,  # 1fps
                     "CoBotPhase":39699,#39807,#39699, #1fps
                     "CoBotStep": 39699#39807#39699 #1fps
                     }

"""
Number of frames

Suturing
Shortest:  1792
Longest:  9024

Needle_Passing
Shortest:  1789
Longest:  4773

Knot_Tying
Shortest:  922
Longest:  3864
"""

class Cholec80:
    """Define parameters related to the Cholec80 dataset."""

    num_phases = 7
    """Number of surgical phases."""

    phase_map = dict()
    phase_map['Preparation'] = 0
    phase_map['CalotTriangleDissection'] = 1
    phase_map['ClippingCutting'] = 2
    phase_map['GallbladderDissection'] = 3
    phase_map['GallbladderPackaging'] = 4
    phase_map['CleaningCoagulation'] = 5
    phase_map['GallbladderRetraction'] = 6

    op_sets = ['A', 'B', 'C', 'D']
    op_set_size = 20
    videos_in_set = [['02','04','06','12','24','29','34','37','38','39','44','58','60','61','64','66','75','78','79','80'],
                    ['01','03','05','09','13','16','18','21','22','25','31','36','45','46','48','50','62','71','72','73'],
                    ['10','15','17','20','32','41','42','43','47','49','51','52','53','55','56','69','70','74','76','77'],
                    ['07','08','11','14','19','23','26','27','28','30','33','35','40','54','57','59','63','65','67','68']]
    train_sets = op_sets[:2]
    test_sets = op_sets[2:]

    train_set = videos_in_set[0] + videos_in_set[1]
    val_set = (videos_in_set[2] + videos_in_set[3])[0:8]
    test_set = (videos_in_set[2] + videos_in_set[3])[8:]

    test_videos_20 = []
    for i in range(20):
        test_videos_20.append("video" + videos_in_set[3][i])


LABEL_IGN_IDX = -100

def log(file, msg):
    """Log a message.

    :param file: File object to which the message will be written.
    :param msg:  Message to log (str).
    """
    print(time.strftime("[%d.%m.%Y %H:%M:%S]: "), msg, file=stderr)
    file.write(time.strftime("[%d.%m.%Y %H:%M:%S]: ") + msg + os.linesep)


class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count
