import torch
import torch.utils.data as data
import os
import numpy as np
import math

from util import JIGSAWS_FPS, LABEL_IGN_IDX


class MyDataSet(data.Dataset):
    # return complete (padded) sequence
    # if padded_length < 0, simply return complete sequence
    # if sequence is longer than 'padded length', randomly select subsequence of length 'padded_length'
    # apply data augmentation
    #
    # return: feature_seq, label_seq, position_ids, key_padding_mask
    def __init__(self, feature_file, mode, max_pos_id, padded_length=-1,
                 video_fps=JIGSAWS_FPS, video_sampling_step=3,
                 noise_factor=-1, added_noise=-1,
                 max_duplicate_frames_prob=-1, duplicated_duration=0,
                 max_drop_frames_prob=-1, dropped_duration=0,
                 max_randomly_replace_prob=-1, randomly_replaced_duration=0,
                 max_mask_prob=-1, masked_duration=0,
                 return_video_ids = False
                 ):
        super(MyDataSet, self).__init__()

        assert(mode in ["train", "test", "val"])
        assert(max_duplicate_frames_prob <= 1 and max_drop_frames_prob <= 1
               and max_randomly_replace_prob <= 1 and max_mask_prob <= 1)

        self.max_pos_id = max_pos_id
        self.padded_length = padded_length
        self.video_fps = video_fps
        self.video_sampling_step = video_sampling_step
        self.return_video_ids = return_video_ids

        # data structure
        self.video_ids = []
        self.label_seqs = {}  # dict of tensors (seq_length)
        self.feature_seqs = {}  # dict of tensors (seq_length x feature_dim)
        self.feature_dim = -1

        self._create_dataset(feature_file, mode)
        self.considered_video_ids = self.video_ids[:]

        # data augmentation
        # if noise_factor < 0:
        #     added_noise = -1
        # else:
        #    added_noise = self.calculate_noise(noise_factor)

        self.config = { # save for switching data augmentation on/ off
            "added_noise": added_noise,
            "max_duplicate_frames_prob": max_duplicate_frames_prob,
            "max_drop_frames_prob": max_drop_frames_prob,
            "max_randomly_replace_prob": max_randomly_replace_prob,
            "max_mask_prob": max_mask_prob
        }

        self.added_noise = self.config["added_noise"]
        self.max_duplicate_frames_prob = self.config["max_duplicate_frames_prob"]
        self.max_drop_frames_prob = self.config["max_drop_frames_prob"]
        self.max_randomly_replace_prob = self.config["max_randomly_replace_prob"]
        self.max_mask_prob = self.config["max_mask_prob"]

        sampling_rate = video_fps / video_sampling_step
        self.span_duplicated = int(sampling_rate * duplicated_duration)
        self.span_dropped = int(sampling_rate * dropped_duration)
        self.span_randomly_replaced = int(sampling_rate * randomly_replaced_duration)
        self.span_masked = int(sampling_rate * masked_duration)

    def _create_dataset(self, feature_file, mode):
        data = torch.load(feature_file)
        data_fps = data['fps']

        print(data_fps, self.video_fps, self.video_sampling_step)

        assert(data_fps == (self.video_fps / self.video_sampling_step))  # TODO

        self.video_ids = sorted(data[mode].keys())

        self.feature_dim = -1
        for video_id in self.video_ids:
            label_seq = data['labels'][video_id]
            feature_seq = data[mode][video_id].squeeze()
            if self.feature_dim < 0:
                self.feature_dim = feature_seq.shape[1]
            else:
                assert(self.feature_dim == feature_seq.shape[1])

            assert(label_seq.shape[0] == feature_seq.shape[0])
            self.label_seqs[video_id] = label_seq
            self.feature_seqs[video_id] = feature_seq

    def calculate_noise(self, noise_factor):
        data = torch.tensor([])
        for features in self.feature_seqs.values():
            data = torch.cat([data, features], dim=0)
        # print(data.shape)
        min = torch.min(data)
        max = torch.max(data)
        mean = torch.mean(data)
        std = torch.std(data)
        print("Values: [{:.4f}..{:.4f}], {:.4f} +- {:.4f}".format(min, max, mean, std))

        added_noise = std * noise_factor
        print("Noise to add ~N(0, {:.4f})".format(added_noise))

        return added_noise

    @staticmethod
    def read_label_sequence(transcriptions_dir, gesture_ids, video_id, video_sampling_step, expected_length):
        gestures_file = os.path.join(transcriptions_dir, video_id + ".txt")
        gestures = [[int(x.strip().split(' ')[0]), int(x.strip().split(' ')[1]), x.strip().split(' ')[2]]
                    for x in open(gestures_file)]  # --> list of [start_frame, end_frame, gesture_id]

        # adjust indices to temporal downsampling as specified by "video_sampling_step"
        prefix = int(round(gestures[0][0] / video_sampling_step))
        last_annotated_frame = int(round((gestures[-1][1] + 1) / video_sampling_step)) - 1
        # print("expected length: {}, prefix: {}, last_annotated_frame: {}".format(expected_length, prefix, last_annotated_frame))
        if expected_length < last_annotated_frame - prefix + 1:
            last_annotated_frame -= 1
        elif expected_length > last_annotated_frame - prefix + 1:
            last_annotated_frame += 1
        assert (last_annotated_frame - prefix + 1 == expected_length)

        for i in range(len(gestures)):
            gestures[i][0] = int(round(gestures[i][0] / video_sampling_step))
        for i in range(len(gestures)):
            if i + 1 < len(gestures):
                gestures[i][1] = gestures[i + 1][0] - 1
            else:
                gestures[i][1] = last_annotated_frame
        assert (gestures[-1][1] - prefix == expected_length - 1)

        label_seq = []
        g_no = 0
        for idx in range(expected_length):
            i = idx + prefix
            if i > gestures[g_no][1]:
                g_no += 1
            assert (gestures[g_no][0] <= i and i <= gestures[g_no][1])
            gesture_id = gestures[g_no][2]
            gesture_label = gesture_ids.index(gesture_id)
            label_seq.append(gesture_label)

        return torch.tensor(label_seq, dtype=torch.long)

    def eval(self):
        self.added_noise = 0.0
        self.max_duplicate_frames_prob = -1
        self.max_drop_frames_prob = -1
        self.max_randomly_replace_prob = -1
        self.max_mask_prob = -1

    def train(self):
        self.added_noise = self.config["added_noise"]
        self.max_duplicate_frames_prob = self.config["max_duplicate_frames_prob"]
        self.max_drop_frames_prob = self.config["max_drop_frames_prob"]
        self.max_randomly_replace_prob = self.config["max_randomly_replace_prob"]
        self.max_mask_prob = self.config["max_mask_prob"]

    def restrict_video_ids(self, allowed_video_ids):
        self.considered_video_ids = []
        for video_id in allowed_video_ids:
            assert(video_id in self.video_ids)
            self.considered_video_ids.append(video_id)

    def unrestrict_video_ids(self):
        del self.considered_video_ids
        self.considered_video_ids = self.video_ids[:]

    def calculate_class_weights(self, num_class=10):
        counts = np.zeros(num_class, dtype=np.float)
        total_frames = 0
        for label_seq in self.label_seqs.values():
            label_seq = label_seq.numpy()
            for i in range(label_seq.shape[0]):
                label = label_seq[i]
                assert(label < num_class)
                counts[label] += 1
                total_frames += 1

        freqs = counts / total_frames
        median_freq = np.median(freqs)

        missing_labels = []
        for label in range(freqs.shape[0]):
            if freqs[label] == 0:
                print("No frames with label {} found!".format(label))
                freqs[label] = 1
                missing_labels.append(label)

        weights = np.ones(num_class, dtype=np.float) * median_freq
        weights = weights / freqs
        for missing in missing_labels:
            weights[missing] = 0

        weights = (weights / np.sum(weights)) * num_class
        print(weights)

        return torch.from_numpy(weights).float()

    def get_longest_sequence(self):
        max_len = 0
        for label_seq in self.label_seqs.values():
            max_len = max(max_len, label_seq.shape[0])

        return max_len

    def __getitem__(self, index):
        video_id = self.considered_video_ids[index]
        feature_seq = self.feature_seqs[video_id]
        label_seq = self.label_seqs[video_id]
        assert(label_seq.shape[0] == feature_seq.shape[0])

        # ***** data augmentation *****

        if self.max_duplicate_frames_prob > 0:
            p = torch.tensor((1,), dtype=torch.float).uniform_(0, self.max_duplicate_frames_prob).item()
            probs = torch.ones((feature_seq.shape[0],), dtype=torch.float) * p
            seeds = torch.bernoulli(probs).bool()
            new_index = list()
            last = -1
            for i in range(feature_seq.shape[0]):
                if seeds[i].item():
                    span = int(torch.tensor((1,), dtype=torch.float).normal_(self.span_duplicated,
                                                                             self.span_duplicated * 0.16667).item())
                    span = min(feature_seq.shape[0] - i, max(0, span))

                    if span > 0:
                        f = torch.tensor((1,), dtype=torch.float).uniform_(0, 1).item()
                        if f >= 0.9:
                            factor = 4
                        elif f >= 0.6:
                            factor = 3
                        else:
                            factor = 2
                        for j in range(span):
                            idx = i + j
                            new_index.extend([idx] * factor)
                        last = idx
                    else:
                        new_index.append(i)
                else:
                    if i > last:
                        new_index.append(i)
            feature_seq = feature_seq[new_index, :]
            label_seq = label_seq[new_index]

        if self.max_drop_frames_prob > 0:
            p = torch.tensor((1,), dtype=torch.float).uniform_(0, self.max_drop_frames_prob).item()
            probs = torch.ones((feature_seq.shape[0],), dtype=torch.float) * p
            seeds = torch.bernoulli(probs).bool()
            new_index = list()
            last = -1
            for i in range(feature_seq.shape[0]):
                if seeds[i].item():
                    span = int(torch.tensor((1,), dtype=torch.float).normal_(self.span_dropped,
                                                                             self.span_dropped * 0.16667).item())
                    span = min(feature_seq.shape[0] - i, max(0, span))

                    if span > 0:
                        f = torch.tensor((1,), dtype=torch.float).uniform_(0, 1).item()
                        if f >= 0.9:
                            factor = 4
                        elif f >= 0.6:
                            factor = 3
                        else:
                            factor = 2
                        j = factor - 1
                        while j < span:
                            new_index.append(i + j)
                            j += factor
                        last = i + (span - 1)
                    else:
                        new_index.append(i)
                else:
                    if i > last:
                        new_index.append(i)
            feature_seq = feature_seq[new_index, :]
            label_seq = label_seq[new_index]

        feature_seq = feature_seq.clone().detach()

        if self.max_randomly_replace_prob > 0:
            p = torch.tensor((1,), dtype=torch.float).uniform_(0, self.max_randomly_replace_prob).item()
            probs = torch.ones((feature_seq.shape[0],), dtype=torch.float) * p
            seeds = torch.bernoulli(probs).bool()
            for i in range(feature_seq.shape[0]):
                if seeds[i].item():
                    span = int(torch.tensor((1,), dtype=torch.float).normal_(self.span_randomly_replaced,
                                                                             self.span_randomly_replaced * 0.16667).item())
                    span = min(feature_seq.shape[0] - i, span)
                    if span > 0:
                        random_offset = torch.randint(0, feature_seq.shape[0] - span + 1, (1,)).item()
                        feature_seq[i: i + span, :] = feature_seq[random_offset: random_offset + span, :].clone().detach()

        if self.added_noise > 0:
            noise = torch.normal(mean=0, std=self.added_noise, size=feature_seq.size())
            feature_seq = feature_seq + noise

        if self.max_mask_prob > 0:
            p = torch.tensor((1,), dtype=torch.float).uniform_(0, self.max_mask_prob).item()
            probs = torch.ones((feature_seq.shape[0],), dtype=torch.float) * p
            seeds = torch.bernoulli(probs).bool()
            for i in range(feature_seq.shape[0]):
                if seeds[i].item():
                    span = int(torch.tensor((1,), dtype=torch.float).normal_(self.span_masked,
                                                                             self.span_masked * 0.16667).item())
                    span = min(feature_seq.shape[0] - i, span)
                    if span > 0:
                        feature_seq[i: i + span, :] = torch.zeros((span, feature_seq.shape[1]), dtype=feature_seq.dtype)

        position_ids = list(range(feature_seq.shape[0]))
        max_pos = position_ids[-1]
        if max_pos > self.max_pos_id - 1:
            position_ids[self.max_pos_id : max_pos] = [self.max_pos_id - 1] * (max_pos - self.max_pos_id)
            max_pos = self.max_pos_id - 1

        if self.padded_length >= 0:
            # ***** adjust sequence length *****
            seq_len = feature_seq.shape[0]
            if seq_len >= self.padded_length:
                key_padding_mask = torch.tensor([False for _ in range(self.padded_length)], dtype=torch.bool)
                if seq_len > self.padded_length:  # select random subsequence
                    offset = torch.randint(0, seq_len - self.padded_length + 1, (1,)).item()
                    feature_seq = feature_seq[offset: offset + self.padded_length, :]
                    label_seq = label_seq[offset: offset + self.padded_length]
                    position_ids = position_ids[offset: offset + self.padded_length]
            else:  # seq_len < self.padded_length --> add padding
                to_pad = self.padded_length - seq_len
                feature_seq = MyDataSet.add_padding_right(feature_seq, to_pad,
                                                          value=torch.zeros((feature_seq.shape[1],), dtype=feature_seq.dtype))
                label_seq = MyDataSet.add_padding_right(label_seq, to_pad, value=LABEL_IGN_IDX)
                position_ids = MyDataSet.add_padding_right(position_ids, to_pad, value=max_pos)

                key_padding_mask = torch.tensor(([False for _ in range(self.padded_length - to_pad)]
                                            + [True for _ in range(to_pad)]), dtype=torch.bool)
            assert(feature_seq.shape[0] == self.padded_length)
        else:
            key_padding_mask = torch.tensor([False for _ in range(feature_seq.shape[0])], dtype=torch.bool)

        assert(feature_seq.shape[0] == label_seq.shape[0])
        assert(feature_seq.shape[0] == len(position_ids))

        return feature_seq, label_seq, torch.as_tensor(position_ids, dtype=torch.long), key_padding_mask, video_id

    @staticmethod
    def add_padding_right(sequence, to_pad, value=0):
        if isinstance(sequence, torch.Tensor):
            if sequence.dim() == 1:
                padded = torch.cat([sequence, torch.tensor([value] * to_pad, dtype=sequence.dtype)], dim=0)
            else:
                assert(sequence.dim() == 2)
                if isinstance(value, torch.Tensor):
                    assert(len(value) == sequence.shape[1] and value.dtype == sequence.dtype)
                    padded = torch.cat([sequence, value.expand(to_pad, sequence.shape[1])], dim=0)
                else:
                    padded = torch.cat([sequence,
                                        torch.tensor([value] * sequence.shape[1], dtype=sequence.dtype)
                                       .expand(to_pad, sequence.shape[1])], dim=0)
        else:
            assert (isinstance(sequence, list))
            padded = sequence + ([value] * to_pad)
        return padded

    def __len__(self):
        return len(self.considered_video_ids)


class MyDataSet_short(data.Dataset):
    # return complete (padded) sequence
    # if padded_length < 0, simply return complete sequence
    # if sequence is longer than 'padded length', randomly select subsequence of length 'padded_length'
    # apply data augmentation
    #
    # return: feature_seq, label_seq, position_ids, key_padding_mask
    def __init__(self, feature_file, mode, max_pos_id, padded_length=-1,
                 video_fps=JIGSAWS_FPS, video_sampling_step=3,
                 noise_factor=-1, added_noise=-1,
                 max_duplicate_frames_prob=-1, duplicated_duration=0,
                 max_drop_frames_prob=-1, dropped_duration=0,
                 max_randomly_replace_prob=-1, randomly_replaced_duration=0,
                 max_mask_prob=-1, masked_duration=0,
                 return_video_ids = False
                 ):
        super(MyDataSet_short, self).__init__()

        assert(mode in ["train", "test", "val"])
        assert(max_duplicate_frames_prob <= 1 and max_drop_frames_prob <= 1
               and max_randomly_replace_prob <= 1 and max_mask_prob <= 1)

        self.max_pos_id = max_pos_id
        self.padded_length = padded_length
        self.video_fps = video_fps
        self.video_sampling_step = video_sampling_step
        self.return_video_ids = return_video_ids

        # data structure
        self.video_ids = []
        self.label_seqs = {}  # dict of tensors (seq_length)
        self.feature_seqs = {}  # dict of tensors (seq_length x feature_dim)
        self.feature_dim = -1
        self.dataset_length = []
        self.index_to_it = {}
        self._create_dataset(feature_file, mode)
        self.considered_video_ids = self.video_ids[:]



        # data augmentation
        # if noise_factor < 0:
        #     added_noise = -1
        # else:
        #    added_noise = self.calculate_noise(noise_factor)

        self.config = { # save for switching data augmentation on/ off
            "added_noise": added_noise,
            "max_duplicate_frames_prob": max_duplicate_frames_prob,
            "max_drop_frames_prob": max_drop_frames_prob,
            "max_randomly_replace_prob": max_randomly_replace_prob,
            "max_mask_prob": max_mask_prob
        }

        self.added_noise = self.config["added_noise"]
        self.max_duplicate_frames_prob = self.config["max_duplicate_frames_prob"]
        self.max_drop_frames_prob = self.config["max_drop_frames_prob"]
        self.max_randomly_replace_prob = self.config["max_randomly_replace_prob"]
        self.max_mask_prob = self.config["max_mask_prob"]

        sampling_rate = video_fps / video_sampling_step
        self.span_duplicated = int(sampling_rate * duplicated_duration)
        self.span_dropped = int(sampling_rate * dropped_duration)
        self.span_randomly_replaced = int(sampling_rate * randomly_replaced_duration)
        self.span_masked = int(sampling_rate * masked_duration)

    def _create_dataset(self, feature_file, mode):
        data = torch.load(feature_file)
        data_fps = data['fps']

        print(data_fps, self.video_fps, self.video_sampling_step)

        assert(data_fps == (self.video_fps / self.video_sampling_step))  # TODO

        self.video_ids = sorted(data[mode].keys())

        self.feature_dim = -1
        for video_id in self.video_ids:
            label_seq = data['labels'][video_id]
            feature_seq = data[mode][video_id].squeeze()
            if self.feature_dim < 0:
                self.feature_dim = feature_seq.shape[1]
            else:
                assert(self.feature_dim == feature_seq.shape[1])

            assert(label_seq.shape[0] == feature_seq.shape[0])
            split = math.ceil(label_seq.shape[0] / self.padded_length)
            label_seq_l = []
            feature_seq_l = []
            for i in range(split):
                if i == 0:
                    start = 0
                    end = label_seq.shape[0] % self.padded_length
                else:
                    start = end
                    end = start + self.padded_length
                feature_seq_l.append(feature_seq[start:end,:])
                label_seq_l.append(label_seq[start:end])
                self.index_to_it[len(self.dataset_length)] = i
                self.dataset_length.append(video_id)
            self.label_seqs[video_id] = label_seq_l
            self.feature_seqs[video_id] = feature_seq_l
        print(self.index_to_it)

    def calculate_noise(self, noise_factor):
        data = torch.tensor([])
        for features in self.feature_seqs.values():
            data = torch.cat([data, features], dim=0)
        # print(data.shape)
        min = torch.min(data)
        max = torch.max(data)
        mean = torch.mean(data)
        std = torch.std(data)
        print("Values: [{:.4f}..{:.4f}], {:.4f} +- {:.4f}".format(min, max, mean, std))

        added_noise = std * noise_factor
        print("Noise to add ~N(0, {:.4f})".format(added_noise))

        return added_noise

    @staticmethod
    def read_label_sequence(transcriptions_dir, gesture_ids, video_id, video_sampling_step, expected_length):
        gestures_file = os.path.join(transcriptions_dir, video_id + ".txt")
        gestures = [[int(x.strip().split(' ')[0]), int(x.strip().split(' ')[1]), x.strip().split(' ')[2]]
                    for x in open(gestures_file)]  # --> list of [start_frame, end_frame, gesture_id]

        # adjust indices to temporal downsampling as specified by "video_sampling_step"
        prefix = int(round(gestures[0][0] / video_sampling_step))
        last_annotated_frame = int(round((gestures[-1][1] + 1) / video_sampling_step)) - 1
        # print("expected length: {}, prefix: {}, last_annotated_frame: {}".format(expected_length, prefix, last_annotated_frame))
        if expected_length < last_annotated_frame - prefix + 1:
            last_annotated_frame -= 1
        elif expected_length > last_annotated_frame - prefix + 1:
            last_annotated_frame += 1
        assert (last_annotated_frame - prefix + 1 == expected_length)

        for i in range(len(gestures)):
            gestures[i][0] = int(round(gestures[i][0] / video_sampling_step))
        for i in range(len(gestures)):
            if i + 1 < len(gestures):
                gestures[i][1] = gestures[i + 1][0] - 1
            else:
                gestures[i][1] = last_annotated_frame
        assert (gestures[-1][1] - prefix == expected_length - 1)

        label_seq = []
        g_no = 0
        for idx in range(expected_length):
            i = idx + prefix
            if i > gestures[g_no][1]:
                g_no += 1
            assert (gestures[g_no][0] <= i and i <= gestures[g_no][1])
            gesture_id = gestures[g_no][2]
            gesture_label = gesture_ids.index(gesture_id)
            label_seq.append(gesture_label)

        return torch.tensor(label_seq, dtype=torch.long)

    def eval(self):
        self.added_noise = 0.0
        self.max_duplicate_frames_prob = -1
        self.max_drop_frames_prob = -1
        self.max_randomly_replace_prob = -1
        self.max_mask_prob = -1

    def train(self):
        self.added_noise = self.config["added_noise"]
        self.max_duplicate_frames_prob = self.config["max_duplicate_frames_prob"]
        self.max_drop_frames_prob = self.config["max_drop_frames_prob"]
        self.max_randomly_replace_prob = self.config["max_randomly_replace_prob"]
        self.max_mask_prob = self.config["max_mask_prob"]

    def restrict_video_ids(self, allowed_video_ids):
        self.considered_video_ids = []
        for video_id in allowed_video_ids:
            assert(video_id in self.video_ids)
            self.considered_video_ids.append(video_id)

    def unrestrict_video_ids(self):
        del self.considered_video_ids
        self.considered_video_ids = self.video_ids[:]

    def calculate_class_weights(self, num_class=10):
        counts = np.zeros(num_class, dtype=np.float)
        total_frames = 0
        for label_seq in self.label_seqs.values():
            label_seq = label_seq.numpy()
            for i in range(label_seq.shape[0]):
                label = label_seq[i]
                assert(label < num_class)
                counts[label] += 1
                total_frames += 1

        freqs = counts / total_frames
        median_freq = np.median(freqs)

        missing_labels = []
        for label in range(freqs.shape[0]):
            if freqs[label] == 0:
                print("No frames with label {} found!".format(label))
                freqs[label] = 1
                missing_labels.append(label)

        weights = np.ones(num_class, dtype=np.float) * median_freq
        weights = weights / freqs
        for missing in missing_labels:
            weights[missing] = 0

        weights = (weights / np.sum(weights)) * num_class
        print(weights)

        return torch.from_numpy(weights).float()

    def get_longest_sequence(self):
        max_len = 0
        for label_seq in self.label_seqs.values():
            max_len = max(max_len, label_seq.shape[0])

        return max_len

    def __getitem__(self, index):
        #video_id = self.considered_video_ids[index]
        video_id = self.dataset_length[index]
        feature_seq_l = self.feature_seqs[video_id]
        label_seq_l = self.label_seqs[video_id]


        it = self.index_to_it[index]

        label_seq = label_seq_l[it]
        feature_seq = feature_seq_l[it]

        assert(label_seq.shape[0] == feature_seq.shape[0])

        # ***** data augmentation *****

        # if self.max_duplicate_frames_prob > 0:
        #     p = torch.tensor((1,), dtype=torch.float).uniform_(0, self.max_duplicate_frames_prob).item()
        #     probs = torch.ones((feature_seq.shape[0],), dtype=torch.float) * p
        #     seeds = torch.bernoulli(probs).bool()
        #     new_index = list()
        #     last = -1
        #     for i in range(feature_seq.shape[0]):
        #         if seeds[i].item():
        #             span = int(torch.tensor((1,), dtype=torch.float).normal_(self.span_duplicated,
        #                                                                      self.span_duplicated * 0.16667).item())
        #             span = min(feature_seq.shape[0] - i, max(0, span))

        #             if span > 0:
        #                 f = torch.tensor((1,), dtype=torch.float).uniform_(0, 1).item()
        #                 if f >= 0.9:
        #                     factor = 4
        #                 elif f >= 0.6:
        #                     factor = 3
        #                 else:
        #                     factor = 2
        #                 for j in range(span):
        #                     idx = i + j
        #                     new_index.extend([idx] * factor)
        #                 last = idx
        #             else:
        #                 new_index.append(i)
        #         else:
        #             if i > last:
        #                 new_index.append(i)
        #     feature_seq = feature_seq[new_index, :]
        #     label_seq = label_seq[new_index]

        # if self.max_drop_frames_prob > 0:
        #     p = torch.tensor((1,), dtype=torch.float).uniform_(0, self.max_drop_frames_prob).item()
        #     probs = torch.ones((feature_seq.shape[0],), dtype=torch.float) * p
        #     seeds = torch.bernoulli(probs).bool()
        #     new_index = list()
        #     last = -1
        #     for i in range(feature_seq.shape[0]):
        #         if seeds[i].item():
        #             span = int(torch.tensor((1,), dtype=torch.float).normal_(self.span_dropped,
        #                                                                      self.span_dropped * 0.16667).item())
        #             span = min(feature_seq.shape[0] - i, max(0, span))

        #             if span > 0:
        #                 f = torch.tensor((1,), dtype=torch.float).uniform_(0, 1).item()
        #                 if f >= 0.9:
        #                     factor = 4
        #                 elif f >= 0.6:
        #                     factor = 3
        #                 else:
        #                     factor = 2
        #                 j = factor - 1
        #                 while j < span:
        #                     new_index.append(i + j)
        #                     j += factor
        #                 last = i + (span - 1)
        #             else:
        #                 new_index.append(i)
        #         else:
        #             if i > last:
        #                 new_index.append(i)
        #     feature_seq = feature_seq[new_index, :]
        #     label_seq = label_seq[new_index]

        feature_seq = feature_seq.clone().detach()

        # if self.max_randomly_replace_prob > 0:
        #     p = torch.tensor((1,), dtype=torch.float).uniform_(0, self.max_randomly_replace_prob).item()
        #     probs = torch.ones((feature_seq.shape[0],), dtype=torch.float) * p
        #     seeds = torch.bernoulli(probs).bool()
        #     for i in range(feature_seq.shape[0]):
        #         if seeds[i].item():
        #             span = int(torch.tensor((1,), dtype=torch.float).normal_(self.span_randomly_replaced,
        #                                                                      self.span_randomly_replaced * 0.16667).item())
        #             span = min(feature_seq.shape[0] - i, span)
        #             if span > 0:
        #                 random_offset = torch.randint(0, feature_seq.shape[0] - span + 1, (1,)).item()
        #                 feature_seq[i: i + span, :] = feature_seq[random_offset: random_offset + span, :].clone().detach()

        # if self.added_noise > 0:
        #     noise = torch.normal(mean=0, std=self.added_noise, size=feature_seq.size())
        #     feature_seq = feature_seq + noise

        # if self.max_mask_prob > 0:
        #     p = torch.tensor((1,), dtype=torch.float).uniform_(0, self.max_mask_prob).item()
        #     probs = torch.ones((feature_seq.shape[0],), dtype=torch.float) * p
        #     seeds = torch.bernoulli(probs).bool()
        #     for i in range(feature_seq.shape[0]):
        #         if seeds[i].item():
        #             span = int(torch.tensor((1,), dtype=torch.float).normal_(self.span_masked,
        #                                                                      self.span_masked * 0.16667).item())
        #             span = min(feature_seq.shape[0] - i, span)
        #             if span > 0:
        #                 feature_seq[i: i + span, :] = torch.zeros((span, feature_seq.shape[1]), dtype=feature_seq.dtype)

        position_ids = list(range(feature_seq.shape[0]))
        max_pos = position_ids[-1]
        if max_pos > self.max_pos_id - 1:
            position_ids[self.max_pos_id : max_pos] = [self.max_pos_id - 1] * (max_pos - self.max_pos_id)
            max_pos = self.max_pos_id - 1

        if self.padded_length >= 0:
            # ***** adjust sequence length *****
            seq_len = feature_seq.shape[0]
            if seq_len >= self.padded_length:
                key_padding_mask = torch.tensor([False for _ in range(self.padded_length)], dtype=torch.bool)
                if seq_len > self.padded_length:  # select random subsequence
                    offset = torch.randint(0, seq_len - self.padded_length + 1, (1,)).item()
                    feature_seq = feature_seq[offset: offset + self.padded_length, :]
                    label_seq = label_seq[offset: offset + self.padded_length]
                    position_ids = position_ids[offset: offset + self.padded_length]
            else:  # seq_len < self.padded_length --> add padding
                to_pad = self.padded_length - seq_len
                feature_seq = MyDataSet.add_padding_right(feature_seq, to_pad,
                                                          value=torch.zeros((feature_seq.shape[1],), dtype=feature_seq.dtype))
                label_seq = MyDataSet.add_padding_right(label_seq, to_pad, value=LABEL_IGN_IDX)
                position_ids = MyDataSet.add_padding_right(position_ids, to_pad, value=max_pos)

                key_padding_mask = torch.tensor(([False for _ in range(self.padded_length - to_pad)]
                                            + [True for _ in range(to_pad)]), dtype=torch.bool)
            assert(feature_seq.shape[0] == self.padded_length)
        else:
            key_padding_mask = torch.tensor([False for _ in range(feature_seq.shape[0])], dtype=torch.bool)

        assert(feature_seq.shape[0] == label_seq.shape[0])
        assert(feature_seq.shape[0] == len(position_ids))

        return feature_seq, label_seq, torch.as_tensor(position_ids, dtype=torch.long), key_padding_mask, video_id, it

    @staticmethod
    def add_padding_right(sequence, to_pad, value=0):
        if isinstance(sequence, torch.Tensor):
            if sequence.dim() == 1:
                padded = torch.cat([sequence, torch.tensor([value] * to_pad, dtype=sequence.dtype)], dim=0)
            else:
                assert(sequence.dim() == 2)
                if isinstance(value, torch.Tensor):
                    assert(len(value) == sequence.shape[1] and value.dtype == sequence.dtype)
                    padded = torch.cat([sequence, value.expand(to_pad, sequence.shape[1])], dim=0)
                else:
                    padded = torch.cat([sequence,
                                        torch.tensor([value] * sequence.shape[1], dtype=sequence.dtype)
                                       .expand(to_pad, sequence.shape[1])], dim=0)
        else:
            assert (isinstance(sequence, list))
            padded = sequence + ([value] * to_pad)
        return padded

    def __len__(self):
        return len(self.dataset_length)#len(self.considered_video_ids)

""" 
class MySequentialDataSet(data.Dataset):
    def __init__(self, video_id, transcriptions_dir, gesture_ids,
                 feature_file, kinematics_file=None, datatype="visual",
                 d_model=128, attention_type="backward",
                 size=128, overlap=0.5, pad_duration=5, video_sampling_step=6,
                 normalize=False, normalization_params=None):
        super(MySequentialDataSet, self).__init__()

        self.video_id = video_id
        self.no_frames = -1
        self.gesture_ids = gesture_ids

        self.feature_file = feature_file
        self.kinematics_file = kinematics_file
        self.datatype = datatype  # visual, kinematic, visAndKin

        self.d_model = d_model
        assert (attention_type in ["backward", "bidirectional"])
        self.attention_type = attention_type
        self.return_attention_mask = self.attention_type == "backward"

        self.size = size  # length of each part
        assert(0 <= overlap and overlap <= 1)
        if overlap >= 0.99:
            step = 1
        else:
            step = int(size * (1 - overlap))
        assert (size % step == 0)
        self.step = step

        sampling_rate = JIGSAWS_FPS / video_sampling_step
        self.padding_size = int(sampling_rate * pad_duration)
        self.video_sampling_step = video_sampling_step
        self.normalize = normalize
        self.normalization_params = normalization_params,

        self.label_seqs = []  # list of torch tensors
        self.feature_seqs = [] # list of torch tensors
        self.pos_encodings = []  # list of torch tensors
        self.last_part_key_padding = 0 
        self.feature_dim = -1
        self.attention_mask = None  # torch tensor

        self._create_dataset(transcriptions_dir)

    def _create_dataset(self, transcriptions_dir):

        feature_seq, self.feature_dim = MyDataSet.read_feature_sequence(self.datatype, self.video_id,
                                                                        self.feature_file)
        seq_length = feature_seq.shape[0]
        self.no_frames = seq_length
        label_seq = MyDataSet.read_label_sequence(transcriptions_dir, self.gesture_ids, self.video_id,
                                                  self.video_sampling_step, seq_length)
        pos_enc = MyDataSet.get_positional_encoding(seq_length, self.d_model)  # TODO consistent padding for pos enc

        # add padding to the left
        feature_seq = MyDataSet.get_sequence_left(feature_seq, seq_length + self.padding_size, self.padding_size)
        label_seq = MyDataSet.get_sequence_left(label_seq, seq_length + self.padding_size, self.padding_size)
        pos_enc = MyDataSet.get_sequence_left(pos_enc, seq_length + self.padding_size, self.padding_size)
        seq_length = feature_seq.shape[0]

        if self.attention_type == "bidirectional":
            # add padding to the right
            feature_seq = MyDataSet.get_sequence_right(feature_seq, seq_length + self.padding_size, self.padding_size)
            label_seq = MyDataSet.get_sequence_right(label_seq, seq_length + self.padding_size, self.padding_size)
            pos_enc = MyDataSet.get_sequence_right(pos_enc, seq_length + self.padding_size, self.padding_size)
            seq_length = feature_seq.shape[0]

        # partition into smaller sequences

        done = False
        i = 0
        while i < seq_length and not done:
            if i + self.size <= seq_length:
                self.feature_seqs.append(feature_seq[i: i + self.size, :])
                self.pos_encodings.append(pos_enc[i: i + self.size, :])
                self.label_seqs.append(torch.as_tensor(label_seq[i: i + self.size], dtype=torch.long))
            else:
                done = True

                # additional padding required for last part
                to_pad = i - (seq_length - self.size)
                self.feature_seqs.append(MyDataSet.get_sequence_right(feature_seq, self.size, to_pad))
                self.label_seqs.append(torch.as_tensor(
                    MyDataSet.get_sequence_right(label_seq, self.size, to_pad), dtype=torch.long))
                self.pos_encodings.append(MyDataSet.get_sequence_right(pos_enc, self.size, to_pad))
                self.last_part_key_padding = to_pad
            
            i += self.step

        if self.return_attention_mask:
            self.attention_mask = MyDataSet.generate_square_subsequent_mask(self.size)

    def __getitem__(self, index):
        attn_mask = self.attention_mask if self.return_attention_mask else NONE_VAL

        return self.feature_seqs[index], self.label_seqs[index], self.pos_encodings[index], attn_mask

    def __len__(self):
        return len(self.label_seqs)
"""
