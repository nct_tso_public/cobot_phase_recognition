import argparse
import os
import scipy.io
import torch

from util import splits_LOUO, splits_LOUO_NP, splits_LOSO

"""
file format:

features:
'train' --> video_id --> torch.tensor, dim: length x feature_dim
                            JIGSAWS: ignore the video frames without annotation
'test'
opt. 'val'
'labels' --> video_id --> torch.tensor, dim: length

further info:
'dataset'
'split' (e.g. LOUO-0)
'fps'
'feature_extractor' (e.g. ColinCNN)                
"""


def get_all_video_ids(task):
    video_lists_dir = "./Splits/{}/LOUO".format(task)

    video_ids = []
    for f in [f for f in os.listdir(video_lists_dir) if f.startswith("data_")]:
        video_ids.extend([(x.strip().split(',')[0]) for x in open(os.path.join(video_lists_dir, f))])

    return video_ids


def get_train_video_ids(task, eval_scheme, split):
    if eval_scheme == 'LOSO':
        splits = splits_LOSO
    elif eval_scheme == 'LOUO':
        if task == "Needle_Passing":
            splits = splits_LOUO_NP
        else:
            splits = splits_LOUO
    assert (split >= 0 and split < len(splits))
    train_lists = splits[0:split] + splits[split + 1:]

    video_lists_dir = "./Splits/{}/{}".format(task, eval_scheme)

    video_ids = []
    for l in train_lists:
        video_ids.extend([(x.strip().split(',')[0]) for x in open(os.path.join(video_lists_dir, l))])

    return video_ids


def get_feature_files_ColinCNN(feature_dir, out_dir, normalize=False):
    fps = 10
    eval_scheme = "LOUO"
    dataset = "JIGSAWS_Suturing"
    feature_extractor = "ColinCNN"
    task = "Suturing"
    all_videos = get_all_video_ids(task)

    for modality in ["visual", "kinematic"]:

        for split in range(len(splits_LOUO)):

            storage = dict()
            # add some general info
            storage['dataset'] = dataset
            storage['split'] = "{}-{}".format(eval_scheme, split)
            storage['fps'] = fps
            storage['feature_extractor'] = feature_extractor

            storage['train'] = {}
            storage['test'] = {}
            storage['labels'] = {}
            # get features
            train_videos = get_train_video_ids("Suturing", eval_scheme, split)
            for video_id in all_videos:
                f = os.path.join(feature_dir, "Split_{:d}".format(split + 1), "{}.avi.mat".format(video_id))
                mat = scipy.io.loadmat(f)
                """
                data types
                    A: feature vector (128-d)
                    S: sensor data
                    Y: labels        
                """
                labels = torch.from_numpy(mat['Y']).long().squeeze()
                storage['labels'][video_id] = labels

                if modality == "visual":
                    features = mat['A']
                else:
                    features = mat['S']
                feature_tensor = torch.from_numpy(features)
                if modality == "kinematic":
                    feature_tensor = torch.transpose(feature_tensor, 0, 1)
                if video_id in train_videos:
                    storage['train'][video_id] = feature_tensor
                else:
                    storage['test'][video_id] = feature_tensor

                assert(labels.shape[0] == feature_tensor.shape[0])

            if normalize:
                # calculate mean and std on available training data
                train_data = []
                for video_id in train_videos:
                    train_data.append(storage['train'][video_id])
                train_data = torch.cat(train_data, dim=0)
                mean_vector = torch.mean(train_data, dim=0)
                std_vector = torch.std(train_data, dim=0)

                # normalize
                for video_id in all_videos:
                    part = 'train' if video_id in train_videos else 'test'
                    storage[part][video_id] = (storage[part][video_id] - mean_vector.unsqueeze(0)) / std_vector.unsqueeze(0)

            if normalize:
                filename = "Colin_normalized_{}_{:d}.pth.tar".format(modality, split)
            else:
                filename = "Colin_{}_{:d}.pth.tar".format(modality, split)
            torch.save(storage, os.path.join(out_dir, filename))


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description=".")
    parser.add_argument('--data_dir', type=str, help=".", default="/home/funkeisab/Data/Colin/SpatialCNN/")
    parser.add_argument('--out_dir', type=str, help=".", default="./features")

    args = parser.parse_args()
    for normalize in [True, False]:
        get_feature_files_ColinCNN(args.data_dir, args.out_dir, normalize=normalize)

    # test
    features = torch.load(os.path.join(args.out_dir, "Colin_kinematic_0.pth.tar"))

    print(features['split'])
    print(features['train'].keys())
    print(features['test'].keys())
    print(features['labels'].keys())

    test_labels = features['labels']["Suturing_B001"]
    print(test_labels.shape, test_labels)

    test_features = features['test']["Suturing_B001"]
    print(test_features.shape, torch.mean(test_features), torch.std(test_features))
    features = torch.load(os.path.join(args.out_dir, "Colin_visual_0.pth.tar"))
    test_features = features['test']["Suturing_B001"]
    print(test_features.shape, torch.mean(test_features), torch.std(test_features))

    features = torch.load(os.path.join(args.out_dir, "Colin_normalized_kinematic_0.pth.tar"))
    test_features = features['test']["Suturing_B001"]
    print(test_features.shape, torch.mean(test_features), torch.std(test_features))
    features = torch.load(os.path.join(args.out_dir, "Colin_normalized_visual_0.pth.tar"))
    test_features = features['test']["Suturing_B001"]
    print(test_features.shape, torch.mean(test_features), torch.std(test_features))
