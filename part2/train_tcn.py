from train_opts import parser
from models import GestureTransformer, MultiStageModel
from dataset import MyDataSet, MySequentialDataSet
from metrics import accuracy, average_F1, edit_score, overlap_f1
from util import AverageMeter, splits_LOSO, splits_LOUO, splits_LOUO_NP, gestures_SU, gestures_NP, gestures_KT, \
    NONE_VAL, JIGSAWS_FPS, MAX_VIDEO_LENGTHS, LABEL_IGN_IDX
import util

import os.path
import datetime
import numpy as np
import string
import torch
import torchvision
import torch.nn.functional as F
from torch.utils.tensorboard import SummaryWriter


def main(args):
    if not torch.cuda.is_available():
        print("GPU not found - exit")
        return

    device_gpu = torch.device("cuda:0")
    device_cpu = torch.device("cpu")

    checkpoint = None
    if args.resume_exp:
        output_folder = args.resume_exp
    else:
        output_folder = os.path.join(args.out, args.exp + "_" + datetime.datetime.now().strftime("%Y%m%d"),
                                     args.eval_scheme, str(args.split), datetime.datetime.now().strftime("%H%M"))
        os.makedirs(output_folder)

    f_log = open(os.path.join(output_folder, "log.txt"), "a")
    def log(msg):
        util.log(f_log, msg)
    checkpoint_file = os.path.join(output_folder, "checkpoint" + ".pth.tar")

    if args.resume_exp:
        checkpoint = torch.load(checkpoint_file)
        args_checkpoint = checkpoint['args']
        for arg in args_checkpoint:
            setattr(args, arg, args_checkpoint[arg])
        log("====================================================================")
        log("Resuming experiment...")
        log("====================================================================")
    else:
        if len([t for t in string.Formatter().parse(args.video_lists_dir)]) > 1:
            args.video_lists_dir = args.video_lists_dir.format(args.task)
        if len([t for t in string.Formatter().parse(args.transcriptions_dir)]) > 1:
            args.transcriptions_dir = args.transcriptions_dir.format(args.task)

        # TODO -- check whether all arguments are valid

        log("Used parameters...")
        for arg in sorted(vars(args)):
            log("\t" + str(arg) + " : " + str(getattr(args, arg)))

    args_dict = {}
    for arg in vars(args):
        args_dict[str(arg)] = getattr(args, arg)

    writer = SummaryWriter(log_dir=output_folder, purge_step=checkpoint['epoch'] if args.resume_exp else None)

    gesture_ids = None
    if args.task == "Suturing":
        gesture_ids = gestures_SU
    elif args.task == "Needle_Passing":
        gesture_ids = gestures_NP
    elif args.task == "Knot_Tying":
        gesture_ids = gestures_KT
    num_class = len(gesture_ids)

    splits = None  # TODO
    if args.eval_scheme == 'LOSO':
        splits = splits_LOSO
    elif args.eval_scheme == 'LOUO':
        if args.task == "Needle_Passing":
            splits = splits_LOUO_NP
        else:
            splits = splits_LOUO
    assert (args.split >= 0 and args.split < len(splits))
    test_lists = splits[args.split:args.split + 1]
    train_lists = splits[0:args.split] + splits[args.split + 1:]

    # ===== load data =====

    lists_dir = os.path.join(args.video_lists_dir, args.eval_scheme)
    test_lists = list(map(lambda x: os.path.join(lists_dir, x), test_lists))
    train_lists = list(map(lambda x: os.path.join(lists_dir, x), train_lists))
    log("Splits in train set :" + str(train_lists))
    log("Splits in test set: " + str(test_lists))

    log("Creating data sets... ")

    # ----- train data -----

    sampling_rate = JIGSAWS_FPS / args.video_sampling_step
    padding_size = int(sampling_rate * args.pad_duration)
    
    max_seq_length = int(round(MAX_VIDEO_LENGTHS[args.task] / args.video_sampling_step)) + 1 + \
                     ((2 if args.attn_type == "bidirectional" else 1) * padding_size)

    if args.max_length < 0:
        length = max_seq_length
    else:
        length = args.max_length
    train_set = MyDataSet(train_lists, args.transcriptions_dir, gesture_ids, max_seq_length,
                          args.feature_file, kinematics_file=None, datatype=args.feature_type,
                          d_model=args.d_model, attention_type=args.attn_type,
                          padded_length=length, pad_duration=args.pad_duration,
                          video_sampling_step=args.video_sampling_step,
                          noise_factor=args.added_noise, max_mask_prob=args.mask_prob, jitter_prob=args.jitter_prob,
                          normalize=False, normalization_params=None)
    train_loader = torch.utils.data.DataLoader(train_set, batch_size=args.batch_size, shuffle=True,
                                               num_workers=args.workers,
                                               pin_memory=False, persistent_workers=False)

    # ----- test data -----

    if args.max_length < 0:
        eval_train_set = MyDataSet(train_lists, args.transcriptions_dir, gesture_ids, max_seq_length,
                                   args.feature_file, kinematics_file=None, datatype=args.feature_type,
                                   d_model=args.d_model, attention_type=args.attn_type,
                                   padded_length=length, pad_duration=args.pad_duration,
                                   video_sampling_step=args.video_sampling_step,
                                   noise_factor=0.0, normalize=False, normalization_params=None)
        eval_train_loader = torch.utils.data.DataLoader(eval_train_set, batch_size=args.batch_size, shuffle=False,
                                                        num_workers=1, pin_memory=False, persistent_workers=False)

        eval_test_set = MyDataSet(test_lists, args.transcriptions_dir, gesture_ids, max_seq_length,
                                  args.feature_file, kinematics_file=None, datatype=args.feature_type,
                                  d_model=args.d_model, attention_type=args.attn_type,
                                  padded_length=length, pad_duration=args.pad_duration,
                                  video_sampling_step=args.video_sampling_step,
                                  noise_factor=0.0, normalize=False, normalization_params=None)
        eval_test_loader = torch.utils.data.DataLoader(eval_test_set, batch_size=args.batch_size, shuffle=False,
                                                        num_workers=1, pin_memory=False, persistent_workers=False)
    else:
        train_videos = list()
        for list_file in train_lists:
            train_videos.extend([(x.strip().split(',')[0], x.strip().split(',')[1]) for x in open(list_file)])
        eval_train_loaders = list()
        for video in train_videos:
            dataset = MySequentialDataSet(video[0], args.transcriptions_dir, gesture_ids,
                                          args.feature_file, kinematics_file=None, datatype=args.feature_type,
                                          d_model=args.d_model, attention_type=args.attn_type,
                                          size=args.max_length, overlap=args.eval_overlap,
                                          pad_duration=args.pad_duration, video_sampling_step=args.video_sampling_step,
                                          normalize=False, normalization_params=None)
            eval_train_loaders.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size,
                                                                  shuffle=False, num_workers=1,
                                                                  pin_memory=False, persistent_workers=False))

        test_videos = list()
        for list_file in test_lists:
            test_videos.extend([(x.strip().split(',')[0], x.strip().split(',')[1]) for x in open(list_file)])
        eval_test_loaders = list()
        for video in test_videos:
            dataset = MySequentialDataSet(video[0], args.transcriptions_dir, gesture_ids,
                                          args.feature_file, kinematics_file=None, datatype=args.feature_type,
                                          d_model=args.d_model, attention_type=args.attn_type,
                                          size=args.max_length, overlap=args.eval_overlap,
                                          pad_duration=args.pad_duration, video_sampling_step=args.video_sampling_step,
                                          normalize=False, normalization_params=None)
            eval_test_loaders.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size,
                                                                 shuffle=False, num_workers=1,
                                                                 pin_memory=False, persistent_workers=False))

    # TODO print info about datasets for logging/ debugging

    log("Creating model... ")

    # ===== prepare model =====

    dim_feature = train_set.feature_dim

    model = MultiStageModel(num_stages=args.nstages, num_layers=args.tcn_layers, num_f_maps=args.d_tcn,
                            dim=dim_feature, num_classes=num_class, causal_conv=(args.attn_type == "backward"))
    if checkpoint:
        # load model weights
        model.load_state_dict(checkpoint['model_weights'])

    log("param count: {}".format(sum(p.numel() for p in model.parameters())))
    log("trainable params: {}".format(sum(p.numel() for p in model.parameters() if p.requires_grad)))

    assert(LABEL_IGN_IDX >= num_class)
    criterion = torch.nn.CrossEntropyLoss(ignore_index=LABEL_IGN_IDX)
    optimizer = torch.optim.AdamW(filter(lambda p: p.requires_grad, model.parameters()), lr=args.lr,
                                  betas=(0.9, 0.999), eps=1e-08, weight_decay=0.01, amsgrad=False)
    """ 
    if args.do_SGD:
        optimizer = torch.optim.SGD(filter(lambda p: p.requires_grad, model.parameters()), lr=args.lr,
                                    momentum=0.9, weight_decay=0.001)
    """
    if checkpoint:
        # load optimizer state
        optimizer.load_state_dict(checkpoint['optimizer'])
        for state in optimizer.state.values():
            for k, v in state.items():
                if isinstance(v, torch.Tensor):
                    state[k] = v.to(device_gpu)

    last_epoch = -1
    if checkpoint:
        last_epoch = checkpoint['epoch']
    def lr_lambda(current_step):
        if current_step < args.warmup_steps:
            return float(current_step) / float(max(1, args.warmup_steps))
        return max(0.0, float(args.steps - current_step) / float(max(1, args.steps - args.warmup_steps)))
    scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer, lr_lambda, last_epoch)

    # ===== train model =====

    torch.manual_seed(args.seed)
    # np.random.seed(args.seed)
    if checkpoint:
        torch.set_rng_state(checkpoint['rng'])

    # for strict reproducibility --> testing
    # torch.backends.cudnn.benchmark = False
    # torch.backends.cudnn.deterministic = True

    log("Start training...")

    model = model.to(device_gpu)

    train_loss = AverageMeter()
    train_acc = AverageMeter()

    start_epoch = max(last_epoch, 0)
    for epoch in range(start_epoch, args.steps):

        model.train()
        for j, batch in enumerate(train_loader):

            optimizer.zero_grad()
            # accumulate gradients over batches ?

            seq, target, pos_enc, key_padding, attn_mask = batch
            batch_size = seq.shape[0]
            num_frames = (seq.shape[0] * seq.shape[1]) - key_padding.sum()

            seq = seq.to(device_gpu)
            target[key_padding] = LABEL_IGN_IDX
            target = target.to(device_gpu)
            # pos_enc = pos_enc.to(device_gpu)
            key_padding = key_padding.to(device_gpu)


            """ 
            if attn_mask.ndim == 1 and attn_mask[0].item() == NONE_VAL:
                attn_mask = None  # hack ;P
            else:            
                if attn_mask.ndim == 3:
                    attn_mask = attn_mask[0, :, :]
                # print(j, "attn_mask.shape", attn_mask.shape)
                attn_mask = attn_mask.to(device_gpu)
            """

            seq = torch.transpose(seq, 1, 2)  # N x S x F --> N x F x S
            mask = torch.logical_not(key_padding).unsqueeze(1)

            # print(j, "target.shape", target.shape)
            # print(j, "seq.shape", seq.shape)

            output = model(seq, mask)  # out: (N * nstages) x num_class x S

            ms_output = output.view(args.nstages, batch_size, output.shape[-2], output.shape[-1])

            loss = 0
            for s in range(args.nstages):
                p = ms_output[s]
                loss += criterion(p, target)

                # truncated MSE loss
                loss += 0.15 * \
                        torch.mean(
                            torch.clamp(
                                torch.nn.MSELoss(reduction='none')(F.log_softmax(p[:, :, 1:], dim=1),
                                                                   F.log_softmax(p.detach()[:, :, :-1], dim=1)),
                                min=0, max=16) * mask[:, 0:1, 1:]
                        )

            loss.backward()
            # clip gradients ?
            optimizer.step()

            train_loss.update(loss.item(), num_frames)

            final_preds = torch.transpose(ms_output[-1], 1, 2)
            predicted = torch.nn.Softmax(dim=2)(final_preds)
            _, predicted = torch.max(predicted, dim=2, keepdim=False)
            acc = torch.masked_select((predicted == target), mask.squeeze(1)).sum().item() / num_frames
            train_acc.update(acc, num_frames)

        if (epoch + 1) % args.log_freq == 0:
            writer.add_scalar('Train/loss_estimate', train_loss.avg, epoch)
            writer.add_scalar('Train/acc_estimate', train_acc.avg, epoch)

            log("Epoch {}: Train loss: {:.4f} Train acc: {:.3f}".format(epoch, train_loss.avg, train_acc.avg))
            f_log.flush()
            os.fsync(f_log) 

            train_loss.reset()
            train_acc.reset()

        if (epoch + 1) % args.eval_freq == 0 or epoch == args.steps - 1:  # validation

            log("Validating (train)...")

            # check performance on train data
            if args.max_length < 0:
                eval_train_loader.dataset.mask_prob = -1
                eval_train_loader.dataset.jitter_prob = -1
                eval_train_loss, eval_train_acc, eval_train_avg_f1, eval_train_edit, eval_train_f1_10 \
                    = test_complete_sequences(eval_train_loader, model, criterion, padding_size, args.attn_type,
                                              num_class, device_gpu, device_cpu)
            """        
            else:
                eval_train_loss, eval_train_acc, eval_train_avg_f1, eval_train_edit, eval_train_f1_10 \
                    = test_split_sequences(eval_train_loaders, model, criterion, padding_size, args.attn_type,
                                           num_class, device_gpu, device_cpu)
            """
            # log results
            writer.add_scalar('Val_loss/train', eval_train_loss, epoch)
            writer.add_scalars('Metrics_frame/train', {'acc': eval_train_acc,
                                                      'avg_f1': eval_train_avg_f1}, epoch)
            writer.add_scalars('Metrics_segm/train', {'edit': eval_train_edit,
                                                      'f1_10': eval_train_f1_10}, epoch)
            
            log("Epoch {}: Val(train) -- loss: {:.4f} acc: {:.3f} avg_f1: {:.3f} edit: {:.3f} f1_10: {:.3f}"
                .format(epoch, eval_train_loss, eval_train_acc, eval_train_avg_f1, eval_train_edit, eval_train_f1_10))
            
            if (args.mask_prob > 0 or args.jitter_prob > 0) and args.max_length < 0:
                eval_train_loader.dataset.mask_prob = args.mask_prob
                eval_train_loader.dataset.jitter_prob = args.jitter_prob
                eval_train_loss, eval_train_acc, eval_train_avg_f1, eval_train_edit, eval_train_f1_10 \
                    = test_complete_sequences(eval_train_loader, model, criterion, padding_size, args.attn_type,
                                              num_class, device_gpu, device_cpu)
                
                writer.add_scalars('Metrics_frame/train_mskd', {'acc': eval_train_acc,
                                                                'avg_f1': eval_train_avg_f1}, epoch)
                writer.add_scalars('Metrics_segm/train_mskd', {'edit': eval_train_edit,
                                                               'f1_10': eval_train_f1_10}, epoch)

            log("Validating (test)...")

            # check performance on test data
            if args.max_length < 0:
                eval_test_loader.dataset.mask_prob = -1
                eval_test_loader.dataset.jitter_prob = -1
                eval_test_loss, eval_test_acc, eval_test_avg_f1, eval_test_edit, eval_test_f1_10 \
                    = test_complete_sequences(eval_test_loader, model, criterion, padding_size, args.attn_type,
                                              num_class, device_gpu, device_cpu)
            else:
                eval_test_loss, eval_test_acc, eval_test_avg_f1, eval_test_edit, eval_test_f1_10 \
                    = test_split_sequences(eval_test_loaders, model, criterion, padding_size, args.attn_type,
                                           num_class, device_gpu, device_cpu)
            # log results
            writer.add_scalar('Val_loss/test', eval_test_loss, epoch)
            writer.add_scalars('Metrics_frame/test', {'acc': eval_test_acc,
                                                      'avg_f1': eval_test_avg_f1}, epoch)
            writer.add_scalars('Metrics_segm/test', {'edit': eval_test_edit,
                                                     'f1_10': eval_test_f1_10}, epoch)

            log("Epoch {}: Val(test) -- loss: {:.4f} acc: {:.3f} avg_f1: {:.3f} edit: {:.3f} f1_10: {:.3f}"
                .format(epoch, eval_test_loss, eval_test_acc, eval_test_avg_f1, eval_test_edit, eval_test_f1_10))
            
            if (args.mask_prob > 0 or args.jitter_prob > 0) and args.max_length < 0:
                eval_test_loader.dataset.mask_prob = args.mask_prob  
                eval_test_loader.dataset.jitter_prob = args.jitter_prob       
                eval_test_loss, eval_test_acc, eval_test_avg_f1, eval_test_edit, eval_test_f1_10 \
                    = test_complete_sequences(eval_test_loader, model, criterion, padding_size, args.attn_type,
                                              num_class, device_gpu, device_cpu)
                
                writer.add_scalars('Metrics_frame/test_mskd', {'acc': eval_test_acc,
                                                               'avg_f1': eval_test_avg_f1}, epoch)
                writer.add_scalars('Metrics_segm/test_mskd', {'edit': eval_test_edit,
                                                              'f1_10': eval_test_f1_10}, epoch)                

            # ===== save checkpoint =====
            current_state = {'epoch': epoch + 1,
                             'model_weights': model.state_dict(),
                             'optimizer': optimizer.state_dict(),
                             'rng': torch.get_rng_state(),
                             'args': args_dict
                             }
            torch.save(current_state, checkpoint_file)

        scheduler.step()

        if (epoch + 1) % args.save_freq == 0 or epoch == args.steps - 1:
            # ===== save model =====
            model_file = os.path.join(output_folder, "model_" + str(epoch) + ".pth")
            torch.save(model.state_dict(), model_file)
            log("Saved model to " + model_file)

    f_log.close()

    writer.flush()
    writer.close()


def test_complete_sequences(data_loader, model, criterion, padding_size, attn_type, num_class, device_gpu, device_cpu):
    eval_loss = AverageMeter()
    eval_acc = AverageMeter()
    eval_avg_f1 = AverageMeter()
    eval_edit = AverageMeter()
    eval_f1_10 = AverageMeter()

    model.eval()
    with torch.no_grad():
        for _, batch in enumerate(data_loader):
            seq, target, pos_enc, key_padding, attn_mask = batch
            batch_size = seq.shape[0]
            num_frames = (seq.shape[0] * seq.shape[1]) - key_padding.sum()
            out_mask = np.logical_not(key_padding.numpy())
            labels = target.numpy()

            seq = seq.to(device_gpu)
            target[key_padding] = LABEL_IGN_IDX
            target = target.to(device_gpu)
            # pos_enc = pos_enc.to(device_gpu)
            key_padding = key_padding.to(device_gpu)
            mask = torch.logical_not(key_padding).unsqueeze(1)

            """ 
            if attn_mask.ndim == 1 and attn_mask[0].item() == NONE_VAL:
                attn_mask = None  # hack ;P
            else:            
                if attn_mask.ndim == 3:
                    attn_mask = attn_mask[0, :, :]
                # print(j, "attn_mask.shape", attn_mask.shape)
                attn_mask = attn_mask.to(device_gpu)
            """

            seq = torch.transpose(seq, 1, 2)  # N x S x F --> N x F x S
            output = model(seq, mask)  # out: (N * nstages) x num_class x S

            ms_output = output.view(args.nstages, batch_size, output.shape[-2], output.shape[-1])

            loss = 0
            for s in range(args.nstages):
                p = ms_output[s]
                loss += criterion(p, target)

                # truncated MSE loss
                loss += 0.15 * \
                        torch.mean(
                            torch.clamp(
                                torch.nn.MSELoss(reduction='none')(F.log_softmax(p[:, :, 1:], dim=1),
                                                                   F.log_softmax(p.detach()[:, :, :-1], dim=1)),
                                min=0, max=16) * mask[:, 0:1, 1:]
                        )
            eval_loss.update(loss.item(), num_frames)

            final_preds = torch.transpose(ms_output[-1], 1, 2)
            predicted = torch.nn.Softmax(dim=2)(final_preds)
            _, predicted = torch.max(predicted, dim=2, keepdim=False)
            predicted = predicted.to(device_cpu).numpy()

            for i in range(batch_size):
                P = predicted[i, :]
                Y = labels[i, :]

                # remove key padding
                P = P[out_mask[i, :]]
                Y = Y[out_mask[i, :]]

                # remove regular padding
                seq_length = P.shape[0]
                if attn_type == "backward":
                    P = P[padding_size:]
                    Y = Y[padding_size:]
                else:
                    P = P[padding_size: seq_length - padding_size]
                    Y = Y[padding_size: seq_length - padding_size]

                acc = accuracy(P, Y)
                avg_f1, _ = average_F1(P, Y, n_classes=num_class)
                edit = edit_score(P, Y)
                f1_10 = overlap_f1(P, Y, n_classes=num_class, overlap=0.1)

                eval_acc.update(acc)
                eval_avg_f1.update(avg_f1)
                eval_edit.update(edit)
                eval_f1_10.update(f1_10)

    return eval_loss.avg, eval_acc.avg, eval_avg_f1.avg, eval_edit.avg, eval_f1_10.avg


def test_split_sequences(data_loaders, model, criterion, padding_size, attn_type, num_class, device_gpu, device_cpu):
    eval_loss = AverageMeter()
    eval_acc = AverageMeter()
    eval_avg_f1 = AverageMeter()
    eval_edit = AverageMeter()
    eval_f1_10 = AverageMeter()

    for data_loader in data_loaders:
        P = np.array([], dtype=np.int64)
        Y = np.array([], dtype=np.int64)

        # print(data_loader.dataset.video_id)
        video_length = data_loader.dataset.no_frames
        final_padding = data_loader.dataset.last_part_key_padding

        size = data_loader.dataset.size
        step = data_loader.dataset.step
        offset = size - step  # step <= size
        if attn_type == "bidirectional" and (offset > 0):
            assert(offset % 2 == 0)
            offset = int(offset // 2)
        frame_count = 0

        # collect predictions for this video
        model.eval()
        with torch.no_grad():
            for b_no, batch in enumerate(data_loader):
                is_last_batch = (b_no == (len(data_loader) - 1))
                seq, target, pos_enc, attn_mask = batch
                num_frames = (seq.shape[0] * seq.shape[1]) - (final_padding if is_last_batch else 0)
                labels = target.numpy()

                seq = seq.to(device_gpu)
                target = target.to(device_gpu)
                pos_enc = pos_enc.to(device_gpu)
                # attn_mask = attn_mask.to(device_gpu)
                if attn_mask.ndim == 1 and attn_mask[0].item() == NONE_VAL:
                    attn_mask = None  # hack ;P
                else:            
                    if attn_mask.ndim == 3:
                        attn_mask = attn_mask[0, :, :]
                    attn_mask = attn_mask.to(device_gpu)
                    
                if is_last_batch and final_padding > 0:
                    batch_size = seq.shape[0]
                    assert(seq.shape[1] == size)
                    key_padding = torch.zeros((batch_size, size), dtype=torch.bool)
                    key_padding[-1, :] = torch.tensor(([False for _ in range(size - final_padding)]
                                                       + [True for _ in range(final_padding)]), dtype=torch.bool)
                    key_padding = key_padding.to(device_gpu)
                else:
                    key_padding = None

                seq = torch.transpose(seq, 0, 1)  # N x S x F --> S x N x F
                pos_enc = torch.transpose(pos_enc, 0, 1)
                output = model(seq, pos_enc, attn_mask, key_padding)
                output = torch.transpose(output, 0, 1)  # S x N x num_class --> N x S x num_class

                loss = criterion(torch.transpose(output, 1, 2), target)
                eval_loss.update(loss.item(), num_frames)

                predicted = torch.nn.Softmax(dim=2)(output)
                _, predicted = torch.max(predicted, dim=2, keepdim=False)
                predictions = predicted.to(device_cpu).numpy()

                for i in range(predictions.shape[0]):
                    is_last_part = (is_last_batch and (i == (predictions.shape[0] - 1)))
                    part = predictions[i, :]
                    part_labels = labels[i, :]
                    if frame_count == 0:
                        # first part
                        future_offset = offset if (attn_type == "bidirectional" and not is_last_part) else 0
                        P = np.append(P, part[0: size - future_offset])
                        Y = np.append(Y, part_labels[0: size - future_offset])
                        frame_count = size - future_offset
                    else:
                        if is_last_part:
                            P = np.append(P, part[offset:])
                            Y = np.append(Y, part_labels[offset:])
                            frame_count += size - offset
                        else:
                            P = np.append(P, part[offset: offset + step])
                            Y = np.append(Y, part_labels[offset: offset + step])
                            frame_count += step

        assert (P.shape[0] == frame_count and Y.shape[0] == frame_count)

        if final_padding > 0:
            P = P[0: frame_count - final_padding]
            Y = Y[0: frame_count - final_padding]

        # remove padding
        seq_length = P.shape[0]
        if attn_type == "backward":
            P = P[padding_size:]
            Y = Y[padding_size:]
        else:
            P = P[padding_size: seq_length - padding_size]
            Y = Y[padding_size: seq_length - padding_size]

        assert (P.shape[0] == video_length)

        acc = accuracy(P, Y)
        avg_f1, _ = average_F1(P, Y, n_classes=num_class)
        edit = edit_score(P, Y)
        f1_10 = overlap_f1(P, Y, n_classes=num_class, overlap=0.1)
        
        eval_acc.update(acc)
        eval_avg_f1.update(avg_f1)
        eval_edit.update(edit)
        eval_f1_10.update(f1_10)

    return eval_loss.avg, eval_acc.avg, eval_avg_f1.avg, eval_edit.avg, eval_f1_10.avg


if __name__ == '__main__':
    args = parser.parse_args()

    main(args)


