import sys
sys.path.append('../')
from dataset import MyDataSet
from util import Cholec80
import numpy as np

import torch

def main():
    feature_file = "/local_home/funkeisab/cholec80/features/Cholec80_NCT.pth.tar"

    num_class = Cholec80.num_phases 

    for mode in ['train', 'val', 'test']:
        print("***** ***** {} ***** *****".format(mode))

        dataset = MyDataSet(feature_file, mode, 750,
                            padded_length=1000, video_fps=25, video_sampling_step=25, )

        print(len(dataset.label_seqs.keys()))
        weights = dataset.calculate_class_weights(num_class)
        print(weights)

        print("max_len", dataset.get_longest_sequence())


def calc_Cholec80_class_weights(feature_file):

    train_videos = ["video{:02d}".format(i) for i in range(1, 41)]
    val_videos = ["video{:02d}".format(i) for i in range(41, 49)]
    test_videos = ["video{:02d}".format(i) for i in range(49, 81)]

    print("train", train_videos)
    print("val", val_videos)
    print("test", test_videos)

    data = torch.load(feature_file)
    num_class = 7

    for videos in [train_videos, val_videos, test_videos]:
        counts = np.zeros(num_class, dtype=np.float)
        total_frames = 0
        for video_id in videos:
            print(video_id)
            label_seq = data['labels'][video_id]
            label_seq = label_seq.numpy()
            for i in range(label_seq.shape[0]):
                label = label_seq[i]
                assert (label < num_class)
                counts[label] += 1
                total_frames += 1

        freqs = counts / total_frames
        median_freq = np.median(freqs)

        missing_labels = []
        for label in range(freqs.shape[0]):
            if freqs[label] == 0:
                print("No frames with label {} found!".format(label))
                freqs[label] = 1
                missing_labels.append(label)

        weights = np.ones(num_class, dtype=np.float) * median_freq
        weights = weights / freqs
        for missing in missing_labels:
            weights[missing] = 0

        weights = (weights / np.sum(weights)) * num_class

        print(weights)


if __name__ == '__main__':
    feature_file = "/local_home/funkeisab/cholec80/features/Cholec80_NCT.pth.tar"
    calc_Cholec80_class_weights(feature_file)

