import torch

def _calculate_attention_mask(size, is_causal=True, local_window_size=-1, local_stride=1, global_stride=-1,
                              global_attn_type="grid"):
    if local_window_size > 0:  # use (strided) local + global attention pattern
        mask = torch.eye(size, dtype=torch.bool)
        for i in range(size):
            k = local_stride
            while k <= local_window_size:
                if i + k < size:
                    mask[i, i + k] = True
                if i - k >= 0:
                    mask[i, i - k] = True
                k += local_stride
        if global_stride > 0:
            assert (global_attn_type in ["grid", "custom_linear", "custom_double"])
            if global_attn_type == "grid":
                for i in range(int(global_stride / 2), size, global_stride):
                    mask[i, :] = True
                    mask[:, i] = True
            else:
                for i in range(size):
                    k = global_stride
                    while k < size:
                        if i + k < size:
                            mask[i, i + k] = True
                        if i - k >= 0:
                            mask[i, i - k] = True
                        if global_attn_type == "custom_linear":
                            k += global_stride
                        elif global_attn_type == "custom_double":
                            k *= 2
    else:
        mask = torch.ones(size, size, dtype=torch.bool)

    if is_causal:
        causal_mask = torch.triu(torch.ones(size, size)).transpose(0, 1)
        causal_mask = (causal_mask == 1)  # to bool
        mask = mask * causal_mask

    return mask

def test():
    size = 10

    for is_causal in [True, False]:
        mask = _calculate_attention_mask(size*2, is_causal=is_causal, local_window_size=2, local_stride=2, global_stride=2,
                                         global_attn_type="custom_double")

        print(mask.long())

if __name__ == '__main__':
    test()