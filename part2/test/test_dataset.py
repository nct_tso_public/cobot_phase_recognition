import sys
sys.path.append('../')
from dataset import MyDataSet
from util import gestures_SU, JIGSAWS_FPS

import torch
import torch.utils.data as data

def main():
    """
    feature_file = "/media/data/funkeisab/JIGSAWS/gesture-recognition/Eval/LOUO/10Hz/Raw_Features/SU_baseline_2D_20190328/249.pth.tar"
    video_lists_dir = "/media/data/funkeisab/JIGSAWS/Suturing/video_lists"
    transcriptions_dir = "/media/data/funkeisab/JIGSAWS/Suturing/transcriptions"
    """

    feature_file = "../features/Colin_visual_4.pth.tar"

    num_class = len(gestures_SU)
    eval_scheme = "LOUO"
    split = 4
    data_split = torch.load(feature_file)['split']
    assert(data_split == "{}-{}".format(eval_scheme, split))

    for mode in ['train', 'test']:
        print("***** ***** {} ***** *****".format(mode))

        dataset = MyDataSet(feature_file, mode, 750,
                            padded_length=1000, video_fps=JIGSAWS_FPS, video_sampling_step=3,
                            noise_factor=0.0,  # sigma parameter of Gaussian noise distribution
                            max_duplicate_frames_prob=0.1, duplicated_duration=2,
                            max_drop_frames_prob=0.5, dropped_duration=2,
                            max_randomly_replace_prob=0.5, randomly_replaced_duration=2,
                            max_mask_prob=0.5, masked_duration=2, )

        print(len(dataset.label_seqs.keys()))
        weights = dataset.calculate_class_weights(num_class)
        print(weights)

        print("max_len", dataset.get_longest_sequence())

        dataloader = data.DataLoader(dataset, batch_size=1, shuffle=False, num_workers=1)

        longest = 0
        shortest = float("inf")
        for item in dataloader:
            features, labels, position_ids, key_padding_mask = item

            print(features.size())
            print(labels.size())
            print(position_ids.size())
            print(key_padding_mask.size())

            # dim: batch x seq_length x feature_dim
            if features.shape[1] > longest:
                longest = features.shape[1]
            if features.shape[1] < shortest:
                shortest = features.shape[1]

        print("Shortest sequence: ", shortest)
        print("Longest sequence: ", longest)


if __name__ == '__main__':
    main()
