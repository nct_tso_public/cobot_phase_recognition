import torch
import torch.nn as nn
import torch.nn.functional as F


def truncated_MSE_loss(x, key_padding_mask, clamp_max=4, reduction='mean'):
    # ***** from https://github.com/yabufarha/ms-tcn *****

    mse = torch.nn.MSELoss(reduction='none')
    x = x.permute(1, 2, 0)  # S x N x F --> N x F x S
    loss = torch.clamp(
        mse(F.log_softmax(x[:, :, 1:], dim=1), F.log_softmax(x.detach()[:, :, :-1], dim=1)),
        min=0, max=clamp_max ** 2)
    loss = torch.mean(loss, dim=1, keepdim=True)
    loss = loss * torch.logical_not(key_padding_mask)[:, 1:].unsqueeze(1)
    # print("tmse_shape", loss.shape)
    # print("min {:.3f}, max {:.3f}, mean {:.3f}".format(torch.min(loss), torch.max(loss), torch.mean(loss)))
    loss = loss.permute(2, 0, 1)  # N x F x S --> S x N x F

    if reduction == 'none':
        return loss
    else:
        sum = torch.sum(loss)
        if reduction == 'sum':
            return sum
        else:
            assert (reduction == 'mean')
            num_elems = key_padding_mask.shape[0] * (key_padding_mask.shape[1] - 1) - key_padding_mask.sum()
            # print("tmse_fn", sum / num_elems)
            return sum / num_elems

def norm_att(att,y):
    assert(att.size()[0] == 1)
    att = torch.squeeze(att) # remove batch h x s x s
    att = torch.mean(att,0) # mean over head
    att = torch.mean(att,1) # add att over last dim
    # regularize with postitions
    uniq, occ = torch.unique(y,return_counts= True)
    for count,i in enumerate(uniq):
        y[y == i] = int(occ[count])
    y = y.type(torch.cuda.FloatTensor)
    att = torch.div(att,y) #devide by number of unique positions
    return att

def reg_loss(att, pred_feature,y, key_padding_mask):
    ce = nn.CrossEntropyLoss(reduction='none')
    ce_v = torch.squeeze(ce(pred_feature,y))
    return torch.dot(norm_att(att,torch.squeeze(y.clone().detach())),ce_v)


# Copyright 2020-present the HuggingFace Inc. team.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

class LabelSmoothingLoss(nn.Module):
    """
    Adds label-smoothing on a pre-computed output from a Transformers model.
    Args:
        epsilon (:obj:`float`, `optional`, defaults to 0.1):
            The label smoothing factor.
        ignore_index (:obj:`int`, `optional`, defaults to -100):
            The index in the labels to ignore when computing the loss.
    """
    def __init__(self, epsilon=0.1, weight=None, ignore_index=-100):
        super(LabelSmoothingLoss, self).__init__()
        self.epsilon = epsilon
        self.ignore_index = ignore_index
        self.weight = weight

    def to(self, device):
        self.weight = self.weight.to(device)

    def forward(self, logits, labels):
        # logits: S x N x F
        # labels: N x S

        logits = logits.transpose(0, 1)  # S x N x F --> N x S x F
        log_probs = torch.nn.functional.log_softmax(logits, dim=-1)

        # input: N x F x S, labels: N x S
        nll_loss = F.nll_loss(log_probs.transpose(1, 2), labels,
                              weight=self.weight, ignore_index=self.ignore_index, reduction='mean')

        smoothed_loss = (-1 * log_probs).sum(dim=-1, keepdim=True)
        padding_mask = labels.eq(self.ignore_index)
        smoothed_loss.masked_fill_(padding_mask.unsqueeze(-1), 0.0)
        num_active_elements = padding_mask.numel() - padding_mask.sum()
        smoothed_loss = smoothed_loss.sum() / (num_active_elements * log_probs.shape[-1])

        return (1 - self.epsilon) * nll_loss + self.epsilon * smoothed_loss


