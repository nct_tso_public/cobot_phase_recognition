cd part1

## Steps

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 trainCNN_CoBot_cross.py --fold $i --exp "$EXP" --epochs 30 --annotation "step"
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 trainCNN_CoBot_cross.py --fold $i --exp "$EXP" --epochs 30 --annotation "step" --pretrained 1
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 trainCNN_CoBot_cross.py --fold $i --exp "$EXP" --epochs 30 --annotation "step" --pretrained 3
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 trainCNN_CoBot_cross.py --fold $i --exp "$EXP" --epochs 30 --annotation "step" --pretrained 4
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 trainCNN_CoBot_cross.py --fold $i --exp "$EXP" --epochs 30 --annotation "step" --pretrained 5
done


## Phases

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 trainCNN_CoBot_cross.py --fold $i --exp "$EXP" --epochs 30 
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 trainCNN_CoBot_cross.py --fold $i --exp "$EXP" --epochs 30 --pretrained 1
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 trainCNN_CoBot_cross.py --fold $i --exp "$EXP" --epochs 30 --pretrained 3
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 trainCNN_CoBot_cross.py --fold $i --exp "$EXP" --epochs 30 --pretrained 4
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 trainCNN_CoBot_cross.py --fold $i --exp "$EXP" --epochs 30 --pretrained 5
done