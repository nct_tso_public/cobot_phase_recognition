cd part1


# Cholec80 and rest CoBot
EXP=$(date +"%Y%m%d-%H%M")
for i in "1 42" "2 123" "3 512" "4 222"
do
	set -- $i
	python3 train_CNNLSTM_Cholec80.py --exp "$EXP" --run $1 --seed $2 --hidden_size 160 --lr 0.000107 --epochs 50 --num_layers 1 --output "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCholec80/best_optuna_results"
done

exit
'''
for j in 512 256 128 64
do
	EXP=$(date +"%Y%m%d-%H%M")
	for i in "1 42" "2 123" "3 512" "4 222"
	do
		set -- $i
		python3 train_CNNLSTM_Cholec80.py --exp "$EXP" --run $1 --seed $2 --hidden_size $j
	done
done

for j in 256 128 64 32
do
	EXP=$(date +"%Y%m%d-%H%M")
	for i in "1 42" "2 123" "3 512" "4 222"
	do
		set -- $i
		python3 train_CNNLSTM_Cholec80.py --exp "$EXP" --run $1 --seed $2 --hidden_size $j --num_layers 2
	done
done

for j in 512 256 128 64
do
	EXP=$(date +"%Y%m%d-%H%M")
	for i in "1 42" "2 123" "3 512" "4 222"
	do
		set -- $i
		python3 train_CNNLSTM_Cholec80.py --exp "$EXP" --run $1 --seed $2 --hidden_size $j --bidirectional True
	done
done

for j in 256 128 64 32
do
	EXP=$(date +"%Y%m%d-%H%M")
	for i in "1 42" "2 123" "3 512" "4 222"
	do
		set -- $i
		python3 train_CNNLSTM_Cholec80.py --exp "$EXP" --run $1 --seed $2 --hidden_size $j --num_layers 2  --bidirectional True
	done
done
'''

cd part2
'''

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" --annotation "step" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 128 --num_layers 2
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" --annotation "step" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 64 --num_layers 2
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" --annotation "step" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 32 --num_layers 2
done



EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 64 --num_layers 2
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 32 --num_layers 2
done'''