import torch
import torch.nn as nn
import torch.optim as optim
import os.path
import datetime
import argparse
import torchvision
import sys

from datasets import PhaseData, PhaseDataList
from models import CNNmodel
import utils
from utils import Cholec80, DataPrep3, AverageMeter, DataPrep
from torch.optim import lr_scheduler 

import pickle
file = "/mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/cholec80/out/Phase_segmentation_resnet50/no_pretrain/40/20210203-1215/model.pkl"#"modelCholec80Resnet502021128-1348.pkl"
here = os.path.dirname(os.path.abspath(file))
sys.path.append(here)
ResnetModel = CNNmodel(num_class = 7, cnn = "resnet50", pretrainedFile = file)
#ResnetModel.load("mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/cholec80/out/Phase_segmentation_resnet50/no_pretrain/40/20210128-1348/model.pkl")
test_data = []
feature_dict = {}
for op_set in Cholec80.op_sets:
        for op in os.listdir(os.path.join(utils.frames_path_Cholec80, op_set)):
            op_path = os.path.join(utils.frames_path_Cholec80, op_set, op)
            if os.path.isdir(op_path):
                anno_file = os.path.join(utils.annotation_path_Cholec80, "video" + op + "-phase.txt")
                print(anno_file)
                dataset = PhaseData(op_path, anno_file, DataPrep.width, DataPrep.height, DataPrep.standard_transform)
                test_data.append(torch.utils.data.DataLoader(dataset, batch_size=1, shuffle=False,
                                                             num_workers=0))
with torch.no_grad():
    for v in test_data:
        i = 0
        print(v.dataset.frames_path)
        new_list = []
        for loader in v:
            data,target_cpu = loader
            output = ResnetModel.feature(x = data, mode = "resnet")
            output = torch.squeeze(output)
            new_list.append(output)
            i+= 1
        feature_dict[v.dataset.frames_path] = new_list

torch.save(feature_dict, "features_cholec80.pkl")

a = torch.load("features_cholec80.pkl")
print(a)