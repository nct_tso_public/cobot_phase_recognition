"""
train CNN-LSTM with CoBot dataset 
"""

import argparse
import datetime
import os.path

import torch
import torch.nn as nn
import torch.optim as optim
import torchvision

import utils
from datasets import PhaseData_CoBot_
from models import PhaseNet
from utils import CoBot, DataPrep3
import cv2
import albumentations as A
from albumentations.pytorch import ToTensorV2
import imgaug
import numpy as np


# Cholec80 Vortraining: /home/krellstef/master_code/outCholec80/Phase_segmentation/no_pretrain/40/20201031-1724/model.pkl
def get_split(fold):
    split = None
    if fold == 1:
        split = {
        'train': [9, 10, 12, 13, 14, 15, 16, 18, 19, 20, 21, 22, 23, 24, 25, 27, 28, 29, 30, 31, 32],
        'val': [8,17,26,11],
        'test': list(range(0,8))
    }
    elif fold == 2:
        split = {
        'train': [0, 1, 3, 4, 5, 6, 7, 17, 18, 20, 21, 22, 23, 24, 26, 27, 28, 29, 30, 31, 32],
        'val': [16,25,2,19],
        'test': list(range(8,16))
    }
    elif fold == 3:
        split = {
        'train': [0, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 25, 26, 28, 29, 30, 31, 32],
        'val': [24,1,10,27],
        'test': list(range(16,24))
    }
    elif fold == 4:
        split = {
        'train':[1, 2, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20, 21, 22, 23],
        'val': [0,9,18,3],
        'test': list(range(24,33))
    }
    else:
        raise RuntimeError("Not a fold!")
    return split
## model mit endovVis trainiert
## /mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/EndoVis18/out/Phase_segmentation_resnet50/no_pretrain/20210225-1242/model.pkl
VIDEOS = ['20190917','20191001','20191011','20191211','20200122','20200129','20200221','20200317',
            '20200330','20200407','20200416','20200423','20200427','20200710','20200717','20200727','20201002','20201006', '20201201','20201204',
            '20201208','20190214','20200611','20200123','20200302','20200504','20200622','20201017', '20200731','20200827','20191206', '20210113', '20200212']

def main(args):
    transform_train = A.Compose([
        A.RandomBrightnessContrast(brightness_limit=0.1, contrast_limit=0.1, brightness_by_max=True, p=1.0),
        A.OneOf([A.MotionBlur(blur_limit=5, p=0.2),
                 A.GaussNoise(var_limit=(5.0, 75.0), mean=0, p=0.8)], p=0.5),
        A.ShiftScaleRotate(shift_limit=0.1, scale_limit=0.15, rotate_limit=15,
                           interpolation=cv2.INTER_CUBIC, border_mode=cv2.BORDER_CONSTANT, value=0, p=0.8),
        A.ElasticTransform(alpha=1, sigma=50, alpha_affine=7, approximate=True,
                           interpolation=cv2.INTER_CUBIC, border_mode=cv2.BORDER_CONSTANT, value=0, p=0.2),

        A.Resize(height=256, width=320, interpolation=cv2.INTER_CUBIC, always_apply=True, p=1.0),
        A.RandomCrop(224, 224, always_apply=True, p=1.0),
        A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225), max_pixel_value=255.0,
                    always_apply=True, p=1.0),
        ToTensorV2(transpose_mask=True, always_apply=True, p=1.0),
    ], p=0.5)

    transform_test = A.Compose([
        A.Resize(height=256, width=320, interpolation=cv2.INTER_CUBIC, always_apply=True, p=1.0),
        A.CenterCrop(224, 224, always_apply=True, p=1.0),
        A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225), max_pixel_value=255.0,
                    always_apply=True, p=1.0),
        ToTensorV2(transpose_mask=True, always_apply=True, p=1.0),
    ], p=1.0)

    device_gpu = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    device_cpu = torch.device("cpu")

    pretrained_model_file = None
    pretrain_method = "no_pretrain"
    
    if args.model is not None:
        if args.pretrainedCNNLSTM == False:
            pretrained_model_file = args.model
            #if not os.path.exists(pretrained_model_file):
            #    pretrained_model_file = os.path.join(utils.out_path, "Self-supervised", pretrained_model_file, "model.pth")
            pretrain_method = "pretrainedResNet"#os.path.split(os.path.split(os.path.split(pretrained_model_file)[0])[0])[1]
        else:
            pretrain_method = "pretrainedCNNLSTM"

    trial_id = datetime.datetime.now().strftime("%Y%m%d-%H%M")
    if args.exp is not None:
        trial_id = args.exp
    experiment = os.path.join("Phase_segmentation", pretrain_method , trial_id, str(args.fold))

    output_folder = os.path.join(utils.out_path, experiment)

    anno_path = utils.annotation_path
    if args.annotation == "phase":
        num_class = 5        
    elif args.annotation == "step":
        num_class = 5 + 4
        output_folder = os.path.join("./outCoBot_steps", experiment)
        step_file = []
    else:
        raise NotImplementedError("not available as annotation")

    num_classes_pre = None
    if pretrain_method == "pretrainedResNet" and args.pretraining_mode != "self":
        num_classes_pre = num_class
        print("load supervised ResNet")


    os.makedirs(output_folder)
    f_log = open(os.path.join(output_folder, "log.txt"), "w")
    def log(msg):
        utils.log(f_log, msg)
    model_file = os.path.join(output_folder, "model.pkl")
    model_file_final = os.path.join(output_folder, "model_final.pkl")
    model_file_train = os.path.join(output_folder, "model_best_train.pkl")

    log("Run <%s> on device %s" % (experiment, str(device_gpu)))
    if pretrained_model_file is not None:
        log("Using pretrained FeatureNet: " + pretrained_model_file)
    log("Used parameters...")
    for arg in vars(args):
        log("\t" + str(arg) + " : " + str(getattr(args, arg)))

    log("Loading data...")

    split = get_split(args.fold)
    train_data = []

    for id in split['train']:
        op = VIDEOS[id]
        op_path = os.path.join(utils.frames_path, op)
        if os.path.isdir(op_path):
            step_file = None
            if args.annotation == "step":
                step_file = os.path.join(utils.annotation_path_step, op + "step.txt")
            anno_file = os.path.join(anno_path, op + ".txt")
            #dataset = PhaseData_CoBot_old(op_path, anno_file, DataPrep3.width, DataPrep3.height, transform_test,sample_rate = args.sample_rate)
            dataset = PhaseData_CoBot_(op_path, anno_file, DataPrep3.width, DataPrep3.height, transform_train,annotation = args.annotation, step_file = step_file, sample_rate = args.sample_rate)
            train_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False,
                                                             num_workers=4))
    log("\t training data")
    for loader in train_data:
        log("\t\t" + loader.dataset.frames_path)

    val_data = []
    for id in split['val']:
        op = VIDEOS[id]
        op_path = os.path.join(utils.frames_path, op)
        if os.path.isdir(op_path):
            step_file = None
            if args.annotation == "step":
                step_file = os.path.join(utils.annotation_path_step, op + "step.txt")
            anno_file = os.path.join(anno_path, op + ".txt")
            #dataset = PhaseData_CoBot_old(op_path, anno_file, DataPrep3.width, DataPrep3.height, transform_test,sample_rate = args.sample_rate)
            dataset = PhaseData_CoBot_(op_path, anno_file, DataPrep3.width, DataPrep3.height, transform_test,annotation = args.annotation, step_file = step_file, sample_rate = args.sample_rate)
            val_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False,
                                                             num_workers=4))
                
    log("\t val data")
    for loader in val_data:
        log("\t\t" + loader.dataset.frames_path)

    test_data = []
    for id in split['test']:
        op = VIDEOS[id]
        op_path = os.path.join(utils.frames_path, op)
        if os.path.isdir(op_path):
            step_file = None
            if args.annotation == "step":
                step_file = os.path.join(utils.annotation_path_step, op + "step.txt")
            anno_file = os.path.join(anno_path, op + ".txt")
            #dataset = PhaseData_CoBot_old(op_path, anno_file, DataPrep3.width, DataPrep3.height, transform_test,sample_rate = args.sample_rate)
            dataset = PhaseData_CoBot_(op_path, anno_file, DataPrep3.width, DataPrep3.height, transform_test,annotation = args.annotation, step_file = step_file, sample_rate = args.sample_rate)
            test_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False,
                                                             num_workers=4))
                
    log("\t test data")
    for loader in test_data:
        log("\t\t" + loader.dataset.frames_path)

    log("Create PhaseNet")
    if args.cnn == "alexnet":
        log("Alexnet")
        net = PhaseNet(pretrained_model_file, cnn = "alexnet",num_classes = num_class)
    else:
        log("Resnet")
        net = PhaseNet(pretrained_model_file,num_classes = num_class, num_classes_pre = num_classes_pre)
        for param in net.featureNet.resnet.parameters():
            param.requires_grad = False
        for param in net.featureNet.resnet.layer4.parameters():
            param.requires_grad = True

    if args.model is not None and args.pretrainedCNNLSTM == True:
        net.load(args.model,7,num_class)

    net.to(device_gpu)
    if args.w == 1:
        we = utils.class_weights_steps if args.annotation == "step" else utils.class_weights_phases
        print(we)
        we = torch.from_numpy(we).float()
        we = we.to(device_gpu)
        criterion = nn.CrossEntropyLoss(weight=we)
    else:
        criterion = nn.CrossEntropyLoss(size_average=False)#, weight=we)
    optimizer = optim.Adam(filter(lambda p: p.requires_grad, net.parameters()), lr=args.lr)
    torch.backends.cudnn.benchmark = True
    torch.backends.cudnn.enabled = True
    log("Begin training...")

    best_acc = 0
    one = True
    for epoch in range(args.epochs):
        log("Epoch " + str(epoch + 1) + "...")

        train_loss = 0
        train_accuracy = 0
        train_count = 0
        for op_data in train_data:
            hidden_state = net.init_hidden(device_gpu)
            optimizer.zero_grad()
            optimized = False
            loss = 0
            batch_count = 0
            if epoch < 2: 
                print(op_data.dataset.frames_path)
            for loader in op_data:
                images, labels = loader
                images = images.to(device_gpu)
                labels = labels.to(device_gpu)
                outputs, hidden_state = net(images, hidden_state)
                loss += criterion(outputs, labels)
                batch_count += 1
                if batch_count % args.opt_step == 0:
                    loss.backward()
                    optimizer.step()
                    optimized = True
                    optimizer.zero_grad()
                    h, c = hidden_state
                    h = h.detach()
                    c = c.detach()
                    hidden_state = (h, c)
                else:
                    loss.backward(retain_graph=True)
                    optimized = False

                train_loss += loss.item()
                _, predicted = torch.max(outputs.data, 1)
                train_accuracy += (predicted == labels).sum().item()
                train_count += labels.size(0)

                if batch_count % args.opt_step == 0:
                    loss = 0

            if not optimized:
                optimizer.step()

        
        val_loss = 0
        val_accuracy = 0
        val_count = 0
        with torch.no_grad():
            for op_data in val_data:
                hidden_state = net.init_hidden(device_gpu)
                for loader in op_data:
                    images, labels_cpu = loader
                    images = images.to(device_gpu)
                    labels = labels_cpu.to(device_gpu)
                    outputs, hidden_state = net(images, hidden_state)
                    loss = criterion(outputs, labels)

                    val_loss += loss.item()
                    _, predicted = torch.max(outputs.data, 1)
                    val_count += labels.size(0)
                    val_accuracy += (predicted == labels).sum().item()

        summary = "train (loss %.3f, accuracy %.3f) val (loss %.3f, accuracy %.3f)" % \
                  (train_loss/train_count, train_accuracy/train_count, val_loss/val_count, val_accuracy/val_count)
        log("\t" + summary)

        if (epoch + 1) % args.save_freq == 0:
            log("\tSave model to %s..." % model_file_final)
            net.save(model_file_final)
        act_acc = val_accuracy/val_count
        if best_acc < act_acc:
            best_acc = act_acc
            log("\tSave best val model to %s..." % model_file)
            net.save(model_file)

        if one and train_accuracy/train_count > 0.999:
            net.save(model_file_train)
            one = False


    log("Done. Save final model to %s..." % model_file_final)
    net.save(model_file_final)
    
    log("Update predictions with best val model")
    net.load(model_file)

    predictions_path = os.path.join(output_folder, "predictions")
    if not os.path.exists(predictions_path):
        os.makedirs(predictions_path)
    test_accuracy = 0
    test_count = 0
    with torch.no_grad():
        for op_data in test_data:
            frames_path = op_data.dataset.frames_path
            op_id = frames_path.split('/')[-1] if frames_path.split('/')[-1] else frames_path.split('/')[-2]
            f_out = open(os.path.join(predictions_path, op_id + ".txt"), "w")

            hidden_state = net.init_hidden(device_gpu)
            for loader in op_data:
                images, labels_cpu = loader
                images = images.to(device_gpu)
                labels = labels_cpu.to(device_gpu)
                outputs, hidden_state = net(images, hidden_state)
                _, predicted = torch.max(outputs.data, 1)
                predicted_cpu = predicted.to(device_cpu)
                for p, l in zip(predicted_cpu.numpy(), labels_cpu.numpy()):
                    f_out.write(str(p) + "," + str(l) + "\n")
                test_count += labels.size(0)
                test_accuracy += (predicted == labels).sum().item()
            f_out.close()
    log('best Model: Test Accuracy : %.3f ' % (test_accuracy/test_count))
    f_log.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Finetune a CNN-LSTM for surgical phase recognition.")
    parser.add_argument("--num_ops", type=int, default = 25,
                        help="Number of labeled OP videos to use for training.")
    parser.add_argument("-m", "--model", type=str, help="Pretrained FeatureNet to use.")
    parser.add_argument("--pretraining_mode", type=str, default = "self", help="supervised or self-supervised")
    parser.add_argument("--cnn", type=str, choices=["resnet","alexnet"], default = "resnet",help="cnn to use resnet or alexnet")
    parser.add_argument("--lr", type=float, default=0.00001, help="The learning rate.")
    parser.add_argument("--batch_size", type=int, default=128, help="The batch size.") #128
    parser.add_argument("--opt_step", type=int, default=3,
                        help="Number of batches to accumulate before applying the optimizer.")
    parser.add_argument("--epochs", type=int, default=200, help="The maximal number of epochs to train.")
    parser.add_argument("--save_freq", type=int, default=10,
                        help="Defines after how many epochs the current model parameters are saved.")
    parser.add_argument("--sample_rate", type=int, default=1,
                        help="Defines in which rate the pictures are taken 1-> every pic 2-> every 2. pic.")
    parser.add_argument("--annotation", type=str, default="phase" , choices=["phase","step"],
                        help="phases or steps annotation")
    parser.add_argument("--exp", type=str, default=None, help=".")
    parser.add_argument("--pretrainedCNNLSTM", type=bool, default=False , help="if model is defined use model as LSTM")
    parser.add_argument("--fold", type=int, default=1, choices = [1,2,3,4], help=".")
    parser.add_argument("--w", type=int, default=0, choices = [0,1], help=".")
    args = parser.parse_args()
    main(args)
