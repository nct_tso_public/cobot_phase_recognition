 
""" 
training Feedback transformer
"""

import argparse
import datetime
import os.path

import torch
import torch.nn as nn
import torch.optim as optim
import torchvision
from torch.optim import lr_scheduler

import utils
from datasets_features import PhaseData_Features
from models_feedback_transformer import FeedbackTransformerImageFeatures
from utils import AverageMeter, Cholec80
import torch.nn.functional as F
# max len of Cholec80 in train: 5994

def main(args):

    device_gpu = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    device_cpu = torch.device("cpu")

    num_train_op_sets = args.num_ops // Cholec80.op_set_size

    num_class = 7

    pretrained_model_file = None
    pretrain_method = "no_pretrain"
    
    trial_id = datetime.datetime.now().strftime("%Y%m%d-%H%M")
    experiment = os.path.join("Phase_segmentation_FeedbackTransformer", pretrain_method, str(args.num_ops), trial_id)
    
    output_folder = os.path.join(utils.out_path_Cholec80, experiment)
    os.makedirs(output_folder)
    f_log = open(os.path.join(output_folder, "log.txt"), "a")

    def log(msg):
        utils.log(f_log, msg)

    model_file = os.path.join(output_folder, "model.pkl")
    model_file_final = os.path.join(output_folder, "model_final.pkl")

    log("Run <%s> on device %s" % (experiment, str(device_gpu)))
    if pretrained_model_file is not None:
        log("Using pretrained FeatureNet: " + pretrained_model_file)
    log("Used parameters...")
    for arg in vars(args):
        log("\t" + str(arg) + " : " + str(getattr(args, arg)))

    log("Loading data...")
    train_sets = Cholec80.train_sets[0:num_train_op_sets]
    test_sets = Cholec80.test_sets

    # Load Features
    FEATURES = torch.load("features_cholec80.pkl")

    log("Loading Features done.")

    ANNO = utils.annotation_path_Cholec80

    ANNO = "D:/Studium/11.Semester/Masterarbeit/phase_annotations_Cholec80"
    FRAMES = utils.frames_path_Cholec80
    FRAMES = "/mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/cholec80/frames_1fps"

    # Training Loader
    op_pathes = []
    anno_files = []
    train_data = []
    u = 0
    for op_set in train_sets:
        #print(op_set)
        for op in Cholec80.videos_in_set[u]:# os.listdir(os.path.join(utils.frames_path_Cholec80, op_set)):

            #op_path = os.path.join(utils.frames_path_Cholec80, op_set, op)
            op_path = FRAMES +"/" + op_set +"/" + op
            #print(op_path)
            anno_file = os.path.join(ANNO, "video" + op + "-phase.txt")
            dataset = PhaseData_Features(op_path, anno_file, FEATURES[op_path], args.sequence)
            train_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False,num_workers=args.num_workers))
        u += 1
    
    
    
    '''log("\t training data")
    for loader in train_data:
        log("\t\t" + loader.dataset.frames_path)'''
    

    # Validation and Test Loader
    val_data = []
    test_data = []
    for op_set in test_sets:
        for op in Cholec80.videos_in_set[u]:#for op in os.listdir(os.path.join(utils.frames_path_Cholec80, op_set)):
            #op_path = os.path.join(utils.frames_path_Cholec80, op_set, op)
            op_path = FRAMES +"/" + op_set +"/" + op
            anno_file = os.path.join(ANNO, "video" + op + "-phase.txt")
            dataset = PhaseData_Features(op_path, anno_file, FEATURES[op_path], args.sequence)
                
            if len(val_data) == 8:
                test_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False,
                                                             num_workers=args.num_workers))
            else:
                val_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False,
                                                             num_workers=args.num_workers))
        u += 1
                
    '''log("\t val data")
    for loader in val_data:
        log("\t\t" + loader.dataset.frames_path)

    log("\t test data")
    for loader in test_data:
        log("\t\t" + loader.dataset.frames_path)'''

    NUM_TOKENS = 5994
    FEATUR_VECTOR_SIZE = 2048
    # Model
    model = FeedbackTransformerImageFeatures(
        num_tokens = num_class ,           # number of tokens 
        dim = FEATUR_VECTOR_SIZE,           # dimension  -> feature vector size
        depth = args.depth,                 # depth
        seq_len = args.sequence_segment,    # the sequence length of each segment or window
        mem_len = args.mem_len,             # length of the memory buffer
        dim_head = args.dim_head,           # dimension of each head
        heads = args.heads,                 # number of heads
        keep_last_hidden = True
        )    
    #print(model)

    # Eingabe: N x S X F
    
    model = model.to(device_gpu)
    criterion = nn.CrossEntropyLoss(size_average=False)
    optimizer = optim.Adam(filter(lambda p: p.requires_grad, model.parameters()), lr=args.lr)

    torch.backends.cudnn.benchmark = True
    torch.backends.cudnn.enabled = True
    
    #scheduler = lr_scheduler.StepLR(optimizer, step_size=50, gamma=0.1)
    #log("lr_scheduler.StepLR(optimizer, step_size=50, gamma=0.1)")
    
    log("Begin training...")
    best_val = 0
    for epoch in range(args.epochs):
        log("Epoch " + str(epoch + 1) + "...")
        # zero lodd and acc
        train_loss = AverageMeter()
        train_acc = AverageMeter()      
        # Print Learning Rate
        #print('Epoch:', (epoch + 1),'LR:', scheduler.get_lr())
        model.train()
        vid = 0
        for trainloader in train_data:
            #mem = None
            print(vid)
            vid += 1
            for _, batch in enumerate(trainloader):
                data, target = batch
                #print(data.size())
                #print(target.size())
                
                batch_size = target.size(0) * target.size(1)
                data = data.to(device_gpu)
                target = target.to(device_gpu)

                # zero the parameter gradients
                optimizer.zero_grad()

                #forward + return memory
                #output, mem = model(data, memory = mem, return_memory = True)
                #print(mem)
                #mem = mem.detach()
                output = model(data)
                
                output = output.permute(0,2,1)
                print(output.size())
                loss = criterion(output, target)
                # backward + optimize
                loss.backward()
                optimizer.step()
                    
                # statistics
                print(loss.item())
                train_loss.update(loss.item(), batch_size)

                predicted = torch.nn.Softmax(dim=1)(output)
                _, predicted = torch.max(predicted, 1)

                acc = (predicted == target).sum().item() / batch_size
                train_acc.update(acc, batch_size)


        # Decay Learning Rate
        #scheduler.step()
        log("Epoch {}: Train loss: {train_loss.avg:.4f} Train acc: {train_acc.avg:.3f}"
            .format(epoch + 1, train_loss=train_loss, train_acc=train_acc))

        log("testing with val set...")
        predictions_path = os.path.join(output_folder, "predictions_val")
        if not os.path.exists(predictions_path):
            os.makedirs(predictions_path)
        correct = 0
        total = 0
        model.eval()
        with torch.no_grad():
            for v in val_data:
                frames_path = v.dataset.frames_path
                #op_id = frames_path.split('/')[-1] if frames_path.split('/')[-1] else frames_path.split('/')[-2]
                #f_out = open(os.path.join(predictions_path, op_id + ".txt"), "w")
                for loader in v:
                    data,target_cpu = loader
                    batch_size = target.size(0) * target.size(1)
                    data = data.to(device_gpu)
                    target = target_cpu.to(device_gpu)
                    output = model(data)
                    output = output.permute(0,2,1)
                    
                    predicted = torch.nn.Softmax(dim=1)(output)
                    _, predicted = torch.max(predicted, 1)

                    predicted_cpu = predicted.to(device_cpu)
                    #for p, l in zip(predicted_cpu.numpy(), target_cpu.numpy()):
                        #f_out.write(str(p) + "," + str(l) + "\n")
                    correct += (predicted == target).sum().item()
                    total += batch_size
                #f_out.close()
        log('Val Accuracy : %.3f %%' % (100 * correct / total))
        #s = eval__(predictions_path)
        #log("F1_score: "+ s)
        if (epoch + 1) % args.save_freq == 0 or epoch == 0:           
            model_temp = os.path.join(output_folder, "model"+ str(epoch +1) + ".pkl")
            log("\tSave model to %s..." % model_temp)
            model.save(model_temp)

        act_val = correct / total

        if act_val > best_val:
            best_val = act_val
            log("Done. Save final model to %s..." % model_file)
            model.save(model_file)
        
    log("Done. Save final model to %s..." % model_file_final)
    model.save(model_file_final)
    #f_log.close()

    log("Load best val_model %s and test it ..." % model_file)
    model.load(model_file)
    correct = 0
    total = 0
    model.eval()
    predictions_path = os.path.join(output_folder, "predictions")
    if not os.path.exists(predictions_path):
        os.makedirs(predictions_path)
    with torch.no_grad():
        for v in test_data:
            frames_path = v.dataset.frames_path
            #op_id = frames_path.split('/')[-1] if frames_path.split('/')[-1] else frames_path.split('/')[-2]
            #f_out = open(os.path.join(predictions_path, op_id + ".txt"), "w")
            for loader in v:
                data,target_cpu = loader
                batch_size = target.size(0) * target.size(1)
                data = data.to(device_gpu)
                target = target_cpu.to(device_gpu)
                output = model(data)
                output = output.permute(0,2,1)

                predicted = torch.nn.Softmax(dim=1)(output)
                _, predicted = torch.max(predicted, 1)

                predicted_cpu = predicted.to(device_cpu)
                #for p, l in zip(predicted_cpu.numpy(), target_cpu.numpy()):
                    #f_out.write(str(p) + "," + str(l) + "\n")
                correct += (predicted == target).sum().item()
                total += batch_size
            #f_out.close()
        summary = "test ( accuracy %.3f)" % \
                  (correct / total)
        log("\t" + summary)
    f_log.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Run feedbacktransformer")
    parser.add_argument("--num_ops", type=int, default = 40, choices=[20, 40],
                        help="Number of labeled OP videos to use for training.")
    parser.add_argument("--lr", type=float, default=0.0001, help="The learning rate.")
    parser.add_argument("--batch_size", type=int, default=1, help="The batch size.") #128
    parser.add_argument("--epochs", type=int, default=1000, help="The maximal number of epochs to train.")
    parser.add_argument("--save_freq", type=int, default=10,
                        help="Defines after how many epochs the current model parameters are saved.")
    parser.add_argument("--cross_att", type=bool, default= False, help="The learning rate.")
    parser.add_argument("--num_workers", type= int, default= 2, help=".")
    parser.add_argument("--heads", type= int, default= 6, help=".")
    parser.add_argument("--dim_head", type= int, default= 64, help=".")
    parser.add_argument("--depth", type= int, default= 2, help=".")
    parser.add_argument("--sequence_segment", type= int, default = 1, help=".")
    parser.add_argument("--mem_len", type= int, default= 256, help=".")
    parser.add_argument("--sequence", type= int, default= 1000, help=".")
    args = parser.parse_args()
    main(args)
