import torch
import torch.nn as nn
import torch.optim as optim
import os.path
import datetime
import argparse
import torchvision

from datasets import PhaseData
from networks import BayesLSTMAlexNet
import utils
from utils import Cholec80, DataPrep


def main(args):
    transform_train = DataPrep.standard_transform
    transform_test = DataPrep.standard_transform

    device_gpu = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    device_cpu = torch.device("cpu")
    num_class = 7 #cholec80

    num_train_op_sets = args.num_ops // Cholec80.op_set_size

    pretrained_model_file = None
    pretrain_method = "no_pretrain"
    if args.model is not None:
        pretrained_model_file = args.model
        if not os.path.exists(pretrained_model_file):
            pretrained_model_file = os.path.join(utils.out_path, "Self-supervised", pretrained_model_file, "model.pth")
        pretrain_method = os.path.split(os.path.split(os.path.split(pretrained_model_file)[0])[0])[1]

    trial_id = datetime.datetime.now().strftime("%Y%m%d-%H%M")
    experiment = os.path.join("Phase_segmentation_DBN", pretrain_method, str(args.num_ops), trial_id)

    output_folder = os.path.join(utils.out_path, experiment)
    os.makedirs(output_folder)
    f_log = open(os.path.join(output_folder, "log.txt"), "w")
    def log(msg):
        utils.log(f_log, msg)
    model_file = os.path.join(output_folder, "model.pkl")

    log("Run <%s> on device %s" % (experiment, str(device_gpu)))
    if pretrained_model_file is not None:
        log("Using pretrained FeatureNet: " + pretrained_model_file)
    log("Used parameters...")
    for arg in vars(args):
        log("\t" + str(arg) + " : " + str(getattr(args, arg)))

    log("Loading data...")
    train_sets = Cholec80.train_sets[0:num_train_op_sets]
    test_sets = Cholec80.test_sets

    train_data = []
    for op_set in train_sets:
        for op in os.listdir(os.path.join(utils.frames_path, op_set)):
            op_path = os.path.join(utils.frames_path, op_set, op)
            if os.path.isdir(op_path):
                anno_file = os.path.join(utils.annotation_path, "video" + op + "-phase.txt")
                dataset = PhaseData(op_path, anno_file, DataPrep.width, DataPrep.height, transform_train)
                train_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False,
                                                              num_workers=2))
    log("\t training data")
    for loader in train_data:
        log("\t\t" + loader.dataset.frames_path)

    test_data = []
    for op_set in test_sets:
        for op in os.listdir(os.path.join(utils.frames_path, op_set)):
            op_path = os.path.join(utils.frames_path, op_set, op)
            if os.path.isdir(op_path):
                anno_file = os.path.join(utils.annotation_path, "video" + op + "-phase.txt")
                dataset = PhaseData(op_path, anno_file, DataPrep.width, DataPrep.height, transform_test)
                test_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False,
                                                             num_workers=2))
                
    log("\t test data")
    for loader in test_data:
        log("\t\t" + loader.dataset.frames_path)

   # log("AlexNet")
    #net = torchvision.models.alexnet(pretrained=True)

    log("Create PhaseNet")
    net = BayesLSTMAlexNet(num_class)
    net.set_mode(mode = 'DETERMINISTIC')
    net.to(device_gpu)

    criterion = nn.CrossEntropyLoss(size_average=False)
    optimizer = optim.Adam(filter(lambda p: p.requires_grad, net.parameters()), lr=args.lr)

    log("Begin training...")
    for epoch in range(args.epochs):
        log("Epoch " + str(epoch + 1) + "...")

        train_loss = 0
        train_accuracy = 0
        train_count = 0
        for op_data in train_data:
            hidden_state = net.init_hidden()
            optimizer.zero_grad()
            optimized = False
            loss = 0
            batch_count = 0
            for loader in op_data:
                images, labels = loader
                images = images.to(device_gpu)
                labels = labels.to(device_gpu)
                outputs, hidden_state = net(images, hidden_state)
                loss += criterion(outputs, labels)
                batch_count += 1
                if batch_count % args.opt_step == 0:
                    loss.backward()
                    optimizer.step()
                    optimized = True
                    optimizer.zero_grad()
                    h, c = hidden_state
                    h = h.detach()
                    c = c.detach()
                    hidden_state = (h, c)
                else:
                    loss.backward(retain_graph=True)
                    optimized = False

                train_loss += loss.item()
                _, predicted = torch.max(outputs.data, 1)
                train_accuracy += (predicted == labels).sum().item()
                train_count += labels.size(0)

                if batch_count % args.opt_step == 0:
                    loss = 0

            if not optimized:
                optimizer.step()

        predictions_path = os.path.join(output_folder, "predictions")
        if not os.path.exists(predictions_path):
            os.makedirs(predictions_path)
        test_loss = 0
        test_accuracy = 0
        test_count = 0
        with torch.no_grad():
            for op_data in test_data:
                frames_path = op_data.dataset.frames_path
                op_id = frames_path.split('/')[-1] if frames_path.split('/')[-1] else frames_path.split('/')[-2]
                f_out = open(os.path.join(predictions_path, op_id + ".txt"), "w")

                hidden_state = net.init_hidden()
                for loader in op_data:
                    images, labels_cpu = loader
                    images = images.to(device_gpu)
                    labels = labels_cpu.to(device_gpu)
                    outputs, hidden_state = net(images, hidden_state)
                    loss = criterion(outputs, labels)

                    test_loss += loss.item()
                    _, predicted = torch.max(outputs.data, 1)
                    predicted_cpu = predicted.to(device_cpu)
                    for p, l in zip(predicted_cpu.numpy(), labels_cpu.numpy()):
                        f_out.write(str(p) + "," + str(l) + "\n")
                    test_count += labels.size(0)
                    test_accuracy += (predicted == labels).sum().item()
                f_out.close()

        summary = "train (loss %.3f, accuracy %.3f) test (loss %.3f, accuracy %.3f)" % \
                  (train_loss/train_count, train_accuracy/train_count, test_loss/test_count, test_accuracy/test_count)
        log("\t" + summary)

        if train_accuracy/train_count > 0.999:
            break

        if (epoch + 1) % args.save_freq == 0:
            log("\tSave model to %s..." % model_file)
            net.save(model_file)

    log("Done. Save final model to %s..." % model_file)
    net.save(model_file)
    f_log.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Finetune a CNN-LSTM for surgical phase recognition.")
    parser.add_argument("num_ops", type=int, choices=[20, 40, 60],
                        help="Number of labeled OP videos to use for training.")
    parser.add_argument("-m", "--model", type=str, help="Pretrained FeatureNet to use.")
    #parser.add_argument("--cnn", type=str, choices=["resnet","alexnet"], default = "resnet",help="cnn to use resnet or alexnet")
    parser.add_argument("--lr", type=float, default=0.0001, help="The learning rate.")
    parser.add_argument("--batch_size", type=int, default=128, help="The batch size.") #128
    parser.add_argument("--opt_step", type=int, default=3,
                        help="Number of batches to accumulate before applying the optimizer.")
    parser.add_argument("--epochs", type=int, default=200, help="The maximal number of epochs to train.")
    parser.add_argument("--save_freq", type=int, default=10,
                        help="Defines after how many epochs the current model parameters are saved.")
    args = parser.parse_args()
    main(args) 
