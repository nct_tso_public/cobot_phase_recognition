cd ..
cd part2


# EXP=test_attn_conv_cross_pre_un_
# EXP+=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210401-1009/$i/features.pth.tar" \
# --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01 --mask_prob 0.01 --replace_duration 15 --mask_duration 15 \
# --duplicate_frames_prob 0.1 --drop_frames_prob 0.1 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "conv" --d_ff 64 --ff_dropout 0.5 --conv_kernel 5 --conv_dilation 4 --conv_layers 1 \
#  --lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --deep_supervision False --task "CoBotPhase" --max_length 9000 --short True --adam True
# done

# EXP=test_attn_base_mlp_cross_pre_un_
# EXP+=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210401-1009/$i/features.pth.tar" \
# --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "feed_forward" --d_ff 256 --ff_dropout 0.5 \
# --lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --max_length 9000 --short True --task "CoBotPhase" --adam True
# done

# EXP=test_attn_conv_cross_pre_un_
# EXP+=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210406-1007/$i/features.pth.tar" \
# --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01 --mask_prob 0.01 --replace_duration 15 --mask_duration 15 \
# --duplicate_frames_prob 0.1 --drop_frames_prob 0.1 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "conv" --d_ff 64 --ff_dropout 0.5 --conv_kernel 5 --conv_dilation 4 --conv_layers 1 \
# --lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --deep_supervision False --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
# done

# EXP=test_attn_base_mlp_cross_pre_un_
# EXP+=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210406-1007/$i/features.pth.tar" \
# --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "feed_forward" --d_ff 256 --ff_dropout 0.5 \
# --lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --max_length 9000 --short True --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
# done

# cd /home/krellstef/master_code
# EXP=pre_un_
# EXP+=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210406-1007/$i/features.pth.tar" --exp "$EXP" --fold $i --w 1 --epochs 100 --annotation "step"
# done

# EXP=pre_un_
# EXP+=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210401-1009/$i/features.pth.tar" --exp "$EXP" --fold $i --w 1 --epochs 100
# done



# EXP=test_tcn_baseline_cross_pre_un_
# EXP+=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210406-1007/$i/features.pth.tar" \
# --tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 5 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
# done


EXP=test_tcn_baseline_cross_pre_un_
EXP+=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210401-1009/$i/features.pth.tar" \
--tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 5 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotPhase" --adam True
done


##PC02
#cd /home/krellstef/gesture-transformer-master

# EXP=test_attn_mlp
# for i in 1 2 3 4
# do
# python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" \
# --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "feed_forward" --d_ff 256 --ff_dropout 0.5 \
# --lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --max_length 9000 --short True --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps"
# done
# EXP=test_tcn_baseline
# for i in 1 2 3 4
# do
# python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" \
# --tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 5 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps"
# done
# cd /home/krellstef/master_code
# EXP="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210408-0839"
# for i in 1 2 3 4
# do
# python3 get_CoBot_features.py "$EXP" --fold $i --anno "step"
# done

# cd /home/krellstef/gesture-transformer-master


# EXP=test_tcn_baseline_cross_pre_all_
# EXP+=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210408-0839/$i/features.pth.tar" \
# --tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 5 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
# done

# cd /home/krellstef/master_code
# EXP=pre_all_
# EXP+=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210408-0839/$i/features.pth.tar" --exp "$EXP" --fold $i --w 1 --epochs 100 --annotation "step"
# done
#python3 trainCNN_CoBot.py --sample_rate 1 --annotation step --batch_size 256 --pretrained 4 --model /home/krellstef/master_code/outCoBot/Self-supervised/1st+2nd_contrastive/no_pretrain/20210304-1312/model.pth
#python3 trainCNN_CoBot.py --sample_rate 1 --annotation step --batch_size 256 --pretrained 4 --model /home/krellstef/master_code/outCoBot/Self-supervised/1st+2nd_contrastive/EndoVis/20210305-0448/model.pth
# python3 trainCNN_CoBot.py --sample_rate 1 --batch_size 256 --pretrained 4 --model /home/krellstef/master_code/outCoBot/Self-supervised/1st+2nd_contrastive/no_pretrain/20210304-1312/model.pth
# python3 trainCNN_CoBot.py --sample_rate 1 --batch_size 256 --pretrained 4 --model /home/krellstef/master_code/outCoBot/Self-supervised/1st+2nd_contrastive/EndoVis/20210305-0448/model.pth
#python3 train_CNNLSTM_CoBot.py -m /home/krellstef/master_code/outCoBot/Self-supervised/1st+2nd_contrastive/no_pretrain/20210304-1312/model.pth --annotation step
#python3 train_CNNLSTM_CoBot.py --annotation step
#python3 pretrain_self_supervised.py --model /mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/EndoVis18/out/Phase_segmentation_resnet50/no_pretrain/20210225-1242/model.pkl
# EXP=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 trainCNN_CoBot_cross.py --fold $i --exp "$EXP" --epochs 30 --pretrained 5
# done
# EXP=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 trainCNN_CoBot_cross.py --fold $i --exp "$EXP" --epochs 30 --pretrained 5 --annotation step
# done

# python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/1/features.pth.tar" --exp "20210412-1316" --fold 1 --w 1 --epochs 100
# python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/2/features.pth.tar" --exp "20210412-1316" --fold 2 --w 1 --epochs 100
# python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/3/features.pth.tar" --exp "20210412-1316" --fold 3 --w 1 --epochs 100
# python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/4/features.pth.tar" --exp "20210412-1316" --fold 4 --w 1 --epochs 100

# python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/1/features.pth.tar" --exp "20210412-1318" --fold 1 --annotation "step" --w 1 --epochs 100
# python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/2/features.pth.tar" --exp "20210412-1318" --fold 2 --annotation "step" --w 1 --epochs 100
# python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/3/features.pth.tar" --exp "20210412-1318" --fold 3 --annotation "step" --w 1 --epochs 100
# python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/4/features.pth.tar" --exp "20210412-1318" --fold 4 --annotation "step" --w 1 --epochs 100




### TRAIN step S
# cd /home/krellstef/master_code
# #EXP="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359"
# #EXP="/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442"
# EXP="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210406-1007"
# for i in 1 2 3 4
# do
# python3 get_CoBot_features.py "$EXP" --fold $i --anno "step"
# done

# cd /home/krellstef/gesture-transformer-master

# EXP=test_attn_conv_cross_
# EXP+=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210406-1007/$i/features.pth.tar" \
# --causal True --d_model 64 --nlayers 2 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01 --mask_prob 0.01 --replace_duration 15 --mask_duration 15 \
# --duplicate_frames_prob 0.1 --drop_frames_prob 0.1 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "conv" --d_ff 64 --ff_dropout 0.5 --conv_kernel 5 --conv_dilation 4 --conv_layers 1 \
#  --lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --deep_supervision False --task "CoBotStep" --max_length 9000 --short True --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps"
# done

# EXP=test_attn_base_mlp_cross_
# EXP+=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210406-1007/$i/features.pth.tar" \
# --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "feed_forward" --d_ff 256 --ff_dropout 0.5 \
# --lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --max_length 9000 --short True --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps"
# done

# EXP=test_tcn_baseline_cross_
# EXP+=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210406-1007/$i/features.pth.tar" \
# --tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 5 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps"
# done

# cd /home/krellstef/master_code
# EXP=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train_CNNLSTM_CoBot_cross_feature.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210406-1007/$i/features.pth.tar" --exp "$EXP" --fold $i --annotation "step" --w 1 --epochs 100
# done


###TEST

cd /home/krellstef/gesture-transformer-master


for i in 1 2 3 4
do
EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_attn_conv_cross_20210409-0633/train-val/$i"
python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL" --exp "" --seed 12345 --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210406-1007/$i/features.pth.tar" \
--causal True --d_model 64 --nlayers 2 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
--randomly_replace_prob 0.01 --mask_prob 0.01 --replace_duration 15 --mask_duration 15 \
--duplicate_frames_prob 0.1 --drop_frames_prob 0.1 --added_noise 0.3 \
--attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
--pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
--ff_type "conv" --d_ff 64 --ff_dropout 0.5 --conv_kernel 5 --conv_dilation 4 --conv_layers 1 \
 --lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --deep_supervision False --task "CoBotStep" --max_length 9000 --short True --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps"
done


for i in 1 2 3 4
do
EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_attn_base_mlp_cross_20210409-0801/train-val/$i"
python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL" --exp "" --seed 12345 --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210406-1007/$i/features.pth.tar" \
--causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
--pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
--ff_type "feed_forward" --d_ff 256 --ff_dropout 0.5 \
--lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --max_length 9000 --short True --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps"
done


for i in 1 2 3 4
do
EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/test_tcn_baseline_cross_20210409-0946/train-val/$i"
python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL" --exp "" --seed 12345 --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210406-1007/$i/features.pth.tar" \
--tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 5 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps"
done


##TEST Phase

# EVAL=test_attn_conv_cross_
# for i in 1 2 3 4
# do
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL" --exp "" --feature_file "media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210401-1009/$i/features.pth.tar" \
# --causal True --d_model 64 --nlayers 2 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01 --mask_prob 0.01 --replace_duration 15 --mask_duration 15 \
# --duplicate_frames_prob 0.1 --drop_frames_prob 0.1 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "conv" --d_ff 64 --ff_dropout 0.5 --conv_kernel 5 --conv_dilation 4 --conv_layers 1 \
#  --lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --deep_supervision False --task "CoBotPhase" --max_length 9000 --short True
# done

# EVAL=test_attn_base_mlp_cross_
# for i in 1 2 3 4
# do
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL" --exp "" --feature_file "media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210401-1009/$i/features.pth.tar" \
# --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "feed_forward" --d_ff 256 --ff_dropout 0.5 \
# --lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --max_length 9000 --short True --task "CoBotPhase"
# done

# EVAL=test_tcn_baseline_cross_
# for i in 1 2 3 4
# do
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL" --exp "" --feature_file "media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210401-1009/$i/features.pth.tar" \
# --tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 5 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --task "CoBotPhase"
# done


#python3 train_CNNLSTM_CoBot.py --sample_rate 1 --batch_size 256 --lr 0.00005
#python3 train_CNNLSTM_CoBot.py 20 --annotation step --sample_rate 1 --batch_size 256 --lr 0.0001
#python3 train_CNNLSTM_CoBot.py 20 --sample_rate 1 --batch_size 128 --lr 0.00001
#python3 trainCNN_CoBot.py 20 --annotation step --sample_rate 1 --batch_size 256 --cnn resnet50

#python3 trainCNN_CoBot.py --sample_rate 1 --batch_size 256 --pretrained 1
#python3 trainCNN_CoBot.py --sample_rate 1 --batch_size 256
##pc14

#python3 pretrain_self_supervised.py --gamma 600
#python3 pretrain_self_supervised.py --model /mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/EndoVis18/out/Phase_segmentation_resnet50/no_pretrain/20210225-1242/model.pkl --freeze_layers True
#python3 pretrain_self_supervised.py --gamma 600 --model /mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/EndoVis18/out/Phase_segmentation_resnet50/no_pretrain/20210225-1242/model.pkl
#python3 pretrain_self_supervised.py --model /mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/EndoVis18/out/Phase_segmentation_resnet50/no_pretrain/20210222-1316/model.pkl
#python3 pretrain_self_supervised.py --gamma 600 --model /mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/EndoVis18/out/Phase_segmentation_resnet50/no_pretrain/20210222-1316/model.pkl
cd /home/krellstef/master_code
##PC10
#python3 pretrain_self_supervised.py
# EXP=$(date +"%Y%m%d-%H%M")

# for i in 1 2 3 4
# do
# python3 trainCNN_CoBot_cross.py --fold $i --exp "$EXP" --epochs 30 --pretrained 4
# done

EXP=$(date +"%Y%m%d-%H%M")

for i in 1 2 3 4
do
python3 trainCNN_CoBot_cross.py --fold $i --exp "$EXP" --epochs 30 --annotation step --pretrained 4
done

## pc12
cd /home/krellstef/master_code


# EXP=$(date +"%Y%m%d-%H%M")

# for i in 1 2 3 4
# do
# python3 trainCNN_CoBot_cross.py --fold $i --exp "$EXP" --epochs 30 --annotation step
# done

# EXP=$(date +"%Y%m%d-%H%M")

# for i in 1 2 3 4
# do
# python3 trainCNN_CoBot_cross.py --fold $i --exp "$EXP" --epochs 30 --annotation step --pretrained 1
# done

# EXP=$(date +"%Y%m%d-%H%M")

# for i in 1 2 3 4
# do
# python3 trainCNN_CoBot_cross.py --fold $i --exp "$EXP" --epochs 30 --annotation step --pretrained 3
# # done
# EXP=$(date +"%Y%m%d-%H%M")

# for i in 1 2 3 4
# do
# python3 train_CNNLSTM_CoBot_cross_feature.py --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" --exp "$EXP" --fold $i --annotation "step" --w 1 --epochs 100
# done

# EXP=$(date +"%Y%m%d-%H%M")

# for i in 1 2 3 4
# do
# python3 train_CNNLSTM_CoBot_cross_feature.py --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" --exp "$EXP" --fold $i
# done

# EXP=$(date +"%Y%m%d-%H%M")

# for i in 1 2 3 4
# do
# python3 train_CNNLSTM_CoBot_cross_feature.py --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" --exp "$EXP" --fold $i --annotation "step"
# done


# python3 trainCNN_CoBot.py --sample_rate 1 --annotation step --batch_size 256 --pretrained 3 --model /mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/EndoVis18/out/Phase_segmentation_resnet50/no_pretrain/20210222-1316/model.pkl
# python3 trainCNN_CoBot.py --sample_rate 1 --annotation step --batch_size 256

# python3 trainCNN_CoBot.py --sample_rate 1 --batch_size 256 --pretrained 1
# python3 trainCNN_CoBot.py --sample_rate 1 --pretrained 3 --model /mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/EndoVis18/out/Phase_segmentation_resnet50/no_pretrain/20210222-1316/model.pkl

# EXP="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210401-1009"
# for i in 1 2 3 4
# do
# python3 get_CoBot_features.py "$EXP" --fold $i
# done

# cd /home/krellstef/gesture-transformer-master

# EXP=test_attn_conv_cross_
# EXP+=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210401-1009/$i/features.pth.tar" \
# --causal True --d_model 64 --nlayers 2 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01 --mask_prob 0.01 --replace_duration 15 --mask_duration 15 \
# --duplicate_frames_prob 0.1 --drop_frames_prob 0.1 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "conv" --d_ff 64 --ff_dropout 0.5 --conv_kernel 5 --conv_dilation 4 --conv_layers 1 \
#  --lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --deep_supervision False --task "CoBotPhase" --max_length 9000 --short True
# done

# EXP=test_attn_base_mlp_cross_
# EXP+=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210401-1009/$i/features.pth.tar" \
# --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "feed_forward" --d_ff 256 --ff_dropout 0.5 \
# --lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --max_length 9000 --short True --task "CoBotPhase"
# done

# EXP=test_tcn_baseline_cross_
# EXP+=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210401-1009/$i/features.pth.tar" \
# --tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 5 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0.3 --feature_trunc_mse_factor 0.3 --task "CoBotPhase"
# done

# cd /home/krellstef/master_code
# EXP=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train_CNNLSTM_CoBot_cross_feature.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210401-1009/$i/features.pth.tar" --exp "$EXP" --fold $i --w 1 --epochs 100
# done


# EXP="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210406-1016"
# for i in 1 2 3 4
# do
# python3 get_CoBot_features.py "$EXP" --fold $i
# done

cd /home/krellstef/gesture-transformer-master

# EXP=test_attn_conv_cross_pre_all_
# EXP+=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210406-1016/$i/features.pth.tar" \
# --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01 --mask_prob 0.01 --replace_duration 15 --mask_duration 15 \
# --duplicate_frames_prob 0.1 --drop_frames_prob 0.1 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "conv" --d_ff 64 --ff_dropout 0.5 --conv_kernel 5 --conv_dilation 4 --conv_layers 1 \
#  --lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --deep_supervision False --task "CoBotPhase" --max_length 9000 --short True --adam True
# done

# EXP=test_attn_base_mlp_cross_pre_all_
# EXP+=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210406-1016/$i/features.pth.tar" \
# --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "feed_forward" --d_ff 256 --ff_dropout 0.5 \
# --lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --max_length 9000 --short True --task "CoBotPhase" --adam True
# done

# EXP=test_tcn_baseline_cross_pre_all_
# EXP+=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210406-1016/$i/features.pth.tar" \
# --tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 5 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotPhase" --adam True
# done

# cd /home/krellstef/master_code
# EXP=pre_all_
# EXP+=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210406-1016/$i/features.pth.tar" --exp "$EXP" --fold $i --w 1 --epochs 100
# done



EXP=test_attn_conv_cross_pre_un_
EXP+=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210406-1007/$i/features.pth.tar" \
--causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
--randomly_replace_prob 0.01 --mask_prob 0.01 --replace_duration 15 --mask_duration 15 \
--duplicate_frames_prob 0.1 --drop_frames_prob 0.1 --added_noise 0.3 \
--attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
--pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
--ff_type "conv" --d_ff 64 --ff_dropout 0.5 --conv_kernel 5 --conv_dilation 4 --conv_layers 1 \
--lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --deep_supervision False --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True --max_length 9000 --short True
done

# EXP=test_attn_base_mlp_cross_pre_all_
# EXP+=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210408-0839/$i/features.pth.tar" \
# --causal True --d_model 64 --nlayers 3 --nhead 2 --attn_dropout 0.3 --hidden_dropout 0.1 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --attn_impl "bert_hf" --attn_local_window_duration -1 --attn_local_stride_duration -1 --attn_global_stride_duration -1 --attn_global_type "grid" \
# --pos_embed_type "absolute_sinusoidal"  --repeat_pos_info True \
# --ff_type "feed_forward" --d_ff 256 --ff_dropout 0.5 \
# --lr 0.0005 -b 1 --steps 70 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --max_length 9000 --short True --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
# done

# EXP=test_tcn_baseline_cross_pre_all_
# EXP+=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210408-0839/$i/features.pth.tar" \
# --tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 5 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
# done

# cd /home/krellstef/master_code
# EXP=pre_all_
# EXP+=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210408-0839/$i/features.pth.tar" --exp "$EXP" --fold $i --w 1 --epochs 100 --annotation "step"
# done