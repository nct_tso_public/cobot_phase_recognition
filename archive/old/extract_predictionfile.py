import torch
import torch.nn as nn
import torch.optim as optim
import os.path
import datetime
import argparse
import torchvision

from datasets import PhaseData_CoBot, PhaseData_CoBot_old
from models import PhaseNet
import utils
from utils import CoBot, DataPrep3


op_path = "E:/20200622"
anno_file = "E:/newnewn/phase_annotations/20200622.txt"
step_file = "E:/newnewn/step_annotations_adjusted_new/20200622step.txt"
sample_rate = 2

dataset = PhaseData_CoBot(op_path, anno_file, DataPrep3.width, DataPrep3.height, DataPrep3.standard_transform, annotation = "step", step_file = step_file, sample_rate = sample_rate)
op_data = torch.utils.data.DataLoader(dataset, batch_size=64, shuffle=False,
                                                             num_workers=0)

f_out = open(os.path.join("E:/", "labels_20200622_"+ str(sample_rate)+ ".txt"), "w")

for loader in op_data:
    images, labels = loader
    #labels = labels_cpu.to(device_gpu)
    for p, l in zip(labels.numpy(), labels.numpy()):
        f_out.write(str(p) + "," + str(l) + "\n")
f_out.close()





