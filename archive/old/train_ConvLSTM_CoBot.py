import torch
import torch.nn as nn
import torch.optim as optim
import os.path
import datetime
import argparse
import torchvision

from datasets import PhaseData_CoBot
from models import ConvLSTM_PhaseNet, ConvLSTM_PhaseNet2
import utils
from utils import CoBot, DataPrepCoBot



def main(args):
    transform_train = DataPrepCoBot.standard_transform
    transform_test = DataPrepCoBot.standard_transform

    device_gpu = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    device_cpu = torch.device("cpu")

    num_train_op_sets = args.num_ops

    trial_id = datetime.datetime.now().strftime("%Y%m%d-%H%M")
    experiment = os.path.join("Phase_segmentation_ConvLSTM_1", str(args.num_ops), trial_id)

    output_folder = os.path.join(utils.out_path, experiment)
    os.makedirs(output_folder)
    f_log = open(os.path.join(output_folder, "log.txt"), "w")
    def log(msg):
        utils.log(f_log, msg)
    model_file = os.path.join(output_folder, "model.pkl")

    log("Run <%s> on device %s" % (experiment, str(device_gpu)))
    log("Used parameters...")
    for arg in vars(args):
        log("\t" + str(arg) + " : " + str(getattr(args, arg)))

    log("Loading data...")
    train_sets = CoBot.train_sets[0:num_train_op_sets]
    test_sets = CoBot.test_sets

    train_data = []
    for op in train_sets:
        op_path = os.path.join(utils.frames_path, utils.CoBot.videos[op])
        if os.path.isdir(op_path):
            anno_file = os.path.join(utils.annotation_path, utils.CoBot.videos[op] + ".txt")
            dataset = PhaseData_CoBot(op_path, anno_file, DataPrepCoBot.width, DataPrepCoBot.height, transform_test)
            train_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False,
                                                             num_workers=4))
    log("\t training data")
    for loader in train_data:
        log("\t\t" + loader.dataset.frames_path)

    test_data = []
    for op in test_sets:
        op_path = os.path.join(utils.frames_path, utils.CoBot.videos[op])
        if os.path.isdir(op_path):
            anno_file = os.path.join(utils.annotation_path, utils.CoBot.videos[op] + ".txt")
            dataset = PhaseData_CoBot(op_path, anno_file, DataPrepCoBot.width, DataPrepCoBot.height, transform_test)
            test_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False,
                                                             num_workers=4))
                
    log("\t test data")
    for loader in test_data:
        log("\t\t" + loader.dataset.frames_path)

    log("Create ConvLSTM")
    net = ConvLSTM_PhaseNet(input_size = (224,224))
    net.to(device_gpu)    

    criterion = nn.CrossEntropyLoss(size_average=False)
    optimizer = optim.Adam(filter(lambda p: p.requires_grad, net.parameters()), lr=args.lr)

    log("Begin training...")
    for epoch in range(args.epochs):
        log("Epoch " + str(epoch + 1) + "...")

        train_loss = 0
        train_accuracy = 0
        train_count = 0
        for op_data in train_data:
            hidden_state = net.init_hidden(args.batch_size)
            optimizer.zero_grad()
            optimized = False
            loss = 0
            batch_count = 0
            if epoch < 2: 
                print(op_data.dataset.frames_path)
            for loader in op_data:
                images, labels = loader
                if images.size(0) != args.batch_size:
                    break
                #print("images shape: " + str(images.shape))
                #images = images.view(args.batch_size,1,3,224,224)
                images = images.to(device_gpu)
                #print("images shape: " + str(images.shape))
                labels = labels.to(device_gpu)
                outputs, hidden_state = net(images, hidden_state)
                loss += criterion(outputs, labels)
                batch_count += 1
                if batch_count % args.opt_step == 0:
                    #print("Hello")
                    loss.backward()
                    optimizer.step()
                    optimized = True
                    optimizer.zero_grad()
                    h, c = hidden_state[0]
                    h = h.detach()
                    c = c.detach()
                    hidden_state = []
                    hidden_state.append((h, c))
                else:
                    loss.backward(retain_graph=True)
                    optimized = False

                train_loss += loss.item()
                _, predicted = torch.max(outputs.data, 1)
                train_accuracy += (predicted == labels).sum().item()
                train_count += labels.size(0)

                if batch_count % args.opt_step == 0:
                    loss = 0

            if not optimized:
                optimizer.step()

        predictions_path = os.path.join(output_folder, "predictions")
        if not os.path.exists(predictions_path):
            os.makedirs(predictions_path)
        test_loss = 0
        test_accuracy = 0
        test_count = 0
        with torch.no_grad():
            for op_data in test_data:
                frames_path = op_data.dataset.frames_path
                op_id = frames_path.split('/')[-1] if frames_path.split('/')[-1] else frames_path.split('/')[-2]
                f_out = open(os.path.join(predictions_path, op_id + ".txt"), "w")

                hidden_state = net.init_hidden(args.batch_size)
                for loader in op_data:
                    images, labels_cpu = loader
                    if images.size(0) != args.batch_size:
                        break
                    #images = images.view(args.batch_size,1,3,224,224)
                    images = images.to(device_gpu)
                    labels = labels_cpu.to(device_gpu)
                    outputs, hidden_state = net(images, hidden_state)
                    loss = criterion(outputs, labels)

                    test_loss += loss.item()
                    _, predicted = torch.max(outputs.data, 1)
                    predicted_cpu = predicted.to(device_cpu)
                    for p, l in zip(predicted_cpu.numpy(), labels_cpu.numpy()):
                        f_out.write(str(p) + "," + str(l) + "\n")
                    test_count += labels.size(0)
                    test_accuracy += (predicted == labels).sum().item()
                f_out.close()

        summary = "train (loss %.3f, accuracy %.3f) test (loss %.3f, accuracy %.3f)" % \
                  (train_loss/train_count, train_accuracy/train_count, test_loss/test_count, test_accuracy/test_count)
        log("\t" + summary)

        if train_accuracy/train_count > 0.999 and test_accuracy > 85:
            break

        if (epoch + 1) % args.save_freq == 0:
            log("\tSave model to %s..." % model_file)
            net.save(model_file)

    log("Done. Save final model to %s..." % model_file)
    net.save(model_file)
    f_log.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Train ConvLSTM for surgical phase recognition.")
    parser.add_argument("num_ops", type=int, choices=[1,2,3,4,5,6,7,8,9,10,11,12,13],
                        help="Number of labeled OP videos to use for training.")
    parser.add_argument("--lr", type=float, default=0.0001, help="The learning rate.")
    parser.add_argument("--batch_size", type=int, default=128, help="The batch size.") #128
    parser.add_argument("--opt_step", type=int, default=3,
                        help="Number of batches to accumulate before applying the optimizer.")
    parser.add_argument("--epochs", type=int, default=200, help="The maximal number of epochs to train.")
    parser.add_argument("--save_freq", type=int, default=10,
                        help="Defines after how many epochs the current model parameters are saved.")
    args = parser.parse_args()
    main(args)
