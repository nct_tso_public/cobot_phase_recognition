import torch
import torch.nn as nn
import torch.optim as optim
import os.path
import datetime
import argparse
import torchvision



from datasets_sequence import PhaseData_sequence2, PhaseData_sequence_list2
from models_transformer import PhaseTransformerNet
import utils
from utils import CoBot, DataPrepCoBot, AverageMeter, Cholec80,DataPrep3, DataPrep_tr
from torch.optim import lr_scheduler

def main(args):

    transform_train = DataPrepCoBot.standard_transform
    transform_test = DataPrepCoBot.standard_transform

    device_gpu = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    device_cpu = torch.device("cpu")

    trial_id = datetime.datetime.now().strftime("%Y%m%d-%H%M")
    experiment = os.path.join("Phase_segmentation_Transformer", str(args.num_ops), trial_id)
    
    output_folder = os.path.join(utils.out_path, experiment)
    if args.annotation == "step":
        output_folder = os.path.join(utils.out_path + "_step", experiment)
    if args.dataset == "Cholec80":
       output_folder = os.path.join(utils.out_path_Cholec80, experiment) 

    os.makedirs(output_folder)
    f_log = open(os.path.join(output_folder, "log.txt"), "a")
    def log(msg):
        utils.log(f_log, msg)
    model_file = os.path.join(output_folder, "model.pkl")

    log("Run <%s> on device %s" % (experiment, str(device_gpu)))
    log("Used parameters...")
    for arg in vars(args):
        log("\t" + str(arg) + " : " + str(getattr(args, arg)))

    log("Loading data...")
    if args.dataset == "CoBot":
        log("CoBot...")
        if args.annotation == "step":
            num_class = 9
            anno_path = "/mnt/g27prist/TCO/TCO-Studenten/CoBot/phases_and_medial_steps"
        else:
            num_class = 5
            anno_path = utils.annotation_path

        
        train_sets = CoBot.train_sets[0:args.num_ops]
        test_sets = CoBot.test_sets

        train_data = []
        op_pathes = []
        anno_files = []
        for op in train_sets:
            op_path = os.path.join(utils.frames_path, utils.CoBot.videos[op])
            if os.path.isdir(op_path):
                anno_file = os.path.join(anno_path, utils.CoBot.videos[op] + ".txt")
                anno_files.append(anno_file)
                op_pathes.append(op_path)
                #dataset = PhaseData_sequence2(op_path, anno_file,args.sequence_length, args.dataset, transform_test, sample_rate = args.sample_rate, sample_rate_s = args.sample_rate_s)
                #train_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False,num_workers=4, pin_memory=True))

        trainset = PhaseData_sequence_list2(op_pathes, anno_files,args.sequence_length, args.dataset, transform_test, sample_rate = args.sample_rate, sample_rate_s = args.sample_rate_s, annotation_mode = args.annotation)
        trainloader = torch.utils.data.DataLoader(trainset, batch_size=args.batch_size, shuffle=True,num_workers=4, pin_memory=True)
        log("\t training data")
        for op in op_pathes:
            log("\t\t" + op)

        test_data = []
        for op in test_sets:
            op_path = os.path.join(utils.frames_path, utils.CoBot.videos[op])
            if os.path.isdir(op_path):
                anno_file = os.path.join(anno_path, utils.CoBot.videos[op] + ".txt")
                testset = PhaseData_sequence2(op_path, anno_file,args.sequence_length, args.dataset, transform_test, sample_rate = args.sample_rate, sample_rate_s = args.sample_rate_s, annotation_mode = args.annotation)
                test_data.append(torch.utils.data.DataLoader(testset, batch_size=args.batch_size, shuffle=False,num_workers=4, pin_memory=True))
                    
        log("\t test data")
        for loader in test_data:
            log("\t\t" + loader.dataset.frames_path)

    elif args.dataset == "Cholec80":
        log("Cholec80...")
        num_class = 7
        train_sets = Cholec80.train_sets[0:(args.num_ops // Cholec80.op_set_size)]
        #print(train_sets)
        test_sets = Cholec80.test_sets
        train_data = []
        op_pathes = []
        anno_files = []
        for op_set in train_sets:
            for op in os.listdir(os.path.join(utils.frames_path_Cholec80, op_set)):
                op_path = os.path.join(utils.frames_path_Cholec80, op_set, op)
                if os.path.isdir(op_path):
                    #print(op_path)
                    anno_file = os.path.join(utils.annotation_path_Cholec80, "video" + op + "-phase.txt")
                    anno_files.append(anno_file)
                    op_pathes.append(op_path)
                    #dataset = PhaseData_sequence2(op_path, anno_file,args.sequence_length, args.dataset, DataPrep3.standard_transform, sample_rate = args.sample_rate, sample_rate_s = args.sample_rate_s)
                    #train_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False, num_workers=4, pin_memory=True))
        trainset = PhaseData_sequence_list2(op_pathes, anno_files,args.sequence_length, args.dataset, DataPrep_tr.standard_transform, sample_rate = args.sample_rate, sample_rate_s = args.sample_rate_s)
        trainloader = torch.utils.data.DataLoader(trainset, batch_size=args.batch_size, shuffle=True,num_workers=4, pin_memory=True)
        log("\t training data")
        for op in op_pathes:
            log("\t\t" + op)

        test_data = []
        for op_set in test_sets:
            for op in os.listdir(os.path.join(utils.frames_path_Cholec80, op_set)):
                op_path = os.path.join(utils.frames_path_Cholec80, op_set, op)
                if os.path.isdir(op_path):
                    anno_file = os.path.join(utils.annotation_path_Cholec80, "video" + op + "-phase.txt")
                    dataset = PhaseData_sequence2(op_path, anno_file,args.sequence_length, args.dataset, DataPrep_tr.standard_transform, sample_rate = args.sample_rate, sample_rate_s = args.sample_rate_s)
                    test_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False, num_workers=4, pin_memory=True))
                    
        log("\t test data")
        for loader in test_data:
            log("\t\t" + loader.dataset.frames_path)
    
    
    log("qkv_dim = 1024")
    net = PhaseTransformerNet(num_labels = num_class, kvPr = args.vers,qkv_dim = 1024) 
    #resnet: update weights only for last layer
    for param in net.resnet.parameters():
        param.requires_grad = False
    for param in net.resnet.layer4.parameters():
        param.requires_grad = True


    net = net.to(device_gpu)
    criterion = nn.CrossEntropyLoss(size_average=False)
    optimizer = optim.Adam(filter(lambda p: p.requires_grad, net.parameters()), lr=args.lr)
    scheduler = lr_scheduler.StepLR(optimizer, step_size=10, gamma=0.1)
    log("optimizer: step_size = 10 , gamma = 0.1")
    torch.backends.cudnn.benchmark = True
    torch.backends.cudnn.enabled = True
    log("Begin training...")
    breaking_p = True
    for epoch in range(args.epochs):
        log("Epoch " + str(epoch + 1) + "...")

        train_loss = AverageMeter()
        train_acc = AverageMeter()

        net.train()
        batch_count = 0
        optimizer.zero_grad()
        #optimized = False
        for loader in trainloader:
            images, labels = loader
            batch_size = labels.size(0)
            images = images.to(device_gpu)
            labels = labels.to(device_gpu)
            output = net(images)
            loss = criterion(output, labels) / args.opt_step
            batch_count += 1
            if batch_count % args.opt_step == 0:
                loss.backward()                    
                optimizer.step()
                optimizer.zero_grad()
                #optimized = True
            else:
                loss.backward()
                #optimized = False

                    # statistics
            train_loss.update(loss.item(), batch_size)
            _, predicted = torch.max(output, 1)
            acc = (predicted == labels).sum().item() / batch_size
            train_acc.update(acc, batch_size)
        #if not optimized:
            #optimizer.step()
        # Decay Learning Rate
        scheduler.step()

        predictions_path = os.path.join(output_folder, "predictions")
        if not os.path.exists(predictions_path):
            os.makedirs(predictions_path)
        test_loss = 0
        test_accuracy = 0
        test_count = 0
        with torch.no_grad():
            net.eval()
            #print("testing...")
            for op_data in test_data:
                frames_path = op_data.dataset.frames_path
                op_id = frames_path.split('/')[-1] if frames_path.split('/')[-1] else frames_path.split('/')[-2]
                f_out = open(os.path.join(predictions_path, op_id + ".txt"), "w")
                for loader in op_data:
                    images, labels_cpu = loader
                    images = images.to(device_gpu)
                    labels = labels_cpu.to(device_gpu)
                    outputs = net(images)
                    loss = criterion(outputs, labels)

                    test_loss += loss.item()
                    _, predicted = torch.max(outputs.data, 1)
                    predicted_cpu = predicted.to(device_cpu)
                    for p, l in zip(predicted_cpu.numpy(), labels_cpu.numpy()):
                        f_out.write(str(p) + "," + str(l) + "\n")
                    test_count += labels.size(0)
                    test_accuracy += (predicted == labels).sum().item()
                f_out.close()

        summary = "train (loss %.3f, accuracy %.3f) test (loss %.3f, accuracy %.3f)" % \
                  (train_loss.avg, train_acc.avg, test_loss/test_count, test_accuracy/test_count)
        log("\t" + summary)

        if train_acc.avg > 0.999:
            model_temp = os.path.join(output_folder, "model"+ str(epoch +1) + ".pkl")
            net.save(model_temp)
            #breaking_p = False
            break

        if (epoch + 1) % args.save_freq == 0 or epoch == 0:
            log("\tSave model to %s..." % model_file)
            net.save(model_file)

    log("Done. Save final model to %s..." % model_file)
    net.save(model_file)
    f_log.close()



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Run Transformer for surgical phase recognition.")
    parser.add_argument("num_ops", type=int,
                        help="Number of labeled OP videos to use for training. up to 13 for CoBot and 20 40 or 60 for Cholec80")
    parser.add_argument("--lr", type=float, default=0.00001, help="The learning rate.")
    parser.add_argument("--batch_size", type=int, default=256, help="The batch size.") #128
    parser.add_argument("--epochs", type=int, default=60, help="The maximal number of epochs to train.")
    parser.add_argument("--save_freq", type=int, default=5,
                        help="Defines after how many epochs the current model parameters are saved.")
    parser.add_argument("--dataset", type=str, default="CoBot", choices=["CoBot","Cholec80"],
                        help="Choice of dataset")
    parser.add_argument("--sequence_length", type=int, default=5, help="sequence length")
    parser.add_argument("--vers", type=int, default=2, help="version 1 or 2")
    parser.add_argument("--opt_step", type=int, default=5,
                        help="Number of batches to accumulate before applying the optimizer.")
    parser.add_argument("--sample_rate", type=int, default=1, help="sample_rate")
    parser.add_argument("--sample_rate_s", type=int, default=2, help="sample_rate_s")
    parser.add_argument("--annotation", type=str, default="phase" , choices=["phase","step"],
                        help="phases or steps annotation")
  
    args = parser.parse_args()
    main(args)