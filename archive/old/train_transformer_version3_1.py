""" 
training script for feature vector transformer model with sequnece input and sequence output
"""
import argparse
import datetime
import os.path

import torch
import torch.nn as nn
import torch.optim as optim
import torchvision
from torch.optim import lr_scheduler

import utils
from datasets_features import PhaseData_Features_train_list,PhaseData_Features_train
from models_transformer import PhaseTransformerNet_FeatureVector_4
from models import CNNmodel
from utils import (AverageMeter, Cholec80, CoBot, DataPrep3, DataPrep_tr,
                   DataPrep,eval__)



def main(args):

    transform_train = DataPrep3.standard_transform
    transform_test = DataPrep3.standard_transform

    device_gpu = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    device_cpu = torch.device("cpu")

    trial_id = datetime.datetime.now().strftime("%Y%m%d-%H%M")
    experiment = os.path.join("Phase_segmentation_Transformer_v3_1", str(args.num_ops), trial_id)
    
    output_folder = os.path.join(utils.out_path, experiment)
    if args.annotation == "step":
        output_folder = os.path.join(utils.out_path + "_step", experiment)
    if args.dataset == "Cholec80":
       output_folder = os.path.join(utils.out_path_Cholec80, experiment) 

    os.makedirs(output_folder)
    f_log = open(os.path.join(output_folder, "log.txt"), "a")
    def log(msg):
        utils.log(f_log, msg)
    model_file = os.path.join(output_folder, "model.pkl")
    model_file_final = os.path.join(output_folder, "model_final.pkl")

    log("Run <%s> on device %s" % (experiment, str(device_gpu)))
    log("Used parameters...")
    for arg in vars(args):
        log("\t" + str(arg) + " : " + str(getattr(args, arg)))

    log("Loading data...")
    if args.dataset == "CoBot":
        log("CoBot...")
        exit()
        if args.annotation == "step":
            num_class = 9
            anno_path = "/mnt/g27prist/TCO/TCO-Studenten/CoBot/phases_and_medial_steps"
        else:
            num_class = 5
            anno_path = utils.annotation_path

        
        train_sets = CoBot.train_sets[0:args.num_ops]
        test_sets = CoBot.test_sets

        train_data = []
        op_pathes = []
        anno_files = []
        for op in train_sets:
            op_path = os.path.join(utils.frames_path, utils.CoBot.videos[op])
            if os.path.isdir(op_path):
                anno_file = os.path.join(anno_path, utils.CoBot.videos[op] + ".txt")
                anno_files.append(anno_file)
                op_pathes.append(op_path)

        trainset = PhaseData_sequence_list3(op_pathes, anno_files,args.sequence_length, args.dataset, transform_test, sample_rate = args.sample_rate, sample_rate_s = args.sample_rate_s, annotation_mode = args.annotation)
        trainloader = torch.utils.data.DataLoader(trainset, batch_size=args.batch_size, shuffle=True,num_workers=4, pin_memory=True)
        log("\t training data")
        for op in op_pathes:
            log("\t\t" + op)

        val_data = []
        for op in test_sets:
            op_path = os.path.join(utils.frames_path, utils.CoBot.videos[op])
            if os.path.isdir(op_path):
                anno_file = os.path.join(anno_path, utils.CoBot.videos[op] + ".txt")
                testset = PhaseData_sequence3(op_path, anno_file,args.sequence_length, args.dataset, transform_test, sample_rate = args.sample_rate, sample_rate_s = args.sample_rate_s, annotation_mode = args.annotation)
                val_data.append(torch.utils.data.DataLoader(testset, batch_size=args.batch_size, shuffle=False,num_workers=4, pin_memory=True))
                    
        log("\t val data")
        for loader in val_data:
            log("\t\t" + loader.dataset.frames_path)

    elif args.dataset == "Cholec80":
        log("Cholec80...")
        # Load Features
        FEATURES = torch.load("features_cholec80.pkl")

        log("Loading Features done.")
        num_class = 7
        train_sets = Cholec80.train_sets[0:(args.num_ops // Cholec80.op_set_size)]
        #print(train_sets)
        test_sets = Cholec80.test_sets
        train_data = []
        op_pathes = []
        anno_files = []
        for op_set in train_sets:
            for op in os.listdir(os.path.join(utils.frames_path_Cholec80, op_set)):
                op_path = os.path.join(utils.frames_path_Cholec80, op_set, op)
                if os.path.isdir(op_path):
                    #print(op_path)
                    anno_file = os.path.join(utils.annotation_path_Cholec80, "video" + op + "-phase.txt")
                    anno_files.append(anno_file)
                    op_pathes.append(op_path)
        trainset = PhaseData_Features_train_list(op_pathes, anno_files,features_ = FEATURES, sequence = args.sequence_length)
        trainloader = torch.utils.data.DataLoader(trainset, batch_size=args.batch_size, shuffle=True,num_workers=4, pin_memory=True)
        log("\t training data")
        for op in op_pathes:
            log("\t\t" + op)

        test_data = []
        val_data =[]
        for op_set in test_sets:
            for op in os.listdir(os.path.join(utils.frames_path_Cholec80, op_set)):
                op_path = os.path.join(utils.frames_path_Cholec80, op_set, op)
                if os.path.isdir(op_path):
                    anno_file = os.path.join(utils.annotation_path_Cholec80, "video" + op + "-phase.txt")
                    dataset = PhaseData_Features_train(op_path, anno_file,features = FEATURES[op_path], sequence = args.sequence_length)
                    
                    if len(val_data) == 8:
                        # 32 videos
                        test_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False, num_workers=4, pin_memory=True))
                    else:
                        # 8 videos
                        val_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False, num_workers=4, pin_memory=True))

        log("\t val data")
        for loader in val_data:
            log("\t\t" + loader.dataset.frames_path)
           
        log("\t test data")
        for loader in test_data:
            log("\t\t" + loader.dataset.frames_path)
    
    # create model
    model = PhaseTransformerNet_FeatureVector_4(num_labels = num_class, featurevector_dim = 2048, max_len = args.sequence_length) 
    model = model.to(device_gpu)

    criterion = nn.CrossEntropyLoss()
    #criterion_2 = nn.CrossEntropyLoss(size_average=False)
    optimizer = optim.Adam(filter(lambda p: p.requires_grad, model.parameters()), lr=args.lr)
    scheduler = lr_scheduler.StepLR(optimizer, step_size=20, gamma=0.1)
    log("optimizer: step_size = 20 , gamma = 0.1")

    torch.backends.cudnn.benchmark = True
    torch.backends.cudnn.enabled = True

    best_val = 0
    log("Begin training...")
    src_mask = None
    for epoch in range(args.epochs):
        log("Epoch " + str(epoch + 1) + "...")

        train_loss = 0
        train_accuracy = 0
        train_count = 0

        model.train()
        batch_count = 0
        optimizer.zero_grad()
        #loss = 0
        optimized = False
        if args.mask:
            src_mask = model.generate_square_subsequent_mask(args.batch_size).to(device_gpu)
        for loader in trainloader:
            #break
            images, labels = loader
            images = images.to(device_gpu)
            labels = labels.to(device_gpu)

            if args.mask and images.size(0) != args.batch_size:
                src_mask = model.generate_square_subsequent_mask(images.size(0)).to(device_gpu)
                
            output = model(images,src_mask)

            loss = criterion(output, labels) / args.opt_step
            batch_count += 1
            if batch_count % args.opt_step == 0:
                loss.backward()                    
                optimizer.step()
                optimizer.zero_grad()
                optimized = True
                #break
            else:
                loss.backward()
                optimized = False

            # statistics
            train_loss += loss.item()
            _, predicted = torch.max(output, 1)
            train_accuracy += (predicted == labels).sum().item()
            train_count += (labels.size(0) * labels.size(1))
            #break
        #if not optimized:
        #    optimizer.step()
        #    optimizer.zero_grad()
        # Decay Learning Rate
        scheduler.step()

        predictions_path = os.path.join(output_folder, "predictions_val")
        if not os.path.exists(predictions_path):
            os.makedirs(predictions_path)
        test_loss = 0
        test_accuracy = 0
        test_count = 0

        test_loss_all = 0
        test_accuracy_all = 0
        test_count_all = 0

        #Validation
        with torch.no_grad():
            model.eval()
            #print("testing...")
            for op_data in val_data:
                frames_path = op_data.dataset.frames_path
                op_id = frames_path.split('/')[-1] if frames_path.split('/')[-1] else frames_path.split('/')[-2]
                f_out = open(os.path.join(predictions_path, op_id + ".txt"), "w")
                if args.mask:
                    src_mask = model.generate_square_subsequent_mask(args.batch_size).to(device_gpu)
                for loader in op_data:
                    images,labels_cpu_  = loader
                    images = images.to(device_gpu)
                    labels_cpu = labels_cpu_[:,-1]
                    labels = labels_cpu.to(device_gpu)


                    if args.mask and images.size(0) != args.batch_size:
                        src_mask = model.generate_square_subsequent_mask(images.size(0)).to(device_gpu)

                    outputs = model(images,src_mask)
                    #print(outputs[:,-1])
                    loss = criterion(outputs[:,-1], labels)

                    test_loss += loss.item()
                    _, predicted_ = torch.max(outputs.data, 1)
                    predicted = predicted_[:,-1]
                    predicted_cpu = predicted.to(device_cpu)
                    for p, l in zip(predicted_cpu.numpy(), labels_cpu.numpy()):
                        f_out.write(str(p) + "," + str(l) + "\n")
                    test_count += labels.size(0)
                    test_accuracy += (predicted == labels).sum().item()

                    labels_all = labels_cpu_.to(device_gpu)
                    loss_all = criterion(outputs, labels_all)
                    test_loss_all += loss_all.item()
                    test_count_all += (labels.size(0) * labels_all.size(1))
                    test_accuracy_all += (predicted_ == labels_all).sum().item()
                    #print("here")
                    #break
                    #n = test_accuracy/test_count
                    #print(n)
                f_out.close()
        f1  = eval__(predictions_path)

        summary = "train (loss %.3f, accuracy %.3f) val (loss %.3f, accuracy %.3f) val_all (loss %.3f, accuracy %.3f)" % \
                  (train_loss/train_count, train_accuracy/train_count, test_loss/test_count, test_accuracy/test_count, test_loss_all/test_count_all, test_accuracy_all/test_count_all)
        log("\t" + summary +  ", F1-score: " + f1)

        act_val = test_accuracy/test_count

        if act_val > best_val:
            best_val = act_val
            log("\tSave model with best val_acc to %s..." % model_file)
            model.save(model_file)

        if (epoch + 1) % args.save_freq == 0:
            log("\tSave model to %s..." % model_file_final)
            model.save(model_file_final)

    log("Done. Save final model to %s..." % model_file_final)
    model.save(model_file_final)
    

    if dataset == "CoBot":
        f_log.close()
        return
    log("Testing with best validation model")
    model.load(model_file)

    # Testing
    predictions_path = os.path.join(output_folder, "predictions")
    if not os.path.exists(predictions_path):
        os.makedirs(predictions_path)
    with torch.no_grad():
        model.eval()
            #print("testing...")
        test_count = 0
        test_accuracy = 0
        for op_data in test_data:
            frames_path = op_data.dataset.frames_path
            op_id = frames_path.split('/')[-1] if frames_path.split('/')[-1] else frames_path.split('/')[-2]
            f_out = open(os.path.join(predictions_path, op_id + ".txt"), "w")
            if args.mask:
                src_mask = model.generate_square_subsequent_mask(args.batch_size).to(device_gpu)
            for loader in op_data:
                images, labels_cpu_ = loader
                images = images.to(device_gpu)
                labels_cpu = labels_cpu_[:,-1]
                labels = labels_cpu.to(device_gpu)
                if args.mask and images.size(0) != args.batch_size:
                    src_mask = model.generate_square_subsequent_mask(images.size(0)).to(device_gpu)
                outputs = model(images, src_mask)

                test_loss += loss.item()
                _, predicted_ = torch.max(outputs.data, 1)
                predicted = predicted_[:,-1]
                predicted_cpu = predicted.to(device_cpu)
                for p, l in zip(predicted_cpu.numpy(), labels_cpu.numpy()):
                    f_out.write(str(p) + "," + str(l) + "\n")
                test_count += labels.size(0)
                test_accuracy += (predicted == labels).sum().item()
            f_out.close()

        summary = "test ( accuracy %.3f)" % \
                  (test_accuracy/test_count)
        log("\t" + summary)

    f_log.close()



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Run Transformer for surgical phase recognition.")
    parser.add_argument("--num_ops", type=int, default = 40,
                        help="Number of labeled OP videos to use for training. up to 13 for CoBot and 20 40 or 60 for Cholec80")
    parser.add_argument("--lr", type=float, default=0.00001, help="The learning rate.")
    parser.add_argument("--batch_size", type=int, default=128, help="The batch size.") #128
    parser.add_argument("--epochs", type=int, default=60, help="The maximal number of epochs to train.")
    parser.add_argument("--save_freq", type=int, default=10,
                        help="Defines after how many epochs the current model parameters are saved.")
    parser.add_argument("--dataset", type=str, default="Cholec80", choices=["CoBot","Cholec80"],
                        help="Choice of dataset")
    parser.add_argument("--sequence_length", type=int, default=30, help="sequence length")
    parser.add_argument("--vers", type=int, default=2, help="version 1 or 2")
    parser.add_argument("--opt_step", type=int, default=5,
                        help="Number of batches to accumulate before applying the optimizer.")
    parser.add_argument("--sample_rate", type=int, default=1, help="sample_rate")
    parser.add_argument("--sample_rate_s", type=int, default=1, help="sample_rate_s")
    parser.add_argument("--annotation", type=str, default="phase" , choices=["phase","step"],
                        help="phases or steps annotation")
    parser.add_argument("--mask", type=bool, default=True,
                        help="use mask or not")
  
    args = parser.parse_args()
    main(args)
