#!/bin/bash
#SBATCH -e err-Test-newB-0-%J.err
#SBATCH -o out-Test-newB-0-%J.out
#SBATCH --time=3-00:00:00
#SBATCH --cpus-per-task=4
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --gpus=1
#SBATCH --mail-user=stefanie.krell@mailbox.tu-dresden.de
#SBATCH --mail-type=BEGIN,END,FAIL,REQUEUE,TIME_LIMIT_90
#SBATCH --mem=48G

cd /home/krellstef/master_code
python3 train_CNNLSTM_CoBot.py 14 --annotation step -m /home/krellstef/master_code/outCoBot_old/Phase_segmentation/no_pretrain/13/20201111-1316/model.pkl --sample_rate 2 --pretrainedCNNLSTM True
python3 train_CNNLSTM_CoBot.py 14 --annotation step -m /home/krellstef/master_code/outCoBot_old/Phase_segmentation/no_pretrain/13/20201111-1316/model.pkl --batch_size 512 --pretrainedCNNLSTM True