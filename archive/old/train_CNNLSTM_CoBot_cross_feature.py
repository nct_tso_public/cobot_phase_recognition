"""
train LSTM with CoBot dataset 
"""

import argparse
import datetime
import os.path

import torch
import torch.nn as nn
import torch.optim as optim
import torchvision

import utils
from datasets import MyDataSet
from models import LSTMNet
from utils import CoBot, DataPrep3
import numpy as np

def main(args):

    device_gpu = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    device_cpu = torch.device("cpu")

    trial_id = datetime.datetime.now().strftime("%Y%m%d-%H%M")
    if args.exp is not None:
        trial_id = args.exp
    experiment = os.path.join("LSTM", trial_id, str(args.fold))

    output_folder = os.path.join(utils.out_path, experiment)

    anno_path = utils.annotation_path
    if args.annotation == "phase":
        num_class = 5        
    elif args.annotation == "step":
        num_class = 5 + 4
        output_folder = os.path.join("/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps", experiment)
        step_file = []
    else:
        raise NotImplementedError("not available as annotation")

    os.makedirs(output_folder)
    f_log = open(os.path.join(output_folder, "log.txt"), "w")
    def log(msg):
        utils.log(f_log, msg)
    model_file = os.path.join(output_folder, "model.pkl")
    model_file_final = os.path.join(output_folder, "model_final.pkl")
    model_file_train = os.path.join(output_folder, "model_best_train.pkl")

    log("Run <%s> on device %s" % (experiment, str(device_gpu)))

    log("Used parameters...")
    for arg in vars(args):
        log("\t" + str(arg) + " : " + str(getattr(args, arg)))

    log("Loading data...")
    
    feature_file = torch.load(args.feature_file)

    assert(args.fold == feature_file['split'])

    train_data = []
    video_ids = sorted(feature_file['train'].keys())
    log("\t training data")
    for id in video_ids:
        log("\t" + id)
        dataset = MyDataSet(mode = 'train', feature_file = feature_file,video_id =id)
        train_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False,
                                                             num_workers=4))

    val_data = []
    video_ids = sorted(feature_file['val'].keys())
    log("\t val data")
    for id in video_ids:
        log("\t" + id)
        dataset = MyDataSet(mode = 'val', feature_file = feature_file,video_id = id)
        val_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False,
                                                             num_workers=4))
    
    test_data = []
    video_ids = sorted(feature_file['test'].keys())
    log("\t test data")
    for id in video_ids:
        log("\t" + id)
        dataset = MyDataSet(mode = 'test', feature_file = feature_file,video_id = id)
        test_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False,
                                                             num_workers=4))
                
    net = LSTMNet(num_classes = num_class)
    net.to(device_gpu)
    if args.w == 1:
        we = utils.class_weights_steps if args.annotation == "step" else utils.class_weights_phases
        print(we)
        we = torch.from_numpy(we).float()
        we = we.to(device_gpu)
        criterion = nn.CrossEntropyLoss(weight=we)
    else:
        criterion = nn.CrossEntropyLoss(size_average=False)#, weight=we)

    optimizer = optim.Adam(filter(lambda p: p.requires_grad, net.parameters()), lr=args.lr)
    torch.backends.cudnn.benchmark = True
    torch.backends.cudnn.enabled = True
    log("Begin training...")

    best_acc = 0
    one = True
    for epoch in range(args.epochs):
        log("Epoch " + str(epoch + 1) + "...")

        train_loss = 0
        train_accuracy = 0
        train_count = 0
        for op_data in train_data:
            hidden_state = net.init_hidden(device_gpu)
            optimizer.zero_grad()
            optimized = False
            loss = 0
            batch_count = 0
            if epoch < 2: 
                print(op_data.dataset.video_id)
            for loader in op_data:
                images, labels = loader
                images = images.to(device_gpu)
                labels = labels.to(device_gpu)
                outputs, hidden_state = net(images, hidden_state)
                loss += criterion(outputs, labels)
                batch_count += 1
                if batch_count % args.opt_step == 0:
                    loss.backward()
                    optimizer.step()
                    optimized = True
                    optimizer.zero_grad()
                    h, c = hidden_state
                    h = h.detach()
                    c = c.detach()
                    hidden_state = (h, c)
                else:
                    loss.backward(retain_graph=True)
                    optimized = False

                train_loss += loss.item()
                _, predicted = torch.max(outputs.data, 1)
                train_accuracy += (predicted == labels).sum().item()
                train_count += labels.size(0)

                if batch_count % args.opt_step == 0:
                    loss = 0

            if not optimized:
                optimizer.step()

        
        val_loss = 0
        val_accuracy = 0
        val_count = 0
        with torch.no_grad():
            for op_data in val_data:
                hidden_state = net.init_hidden(device_gpu)
                for loader in op_data:
                    images, labels_cpu = loader
                    images = images.to(device_gpu)
                    labels = labels_cpu.to(device_gpu)
                    outputs, hidden_state = net(images, hidden_state)
                    loss = criterion(outputs, labels)

                    val_loss += loss.item()
                    _, predicted = torch.max(outputs.data, 1)
                    val_count += labels.size(0)
                    val_accuracy += (predicted == labels).sum().item()

        summary = "train (loss %.3f, accuracy %.3f) val (loss %.3f, accuracy %.3f)" % \
                  (train_loss/train_count, train_accuracy/train_count, val_loss/val_count, val_accuracy/val_count)
        log("\t" + summary)

        if (epoch + 1) % args.save_freq == 0:
            log("\tSave model to %s..." % model_file_final)
            net.save(model_file_final)
        act_acc = val_accuracy/val_count
        if best_acc < act_acc:
            best_acc = act_acc
            log("\tSave best val model to %s..." % model_file)
            net.save(model_file)

        if one and train_accuracy/train_count > 0.999:
            net.save(model_file_train)
            one = False


    log("Done. Save final model to %s..." % model_file_final)
    net.save(model_file_final)
    
    log("Update predictions with best val model")
    net.load(model_file)

    predictions_path = os.path.join(output_folder, "predictions")
    if not os.path.exists(predictions_path):
        os.makedirs(predictions_path)
    test_accuracy = 0
    test_count = 0
    with torch.no_grad():
        for op_data in test_data:
            f_out = open(os.path.join(predictions_path, op_data.dataset.video_id + ".txt"), "w")

            hidden_state = net.init_hidden(device_gpu)
            for loader in op_data:
                images, labels_cpu = loader
                images = images.to(device_gpu)
                labels = labels_cpu.to(device_gpu)
                outputs, hidden_state = net(images, hidden_state)
                _, predicted = torch.max(outputs.data, 1)
                predicted_cpu = predicted.to(device_cpu)
                for p, l in zip(predicted_cpu.numpy(), labels_cpu.numpy()):
                    f_out.write(str(p) + "," + str(l) + "\n")
                test_count += labels.size(0)
                test_accuracy += (predicted == labels).sum().item()
            f_out.close()
    log('best Model: Test Accuracy : %.3f ' % (test_accuracy/test_count))
    f_log.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Finetune a feauture LSTM for surgical phase recognition.")
    parser.add_argument("--feature_file", type=str, default=0, help="feature file to use")
    parser.add_argument("--lr", type=float, default=0.0001, help="The learning rate.")
    parser.add_argument("--batch_size", type=int, default=40000, help="The batch size.") #128
    parser.add_argument("--opt_step", type=int, default=1,
                        help="Number of batches to accumulate before applying the optimizer.")
    parser.add_argument("--epochs", type=int, default=200, help="The maximal number of epochs to train.")
    parser.add_argument("--save_freq", type=int, default=10,
                        help="Defines after how many epochs the current model parameters are saved.")
    parser.add_argument("--annotation", type=str, default="phase" , choices=["phase","step"],
                        help="phases or steps annotation")
    parser.add_argument("--exp", type=str, default=None, help=".")
    parser.add_argument("--pretrainedCNNLSTM", type=bool, default=False , help="if model is defined use model as LSTM")
    parser.add_argument("--fold", type=int, default=1, choices = [1,2,3,4], help=".")
    parser.add_argument("--w", type=int, default=0, choices = [0,1], help=".")
    args = parser.parse_args()
    main(args)
