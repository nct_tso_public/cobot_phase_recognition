

import numpy as np
import argparse

import torch
import torch.nn as nn
import torch.optim as optim


import os.path
import datetime
import argparse
import torchvision

from datasets import PhaseData, PhaseData_CoBot
from models import PhaseNet, CNNmodel
import utils
from utils import Cholec80, DataPrep3, AverageMeter, CoBot, DataPrepCoBot
from datasets import PhaseData_CoBot, PhaseData_CoBot_old
from utils import CoBot, DataPrep3

def _fmt(m):
    return "%2.1f" % (m * 100)



# Cholec80 Vortraining: /home/krellstef/master_code/outCholec80/Phase_segmentation/no_pretrain/40/20201031-1724/model.pkl

def main(args):
    transform_train = DataPrep3.standard_transform
    transform_test = DataPrep3.standard_transform

    device_gpu = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    device_cpu = torch.device("cpu")

       

    anno_path = utils.annotation_path
    if args.annotation == "phase":
        num_class = 5        
    elif args.annotation == "step":
        num_class = 5 + 4
        #output_folder = os.path.join("./outCoBot_steps", experiment)
        step_file = []
    else:
        raise NotImplementedError("not available as annotation")

    print("Loading data...")
    predictions_path = os.path.join(args.dir, "predictions")

    test_sets = CoBot.videos_test

    test_data = []
    for op in test_sets:
        op_path = os.path.join(utils.frames_path, op)
        if os.path.isdir(op_path):
            step_file = None
            if args.annotation == "step":
                step_file = os.path.join(utils.annotation_path_step, op + "step.txt")
            anno_file = os.path.join(anno_path, op + ".txt")
            #dataset = PhaseData_CoBot_old(op_path, anno_file, DataPrep3.width, DataPrep3.height, transform_test,sample_rate = args.sample_rate)
            dataset = PhaseData_CoBot(op_path, anno_file, DataPrep3.width, DataPrep3.height, transform_test,annotation = args.annotation, step_file = step_file, sample_rate = args.sample_rate)
            test_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False,
                                                             num_workers=4))

    net = PhaseNet(num_classes = num_class)

    net.to(device_gpu)
    model_file = os.path.join(args.dir, args.model)
    net.load(model_file)

    test_accuracy = 0
    test_count = 0
    with torch.no_grad():
        for op_data in test_data:
            frames_path = op_data.dataset.frames_path
            op_id = frames_path.split('/')[-1] if frames_path.split('/')[-1] else frames_path.split('/')[-2]
            f_out = open(os.path.join(predictions_path, op_id + ".txt"), "w")

            hidden_state = net.init_hidden(device_gpu)
            for loader in op_data:
                images, labels_cpu = loader
                images = images.to(device_gpu)
                labels = labels_cpu.to(device_gpu)
                outputs, hidden_state = net(images, hidden_state)
                _, predicted = torch.max(outputs.data, 1)
                predicted_cpu = predicted.to(device_cpu)
                for p, l in zip(predicted_cpu.numpy(), labels_cpu.numpy()):
                    f_out.write(str(p) + "," + str(l) + "\n")
                test_count += labels.size(0)
                test_accuracy += (predicted == labels).sum().item()
            f_out.close()
    print('best Model: Val Accuracy : %.3f ' % (test_accuracy/test_count))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Evaluate phase segmentation results.")
    parser.add_argument("dir", type=str,
                        help="...")
    parser.add_argument("--model", type=str, default = "model.pkl",
                        help="model...")
    parser.add_argument("--batch_size", type=int, default=128, help="The batch size.")
    parser.add_argument("--sample_rate", type=int, default=1,
                        help="Defines in which rate the pictures are taken 1-> every pic 2-> every 2. pic.")
    parser.add_argument("--annotation", type=str, default="phase" , choices=["phase","step"],
                        help="phases or steps annotation")
    args = parser.parse_args()
    main(args)