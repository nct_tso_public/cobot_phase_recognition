#from models import ConvLSTM_PhaseNet, CLSTM, weights_init
#from models_transformer import PhaseTransformerNet_FeatureVector, PhaseTransformerNet_FeatureVector_2
#from models_feedback_transformer import	FeedbackTransformer,FeedbackTransformerImageFeatures
import torch
import numpy as np
import torch.nn as nn
from torch.autograd import Variable

import torch.nn.functional as F

#FEATURES = torch.load("features_cholec80.pkl")
x = torch.tensor([1, 2, np.nan])
if torch.isnan(x).any():
    print("Nan")
else:
    print("bib")


print("True")
exit()

data = torch.randn((5,4))
print(data)
print(F.softmax(data, dim=0))
print(F.softmax(data, dim=0).sum())  # Sums to 1 because it is a distribution!
print(F.log_softmax(data, dim=0))


exit()
model = FeedbackTransformer(
    num_tokens = 5994,           # number of tokens
    dim = 2048,                    # dimension
    depth = 2,                    # depth
    seq_len = 1,                  # the sequence length of each segment or window
    mem_len = 256,                # length of the memory buffer
    dim_head = 64,                # dimension of each head
    heads = 8,                    # number of heads
    attn_dropout = 0.1,           # attention dropout
    ff_dropout = 0.1              # feedforward dropout
).cuda()

x = torch.randint(0, 2048, (1, 5400)).cuda()


#for x in x.split(301, dim = 1):
#            print(x.size())

print(x.size())
x = model(x)  # 2, 64
print(x.size())
exit()

'''
filter_size=5
batch_size=4
#shape=(216,384)#H,W
shape=(216,216)
inp_chans=3
nlayers=1
seq_len=5
num_classes = 6
input = Variable(torch.rand(batch_size,seq_len,inp_chans,shape[0],shape[1])).cuda()

mod = PhaseTransformerNet(input_dim = shape,num_labels = num_classes)
mod.cuda()

x = mod(input)
print(x.size())'''


import numpy as np


predicted = torch.tensor([[1,2],[1,3]])
print(predicted)
predicted[-1,-1] = 4
print(predicted[:,-1])
print(predicted)
labels = torch.tensor([[1,1],[1,1]])
print(labels)
gg = (predicted == labels).sum().item()
print(gg)
exit()





z = torch.rand(5)
new = []
new.append(z)
new.append(z)
x = torch.stack(new)

print(x.size())
exit()
my_list = [1, 2, 3, 4, 5, 6, 7, 8]
l= my_list[0:2]
print("List to array: ")
print(np.asarray(my_list))
my_tuple = [[8, 4, 6], [1, 2, 3]]
print("Tuple to array: ")
x = np.asarray(my_tuple)
print(x)
print(x.shape)

exit()
filter_size=5
batch_size=2
#shape=(216,384)#H,W
shape=(216,216)
inp_chans=3
nlayers=1
seq_len=5
num_classes = 7
criterion = nn.CrossEntropyLoss(size_average=False)
featurevector_dim = 2048
input = Variable(torch.rand(batch_size,seq_len,3,216,384))
print(input.size())
mod = PhaseTransformerNet_FeatureVector_2(num_labels= num_classes,featurevector_dim=featurevector_dim)
x = mod(input)
print(x.size())
label = Variable(torch.zeros(batch_size,seq_len))
loss = criterion(x,label)
print(loss)
'''
import utils
import os.path
f_log = open(os.path.join("/home/krellstef/master_code", "logstuff_cluster.txt"), "a")
def log(msg):
    utils.log(f_log, msg)
log("Hello")
f_log.close()'''
'''
num_features=64 #Number of channels of hidden state.
filter_size=5
batch_size=1
shape=(224,224)#H,W
inp_chans=3
nlayers=1
seq_len=1
num_classes = 6
#If using this format, then we need to transpose in CLSTM
input = Variable(torch.rand(batch_size,seq_len,inp_chans,shape[0],shape[1])).cuda()

conv1 =  nn.Conv2d(64, 128, kernel_size=3,padding=1)
conv1.cuda()
conv2 =  nn.Conv2d(128, 256, kernel_size=3, padding=1)
conv2.cuda()
avgpool = nn.AdaptiveAvgPool2d((6, 6))
avgpool.cuda()
classifier = nn.Linear(256 * 6 * 6, num_classes)
classifier.cuda()


#conv_lstm_new= ConvLSTM_PhaseNet(shape)
conv_lstm=CLSTM(shape, inp_chans, filter_size, num_features,nlayers)
conv_lstm.apply(weights_init)
conv_lstm.cuda()
#conv_lstm_new.cuda()


#print('convlstm phase:'+ str(conv_lstm_new))
#hidden_state=conv_lstm_new.init_hidden(batch_size)
#x, h =conv_lstm_new(input,hidden_state)
#print('out shape' + str(x.size()))
#exit()
#print('convlstm module:'+ str(conv_lstm))


print('params:')
params=conv_lstm.parameters()
for p in params:
   print('param ' + str(p.size()))
   print('mean ' + str(torch.mean(p)))


hidden_state=conv_lstm.init_hidden(batch_size)
print('hidden_h shape ' + str(len(hidden_state)))
print('hidden_h shape ' + str(hidden_state[0][0].size()))
out=conv_lstm(input,hidden_state)
o = out[1]
print('out shape' + str(out[1].size()))
#o = o.view(1,64,224,224)
o2 = conv1(o)
print(o2.size())
o3 = conv2(o2)#
print(o3.size())

x = avgpool(o3)
print(x.size())
x = torch.flatten(x,1)
print(x.size())
x = classifier(x)
print(x.size())


print('len hidden ' + str(len(out[0])))
print('next hidden' + str(out[0][0][0].size()))
print('convlstm dict'+ str(conv_lstm.state_dict().keys()))


L=torch.sum(out[1])
L.backward()'''


