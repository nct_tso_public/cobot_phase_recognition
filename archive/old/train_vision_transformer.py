"""
    train vision transformer with Cholec80
"""

import argparse
import datetime
import os.path

import torch
import torch.nn as nn
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms
from linformer import Linformer
from torch.optim import lr_scheduler
from vit_pytorch.efficient import ViT

import utils
from datasets import PhaseData, PhaseDataList
from utils import AverageMeter, Cholec80, DataPrep3


def main(args):

    image_size = 224
    standard_transform = transforms.Compose(
        [transforms.Resize(size = (image_size,image_size)),
        transforms.ToTensor(),
         transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])])
    transform_train = standard_transform
    transform_test = standard_transform

    device_gpu = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    device_cpu = torch.device("cpu")

    num_train_op_sets = args.num_ops // Cholec80.op_set_size

    num_class = 7
    
    assert(image_size % args.patch == 0)

    pretrained_model_file = None
    pretrain_method = "no_pretrain"
    
    trial_id = datetime.datetime.now().strftime("%Y%m%d-%H%M")
    experiment = os.path.join("Phase_segmentation_VisionTransformer", pretrain_method, str(args.num_ops), trial_id)
    
    output_folder = os.path.join(utils.out_path_Cholec80, experiment)
    os.makedirs(output_folder)
    f_log = open(os.path.join(output_folder, "log.txt"), "a")
    def log(msg):
        utils.log(f_log, msg)
    model_file = os.path.join(output_folder, "model.pth")

    log("Run <%s> on device %s" % (experiment, str(device_gpu)))
    if pretrained_model_file is not None:
        log("Using pretrained FeatureNet: " + pretrained_model_file)
    log("Used parameters...")
    for arg in vars(args):
        log("\t" + str(arg) + " : " + str(getattr(args, arg)))
    log("CHolec80...")
    log("Loading data...")
    train_sets = Cholec80.train_sets[0:num_train_op_sets]
    test_sets = Cholec80.test_sets

    train_data = []
    op_pathes = []
    anno_files = []

    f_path = utils.frames_path_Cholec80
    a_path = utils.annotation_path_Cholec80

    for op_set in train_sets:
        for op in os.listdir(os.path.join(f_path, op_set)):
            op_path = os.path.join(f_path, op_set, op)
            if os.path.isdir(op_path):
                anno_file = os.path.join(a_path, "video" + op + "-phase.txt")
                op_pathes.append(op_path)
                anno_files.append(anno_file)
    dataset = PhaseDataList(op_pathes, anno_files, image_size, image_size, transform_train)
    trainloader = torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False,num_workers=4)
    log("\t training data")
    for path in op_pathes:
        log("\t\t" + path)
    num = 0
    val_data = []
    for op_set in test_sets:
        for op in os.listdir(os.path.join(f_path, op_set)):
            op_path = os.path.join(f_path, op_set, op)
            if os.path.isdir(op_path):
                anno_file = os.path.join(a_path, "video" + op + "-phase.txt")
                dataset = PhaseData(op_path, anno_file, image_size, image_size, transform_test)
                val_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False,
                                                             num_workers=4))
                num +=1
                if num == 4:
                    break
    log("\t val data")
    for loader in val_data:
        log("\t\t" + loader.dataset.frames_path)


    
    patches_per_dim = image_size // args.patch
    efficient_transformer = Linformer(dim = 1024,
    seq_len = (patches_per_dim*patches_per_dim) + 1,  # 64 x 64 patches + 1 cls token
    depth = 6,
    heads = 8, #8
    k = 256
    )
    model = ViT(dim = 1024,image_size = image_size,patch_size = args.patch ,num_classes = num_class, transformer = efficient_transformer)
    print(model)


    model = model.to(device_gpu)
    criterion = nn.CrossEntropyLoss()
    #optimizer = optim.Adam(filter(lambda p: p.requires_grad, model.parameters()), lr=args.lr)
    optimizer = optim.Adam(model.parameters(), lr=args.lr)
    scheduler = lr_scheduler.StepLR(optimizer, step_size=5, gamma=0.75)
    log("lr_scheduler.StepLR(optimizer, step_size=50, gamma=0.1)")
    log("Begin training...")
    torch.backends.cudnn.benchmark = True
    torch.backends.cudnn.enabled = True
    total_samples = len(trainloader.dataset)
    for epoch in range(args.epochs):
        log("Epoch " + str(epoch + 1) + "...")
        # zero lodd and acc
        train_loss = AverageMeter()
        train_acc = AverageMeter()

        
        # Print Learning Rate
        print('Epoch:', (epoch + 1),'LR:', scheduler.get_lr())
        model.train()
        for i, (data, target) in enumerate(trainloader):
            batch_size = target.size(0)
            data = data.to(device_gpu)
            target = target.to(device_gpu)

            # zero the parameter gradients
            optimizer.zero_grad()

            #forward
            output = model(data)
            loss = criterion(output, target)
            # backward + optimize
            loss.backward()
            optimizer.step()
                
            # statistics
            train_loss.update(loss.item(), batch_size)                
            #predicted = torch.nn.Softmax(dim=1)(output)
            _, predicted = torch.max(output, 1)
            acc = (predicted == target).sum().item() / batch_size
            train_acc.update(acc, batch_size)

            if i % (10 * args.batch_size) == 0:
                print('[' +  '{:5}'.format(i * len(data)) + '/' + '{:5}'.format(total_samples) +
                      ' (' + '{:3.0f}'.format(10 * args.batch_size * i / len(trainloader)) + '%)]  Loss: ' +
                      '{:6.4f}'.format(loss.item()))

        # Decay Learning Rate
        scheduler.step()
        log("Epoch {}: Train loss: {train_loss.avg:.4f} Train acc: {train_acc.avg:.3f}"
            .format(epoch + 1, train_loss=train_loss, train_acc=train_acc))
        log("testing with val set...")
        correct = 0
        total = 0
        model.eval()
        with torch.no_grad():
            for v in val_data:
                for loader in v:
                    data,target = loader
                    batch_size = target.size(0)
                    data = data.to(device_gpu)
                    target = target.to(device_gpu)
                    output = model(data)
                    #predicted = torch.nn.Softmax(dim=1)(output)
                    _, predicted = torch.max(output, 1)
                    correct += (predicted == target).sum().item()
                    total += batch_size
        if (epoch + 1) % args.save_freq == 0 or epoch == 0:
            model_temp = os.path.join(output_folder, "model"+ str(epoch +1) + ".pkl")
            log("\tSave model to %s..." % model_temp)
            torch.save(model.state_dict(), model_temp)
        log('Val Accuracy : %d %%' % (100 * correct / total))

        
    log("Done. Save final model to %s..." % model_file)
    torch.save(model.state_dict(), model_file)
    f_log.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Run VisionTransformer for surgical phase recognition.")
    parser.add_argument("num_ops", type=int, choices=[20, 40, 60],
                        help="Number of labeled OP videos to use for training.")
    parser.add_argument("--lr", type=float, default=0.003, help="The learning rate.")
    parser.add_argument("--batch_size", type=int, default=256, help="The batch size.") #128
    parser.add_argument("--epochs", type=int, default=200, help="The maximal number of epochs to train.")
    parser.add_argument("--patch", type=int, default=32, help="patchsize for ViT.")
    parser.add_argument("--save_freq", type=int, default=5,
                        help="Defines after how many epochs the current model parameters are saved.")
    args = parser.parse_args()
    main(args)
