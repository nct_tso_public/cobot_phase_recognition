"""
different pytorch transformer models 
"""

import copy
import gc
import math
from typing import Any, Optional
import sys
import os

import numpy as np
import torch
import torch.nn as nn
import torchvision.models.resnet
from torch import Tensor
from torch.autograd import Variable
from torch.nn import Module, MultiheadAttention
from torch.nn import functional as F
from torch.nn.init import constant, normal

from models import CNNmodel
from utils import debug_memory

#https://towardsdatascience.com/how-to-code-the-transformer-in-pytorch-24db27c8f9ec

def attention(q, k, value, d_k, mask=None, dropout=None):
        output = torch.zeros(q.size()).cuda()
        for x in range(k.size(5)):
            for y in range(k.size(4)):
                for i in range(k.size(2)):
                    k_new = k[:,:,i,:,y,x]
                    k_new.unsqueeze_(2)
                    v_new = value[:,:,i,:,y,x]
                    v_new.unsqueeze_(2)
                    scores = torch.matmul(q, k_new.transpose(-2, -1)) /  math.sqrt(d_k)
                    if mask is not None:
                        mask = mask.unsqueeze(1)
                        scores = scores.masked_fill(mask == 0, -1e9)
                        scores = F.softmax(scores, dim=-1)
                    
                    if dropout is not None:
                        scores = dropout(scores)
                    
                    scores.cuda()
                    v_new.cuda()
                    output = output + torch.matmul(scores, v_new)
        return output

class MultiHeadAttentionCustom(nn.Module):
    def __init__(self, heads, d_model, dropout = 0.1):
        super().__init__()
        
        self.d_model = d_model
        self.d_k = d_model // heads
        self.h = heads
        
        #self.q_linear = nn.Linear(d_model, d_model)
        #self.v_linear = nn.Linear(d_model, d_model)
        #self.k_linear = nn.Linear(d_model, d_model)
        self.dropout = nn.Dropout(dropout)
        self.out = nn.Linear(d_model, d_model)
    
    def forward(self, q, k, v, mask=None):
        #batch_size * seq_len * d_model * w * h 
        bs = q.size(0)
        
        # perform linear operation and split into h heads
        
        k = k.view(bs, -1, self.h, self.d_k, k.size(3), k.size(4))
        q = q.view(bs, -1, self.h, self.d_k)
        v = v.view(bs, -1, self.h, self.d_k, v.size(3), v.size(4))
        
        # transpose to get dimensions bs * h * sl * d_model x additional dim
       
        k = k.transpose(1,2)
        q = q.transpose(1,2)
        v = v.transpose(1,2)# calculate attention using function we will define next
        scores = attention(q, k, v, self.d_k, mask, self.dropout)
        
        # concatenate heads and put through final linear layer
        concat = scores.transpose(1,2).contiguous()\
        .view(bs, -1, self.d_model)
        
        output = self.out(concat)
    
        return output
class MultiHeadAttentionCustom2(nn.Module):
    def __init__(self, heads, d_model, dropout = 0.1):
        super().__init__()
        
        self.d_model = d_model
        self.d_k = d_model // heads
        self.h = heads
        
        self.q_linear = nn.Linear(d_model, d_model)
        self.v_linear = nn.Linear(d_model, d_model)
        self.k_linear = nn.Linear(d_model, d_model)
        self.dropout = nn.Dropout(dropout)
        self.out = nn.Linear(d_model, d_model)
    
    def forward(self, q, k, v, mask=None):
        #kv: batch_size * seq_len * d_model * w * h 
        #q: batch_size * seq_len * d_model
        bs = q.size(0)
        
        # perform linear operation and split into h heads
        q = self.q_linear(q).view(bs, -1, self.h, self.d_k)        

        k = self.k_linear(k.permute(0,1,3,4,2)).permute(0,1,4,2,3)        
        k = k.view(bs, -1, self.h, self.d_k, k.size(3), k.size(4))

        v = self.v_linear(v.permute(0,1,3,4,2)).permute(0,1,4,2,3)
        v = v.view(bs, -1, self.h, self.d_k, v.size(3), v.size(4))
        
        # transpose to get dimensions bs * h * sl * d_model x additional dim       
        k = k.transpose(1,2)
        q = q.transpose(1,2)
        v = v.transpose(1,2)# calculate attention using function we will define next
        scores = attention(q, k, v, self.d_k, mask, self.dropout)
        
        # concatenate heads and put through final linear layer
        concat = scores.transpose(1,2).contiguous()\
        .view(bs, -1, self.d_model)
        
        output = self.out(concat)
    
        return output
    
## source: https://pytorch.org/tutorials/beginner/transformer_tutorial.html
## source2: https://buomsoo-kim.github.io/attention/2020/04/22/Attention-mechanism-20.md/

def _get_activation_fn(activation):
    if activation == "relu":
        return F.relu
    elif activation == "gelu":
        return F.gelu

    raise RuntimeError("activation should be relu/gelu, not {}".format(activation))

class TransformerEncoderLayerCustom(Module):
    def __init__(self, d_model, nhead, dim_feedforward=1024, dropout=0.1, activation="relu"):
        super(TransformerEncoderLayerCustom, self).__init__()
        #self.self_attn = nn.MultiheadAttention(d_model, nhead, dropout=dropout)
        self.self_attn = MultiHeadAttentionCustom2(heads = nhead, d_model = d_model, dropout = dropout)
        # Implementation of Feedforward model
        self.linear1 = nn.Linear(d_model, dim_feedforward)
        self.dropout = nn.Dropout(dropout)
        self.linear2 = nn.Linear(dim_feedforward, d_model)

        self.norm1 = nn.LayerNorm(d_model)
        self.norm2 = nn.LayerNorm(d_model)
        self.dropout1 = nn.Dropout(dropout)
        self.dropout2 = nn.Dropout(dropout)

        self.activation = _get_activation_fn(activation)
        
    def __setstate__(self, state):
        if 'activation' not in state:
            state['activation'] = F.relu
        super(TransformerEncoderLayerCustom, self).__setstate__(state)

    def forward(self, src, kv, src_mask=None, src_key_padding_mask=None):
        # type: (Tensor, Optional[Tensor], Optional[Tensor]) -> Tensor
        #src2, weights = self.self_attn(src, kv, kv, attn_mask=src_mask,
                              #key_padding_mask=src_key_padding_mask)
        src2 = self.self_attn(src, kv, kv, mask=src_mask)
        src = src + self.dropout1(src2)
        src = self.norm1(src)
        src2 = self.linear2(self.dropout(self.activation(self.linear1(src))))
        src = src + self.dropout2(src2)
        src = self.norm2(src)
        return src

def _get_clones(module, N):
    return nn.ModuleList([copy.deepcopy(module) for i in range(N)])

class TransformerEncoderCustom(Module):
    __constants__ = ['norm']
    def __init__(self, encoder_layer, num_layers, norm=None):
        super(TransformerEncoderCustom, self).__init__()
        self.layers = _get_clones(encoder_layer, num_layers)
        self.num_layers = num_layers
        self.norm = norm

    def forward(self, src, kv, mask=None, src_key_padding_mask=None):
        # type: (Tensor, Optional[Tensor], Optional[Tensor]) -> Tensor
        output = src
        #weights = []
        for mod in self.layers:
            output = mod(output,kv, src_mask=mask, src_key_padding_mask=src_key_padding_mask)
            #weights.append(weight)

        if self.norm is not None:
            output = self.norm(output)
        return output#, weights

class PositionalEncoding1D(nn.Module):
    def __init__(self, channels):
        """
        :param channels: The last dimension of the tensor you want to apply pos emb to.
        """
        super(PositionalEncoding1D, self).__init__()
        self.channels = channels
        inv_freq = 1. / (10000 ** (torch.arange(0, channels, 2).float() / channels))
        self.register_buffer('inv_freq', inv_freq)

    def forward(self, tensor):
        """
        :param tensor: A 3d tensor of size (batch_size, x, ch)
        :return: Positional Encoding Matrix of size (batch_size, x, ch)
        """
        if len(tensor.shape) != 3:
            raise RuntimeError("The input tensor has to be 3d!")
        _, x, orig_ch = tensor.shape
        pos_x = torch.arange(x, device=tensor.device).type(self.inv_freq.type())
        sin_inp_x = torch.einsum("i,j->ij", pos_x, self.inv_freq)
        emb_x = torch.cat((sin_inp_x.sin(), sin_inp_x.cos()), dim=-1)
        emb = torch.zeros((x,self.channels),device=tensor.device).type(tensor.type())
        emb[:,:self.channels] = emb_x

        return emb[None,:,:orig_ch]

##https://github.com/tatp22/multidim-positional-encoding/blob/master/positional_encodings/positional_encodings.py        
class PositionalEncoding3D(nn.Module):
    def __init__(self, channels):
        """
        :param channels: The last dimension of the tensor you want to apply pos emb to.
        """
        super(PositionalEncoding3D, self).__init__()
        channels = int(np.ceil(channels/3))
        if channels % 2:
            channels += 1
        self.channels = channels
        inv_freq = 1. / (10000 ** (torch.arange(0, channels, 2).float() / channels))
        self.register_buffer('inv_freq', inv_freq)

    def forward(self, tensor):
        """
        :param tensor: A 5d tensor of size (batch_size, x, y, z, ch)
        :return: Positional Encoding Matrix of size (batch_size, x, y, z, ch)
        """
        if len(tensor.shape) != 5:
            raise RuntimeError("The input tensor has to be 5d!")
        _, x, y, z, orig_ch = tensor.shape
        pos_x = torch.arange(x, device=tensor.device).type(self.inv_freq.type())
        pos_y = torch.arange(y, device=tensor.device).type(self.inv_freq.type())
        pos_z = torch.arange(z, device=tensor.device).type(self.inv_freq.type())
        sin_inp_x = torch.einsum("i,j->ij", pos_x, self.inv_freq)
        sin_inp_y = torch.einsum("i,j->ij", pos_y, self.inv_freq)
        sin_inp_z = torch.einsum("i,j->ij", pos_z, self.inv_freq)
        emb_x = torch.cat((sin_inp_x.sin(), sin_inp_x.cos()), dim=-1).unsqueeze(1).unsqueeze(1)
        emb_y = torch.cat((sin_inp_y.sin(), sin_inp_y.cos()), dim=-1).unsqueeze(1)
        emb_z = torch.cat((sin_inp_z.sin(), sin_inp_z.cos()), dim=-1)
        emb = torch.zeros((x,y,z,self.channels*3),device=tensor.device).type(tensor.type())
        emb[:,:,:,:self.channels] = emb_x
        emb[:,:,:,self.channels:2*self.channels] = emb_y
        emb[:,:,:,2*self.channels:] = emb_z

        return emb[None,:,:,:,:orig_ch]

class PositionalEncodingPermute3D(nn.Module):
    def __init__(self, channels):
        """
        Accepts (batchsize, ch, x, y, z) instead of (batchsize, x, y, z, ch)        
        """
        super(PositionalEncodingPermute3D, self).__init__()
        self.penc = PositionalEncoding3D(channels)

    def forward(self, tensor):
        tensor = tensor.permute(0,2,3,4,1)
        enc = self.penc(tensor)
        return enc.permute(0,4,1,2,3)

class PositionalEncoding(nn.Module):
    def __init__(self, d_model, dropout=0.1, max_len=5000):
        super(PositionalEncoding, self).__init__()
        self.dropout = nn.Dropout(p=dropout)
        pe = torch.zeros(max_len, d_model)
        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, d_model, 2).float() * (-torch.log(torch.tensor(10000.0)) / d_model))
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0).transpose(0, 1)
        self.register_buffer('pe', pe)

    def forward(self, x):
        x = x + self.pe[:x.size(0), :]
        return self.dropout(x)

class TransformerNet(nn.Module):
  def __init__(self, num_vocab, embedding_dim, hidden_size, nheads, n_layers, max_len, num_labels, dropout):
    super(TransformerNet, self).__init__()
    # embedding layer
    self.embedding = nn.Embedding(num_vocab, embedding_dim)
    
    # positional encoding layer
    self.pe = PositionalEncoding(embedding_dim, max_len = max_len)

    # encoder  layers
    enc_layer = nn.TransformerEncoderLayer(embedding_dim, nheads, hidden_size, dropout)
    self.encoder = nn.TransformerEncoder(enc_layer, num_layers = n_layers)

    # final dense layer
    self.dense = nn.Linear(embedding_dim*max_len, num_labels)
    self.log_softmax = nn.LogSoftmax()

  def forward(self, x):
    x = self.embedding(x).permute(1, 0, 2)
    x = self.pe(x)
    x = self.encoder(x)
    x = x.reshape(x.shape[1], -1)
    x = self.dense(x)
    return x
    
#model = TransformerNet(VOCAB_SIZE, EMBEDDING_DIM, HIDDEN_SIZE, NUM_HEADS, NUM_LAYERS, MAX_REVIEW_LEN, NUM_LABELS, DROPOUT).to(DEVICE)
#criterion = nn.CrossEntropyLoss()
#optimizer = torch.optim.Adam(model.parameters(), lr = LEARNING_RATE)

##TODOS # Input batch x t x c x h x w
def splitInput(x , dim_split = 1): 
    return torch.split(x, x.size(dim_split) - 1 , dim = dim_split )

class PhaseTransformerNet(nn.Module):
  def __init__(self, input_dim = (224,224), qkv_dim = 1024, nheads = 2, n_layers = 2,  num_labels = 6, dropout = 0.1, kvPr = 2, hidden_size = 512):
    """Lapformer

      Args:
          input_dim (tuple, optional): quadratic image size. Defaults to (224,224).
          qkv_dim (int, optional): [description]. Defaults to 1024.
          nheads (int, optional): numbe of heads. Defaults to 2.
          n_layers (int, optional): number of layers. Defaults to 2.
          num_labels (int, optional): number of classes. Defaults to 6.
          dropout (float, optional): dropout rate. Defaults to 0.1.
          kvPr (int, optional): projection of the key , either 1 or 2. Defaults to 2.
          hidden_size (int, optional): hidden dim of linear alyer. Defaults to 512.

      Raises:
          RuntimeError: occurs when kvpr is not 1 or 2
    """
    super(PhaseTransformerNet, self).__init__()
    # resnet50 layer
    self.resnet = torchvision.models.resnet50(pretrained=True)
    # embedding layer
    #self.embedding = nn.Embedding(num_vocab, embedding_dim) # do I really need that ??
    
    # positional encoding layer
    h1 = int(round(input_dim[0] / 32))
    w1 = int(round(input_dim[1] / 32))
    input_channel = 2048
    self.pe = PositionalEncodingPermute3D(input_channel)  # max_len ????
   
    self.QPr = nn.Conv2d(input_channel, qkv_dim, kernel_size=(h1,w1), padding=0)
    if kvPr == 1:
        self.KVPr1 = nn.Conv2d(input_channel, qkv_dim, kernel_size=(h1,w1), padding=0)
        #self.KVPr1 = nn.Conv3d(input_channel, qkv_dim, kernel_size=(1,h1,w1), padding=0)
    elif kvPr == 2: 
        self.KVPr1 = nn.Conv2d(input_channel, qkv_dim, kernel_size=(1,1), padding=0)
        #self.KVPr1 = nn.Conv3d(input_channel, qkv_dim, kernel_size=(1,1,1), padding=0)
    else:
        raise RuntimeError("Not an option!")

    # encoder  layers
    enc_layer = TransformerEncoderLayerCustom(d_model = qkv_dim, nhead = nheads,dim_feedforward= hidden_size, dropout = dropout)
    self.encoder = TransformerEncoderCustom(enc_layer, num_layers = n_layers)


    # final dense layer
    self.dense = nn.Linear(qkv_dim, num_labels)
    self.log_softmax = nn.LogSoftmax()

  def forward(self, x):
    ### forward resnet50
    # Input b x T x C x H x W
    # batch size
    #print(x.size())
    #exit()
    bs = x.size(0)
    # split T --> resnet for everytimestep
    x = torch.split(x, 1 , dim = 1)
    x_new = []
    for x_i in x: #loop 5x
        #print(x_i.shape)
        x_i = torch.squeeze(x_i,1)
        #print(x_i.shape)
        x_i = self.resnet.conv1(x_i)
        x_i = self.resnet.bn1(x_i)
        x_i = self.resnet.relu(x_i)
        x_i = self.resnet.maxpool(x_i)
        x_i = self.resnet.layer1(x_i)
        x_i = self.resnet.layer2(x_i)
        x_i = self.resnet.layer3(x_i)
        x_i = self.resnet.layer4(x_i)
        #print(x_i.shape)
        x_new.append(x_i)
    x = torch.stack(x_new,dim = 1)
    #del x_new, x_i
    #gc.collect()
    #debug_memory()
    # Output b x T' x C' x H' x W' = b x T x 2048 x H/32 x W/32
    #print("x_resnet_layer4 " + str(x.shape))
    #exit()
    
    ### Transformer
    x = x.transpose(2,1)
    # number of positions: T' x H' x W' (x,y,z)--> add them to output of resnet
    #(batch_size, ch, x, y, z)
    x_pe = torch.squeeze(self.pe(x),0) #positional encoding x_pe: ch x t x h1 x w1
    for i in range(bs):
        x[i,:,:,:,:] += x_pe
    x = x.transpose(2,1)
    #del x_pe
    #gc.collect()

    # b x t x ch x h1 x w1
    
    kv,x = splitInput(x)
    #print("split x " + str(x.shape)) # b x 1 x ch x h1 x w1
    #print("split kv " + str(kv.shape)) # b x t-1 x ch x h1 x w1

    ## apply Conv to kv and q in Processors (for each time step)
    x = torch.squeeze(x,1) 
    x = self.QPr(x)
    x = torch.unsqueeze(x,1) 
    # split T
    kv_new = []
    for i in range(kv.size(1)):
        kv_new.append(self.KVPr1(kv[:,i,:,:,:]))
    kv = torch.stack(kv_new, dim = 1)
    #del kv_new
    #gc.collect()
    #print("QPr x " + str(x.shape)) # b x 1 x 1024 x 1 x 1
    #print("KVPr1 kv " + str(kv.shape)) # b x t-1 x 1024 x 1 x 1 oder  b x t-1 x 1024 x h1 x w1
   
    x = x.reshape(x.shape[0], x.shape[1],-1)
    #debug_memory()
    ## Encoder block
    # x: b x 1 x ch    kv: b x t x ch x h x w 
    x = self.encoder(x, kv)
    #print("encoder " + str(x.shape))  # outpur müsste 1024 sein
    #debug_memory()
    ### classifier layer
    x = self.dense(torch.squeeze(x,1))
    return x

  def save(self, model_file):
    torch.save(self.state_dict(), model_file)

class PhaseTransformerNet_FeatureVector(nn.Module):
  def __init__(self, num_labels, featurevector_dim = 4096, hidden_size = 512, nheads = 2, n_layers = 2, max_len = 5, dropout = 0.1, pretrained  = True, file = None ):
    """Creates Transformermodel, which takes a input sequnece and gives one output
        Pretrained ResNet50 as a feature extracter

      Args:
          num_labels ([type]): number of labels
          featurevector_dim (int, optional): length of the feature vector. Defaults to 4096.
          hidden_size (int, optional): size of linear layer. Defaults to 512.
          nheads (int, optional): number of heads of the Encoder. Defaults to 2.
          n_layers (int, optional): number of layers of the Encoder. Defaults to 2.
          max_len (int, optional): [description]. Defaults to 5.
          dropout (float, optional): length of the input sequence. Defaults to 0.1.
          pretrained (bool, optional): Use a pretrained model ot not. Defaults to True.
          file ([type], optional): input specific model to pretrain if want to use that one rather default. Defaults to None.
    """
    super(PhaseTransformerNet_FeatureVector, self).__init__()

    if pretrained:
        if file == None:
            file = "/mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/cholec80/out/Phase_segmentation_resnet50/no_pretrain/40/20210203-1215/model.pkl"#"modelCholec80Resnet502021128-1348.pkl"
            here = os.path.dirname(os.path.abspath(file))
            sys.path.append(here)
        self.resnet = CNNmodel(num_class = 7, cnn = "resnet50", pretrainedFile = file)
    else:
        self.resnet = CNNmodel(num_class = 7, cnn = "resnet50")
    # positional encoding layer
    self.pe = PositionalEncoding1D(featurevector_dim)

    # encoder  layers
    enc_layer = nn.TransformerEncoderLayer(featurevector_dim, nheads, hidden_size, dropout)
    self.encoder = nn.TransformerEncoder(enc_layer, num_layers = n_layers)

    # final dense layer
    self.dense = nn.Linear(featurevector_dim*max_len, num_labels)

  def forward(self, x):
    x_new = []
    for i in range(x.shape[1]):
        a = self.resnet.feature(x[:,i,:,:,:], "resnet")
        x_new.append(a)
    x = torch.stack(x_new, dim = 2)
    x = x + self.pe(x)
    x = x.permute(0,2,1)
    x = self.encoder(x)
    x = x.reshape(x.shape[0], -1)
    x = self.dense(x)
    return x

  def save(self, model_file):
    torch.save(self.state_dict(), model_file)

  def load(self, model_file):
    self.load_state_dict(torch.load(model_file))

class PhaseTransformerNet_FeatureVector_2(nn.Module):
  def __init__(self, num_labels, featurevector_dim = 4096, hidden_size = 512, nheads = 2, n_layers = 2, max_len = 5, dropout = 0.1, pretrained = True, file = None, mask = None):
    """Creates Transformermodel, which takes a input sequnece and outputs a sequence
        Pretrained ResNet50 as a feature extracter

      Args:
          num_labels ([type]): number of labels
          featurevector_dim (int, optional): length of the feature vector. Defaults to 4096.
          hidden_size (int, optional): size of linear layer. Defaults to 512.
          nheads (int, optional): number of heads of the Encoder. Defaults to 2.
          n_layers (int, optional): number of layers of the Encoder. Defaults to 2.
          max_len (int, optional): [description]. Defaults to 5.
          dropout (float, optional): length of the input sequence. Defaults to 0.1.
          pretrained (bool, optional): Use a pretrained model ot not. Defaults to True.
          file ([type], optional): input specific model to pretrain if want to use that one rather default. Defaults to None.
    """
    super(PhaseTransformerNet_FeatureVector_2, self).__init__()
    if pretrained:
        if file == None:
            file = "/mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/cholec80/out/Phase_segmentation_resnet50/no_pretrain/40/20210203-1215/model.pkl"#"modelCholec80Resnet502021128-1348.pkl"
            here = os.path.dirname(os.path.abspath(file))
            sys.path.append(here)
        self.resnet = CNNmodel(num_class = 7, cnn = "resnet50", pretrainedFile = file)
    else:
        self.resnet = CNNmodel(num_class = 7, cnn = "resnet50")
    # positional encoding layer
    self.pe = PositionalEncoding1D(featurevector_dim)

    # encoder  layers
    enc_layer = nn.TransformerEncoderLayer(featurevector_dim, nheads, hidden_size, dropout)
    self.encoder = nn.TransformerEncoder(enc_layer, num_layers = n_layers)

    # final dense layer
    self.denses =  _get_clones(nn.Linear(featurevector_dim, num_labels),max_len)

  def forward(self, x, src_mask = None):
    x_new = []
    for i in range(x.shape[1]):
        a = self.resnet.feature(x[:,i,:,:,:], "resnet")
        x_new.append(a)
    x = torch.stack(x_new, dim = 2)
    x = x + self.pe(x)
    x = x.permute(0,2,1)
    x = self.encoder(x,src_mask)
    output = []
    for i, l in enumerate(self.denses):
        output.append(l(x[:,i,:]))
    x = torch.stack(output, dim = 2)
    return x

  def save(self, model_file):
    torch.save(self.state_dict(), model_file)
  # https://pytorch.org/tutorials/beginner/transformer_tutorial.html
  def generate_square_subsequent_mask(self, sz):
        mask = (torch.triu(torch.ones(sz, sz)) == 1).transpose(0, 1)
        mask = mask.float().masked_fill(mask == 0, float('-inf')).masked_fill(mask == 1, float(0.0))
        return mask
class PhaseTransformerNet_FeatureVector_3(nn.Module):
  def __init__(self, num_labels, featurevector_dim = 4096, hidden_size = 512, nheads = 2, n_layers = 2, max_len = 5, dropout = 0.1):
    """Creates Transformermodel, which takes a input sequnece and outputs a sequence
        Pretrained ResNet50 as a feature extracter

      Args:
          num_labels ([type]): number of labels
          featurevector_dim (int, optional): length of the feature vector. Defaults to 4096.
          hidden_size (int, optional): size of linear layer. Defaults to 512.
          nheads (int, optional): number of heads of the Encoder. Defaults to 2.
          n_layers (int, optional): number of layers of the Encoder. Defaults to 2.
          max_len (int, optional): [description]. Defaults to 5.
          dropout (float, optional): length of the input sequence. Defaults to 0.1.
          pretrained (bool, optional): Use a pretrained model ot not. Defaults to True.
          file ([type], optional): input specific model to pretrain if want to use that one rather default. Defaults to None.
    """
    super(PhaseTransformerNet_FeatureVector_3, self).__init__()
    # positional encoding layer
    #self.pe = PositionalEncoding1D(featurevector_dim)
    self.pe = PositionalEncoding(d_model = featurevector_dim, max_len = max_len)
    #self.sigmoid = nn.Sigmoid()

    # encoder  layers
    enc_layer = nn.TransformerEncoderLayer(featurevector_dim, nheads, hidden_size, dropout)
    self.encoder = nn.TransformerEncoder(enc_layer, num_layers = n_layers)

    # final dense layer
    self.denses =  nn.Linear(featurevector_dim, num_labels)

  def forward(self, x, src_mask = None):
    #x = self.sigmoid(x)
    x = self.pe(x)
    x = self.encoder(x,src_mask)

    x = self.denses(x)
    return x

  def save(self, model_file):
    torch.save(self.state_dict(), model_file)
  # https://pytorch.org/tutorials/beginner/transformer_tutorial.html
  def generate_square_subsequent_mask(self, sz):
        mask = (torch.triu(torch.ones(sz, sz)) == 1).transpose(0, 1)
        mask = mask.float().masked_fill(mask == 0, float('-inf')).masked_fill(mask == 1, float(0.0))
        return mask

  def load(self, model_file):
    self.load_state_dict(torch.load(model_file))

class PhaseTransformerNet_FeatureVector_4(nn.Module):
  def __init__(self, num_labels, featurevector_dim = 4096, hidden_size = 512, nheads = 2, n_layers = 2, max_len = 5, dropout = 0.1):
    """Creates Transformermodel, which takes a input sequnece and outputs a sequence
        Pretrained ResNet50 as a feature extracter

      Args:
          num_labels ([type]): number of labels
          featurevector_dim (int, optional): length of the feature vector. Defaults to 4096.
          hidden_size (int, optional): size of linear layer. Defaults to 512.
          nheads (int, optional): number of heads of the Encoder. Defaults to 2.
          n_layers (int, optional): number of layers of the Encoder. Defaults to 2.
          max_len (int, optional): [description]. Defaults to 5.
          dropout (float, optional): length of the input sequence. Defaults to 0.1.
          pretrained (bool, optional): Use a pretrained model ot not. Defaults to True.
          file ([type], optional): input specific model to pretrain if want to use that one rather default. Defaults to None.
    """
    super(PhaseTransformerNet_FeatureVector_4, self).__init__()
    # positional encoding layer
    #self.pe = PositionalEncoding1D(featurevector_dim)
    self.pe = PositionalEncoding1D(featurevector_dim)
    self.dropout = nn.Dropout(dropout)
    #self.sigmoid = nn.Sigmoid()

    # encoder  layers
    enc_layer = nn.TransformerEncoderLayer(featurevector_dim, nheads, hidden_size, dropout)
    self.encoder = nn.TransformerEncoder(enc_layer, num_layers = n_layers)

    # final dense layer
    self.denses =  nn.Linear(featurevector_dim, num_labels)

  def forward(self, x, src_mask = None):
    #x = self.sigmoid(x)
    x = x.permute(0,2,1)
    x = x + self.pe(x)
    x = self.dropout(x)
    x = x.permute(0,2,1)
    x = self.encoder(x,src_mask)
    
    x = self.denses(x)
    return x.permute(0,2,1)

  def save(self, model_file):
    torch.save(self.state_dict(), model_file)
  # https://pytorch.org/tutorials/beginner/transformer_tutorial.html
  def generate_square_subsequent_mask(self, sz):
        mask = (torch.triu(torch.ones(sz, sz)) == 1).transpose(0, 1)
        mask = mask.float().masked_fill(mask == 0, float('-inf')).masked_fill(mask == 1, float(0.0))
        return mask

  def load(self, model_file):
    self.load_state_dict(torch.load(model_file))