"""
train CNN-LSTM with CoBot dataset 
"""

import argparse
import datetime
import os.path

import torch
import torch.nn as nn
import torch.optim as optim
import torchvision

import utils
from datasets import PhaseData_CoBot, PhaseData_CoBot_old
from models import PhaseNet
from utils import CoBot, DataPrep3

# Cholec80 Vortraining: /home/krellstef/master_code/outCholec80/Phase_segmentation/no_pretrain/40/20201031-1724/model.pkl

def main(args):
    transform_train = DataPrep3.standard_transform
    transform_test = DataPrep3.standard_transform

    device_gpu = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    device_cpu = torch.device("cpu")

    num_train_op_sets = args.num_ops    

    pretrained_model_file = None
    pretrain_method = "no_pretrain"
    
    if args.model is not None:
        if args.pretrainedCNNLSTM == False:
            pretrained_model_file = args.model
            #if not os.path.exists(pretrained_model_file):
            #    pretrained_model_file = os.path.join(utils.out_path, "Self-supervised", pretrained_model_file, "model.pth")
            pretrain_method = "pretrainedResNet"#os.path.split(os.path.split(os.path.split(pretrained_model_file)[0])[0])[1]
        else:
            pretrain_method = "pretrainedCNNLSTM"

    trial_id = datetime.datetime.now().strftime("%Y%m%d-%H%M")
    experiment = os.path.join("Phase_segmentation", pretrain_method, str(args.num_ops), trial_id)

    output_folder = os.path.join(utils.out_path, experiment)

    anno_path = utils.annotation_path
    if args.annotation == "phase":
        num_class = 5        
    elif args.annotation == "step":
        num_class = 5 + 4
        output_folder = os.path.join("./outCoBot_steps", experiment)
        step_file = []
    else:
        raise NotImplementedError("not available as annotation")

    num_classes_pre = None
    if pretrain_method == "pretrainedResNet" and args.pretraining_mode != "self":
        num_classes_pre = num_class
        print("load supervised ResNet")


    os.makedirs(output_folder)
    f_log = open(os.path.join(output_folder, "log.txt"), "w")
    def log(msg):
        utils.log(f_log, msg)
    model_file = os.path.join(output_folder, "model.pkl")
    model_file_final = os.path.join(output_folder, "model_final.pkl")
    model_file_train = os.path.join(output_folder, "model_best_train.pkl")

    log("Run <%s> on device %s" % (experiment, str(device_gpu)))
    if pretrained_model_file is not None:
        log("Using pretrained FeatureNet: " + pretrained_model_file)
    log("Used parameters...")
    for arg in vars(args):
        log("\t" + str(arg) + " : " + str(getattr(args, arg)))

    log("Loading data...")
    train_sets = CoBot.videos_train[0:num_train_op_sets]
    test_sets = CoBot.videos_test

    train_data = []
    for op in train_sets:
        op_path = os.path.join(utils.frames_path, op)
        if os.path.isdir(op_path):
            step_file = None
            if args.annotation == "step":
                step_file = os.path.join(utils.annotation_path_step, op + "step.txt")
            anno_file = os.path.join(anno_path, op + ".txt")
            #dataset = PhaseData_CoBot_old(op_path, anno_file, DataPrep3.width, DataPrep3.height, transform_test,sample_rate = args.sample_rate)
            dataset = PhaseData_CoBot(op_path, anno_file, DataPrep3.width, DataPrep3.height, transform_test,annotation = args.annotation, step_file = step_file, sample_rate = args.sample_rate)
            train_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False,
                                                             num_workers=4))
    log("\t training data")
    for loader in train_data:
        log("\t\t" + loader.dataset.frames_path)

    test_data = []
    for op in test_sets:
        op_path = os.path.join(utils.frames_path, op)
        if os.path.isdir(op_path):
            step_file = None
            if args.annotation == "step":
                step_file = os.path.join(utils.annotation_path_step, op + "step.txt")
            anno_file = os.path.join(anno_path, op + ".txt")
            #dataset = PhaseData_CoBot_old(op_path, anno_file, DataPrep3.width, DataPrep3.height, transform_test,sample_rate = args.sample_rate)
            dataset = PhaseData_CoBot(op_path, anno_file, DataPrep3.width, DataPrep3.height, transform_test,annotation = args.annotation, step_file = step_file, sample_rate = args.sample_rate)
            test_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False,
                                                             num_workers=4))
                
    log("\t test data")
    for loader in test_data:
        log("\t\t" + loader.dataset.frames_path)

    log("Create PhaseNet")
    if args.cnn == "alexnet":
        log("Alexnet")
        net = PhaseNet(pretrained_model_file, cnn = "alexnet",num_classes = num_class)
    else:
        log("Resnet")
        net = PhaseNet(pretrained_model_file,num_classes = num_class, num_classes_pre = num_classes_pre)
        for param in net.featureNet.resnet.parameters():
            param.requires_grad = False
        for param in net.featureNet.resnet.layer4.parameters():
            param.requires_grad = True

    if args.model is not None and args.pretrainedCNNLSTM == True:
        net.load(args.model,7,num_class)

    net.to(device_gpu)
    # we = utils.class_weights_steps if args.annotation == "step" else utils.class_weights_phases
    # print(we)

    # we = torch.from_numpy(we).float()
    # we = we.to(device_gpu)

    #criterion = nn.CrossEntropyLoss(size_average=False, weight=we)
    criterion = nn.CrossEntropyLoss(size_average=False)#, weight=we)
    optimizer = optim.Adam(filter(lambda p: p.requires_grad, net.parameters()), lr=args.lr)
    torch.backends.cudnn.benchmark = True
    torch.backends.cudnn.enabled = True
    log("Begin training...")

    best_acc = 0
    one = True
    for epoch in range(args.epochs):
        log("Epoch " + str(epoch + 1) + "...")

        train_loss = 0
        train_accuracy = 0
        train_count = 0
        for op_data in train_data:
            hidden_state = net.init_hidden(device_gpu)
            optimizer.zero_grad()
            optimized = False
            loss = 0
            batch_count = 0
            if epoch < 2: 
                print(op_data.dataset.frames_path)
            for loader in op_data:
                images, labels = loader
                images = images.to(device_gpu)
                labels = labels.to(device_gpu)
                outputs, hidden_state = net(images, hidden_state)
                loss += criterion(outputs, labels)
                batch_count += 1
                if batch_count % args.opt_step == 0:
                    loss.backward()
                    optimizer.step()
                    optimized = True
                    optimizer.zero_grad()
                    h, c = hidden_state
                    h = h.detach()
                    c = c.detach()
                    hidden_state = (h, c)
                else:
                    loss.backward(retain_graph=True)
                    optimized = False

                train_loss += loss.item()
                _, predicted = torch.max(outputs.data, 1)
                train_accuracy += (predicted == labels).sum().item()
                train_count += labels.size(0)

                if batch_count % args.opt_step == 0:
                    loss = 0

            if not optimized:
                optimizer.step()

        predictions_path = os.path.join(output_folder, "predictions")
        if not os.path.exists(predictions_path):
            os.makedirs(predictions_path)
        test_loss = 0
        test_accuracy = 0
        test_count = 0
        with torch.no_grad():
            for op_data in test_data:
                frames_path = op_data.dataset.frames_path
                op_id = frames_path.split('/')[-1] if frames_path.split('/')[-1] else frames_path.split('/')[-2]
                f_out = open(os.path.join(predictions_path, op_id + ".txt"), "w")

                hidden_state = net.init_hidden(device_gpu)
                for loader in op_data:
                    images, labels_cpu = loader
                    images = images.to(device_gpu)
                    labels = labels_cpu.to(device_gpu)
                    outputs, hidden_state = net(images, hidden_state)
                    loss = criterion(outputs, labels)

                    test_loss += loss.item()
                    _, predicted = torch.max(outputs.data, 1)
                    predicted_cpu = predicted.to(device_cpu)
                    for p, l in zip(predicted_cpu.numpy(), labels_cpu.numpy()):
                        f_out.write(str(p) + "," + str(l) + "\n")
                    test_count += labels.size(0)
                    test_accuracy += (predicted == labels).sum().item()
                f_out.close()

        summary = "train (loss %.3f, accuracy %.3f) test (loss %.3f, accuracy %.3f)" % \
                  (train_loss/train_count, train_accuracy/train_count, test_loss/test_count, test_accuracy/test_count)
        log("\t" + summary)

        if (epoch + 1) % args.save_freq == 0:
            log("\tSave model to %s..." % model_file_final)
            net.save(model_file_final)
        act_acc = test_accuracy/test_count
        if best_acc < act_acc:
            best_acc = act_acc
            log("\tSave best val model to %s..." % model_file)
            net.save(model_file)

        if one and train_accuracy/train_count > 0.999:
            net.save(model_file_train)
            one = False


    log("Done. Save final model to %s..." % model_file_final)
    net.save(model_file_final)
    
    log("Update predictions with best val model")
    net.load(model_file)

    test_accuracy = 0
    test_count = 0
    with torch.no_grad():
        for op_data in test_data:
            frames_path = op_data.dataset.frames_path
            op_id = frames_path.split('/')[-1] if frames_path.split('/')[-1] else frames_path.split('/')[-2]
            f_out = open(os.path.join(predictions_path, op_id + ".txt"), "w")

            hidden_state = net.init_hidden(device_gpu)
            for loader in op_data:
                images, labels_cpu = loader
                images = images.to(device_gpu)
                labels = labels_cpu.to(device_gpu)
                outputs, hidden_state = net(images, hidden_state)
                _, predicted = torch.max(outputs.data, 1)
                predicted_cpu = predicted.to(device_cpu)
                for p, l in zip(predicted_cpu.numpy(), labels_cpu.numpy()):
                    f_out.write(str(p) + "," + str(l) + "\n")
                test_count += labels.size(0)
                test_accuracy += (predicted == labels).sum().item()
            f_out.close()
    log('best Model: Val Accuracy : %.3f ' % (test_accuracy/test_count))
    f_log.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Finetune a CNN-LSTM for surgical phase recognition.")
    parser.add_argument("--num_ops", type=int, default = 25,
                        help="Number of labeled OP videos to use for training.")
    parser.add_argument("-m", "--model", type=str, help="Pretrained FeatureNet to use.")
    parser.add_argument("--pretraining_mode", type=str, default = "self", help="supervised or self-supervised")
    parser.add_argument("--cnn", type=str, choices=["resnet","alexnet"], default = "resnet",help="cnn to use resnet or alexnet")
    parser.add_argument("--lr", type=float, default=0.00001, help="The learning rate.")
    parser.add_argument("--batch_size", type=int, default=128, help="The batch size.") #128
    parser.add_argument("--opt_step", type=int, default=3,
                        help="Number of batches to accumulate before applying the optimizer.")
    parser.add_argument("--epochs", type=int, default=200, help="The maximal number of epochs to train.")
    parser.add_argument("--save_freq", type=int, default=10,
                        help="Defines after how many epochs the current model parameters are saved.")
    parser.add_argument("--sample_rate", type=int, default=1,
                        help="Defines in which rate the pictures are taken 1-> every pic 2-> every 2. pic.")
    parser.add_argument("--annotation", type=str, default="phase" , choices=["phase","step"],
                        help="phases or steps annotation")
    parser.add_argument("--pretrainedCNNLSTM", type=bool, default=False , help="if model is defined use model as LSTM")
    args = parser.parse_args()
    main(args)
