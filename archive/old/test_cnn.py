
import os.path
import numpy as np
import argparse

import torch
import torch.nn as nn
import torch.optim as optim

import utils
from utils import Cholec80
import os.path
import datetime
import argparse
import torchvision

from datasets import PhaseData, PhaseData_CoBot
from models import PhaseNet, CNNmodel
import utils
from utils import Cholec80, DataPrep3, AverageMeter, CoBot, DataPrepCoBot


def _fmt(m):
    return "%2.1f" % (m * 100)


def main(args):
    device_gpu = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    device_cpu = torch.device("cpu")
    print("Test...")
    num_class = 6 #cholec80
    dataset = 'CoBot'
    test_data = []
    print("Loading data...")
    transform_test = DataPrep3.standard_transform
    if dataset == 'CoBot':
        test_sets = CoBot.test_sets

        for op in test_sets:
            op_path = os.path.join(utils.frames_path, utils.CoBot.videos[op])
            if os.path.isdir(op_path):
                anno_file = os.path.join(utils.annotation_path, utils.CoBot.videos[op] + ".txt")
                dataset = PhaseData_CoBot(op_path, anno_file, DataPrepCoBot.width, DataPrepCoBot.height, transform_test)
                test_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False,
                                                                 num_workers=4))
    else:        
        test_sets = Cholec80.test_sets        
        for op_set in test_sets:
            for op in os.listdir(os.path.join(utils.frames_path, op_set)):
                op_path = os.path.join(utils.frames_path, op_set, op)
                if os.path.isdir(op_path):
                    #num +=1                
                    #if num <= 4:
                      #  continue
                    anno_file = os.path.join(utils.annotation_path, "video" + op + "-phase.txt")
                    dataset = PhaseData(op_path, anno_file, DataPrep3.width, DataPrep3.height, transform_test)
                    test_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False,
                                                                 num_workers=2))
                
    print("\t test data")
    for loader in test_data:
        print("\t\t" + loader.dataset.frames_path)
    output_folder = os.path.join(utils.out_path, "Phase_segmentation_" + args.CNN_type, args.experiment)
    model_file = os.path.join(output_folder, "model70.pkl")
    
    model = CNNmodel(num_class = num_class,cnn=args.CNN_type)
    model_weights = torch.load(model_file)
    model.load_state_dict(model_weights, strict=True)
    model = model.to(device_gpu)
    correct = 0
    total = 0
    model.eval()
    print("Evaluate Testdata")
    i = 0
    predictions_path = os.path.join(output_folder, "predictions")
    if not os.path.exists(predictions_path):
        os.makedirs(predictions_path)
    with torch.no_grad():
        for test in test_data:
            frames_path = test.dataset.frames_path
            print(frames_path)
            op_id = frames_path.split('/')[-1] if frames_path.split('/')[-1] else frames_path.split('/')[-2]
            print(op_id)
            f_out = open(os.path.join(predictions_path, op_id + ".txt"), "w")
            print(str(i))
            i += 1
            for loader in test:
                data,target_cpu = loader
                batch_size = target_cpu.size(0)
                data = data.to(device_gpu)
                target = target_cpu.to(device_gpu)
                output = model(data)
                #predicted = torch.nn.Softmax(dim=1)(output)
                _, predicted = torch.max(output, 1)
                predicted_cpu = predicted.to(device_cpu)
                for p, l in zip(predicted_cpu.numpy(), target_cpu.numpy()):
                    f_out.write(str(p) + "," + str(l) + "\n")


                correct += (predicted == target).sum().item()
                total += batch_size
            f_out.close
    print('Test Accuracy : %d %%' % (100 * correct / total))
    #TODO
    #weitere Metriken: Recall Precision f1-score

#no_pretrain/40/20201101-0437
#no_pretrain/13/20201111-1316
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Evaluate phase segmentation results.")
    parser.add_argument("experiment", type=str,
                        help="Experiment to evaluate, specified as <variant>/<num_ops>/<trial_id>.")
    parser.add_argument("--CNN_type", "--cnn", type=str, default="AlexNet", choices=["AlexNet","resnet18","resnet34","resnet50"],
                        help="AlexNet, resnet18, resnet34, resnet50")
    parser.add_argument("--batch_size", type=int, default=128, help="The batch size.")
    args = parser.parse_args()
    main(args)