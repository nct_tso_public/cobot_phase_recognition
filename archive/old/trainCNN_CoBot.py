""" 
training CNN (Resnets or AlexNet) with CoBot
"""

import argparse
import datetime
import os.path

import torch
import torch.nn as nn
import torch.optim as optim
import torchvision
from torch.optim import lr_scheduler

import utils
from datasets import PhaseData_CoBot, PhaseDataList_CoBot
from models import CNNmodel
from utils import AverageMeter, CoBot, DataPrep3

## model mit endovVis trainiert
## /mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/EndoVis18/out/Phase_segmentation_resnet50/no_pretrain/20210225-1242/model.pkl

def main(args):

    transform_train = DataPrep3.standard_transform
    transform_test = DataPrep3.standard_transform

    device_gpu = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    device_cpu = torch.device("cpu")

    num_train_op_sets = args.num_ops
    
    trial_id = datetime.datetime.now().strftime("%Y%m%d-%H%M")
    experiment = os.path.join("Phase_segmentation_"+args.CNN_type, str(args.num_ops), trial_id)
    
    if args.pretrained == 1:
        experiment = os.path.join("Phase_segmentation_"+args.CNN_type, "pretrain", str(args.num_ops), trial_id)

    
    output_folder = os.path.join(utils.out_path, experiment)

    anno_path = utils.annotation_path
    if args.annotation == "phase":
        num_class = 5        
    elif args.annotation == "step":
        num_class = 5 + 4
        #anno_path = utils.annotation_path_step
        output_folder = os.path.join("./outCoBot_steps", experiment)
    else:
        raise NotImplementedError("not available as annotation")
        

    os.makedirs(output_folder)
    f_log = open(os.path.join(output_folder, "log.txt"), "a")
    def log(msg):
        utils.log(f_log, msg)
    model_file = os.path.join(output_folder, "model.pkl")
    model_file_final = os.path.join(output_folder, "model_final.pkl")

    log("Run <%s> on device %s" % (experiment, str(device_gpu)))
    log("Used parameters...")
    for arg in vars(args):
        log("\t" + str(arg) + " : " + str(getattr(args, arg)))

    log("Loading data...")
    train_sets = CoBot.videos_train[0:num_train_op_sets]
    test_sets = CoBot.videos_test
    train_data = []
    op_pathes = []
    anno_files = []
    step_files = []
    for op in train_sets:
        op_path = os.path.join(utils.frames_path, op)
        if os.path.isdir(op_path):
            if args.annotation == "step":
                step_file = os.path.join(utils.annotation_path_step, op + "step.txt")
                step_files.append(step_file)
            anno_file = os.path.join(anno_path, op + ".txt")
            op_pathes.append(op_path)
            anno_files.append(anno_file)
            

    if args.annotation == "phase":
        step_files = None

    dataset = PhaseDataList_CoBot(op_pathes, anno_files, DataPrep3.width, DataPrep3.height, transform_train,annotation = args.annotation, step_file = step_files, sample_rate = args.sample_rate)
    trainloader = torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=True,num_workers=4)
    log("\t training data")
    for path in op_pathes:
        log("\t\t" + path)
    num = 0
    val_data = []
    for op in test_sets:
        op_path = os.path.join(utils.frames_path, op)
        if os.path.isdir(op_path):
            step_file = None
            if args.annotation == "step":
                step_file = os.path.join(utils.annotation_path_step, op + "step.txt")
            anno_file = os.path.join(anno_path, op + ".txt")
            dataset = PhaseData_CoBot(op_path, anno_file, DataPrep3.width, DataPrep3.height, transform_test,annotation = args.annotation, step_file = step_file, sample_rate = args.sample_rate)
            val_data.append(torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False,
                                                             num_workers=4))
            #break
    log("\t val data")
    for loader in val_data:
        log("\t\t" + loader.dataset.frames_path)


#"./outCholec80/Phase_segmentation_resnet50/no_pretrain/40/20201101-1723/model50.pkl"
#"./outCholec80/Phase_segmentation_AlexNet/no_pretrain/40/20201101-0437/model200.pkl"
    pretrainedFile = None
    if args.CNN_type == "AlexNet":
        pretrainedFile = "./outCholec80/Phase_segmentation_AlexNet/no_pretrain/40/20201101-0437/model200.pkl"
    if args.CNN_type == "resnet50":
        pretrainedFile = "/mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/cholec80/out/Phase_segmentation_resnet50/no_pretrain/40/20210203-1215/model.pkl"
        #pretrainedFile = "modelCholec80Resnet502021128-1348.pkl"
        if args.model != None:
            pretrainedFile = args.model

    if args.pretrained == 1:
        print("pretrianed")
        model = CNNmodel(num_class = num_class, cnn = args.CNN_type, pretrainedFile = pretrainedFile)
    
    else:
        model = CNNmodel(num_class = num_class, cnn = args.CNN_type)

    if args.pretrained == 2:
        # load from Phase Cobot model
        pretrainedFile = "./outCoBot/Phase_segmentation_resnet50/pretrain/20/20210129-1241/model.pkl"
        model.load_different_num_class(pretrainedFile, num_class_load = 5 ,num_class_now = num_class)

    if args.pretrained == 3:
        # load from Phase EndoVIsModel model
        if args.model != None:
            model.load_different_num_class(args.model, num_class_load = 14 ,num_class_now = num_class)

    if args.pretrained == 4:
        # load from Phase EndoVIsModel model
        if args.model != None:
            model.load_different_num_class(args.model, num_class_load = 1000 ,num_class_now = num_class, self_supervised = True)

    # freeze some parameter in ResNet50
    if args.CNN_type == "resnet50":
        for param in model.model.parameters():
            param.requires_grad = False
        #for param in model.model.layer3.parameters():
         #   param.requires_grad = True
        for param in model.model.layer4.parameters():
            param.requires_grad = True
        for param in model.model.fc.parameters():
            param.requires_grad = True


    model = model.to(device_gpu)
    we = utils.class_weights_steps if args.annotation == "step" else utils.class_weights_phases
    print(we)

    we = torch.from_numpy(we).float()
    we = we.to(device_gpu)
    criterion = nn.CrossEntropyLoss(weight=we)
    optimizer = optim.Adam(params=filter(lambda p: p.requires_grad, model.parameters()), lr=args.lr)
    scheduler = lr_scheduler.StepLR(optimizer, step_size=50, gamma=0.1)

    torch.backends.cudnn.benchmark = True
    torch.backends.cudnn.enabled = True
    log("lr_scheduler.StepLR(optimizer, step_size=50, gamma=0.1)")
    log("Begin training...")
    best_val = 0
    one = True
    for epoch in range(args.epochs):
        log("Epoch " + str(epoch + 1) + "...")
        # zero lodd and acc
        train_loss = AverageMeter()
        train_acc = AverageMeter()

        
        # Print Learning Rate
        #print('Epoch:', (epoch + 1),'LR:', scheduler.get_lr())
        model.train()
        for _, batch in enumerate(trainloader):
            data, target = batch
            batch_size = target.size(0)
            data = data.to(device_gpu)
            target = target.to(device_gpu)

            # zero the parameter gradients
            optimizer.zero_grad()

            #forward
            output = model(data)
            loss = criterion(output, target)
            # backward + optimize
            loss.backward()
            optimizer.step()
                
            # statistics
            train_loss.update(loss.item(), batch_size)                
            #predicted = torch.nn.Softmax(dim=1)(output)
            _, predicted = torch.max(output, 1)
            acc = (predicted == target).sum().item() / batch_size
            train_acc.update(acc, batch_size)
        # Decay Learning Rate
        scheduler.step()
        log("Epoch {}: Train loss: {train_loss.avg:.4f} Train acc: {train_acc.avg:.3f}"
            .format(epoch + 1, train_loss=train_loss, train_acc=train_acc))
        log("testing with val set...")
        predictions_path = os.path.join(output_folder, "predictions")
        if not os.path.exists(predictions_path):
            os.makedirs(predictions_path)
        correct = 0
        total = 0
        # Validation
        model.eval()
        val_loss = 0
        with torch.no_grad():
            for v in val_data:
                frames_path = v.dataset.frames_path
                op_id = frames_path.split('/')[-1] if frames_path.split('/')[-1] else frames_path.split('/')[-2]
                f_out = open(os.path.join(predictions_path, op_id + ".txt"), "w")
                for loader in v:
                    data,target_cpu = loader
                    batch_size = target.size(0)
                    data = data.to(device_gpu)
                    target = target_cpu.to(device_gpu)
                    output = model(data)
                    loss = criterion(output, target)

                    val_loss += loss.item()

                        #predicted = torch.nn.Softmax(dim=1)(output)
                    _, predicted = torch.max(output, 1)
                    predicted_cpu = predicted.to(device_cpu)
                    for p, l in zip(predicted_cpu.numpy(), target_cpu.numpy()):
                        f_out.write(str(p) + "," + str(l) + "\n")

                    correct += (predicted == target).sum().item()
                    total += batch_size
                f_out.close()
            log('Val Accuracy : %d %%, Val Loss: %.4f' % (100 * correct / total, val_loss/total))
        if (epoch + 1) % args.save_freq == 0 or epoch == 0:
            log("\tSave model to %s..." % model_file_final)
            torch.save(model.state_dict(), model_file_final)
            
        act_val = correct / total
        if  act_val > best_val:
            best_val = act_val
            log("\tSave model with best val_acc to %s..." % model_file)
            torch.save(model.state_dict(), model_file)

        if one and train_loss.avg > 0.999:
            model.save(model_file_train)
            log("\tSave model with best train to %s..." % model_file_train)
            one = False
   
    log("Done. Save final model to %s..." % model_file_final)
    torch.save(model.state_dict(), model_file_final)

    log("Update predictions with best val model")
    correct = 0
    total = 0
        # Validation
    model.load(model_file)
    model.eval()
    with torch.no_grad():
        for v in val_data:
            frames_path = v.dataset.frames_path
            op_id = frames_path.split('/')[-1] if frames_path.split('/')[-1] else frames_path.split('/')[-2]
            f_out = open(os.path.join(predictions_path, op_id + ".txt"), "w")
            for loader in v:
                data,target_cpu = loader
                batch_size = target.size(0)
                data = data.to(device_gpu)
                target = target_cpu.to(device_gpu)
                output = model(data)
                        #predicted = torch.nn.Softmax(dim=1)(output)
                _, predicted = torch.max(output, 1)
                predicted_cpu = predicted.to(device_cpu)
                for p, l in zip(predicted_cpu.numpy(), target_cpu.numpy()):
                    f_out.write(str(p) + "," + str(l) + "\n")

                correct += (predicted == target).sum().item()
                total += batch_size
            f_out.close()
    log('best Model: Val Accuracy : %d %%' % (100 * correct / total))


    f_log.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Run AlexNet or ResnNet for surgical phase recognition.")
    parser.add_argument("--num_ops", type=int, default = 25,
                        help="Number of labeled OP videos to use for training.")
    parser.add_argument("--lr", type=float, default=0.0001, help="The learning rate.")
    parser.add_argument("--batch_size", type=int, default=256, help="The batch size.") #128
    parser.add_argument("--epochs", type=int, default=100, help="The maximal number of epochs to train.")
    parser.add_argument("--save_freq", type=int, default=10,
                        help="Defines after how many epochs the current model parameters are saved.")
    parser.add_argument("--CNN_type", "--cnn", type=str, default="resnet50", choices=["AlexNet","resnet18","resnet34","resnet50"],
                        help="AlexNet, resnet18, resnet34, resnet50")
    parser.add_argument("--pretrained", type=int, default=0 ,
                        help="takes pretrained model fram cholec80 if true")
    parser.add_argument("--annotation", type=str, default="phase" , choices=["phase","step"],
                        help="phases or steps annotation")
    parser.add_argument("--sample_rate", type=int, default=1,
                        help="Defines in which rate the pictures are taken 1-> every pic 2-> every 2. pic.")
    parser.add_argument("--model", type=str, default=None ,
                        help="pretrained Model")

    args = parser.parse_args()
    main(args)
