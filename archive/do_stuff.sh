cd part1

# for i in 1 2 3 4
# do
# python3 train_LSTM_ex.py --use_class_weights False --fold $i --batch_size 256 --lr 0.00005 --epochs 30 --exp "20210719_1338_55_pretrainedres_lr00005" --annotation "step" --pretrained_ResNet50 "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442"
# done
for i in 1 2 3 4
do
python3 train_LSTM_ex.py --fold $i --batch_size 128 --batch_factor 1 --lr 0.0001 --epochs 30 --exp "20210727_1237_44_pretrainedres_lr0001_batch128" --annotation "step" --pretrained_ResNet50 "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442"
done
#for i in 1 2 3 4
#do
#python3 train_LSTM_ex.py --fold $i --batch_size 256 --epochs 30 --exp "20210708_1537_35_pretrainedreslstm" --pretrained_ResNet50 "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359" --pretrained_LSTM "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/LSTM_new/256_20210609-1249"
#done

#python3 train_LSTM_ex.py --use_class_weights True --batch_size 256 --lr 0.0001 --epochs 30 --exp "20210721_1151_pretrainedres_lr0001" --annotation "cholec80" --pretrained_ResNet50 "/mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/cholec80/out/Phase_segmentation_resnet50/no_pretrain/40/20210223-1733/model.pkl"
#python3 train_LSTM_ex.py --use_class_weights True --batch_size 128 --lr 0.001 --epochs 50 --annotation "cholec80" --pretrained_ResNet50 "/mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/cholec80/model_best_acc.pth.tar" --exp "20210721_1459_pretrainedres_lr001"
#python3 train_LSTM_ex.py --use_class_weights True --batch_size 128 --lr 0.00001 --epochs 50 --annotation "cholec80" --pretrained_ResNet50 "/mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/cholec80/model_best_acc.pth.tar" --exp "20210727_1459_pretrainedres_lr00001"  
#python3 train_LSTM_ex.py --use_class_weights True --batch_size 256 --batch_factor 1 --lr 0.00005 --epochs 50 --annotation "cholec80" --pretrained_ResNet50 "/mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/cholec80/model_best_acc.pth.tar" --exp "20210727_1459_pretrainedres_lr00005_b256"