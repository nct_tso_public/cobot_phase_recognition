# cd part2

# for i in 1 2 3 4
# do
# EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/TCN/20210618-1217/train-val/$i"
# EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/TCN/20210618-1217/train-val/$i"
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" \
# --tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 20 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
# done

# for i in 1 2 3 4
# do
# EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/TCN/20210618-1452/train-val/$i"
# EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/TCN/20210618-1452/train-val/$i"
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" \
# --tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 10 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
# done

# for i in 1 2 3 4
# do
# EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/TCN/20210618-1724/train-val/$i"
# EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/TCN/20210618-1724/train-val/$i"
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" \
# --tcn_baseline True --causal True --d_model 64 --tcn_stages 3 --tcn_layers 5 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
# done

# for i in 1 2 3 4
# do
# EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/TCN/20210618-1956/train-val/$i"
# EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/TCN/20210618-1956/train-val/$i"
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" \
# --tcn_baseline True --causal True --d_model 64 --tcn_stages 3 --tcn_layers 10 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
# done



# for i in 1 2 3 4
# do
# EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/TCN/20210618-1336/train-val/$i"
# EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/TCN/20210618-1336/train-val/$i"
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" \
# --tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 10 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotPhase" --adam True
# done

# for i in 1 2 3 4
# do
# EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/TCN/20210618-1059/train-val/$i"
# EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/TCN/20210618-1059/train-val/$i"
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" \
# --tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 20 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotPhase" --adam True
# done

# for i in 1 2 3 4
# do
# EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/TCN/20210618-1840/train-val/$i"
# EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/TCN/20210618-1840/train-val/$i"
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" \
# --tcn_baseline True --causal True --d_model 64 --tcn_stages 3 --tcn_layers 10 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotPhase" --adam True
# done

# for i in 1 2 3 4
# do
# EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/TCN/20210618-1608/train-val/$i"
# EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/TCN/20210618-1608/train-val/$i"
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" \
# --tcn_baseline True --causal True --d_model 64 --tcn_stages 3 --tcn_layers 5 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotPhase" --adam True
# done

# cd ../part1
# python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/TCN/20210618-1059/train-val"
# python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/TCN/20210618-1336/train-val"
# python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/TCN/20210618-1840/train-val"
# python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/TCN/20210618-1608/train-val"

# python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/TCN/20210618-1217/train-val" --dataset "CoBotSteps"
# python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/TCN/20210618-1452/train-val" --dataset "CoBotSteps"
# python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/TCN/20210618-1724/train-val" --dataset "CoBotSteps"
# python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/TCN/20210618-1956/train-val" --dataset "CoBotSteps"




cd part2

for i in 1 2 3 4
do
EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/TCN_offline/20210621-1724/train-val/$i"
EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/TCN_offline/20210621-1724/train-val/$i"
python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" \
--tcn_baseline True --causal False --d_model 64 --tcn_stages 2 --tcn_layers 20 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
done

for i in 1 2 3 4
do
EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/TCN_offline/20210621-2012/train-val/$i"
EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/TCN_offline/20210621-2012/train-val/$i"
python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" \
--tcn_baseline True --causal False --d_model 64 --tcn_stages 2 --tcn_layers 10 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
done

for i in 1 2 3 4
do
EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/TCN_offline/20210621-2359/train-val/$i"
EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/TCN_offline/20210621-2359/train-val/$i"
python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" \
--tcn_baseline True --causal False --d_model 64 --tcn_stages 3 --tcn_layers 5 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
done

for i in 1 2 3 4
do
EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/TCN_offline/20210622-0114/train-val/$i"
EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/TCN_offline/20210622-0114/train-val/$i"
python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" \
--tcn_baseline True --causal False --d_model 64 --tcn_stages 2 --tcn_layers 5 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
done

for i in 1 2 3 4
do
EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/TCN_offline/20210622-0346/train-val/$i"
EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/TCN_offline/20210622-0346/train-val/$i"
python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" \
--tcn_baseline True --causal False --d_model 64 --tcn_stages 3 --tcn_layers 10 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
done



for i in 1 2 3 4
do
EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/TCN_offline/20210621-1856/train-val/$i"
EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/TCN_offline/20210621-1856/train-val/$i"
python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" \
--tcn_baseline True --causal False --d_model 64 --tcn_stages 2 --tcn_layers 10 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotPhase" --adam True
done

for i in 1 2 3 4
do
EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/TCN_offline/20210621-1516/train-val/$i"
EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/TCN_offline/20210621-1516/train-val/$i"
python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" \
--tcn_baseline True --causal False --d_model 64 --tcn_stages 2 --tcn_layers 20 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotPhase" --adam True
done

for i in 1 2 3 4
do
EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/TCN_offline/20210622-0230/train-val/$i"
EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/TCN_offline/20210622-0230/train-val/$i"
python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" \
--tcn_baseline True --causal False --d_model 64 --tcn_stages 3 --tcn_layers 10 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotPhase" --adam True
done

for i in 1 2 3 4
do
EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/TCN_offline/20210621-2128/train-val/$i"
EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/TCN_offline/20210621-2128/train-val/$i"
python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" \
--tcn_baseline True --causal False --d_model 64 --tcn_stages 3 --tcn_layers 5 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotPhase" --adam True
done


for i in 1 2 3 4
do
EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/TCN_offline/20210621-2243/train-val/$i"
EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/TCN_offline/20210621-2243/train-val/$i"
python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" \
--tcn_baseline True --causal False --d_model 64 --tcn_stages 2 --tcn_layers 5 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotPhase" --adam True
done

cd ../part1
python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/TCN_offline/20210621-1516/train-val"
python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/TCN_offline/20210621-1856/train-val"
python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/TCN_offline/20210621-2128/train-val"
python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/TCN_offline/20210621-2243/train-val"
python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/TCN_offline/20210622-0230/train-val"

python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/TCN_offline/20210621-1724/train-val" --dataset "CoBotSteps"
python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/TCN_offline/20210621-2012/train-val" --dataset "CoBotSteps"
python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/TCN_offline/20210621-2359/train-val" --dataset "CoBotSteps"
python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/TCN_offline/20210622-0114/train-val" --dataset "CoBotSteps"
python3 eval_cross.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/TCN_offline/20210622-0346/train-val" --dataset "CoBotSteps"