cd part1


EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" --annotation "step" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 256
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" --annotation "step" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 128
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" --annotation "step" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 64
done


EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" --annotation "step" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 256 --bidirectional True
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" --annotation "step" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 128 --bidirectional True
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" --annotation "step" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 64 --bidirectional True
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" --annotation "step" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 512 --bidirectional True
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" --annotation "step" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 256 --num_layers 2
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" --annotation "step" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 128 --num_layers 2
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" --annotation "step" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 64 --num_layers 2
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" --annotation "step" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 32 --num_layers 2
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" --annotation "step" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 256 --bidirectional True --num_layers 2
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" --annotation "step" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 128 --bidirectional True --num_layers 2
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" --annotation "step" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 64 --bidirectional True --num_layers 2
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" --annotation "step" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 32 --bidirectional True --num_layers 2
done
