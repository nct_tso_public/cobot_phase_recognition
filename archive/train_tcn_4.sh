cd part2






# EXP="20210630-1107"
# for i in 1 2 3 4
# do
# python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" \
# --tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 14 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotPhase" --adam True
# done

# EXP="20210630-1108"
# for i in 1 2 3 4
# do
# python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" \
# --tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 14 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
# done

# for i in 1 2 3 4
# do
# EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/20210630-1108/train-val/$i"
# EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/20210630-1108/train-val/$i"
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" \
# --tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 14 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
# done


# for i in 1 2 3 4
# do
# EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/20210630-1107/train-val/$i"
# EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/20210630-1107/train-val/$i"
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" \
# --tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 14 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotPhase" --adam True
# done





# EXP="20210630-1207"
# for i in 1 2 3 4
# do
# python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" \
# --tcn_baseline True --causal False --d_model 64 --tcn_stages 2 --tcn_layers 14 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotPhase" --adam True
# done

# EXP="20210630-1208"
# for i in 1 2 3 4
# do
# python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" \
# --tcn_baseline True --causal False --d_model 64 --tcn_stages 2 --tcn_layers 14 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
# done

# for i in 1 2 3 4
# do
# EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/20210630-1208/train-val/$i"
# EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/20210630-1208/train-val/$i"
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" \
# --tcn_baseline True --causal False --d_model 64 --tcn_stages 2 --tcn_layers 14 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
# done


# for i in 1 2 3 4
# do
# EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/20210630-1207/train-val/$i"
# EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/20210630-1207/train-val/$i"
# python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" \
# --tcn_baseline True --causal False --d_model 64 --tcn_stages 2 --tcn_layers 14 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotPhase" --adam True
# done



EXP="20210705-1107"
for i in 1 2 3 4
do
python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" \
--tcn_baseline True --causal True --d_model 64 --tcn_stages 3 --tcn_layers 14 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotPhase" --adam True
done

EXP="20210705-1108"
for i in 1 2 3 4
do
python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" \
--tcn_baseline True --causal True --d_model 64 --tcn_stages 3 --tcn_layers 14 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
done

for i in 1 2 3 4
do
EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/20210705-1108/train-val/$i"
EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/20210705-1108/train-val/$i"
python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" \
--tcn_baseline True --causal True --d_model 64 --tcn_stages 3 --tcn_layers 14 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
done


for i in 1 2 3 4
do
EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/20210705-1107/train-val/$i"
EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/20210705-1107/train-val/$i"
python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" \
--tcn_baseline True --causal True --d_model 64 --tcn_stages 3 --tcn_layers 14 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotPhase" --adam True
done





EXP="20210705-1207"
for i in 1 2 3 4
do
python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" \
--tcn_baseline True --causal False --d_model 64 --tcn_stages 3 --tcn_layers 14 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotPhase" --adam True
done

EXP="20210705-1208"
for i in 1 2 3 4
do
python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" \
--tcn_baseline True --causal False --d_model 64 --tcn_stages 3 --tcn_layers 14 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
done

for i in 1 2 3 4
do
EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/20210705-1208/train-val/$i"
EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/20210705-1208/train-val/$i"
python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" \
--tcn_baseline True --causal False --d_model 64 --tcn_stages 3 --tcn_layers 14 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
done


for i in 1 2 3 4
do
EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/20210705-1207/train-val/$i"
EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/20210705-1207/train-val/$i"
python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" \
--tcn_baseline True --causal False --d_model 64 --tcn_stages 3 --tcn_layers 14 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotPhase" --adam True
done

