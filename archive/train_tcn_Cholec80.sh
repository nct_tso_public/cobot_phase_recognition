cd part2




# EXP="test_tcn_10_3"
# for i in 42 123 512 222 
# do
# python3 train_c.py --exp "$EXP" --seed $i --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/Cholec80_ResNet50.pth.tar" \
# --tcn_baseline True --causal True --d_model 64 --tcn_stages 3 --tcn_layers 10 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "Cholec80" --adam True --out "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCholec80"
# done


# EXP="test_tcn_10_3_offline"
# for i in 42 123 512 222 
# do
# python3 train_c.py --exp "$EXP" --seed $i --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/Cholec80_ResNet50.pth.tar" \
# --tcn_baseline True --causal False --d_model 64 --tcn_stages 3 --tcn_layers 10 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "Cholec80" --adam True --out "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCholec80"
# done

# EXP="test_tcn_10"
# for i in 42 123 512 222 
# do
# python3 train_c.py --exp "$EXP" --seed $i --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/Cholec80_ResNet50.pth.tar" \
# --tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 10 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "Cholec80" --adam True --out "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCholec80"
# done


# EXP="test_tcn_10_offline"
# for i in 42 123 512 222 
# do
# python3 train_c.py --exp "$EXP" --seed $i --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/Cholec80_ResNet50.pth.tar" \
# --tcn_baseline True --causal False --d_model 64 --tcn_stages 2 --tcn_layers 10 \
# --randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
# --lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "Cholec80" --adam True --out "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCholec80"
# done





EVAL="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCholec80/test_tcn_10_offline_20210630/train-val/Tecno"
EVAL_OUT="/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCholec80/test_tcn_10_offline_20210630"
python3 eval.py --to_eval "$EVAL" --eval_out "$EVAL_OUT" --exp "" --seed 12345 --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/Cholec80_ResNet50.pth.tar" \
--tcn_baseline True --causal False --d_model 64 --tcn_stages 2 --tcn_layers 10 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "Cholec80" --out "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCholec80" --adam True





