cd part1

for i in 1 2 3 4
do
python3 train_LSTM_ex.py --fold $i --lr 0.0001 --batch_size 256 --epochs 30 --exp "20210712_1237_40_pretrainedresnetlr0001" --annotation "step" --pretrained_ResNet50 "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442"
done
#python3 train_LSTM_ex_testi.py --fold 1 --batch_size 256 --epochs 1 --exp "testlau88f" --annotation "step" --pretrained_ResNet50 "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442" --pretrained_LSTM "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/LSTM_new/256_20210609-1258"
# for i in 1 2 3 4
# do
# python3 train_LSTM_ex_test.py --fold 1 --batch_size 256 --freeze_res True --epochs 30 --exp "testlauf" --annotation "step" --pretrained_ResNet50 "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442" --pretrained_LSTM "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/LSTM_new/256_20210609-1258"
# done

#EXP=$(date +"%Y%m%d-%H%M")
#for i in 1 2 3 4
#do
#python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 256
#done

# EXP=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 128
# done

# EXP=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 64
# done


# EXP=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 256 --bidirectional True
# done

# EXP=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 128 --bidirectional True
# done

# EXP=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 64 --bidirectional True
# done

# EXP=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 512 --bidirectional True
# done

# EXP=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 256 --num_layers 2
# done

# EXP=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 128 --num_layers 2
# done

# EXP=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 64 --num_layers 2
# done

# EXP=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 32 --num_layers 2
# done

# EXP=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 256 --bidirectional True --num_layers 2
# done

# EXP=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 128 --bidirectional True --num_layers 2
# done

# EXP=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 64 --bidirectional True --num_layers 2
# done

# EXP=$(date +"%Y%m%d-%H%M")
# for i in 1 2 3 4
# do
# python3 train_CNNLSTM_CoBot_cross_feature_.py --feature_file "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" --exp "$EXP" --fold $i --w 1 --epochs 100 --hidden_size 32 --bidirectional True --num_layers 2
# done
