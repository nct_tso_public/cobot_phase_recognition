cd part2




EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" \
--tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 20 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotPhase" --adam True
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" \
--tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 20 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" \
--tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 10 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotPhase" --adam True
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" \
--tcn_baseline True --causal True --d_model 64 --tcn_stages 2 --tcn_layers 10 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" \
--tcn_baseline True --causal True --d_model 64 --tcn_stages 3 --tcn_layers 5 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotPhase" --adam True
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" \
--tcn_baseline True --causal True --d_model 64 --tcn_stages 3 --tcn_layers 5 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
done



EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359/$i/features.pth.tar" \
--tcn_baseline True --causal True --d_model 64 --tcn_stages 3 --tcn_layers 10 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotPhase" --adam True
done

EXP=$(date +"%Y%m%d-%H%M")
for i in 1 2 3 4
do
python3 train.py --exp "$EXP" --seed 42 --split $i --feature_file "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442/$i/features.pth.tar" \
--tcn_baseline True --causal True --d_model 64 --tcn_stages 3 --tcn_layers 10 \
--randomly_replace_prob 0.01  --mask_prob 0.01 --replace_duration 15 --mask_duration 15 --added_noise 0.3 \
--lr 0.0005 -b 2 --steps 50 --label_smoothing_eps 0 --feature_trunc_mse_factor 0 --task "CoBotStep" --out "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps" --adam True
done