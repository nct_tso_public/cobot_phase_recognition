#!/bin/bash
#SBATCH -e err-Test-newB-0-%J.err
#SBATCH -o out-Test-newB-0-%J.out
#SBATCH --time=1-00:00:00
#SBATCH --cpus-per-task=4
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --gpus=1
#SBATCH --mail-user=stefanie.krell@mailbox.tu-dresden.de
#SBATCH --mail-type=BEGIN,END,FAIL,REQUEUE,TIME_LIMIT_90
#SBATCH --mem=48G

cd /home/krellstef/Master_code/part1
#python3 train_LSTM_ex_c.py --use_class_weights True --batch_size 1024 --batch_factor 1 --lr 0.00005 --epochs 50 --annotation "cholec80" --pretrained_ResNet50 "/mnt/g27prist/TCO/TCO-Studenten/PhaseRecognition/cholec80/model_best_acc.pth.tar" --exp "20210729_1139_pretrainedres_lr00005_b1024"

for i in 1 2 3 4
do
python3 train_LSTM_ex_c.py --fold $i --use_class_weights True --batch_size 1024 --batch_factor 1 --lr 0.00005 --epochs 50 --exp "20210806_1341_pretrainedres_lr00005batch1024" --annotation "step" --pretrained_ResNet50 "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442"
done