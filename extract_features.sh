cd part1

# Phases

# no pretraining
for i in 1 2 3 4
do
python3 get_CoBot_features.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210324-1359" --anno "phase" --fold $i
done

# c
for i in 1 2 3 4
do
python3 get_CoBot_features.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/pretrain_cholec80/20210324-1536" --anno "phase" --fold $i
done

# c + e
for i in 1 2 3 4
do
python3 get_CoBot_features.py "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/pretrain_CoEn/20210326-0223" --anno "phase" --fold $i
done

# c + e + s
for i in 1 2 3 4
do
python3 get_CoBot_features.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210406-1016" --anno "phase" --fold $i
done

# s
for i in 1 2 3 4
do
python3 get_CoBot_features.py "/media/TCO/TCO-Studenten/Homes/krellstef/outCoBot/Phase_segmentation_resnet50/no_pretrain/20210401-1009" --anno "phase" --fold $i
done

# Steps

# no pretraining
for i in 1 2 3 4
do
python3 get_CoBot_features.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210324-1442" --anno "step" --fold $i
done

# c
for i in 1 2 3 4
do
python3 get_CoBot_features.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/pretrain_cholec80/20210329-1416" --anno "step" --fold $i
done

# c + e
for i in 1 2 3 4
do
python3 get_CoBot_features.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/pretrain_CoEn/20210401-1121" --anno "step" --fold $i
done

# c + e + s
for i in 1 2 3 4
do
python3 get_CoBot_features.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210408-0839" --anno "step" --fold $i
done

# s
for i in 1 2 3 4
do
python3 get_CoBot_features.py "/mnt/g27prist/TCO/TCO-Studenten/Homes/krellstef/outCoBot_steps/Phase_segmentation_resnet50/no_pretrain/20210406-1007" --anno "step" --fold $i
done